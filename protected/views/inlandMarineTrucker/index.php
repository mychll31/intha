<?php
$this->breadcrumbs=array(
	'Inland Marine Truckers',
);

$this->menu=array(
	array('label'=>'Create InlandMarineTrucker', 'url'=>array('create')),
	array('label'=>'Manage InlandMarineTrucker', 'url'=>array('admin')),
);
?>

<h1>Inland Marine Truckers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
