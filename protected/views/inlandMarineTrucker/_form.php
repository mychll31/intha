
	<div class="edit_bttns">
    <ul>
    <li><?php echo CHtml::link(Yii::t('courses','<span>Back</span>'), array('inlandMarineTrucker/manage'),array('class'=>'edit last')); ?></li>
    </ul>
    </div>
	<br>
    <div class="clear"></div>
    <div class="emp_tabwrapper">
    <div class="clear"></div>

<div class="form">
<?php
	$criteria = new CDbCriteria;
	$criteria_f = new CDbCriteria;
	$criteria->compare('active',1);
	$criteria_f->compare('status',1);
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'inland-marine-trucks-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php #echo $form->errorSummary($model); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <div class="formCon">
	<div class="formConInner">
        <div class="formConInner"  style="padding:5px;">
		<?php
			if($model->isNewRecord){
				echo $form->hiddenField($model,'trucker_id',array('size'=>30,'maxlength'=>225,'value'=>$_REQUEST['truckerid']));
			}else{
				echo $form->hiddenField($model,'trucker_id',array('size'=>30,'maxlength'=>225));}
		?>
          <table width="90%" border="0" cellspacing="0" cellpadding="0">
            <tr>
			  <td><?php echo $form->labelEx($model,Yii::t('InlandMarineTrucks','broker_name')); ?></td>
			  <td><?php $brokers = CHtml::listData(Inlandbrokers::model()->findAll($criteria_f),'id','name');
					echo $form->dropDownList($model,'broker_name',$brokers,array('empty'=>''));?>
				  <?php echo $form->error($model,'broker_name'); ?>
			  </td>
			  <td><?php echo $form->labelEx($model,Yii::t('InlandMarineTrucks','status')); ?></td>
			  <td><?php 
					$inlandstat = CHtml::listData(InlandMarineStatus::model()->findAll($criteria),'id','name');
					echo $form->dropDownList($model,'status',$inlandstat,array('empty'=>''));?>
				  <?php echo $form->error($model,'status'); ?></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
			  <td><?php echo $form->labelEx($model,Yii::t('InlandMarineTrucks','policy_number')); ?></td>
			  <td><?php echo $form->TextField($model,'policy_number');?>
				  <?php echo $form->error($model,'policy_number'); ?>
			  </td>
			  <td></td>
			  <td></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
			  <td><?php echo $form->labelEx($model,Yii::t('InlandMarineTrucks','subs_date')); ?></td>
			  <td><?php 
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'subs_date',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))?>
				<?php echo $form->error($model,'subs_date'); ?></td>
			  <td><?php echo $form->labelEx($model,Yii::t('InlandMarineTrucks','expire_date')); ?></td>
			  <td><?php
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'expire_date',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))?>
				<?php echo $form->error($model,'expire_date'); ?></td></td>
			</tr>
		  </table>
		</div>
	</div>
	<br>
		<div style="margin-left:20px;margin-bottom:20px;padding:px 0 0 0px; text-align:left">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Update',array('class'=>'formbut')); ?>
	</div>
		</div></div>
 </div>

<?php $this->endWidget(); ?>

</div><!-- form -->


 
 
 
</div>
</div>
</div>
    
    </td>
  </tr>
</table>
