<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
     <?php $this->renderPartial('left_side');?>
     
    </td>
    <td valign="top">
        <div class="cont_right formWrapper">
	<h1>Inland Marine</h1>
	<div class="emp_cntntbx" style="padding: 0px 0px 30px !important;">
<div class="form">
  <div class="pdtab_Con" style="width:97%;padding:0px !important;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<?php		if($inlandmarine!=null){ ?>
	  <tbody>
	    <tr class="pdtab-h">
		<td align="center">No.</td>
		<td align="center">Broker Name</td>
		<td align="center">Policy Number</td>
		<td align="center">Date of Subscription</td>
		<td align="center">Date of Expiration</td>
             </tr>
	     </tbody>
	     <?php	}	$no=1; 	foreach ($inlandmarine as $t){ ?>
		     <tr>
			     <td align="center" style="padding:3px 15px !important"><?php  echo $no++?></td>
			     <?php $thisid=Students::model()->findByAttributes(array('assoc_truckerid'=>$t->trucker_id));?>
			     <?php $broker_name=Inlandbrokers::model()->findByAttributes(array('id'=>$t->broker_name));?>
			     <td align="center" style="padding:3px 15px !important"><?php  echo $broker_name->name?></td>
			     <td align="center" style="padding:3px 15px !important"><?php echo $t->policy_number;?></td>
			     <td style="padding:3px 15px !important;text-align:center"><?php	if($t->subs_date!=null)	echo date('M. d, Y',strtotime($t->subs_date));?></td>
			     <td style="padding:3px 15px !important;text-align:center"><?php	if($t->expire_date!=null) echo date('M. d, Y',strtotime($t->expire_date));?></td>
			     </tr>
			     <?php	} ?>
			     </table>
			     
			     </div>
			     <br>
			     <h3><?php echo Yii::t('Courses','Members');?></h3>
		<?php
		   $posts_1 = Students::model()->findByAttributes(array('id'=>Yii::app()->user->id));
		   $batch=InlandMarineTrucks::model()->findAll("truckerid=:x", array(':x'=>$posts_1->assoc_truckerid));
		 ?>
  <div class="pdtab_Con" style="width:97%;padding:0px !important;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tbody>
		  <tr class="pdtab-h">
			<td align="center"><?php echo Yii::t('Courses','No.');?></td>
			<td align="center"><?php echo Yii::t('Courses','Truck ID');?></td>
			<td align="center"><?php echo Yii::t('Courses','Plate Number');?></td>
			<td align="center"><?php echo Yii::t('Courses','Engine Number');?></td>
			<td align="center"><?php echo Yii::t('Courses','Description');?></td>
			<td align="center"><?php echo Yii::t('Courses','Registered Owner');?></td>
			<td align="center"><?php echo Yii::t('Courses','Date Added to IM');?></td>
		  </tr>
          <?php $no=1;
		  foreach($batch as $batch_1)
				{
					echo '<tr id="batchrow'.$batch_1->id.'">';
					echo '<td align="center">'.$no++.'</td>';
					echo '<td align="center">'.$batch_1->truck_id.'</td>';
					$platenumber = Courses::model()->findByAttributes(array('association_truck_id'=>$batch_1->truck_id));
					echo '<td align="center">'.$platenumber->plate_number.'</td>';
					echo '<td align="center">'.$platenumber->truck_engine_number.'</td>';					
					echo '<td align="center">';
					$brand = TruckerBrand::model()->findByAttributes(array('id'=>$platenumber->brand));
					$tractorType = TruckersTractorType::model()->findByAttributes(array('id'=>$platenumber->tractor_type));
					$wheel = TruckerWheeler::model()->findByAttributes(array('id'=>$platenumber->wheeler));
					echo strtoupper($tractorType->name.' '.$wheel->name);
					echo '</td>';
					echo '<td align="center">'.$platenumber->reg_owner.'</td>';
					echo '<td align="center">'.date('M. d, Y',strtotime($batch_1->subs_date)).'</td>';
					echo '</tr>';
					echo '<div id="jobDialog123"></div>';
				}
			   ?>
         </tbody>
        </table>
		</div>
</div>
</div>

    </td>
  </tr>
</table>

