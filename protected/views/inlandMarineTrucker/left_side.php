<?php
		#Mailbox
		$criteria_mail = new CDbCriteria;
		$criteria_mail->compare('interlocutor_id',Yii::app()->user->id);
		$criteria_mail->condition=$criteria_mail->condition.' and (bm_read=2 and bm_read = 1)';
		$mail = $total = MailboxConversation::model()->count($criteria_mail);

		##Trucker membership notifications
                $criteria_a = new CDbCriteria;
                $exp1 = date('Y-m-d',time());
                $exp2 = date('Y-m-d',(date(strtotime('+60 day'),time())));

                #Truck Registration Date
                $criteria_b = new CDbCriteria;
                $truckCount = 0;
                $criteria_b->condition=' reg_enddate >=  :expiry and reg_enddate <= :expiry1';
                $criteria_b->params[':expiry']=$exp1;
                $criteria_b->params[':expiry1']=$exp2;
                $truckCount = Courses::model()->count($criteria_b);

                #TruckerRegdate Date
                $criteria_lic = new CDbCriteria;
                $licCount = 0;
                $criteria_lic->condition=' active=1 and expiry_date>=  :expiry and expiry_date <= :expiry1';
                $criteria_lic->params[':expiry']=$exp1;
                $criteria_lic->params[':expiry1']=$exp2;
                $licCount = EmpLicense::model()->count($criteria_lic);

                #Comprehensive Insurance
                $criteria_99 = new CDbCriteria;
                $orcrCount = 0;
                $criteria_99->condition=' compre_dateexpire >=  :expiry and compre_dateexpire <= :expiry1';
                $criteria_99->params[':expiry']=$exp1;
                $criteria_99->params[':expiry1']=$exp2;
                $compreCount = Insurance::model()->count($criteria_99);

                $totalCount = $licCount + $truckCount + $compreCount;

                #Start Alert for Events
		#Today
                $alertEvents = 0;
                $criteria_events = new CDbCriteria;
                $criteria_events->order = 'start desc';
                $allEvents = Events::model()->findAll($criteria_events);
                foreach($allEvents as $eve){
                        if(date('Y m d',$eve->start)==date('Y m d',time())){$alertEvents++;}
                }
                #End Alert for Events
?>
<!--upgrade_div_starts-->
<div class="upgrade_bx">
	<div class="up_banr_imgbx"><a href="http://open-school.org/contact.php" target="_blank"><img src="http://tryopenschool.com/images/promo_bnnr_innerpage.png" width="231" height="200" /></a></div>
	<div class="up_banr_firstbx">
   	  <h1>You are Using Community Edition</h1>
	  <a href="http://open-school.org/contact.php" target="_blank">upgrade to premium version!</a>
    </div>
	
</div>
<!--upgrade_div_ends-->
<div id="othleft-sidebar">
             <!--<div class="lsearch_bar">
             	<input type="text" value="Search" class="lsearch_bar_left" name="">
                <input type="button" class="sbut" name="">
                <div class="clear"></div>
  </div>-->     <?php
			function t($message, $category = 'cms', $params = array(), $source = null, $language = null) 
{
    return $message;
}
	$role=UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id));
        if($role->role==1){
            $this->widget('zii.widgets.CMenu',array(
            'encodeLabel'=>false,
            'activateItems'=>true,
            'activeCssClass'=>'list_active',
            'items'=>array(
				array('label'=>''.'<h1>'.Yii::t('employees','My Account').'</h1>'), 
						array('label'=>Yii::t('employees','Mailbox( <font color="red">'.$mail.'</font> )').'<span>'.Yii::t('employees','All Received Messages').'</span>', 'url'=>array('/mailbox'),'linkOptions'=>array('class'=>'lbook_ico'),'active'=> (Yii::app()->controller->id=='mailbox')),
				
				array('label'=>''.t('<h1>Events</h1>'),
				'active'=> ((Yii::app()->controller->module->id=='cal') ? true : false)),
				
				array('label'=>t('Events List ( <font color=\'red\'>'.$alertEvents.'</font> ) <span>All Events</span>'), 'url'=>array('/dashboard/default/events'),
				'active'=> ((Yii::app()->controller->module->id=='dashboard') ? true : false),'linkOptions'=>array('class'=>'evntlist_ico')),
				
				array('label'=>t('Calendar<span>Schedule Events</span>'), 'url'=>array('/cal'),
				'active'=> (((Yii::app()->controller->module->id=='cal') and (Yii::app()->controller->id != 'eventsType')) ? true : false),'linkOptions'=>array('class'=>'cal_ico')),
				
				array('label'=>t('Event Types<span>Manage Event Types</span>'), 'url'=>array('/cal/eventsType'),
				'active'=> ((Yii::app()->controller->id=='eventsType') ? true : false),'linkOptions'=>array('class'=>'evnttype_ico')),
				
				array('label'=>t('Inland Marine Information<span>Manage Event Types</span>'), 'url'=>array('/inlandMarineTrucker/manage'),
				'active'=> (((Yii::app()->controller->id=='inlandMarineTrucker')or(Yii::app()->controller->id=='inlandMarineTrucks')) ? true : false),'linkOptions'=>array('class'=>'vt_ico')),
				
				array('label'=>t('Notifications ( <font color=\'red\'>'.$totalCount.'</font> ) <span>Manage Notifications</span>'), 'url'=>array('/default/noti'),
				'active'=> ((Yii::app()->controller->action->id=='noti') ? true : false),'linkOptions'=>array('class'=>'notify_ico')),
				
            ),
            )); }else{
            $this->widget('zii.widgets.CMenu',array(
            'encodeLabel'=>false,
            'activateItems'=>true,
            'activeCssClass'=>'list_active',
            'items'=>array(
				array('label'=>''.'<h1>'.Yii::t('employees','My Account').'</h1>'), 
						array('label'=>Yii::t('employees','Mailbox(<font color="red">'.$mail.'</font>)').'<span>'.Yii::t('employees','All Received Messages').'</span>', 'url'=>array('/mailbox'),'linkOptions'=>array('class'=>'lbook_ico'),'active'=> (Yii::app()->controller->id=='mailbox')),
				
				array('label'=>''.t('<h1>Events</h1>'),
				'active'=> ((Yii::app()->controller->module->id=='cal') ? true : false)),
				
				array('label'=>t('Events List ( <font color=\'red\'>'.$alertEvents.'</font> ) <span>All Events</span>'), 'url'=>array('/dashboard/default/events'),
				'active'=> ((Yii::app()->controller->module->id=='dashboard') ? true : false),'linkOptions'=>array('class'=>'evntlist_ico')),
				
				array('label'=>t('Calendar<span>Schedule Events</span>'), 'url'=>array('/cal'),
				'active'=> (((Yii::app()->controller->module->id=='cal') and (Yii::app()->controller->id != 'eventsType')) ? true : false),'linkOptions'=>array('class'=>'cal_ico')),
				
				array('label'=>t('Inland Marine Information<span>Manage Event Types</span>'), 'url'=>array('/inlandMarineTrucker/manage'),
				'active'=> ((Yii::app()->controller->id=='inlandMarineTrucker') ? true : false),'linkOptions'=>array('class'=>'vt_ico')),
				
				array('label'=>t('Notifications ( <font color=\'red\'>'.$totalCount.'</font> ) <span>Manage Notifications</span>'), 'url'=>array('/default/noti'),
				'active'=> ((Yii::app()->controller->action->id=='noti') ? true : false),'linkOptions'=>array('class'=>'notify_ico')),
				
            ),
            )); 
	}
?>		
		</div>
        <script type="text/javascript">

	$(document).ready(function () {
            //Hide the second level menu
            $('#othleft-sidebar ul li ul').hide();            
            //Show the second level menu if an item inside it active
            $('li.list_active').parent("ul").show();
            
            $('#othleft-sidebar').children('ul').children('li').children('a').click(function () {                    
                
                 if($(this).parent().children('ul').length>0){                  
                    $(this).parent().children('ul').toggle();    
                 }
                 
            });
          
            
        });

    </script>
