<?php
$this->breadcrumbs=array(
	'Inland Marine Truckers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List InlandMarineTrucker', 'url'=>array('index')),
	array('label'=>'Create InlandMarineTrucker', 'url'=>array('create')),
	array('label'=>'Update InlandMarineTrucker', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete InlandMarineTrucker', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InlandMarineTrucker', 'url'=>array('admin')),
);
?>

<h1>View InlandMarineTrucker #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'trucker_id',
		'policy_number',
		'broker_name',
		'subs_date',
		'expire_date',
		'status',
	),
)); ?>
