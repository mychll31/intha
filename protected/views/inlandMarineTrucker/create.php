<?php
$this->breadcrumbs=array(
	'Inland Marine'
);

?>
<style>.emp_cntntbx{min-height:0px;}</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
 <?php $this->renderPartial('left_side');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
        <h2>Add New Inland Marine Details</h2>
		
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>