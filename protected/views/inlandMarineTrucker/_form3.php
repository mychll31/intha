<style>
#jobDialog123
{
	height:auto;
}
</style>

<?php 
  $posts=Students::model()->findAll();
 ?>
 
<?php if($posts!=NULL)
{?>
<script>
function details(id)
{
	
	var rr= document.getElementById("dropwin"+id).style.display;
	
	 if(document.getElementById("dropwin"+id).style.display=="block")
	 {
		 document.getElementById("dropwin"+id).style.display="none"; 
		 $("#openbutton"+id).removeClass('open');
		  $("#openbutton"+id).addClass('view');
	 }
	 else if(  document.getElementById("dropwin"+id).style.display=="none")
	 {
		 document.getElementById("dropwin"+id).style.display="block"; 
		   $("#openbutton"+id).removeClass('view');
		  $("#openbutton"+id).addClass('open');
	 }
}
function rowdelete(id)
{
	 $("#batchrow"+id).fadeOut("slow");
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
     <?php $this->renderPartial('left_side');?>
     
    </td>
    <td valign="top">
        <div class="cont_right formWrapper">
	<h1>Inland Marine</h1>
	<div class="emp_cntntbx" style="padding: 0px 0px 30px !important;">
<div class="form">
<?php 
	if($inlandmarine==null){
	
	$form=$this->beginWidget('CActiveForm', array(
	'id'=>'inland-marine-trucks-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php #echo $form->errorSummary($model); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <div class="formCon">
	<div class="formConInner">
        <div class="formConInner"  style="padding:5px;">
		<?php
			if($model->isNewRecord){
				echo $form->hiddenField($model,'trucker_id',array('size'=>30,'maxlength'=>225,'value'=>$_REQUEST['truckerid']));
			}else{
				echo $form->hiddenField($model,'trucker_id',array('size'=>30,'maxlength'=>225));}
		?>
          <table width="90%" border="0" cellspacing="0" cellpadding="0">
            <tr>
			  <td><?php echo $form->labelEx($model,Yii::t('InlandMarineTrucks','broker_name')); ?></td>
			  <td><?php $brokers = CHtml::listData(Inlandbrokers::model()->findAll($criteria_f),'id','name');
					echo $form->dropDownList($model,'broker_name',$brokers,array('empty'=>''));?>
				  <?php echo $form->error($model,'broker_name'); ?>
			  </td>
			  <td><?php echo $form->labelEx($model,Yii::t('InlandMarineTrucks','status')); ?></td>
			  <td><?php 
					$inlandstat = CHtml::listData(InlandMarineStatus::model()->findAll($criteria),'id','name');
					echo $form->dropDownList($model,'status',$inlandstat,array('empty'=>''));?>
				  <?php echo $form->error($model,'status'); ?></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
			  <td><?php echo $form->labelEx($model,Yii::t('InlandMarineTrucks','subs_date')); ?></td>
			  <td><?php 
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'subs_date',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))?>
				<?php echo $form->error($model,'subs_date'); ?></td>
			  <td><?php echo $form->labelEx($model,Yii::t('InlandMarineTrucks','expire_date')); ?></td>
			  <td><?php
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'expire_date',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))?>
				<?php echo $form->error($model,'expire_date'); ?></td></td>
			</tr>
		  </table>
		</div>
	</div>
	<br>
		<div style="margin-left:20px;margin-bottom:20px;padding:px 0 0 0px; text-align:left">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Update',array('class'=>'formbut')); ?>
	</div>
		</div></div>
 </div>

<?php $this->endWidget(); } else{ ?>	
  <div class="pdtab_Con" style="width:97%;padding:0px !important;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<?php		if($inlandmarine!=null){ ?>
	  <tbody>
	    <tr class="pdtab-h">
		<td align="center">No.</td>
		<td align="center">Broker Name</td>
		<td align="center">Policy Number</td>
		<td align="center">Date of Subscription</td>
		<td align="center">Date of Expiration</td>
		<td align="center">Action</td>
             </tr>
	  </tbody>
	   <?php	}	$no=1; 	foreach ($inlandmarine as $t){ ?>
							  <tr>
								<td align="center" style="padding:3px 15px !important"><?php  echo $no++?></td>
								<?php $thisid=Students::model()->findByAttributes(array('assoc_truckerid'=>$t->trucker_id));?>
								<?php $broker_name=Inlandbrokers::model()->findByAttributes(array('id'=>$t->broker_name));?>
								<td align="center" style="padding:3px 15px !important"><?php  echo $broker_name->name?></td>
								<td align="center" style="padding:3px 15px !important"><?php echo $t->policy_number;?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php	if($t->subs_date!=null)	echo date('M. d, Y',strtotime($t->subs_date));?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php	if($t->expire_date!=null) echo date('M. d, Y',strtotime($t->expire_date));?></td>
								<td style="padding:3px 15px !important;text-align:center">
								<a href="index.php?r=inlandMarineTrucker/create" title="Setup New Inland Marine">
								<img src='images/add_all.png' /></a>
								&nbsp;&nbsp;&nbsp;
								<a href="index.php?r=inlandMarineTrucker/update&id=<?php echo $t->id;?>" title='Edit Inland Marine Details'>
								<img src="images/edit.png" /></a>
								</td>
							  </tr>
							<?php	} ?>
							</table>
							</div>
<br>
<h3><?php echo Yii::t('Courses','Members');?></h3>
<div class="bttns_addstudent" style="top: 85px !important;left: 600px !important;">
<ul>
<li><a class="addbttn last" href="index.php?r=inlandMarineTrucks/manage">Add Member</a></li>
</ul>
</div>
 <div id="jobDialog">
 <div id="jobDialog1">
 <?php 
  $posts=Students::model()->findAll();
   ?>
 <?php $this->renderPartial('_flash');?>

 </div>
  </div>
    <div class="mcb_Con">

<?php foreach($posts as $posts_1)
{ ?>
<div class="mcbrow" id="jobDialog1">
	<ul>
    	<li class="gtcol1" onclick="details('<?php echo $posts_1->id;?>');" style="cursor:pointer;">
		<?php echo $posts_1->trucker; ?>
		<?php
		   #$batch_truckerid = Courses::model()->findByAttributes(array('association_truck_id'=>));
		   $batch=InlandMarineTrucks::model()->findAll("truckerid=:x", array(':x'=>$posts_1->assoc_truckerid));
		 ?>
        <span><?php echo count($batch); ?> - Truck(s)</span>
        </li>
        <a href="#" id="openbutton<?php echo $posts_1->id;?>" onclick="details('<?php echo $posts_1->id;?>');" class="view"><li class="col5"><span class="dwnbg">&nbsp;</span></li></a>
    </ul>
    
 <div class="clear"></div>
</div>
         
<div class="pdtab_Con" id="dropwin<?php echo $posts_1->id; ?>" style="display: none; padding:0px 0px 10px 0px; ">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tbody>
		  <tr class="pdtab-h">
			<td align="center"><?php echo Yii::t('Courses','No.');?></td>
			<td align="center"><?php echo Yii::t('Courses','Truck ID');?></td>
			<td align="center"><?php echo Yii::t('Courses','Plate Number');?></td>
			<td align="center"><?php echo Yii::t('Courses','Engine Number');?></td>
			<td align="center"><?php echo Yii::t('Courses','Description');?></td>
			<td align="center"><?php echo Yii::t('Courses','Registered Owner');?></td>
			<td align="center"><?php echo Yii::t('Courses','Date Added to IM');?></td>
		  </tr>
          <?php $no=1;
		  foreach($batch as $batch_1)
				{
					echo '<tr id="batchrow'.$batch_1->id.'">';
					echo '<td align="center">'.$no++.'</td>';
					echo '<td align="center">'.$batch_1->truck_id.'</td>';
					$platenumber = Courses::model()->findByAttributes(array('association_truck_id'=>$batch_1->truck_id));
					echo '<td align="center">'.$platenumber->plate_number.'</td>';
					echo '<td align="center">'.$platenumber->truck_engine_number.'</td>';					
					echo '<td align="center">';
					$brand = TruckerBrand::model()->findByAttributes(array('id'=>$platenumber->brand));
					$tractorType = TruckersTractorType::model()->findByAttributes(array('id'=>$platenumber->tractor_type));
					$wheel = TruckerWheeler::model()->findByAttributes(array('id'=>$platenumber->wheeler));
					echo strtoupper($tractorType->name.' '.$wheel->name);
					echo '</td>';
					echo '<td align="center">'.$platenumber->reg_owner.'</td>';
					echo '<td align="center">'.date('M. d, Y',strtotime($batch_1->subs_date)).'</td>';
					echo '</tr>';
					echo '<div id="jobDialog123"></div>';
				}
			   ?>
         </tbody>
        </table>
		</div>
        <div id='check'></div>
<?php } ?>        

</div>
<?php } ?>
</div>

    </td>
  </tr>
</table>

<?php }
else
{ ?>
<link rel="stylesheet" type="text/css" href="/openschool/css/style.css" />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
    <?php $this->renderPartial('left_side');?>
    
    </td>
    <td valign="top">
    <div style="padding:20px 20px">
<div class="yellow_bx" style="background-image:none;width:680px;padding-bottom:45px;">
                
                	<div class="y_bx_head" style="width:650px;">
                    	It appears that this is the first time that you are using this System Setup. For any new installation we recommend that you configure the following:
                    </div>
                    <div class="y_bx_list" style="width:650px;">
                    	<h1><?php echo CHtml::link(Yii::t('Courses','Add New Course &amp; Batch'),array('courses/create')) ?></h1>
                    </div>
                    
                </div>

                </div>
    
    
    </td>
  </tr>
</table>

<?php } ?>
