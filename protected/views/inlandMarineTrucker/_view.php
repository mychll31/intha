<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trucker_id')); ?>:</b>
	<?php echo CHtml::encode($data->trucker_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('policy_number')); ?>:</b>
	<?php echo CHtml::encode($data->policy_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('broker_name')); ?>:</b>
	<?php echo CHtml::encode($data->broker_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subs_date')); ?>:</b>
	<?php echo CHtml::encode($data->subs_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expire_date')); ?>:</b>
	<?php echo CHtml::encode($data->expire_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>