<?php
$this->breadcrumbs=array(
	'Employee Case Remarks'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List EmployeeCaseRemarks', 'url'=>array('index')),
	array('label'=>'Create EmployeeCaseRemarks', 'url'=>array('create')),
	array('label'=>'Update EmployeeCaseRemarks', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmployeeCaseRemarks', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmployeeCaseRemarks', 'url'=>array('admin')),
);
?>

<h1>View EmployeeCaseRemarks #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
