<?php
$this->breadcrumbs=array(
	'Employee Case Remarks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmployeeCaseRemarks', 'url'=>array('index')),
	array('label'=>'Manage EmployeeCaseRemarks', 'url'=>array('admin')),
);
?>

<h1>Create EmployeeCaseRemarks</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>