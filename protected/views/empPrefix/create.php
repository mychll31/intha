<?php
$this->breadcrumbs=array(
	'Emp Prefixes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmpPrefix', 'url'=>array('index')),
	array('label'=>'Manage EmpPrefix', 'url'=>array('admin')),
);
?>

<h1>Create EmpPrefix</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>