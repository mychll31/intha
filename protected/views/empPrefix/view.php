<?php
$this->breadcrumbs=array(
	'Emp Prefixes'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List EmpPrefix', 'url'=>array('index')),
	array('label'=>'Create EmpPrefix', 'url'=>array('create')),
	array('label'=>'Update EmpPrefix', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmpPrefix', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmpPrefix', 'url'=>array('admin')),
);
?>

<h1>View EmpPrefix #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
