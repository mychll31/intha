<?php
$this->breadcrumbs=array(
	'Emp Prefixes',
);

$this->menu=array(
	array('label'=>'Create EmpPrefix', 'url'=>array('create')),
	array('label'=>'Manage EmpPrefix', 'url'=>array('admin')),
);
?>

<h1>Emp Prefixes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
