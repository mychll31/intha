<?php
  $this->breadcrumbs=array(
	'Trucks'=>array('/courses/courses/managecourse'),
	'View',
  );
  $role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
?>
<style>.emp_cntntbx{min-height:0px;}</style>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="247" valign="top"><?php $this->renderPartial('/default/left_side');?></td>
      <td valign="top">
      <div class="cont_right formWrapper">
	<?php if($role->role==1){ ?>
	<div class="edit_bttns">
	<ul>
	  <li><?php echo CHtml::link(Yii::t('employees','<span>Edit</span>'), array('update', 'id'=>$_REQUEST['id']),array('class'=>'edit last')); ?></li>
        </ul>
    </div>
    <?php }  ?>
<h1>Trailer Details</h1>
    <div class="table_listbx">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr class="listbxtop_hdng">
			<td colspan=2><?php echo Yii::t('employees','Trailer Details');?></td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Type :</span>
				<span class="data">
				</span>
			</td>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Plate Number :</span>
				<span class="data"></span>
			</td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Footer :</span>
				<span class="data">
				</span>
			</td>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Axle :</span>
				<span class="data">
				</span>
			</td>
		</tr>
		</table>
  <div class="ea_pdf" style="top:4px; right:6px;"><?php echo CHtml::link('<img src="images/pdf-but.png">', array('Employees/pdf','id'=>$_REQUEST['id']),array('target'=>'_blank')); ?></div>

 </div>

 </div>



</div>
</div>
</div>

    </td>
  </tr>
</table>
