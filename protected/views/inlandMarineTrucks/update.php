<?php
$tid = InlandMarineTrucks::model()->findByAttributes(array('id'=>$_REQUEST['id'])); 
$trucker = Students::model()->findByAttributes(array('assoc_truckerid'=>$tid->truckerid));
$this->breadcrumbs=array(
        'Inland Marine Details'=>array('/inlandMarineTrucker/manage'),
	        'Add Member'=>array('/inlandMarineTrucks/manage'),
	        'Member List'=>array('/inlandMarineTrucks/members','id'=>$trucker->id),
		        'Update',
			);
			?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
