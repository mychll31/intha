<style>.emp_cntntbx{min-height:0px;}</style>
<table width="70%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
 <?php $this->renderPartial('left_side');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
        
    <div class="clear"></div>
    <div class="emp_right_contner">
    <?php $truckerid12 = Students::model()->findByAttributes(array('id'=>$_REQUEST['tid']));?>
    <h3>Inland Marine Add Member to <?php echo $truckerid12->trucker;?></h3>
    <div class="emp_tabwrapper">
    <div class="clear"></div>

<div class="form">
<?php
	$criteria = new CDbCriteria;
	$criteria->compare('active',1);
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'inland-marine-trucks-form',
	'enableAjaxValidation'=>false,
)); ?>
<?php	
  echo $form->hiddenField($model,'truck_id',array('size'=>30,'maxlength'=>225,'value'=>$_REQUEST['id'],'readonly'=>true));
  echo $form->hiddenField($model,'truckerid',array('size'=>30,'maxlength'=>225,'value'=>$truckerid12['assoc_truckerid'],'readonly'=>true));
?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <div class="formCon" style="width:70% !important;">
	<div class="formConInner">
        <div class="formConInner">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
	      <td><?php echo $form->labelEx($model,Yii::t('InlandMarineTrucks','status')); ?></td>
	      <td><?php 
	      $inlandstat = CHtml::listData(InlandMarineStatus::model()->findAll($criteria),'id','name');
	      echo $form->dropDownList($model,'status',$inlandstat,array('empty'=>''));?>
	      <?php echo $form->error($model,'status'); ?></td>
	      <?php #$form->labelEx($model,Yii::t('InlandMarineTrucks','broker_name')); ?></td>
	      <?php #echo $form->hiddenField($model,'broker_name',array('size'=>30,'maxlength'=>225)); ?>
	      <?php echo $form->error($model,'broker_name'); ?>
	      <td><?php echo $form->labelEx($model,Yii::t('InlandMarineTrucks','subs_date')); ?></td>
	      <td><?php 
	      $date = 'yy-mm-dd';
	      $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				      'attribute'=>'subs_date',
				      'model'=>$model,
				      'options'=>array(
					      'showAnim'=>'fold',
					      'dateFormat'=>$date,
					      'changeMonth'=> true,
					      'changeYear'=>true,
					      'yearRange'=>'1950:2050'
					      ),
				      'htmlOptions'=>array(
					      'style' => 'width:100px;',
					      ),
				      ))?>
	      <?php echo $form->error($model,'subs_date'); ?></td>
	      <?php #echo $form->labelEx($model,Yii::t('InlandMarineTrucks','expire_date')); ?>
	      <?php /*
		       $this->widget('zii.widgets.jui.CJuiDatePicker', array(
		       'attribute'=>'expire_date',
		       'model'=>$model,
		       'options'=>array(
		       'showAnim'=>'fold',
		       'dateFormat'=>$date,
		       'changeMonth'=> true,
		       'changeYear'=>true,
		       'yearRange'=>'1950:2050'
		       ),
		       'htmlOptions'=>array(
		       'style' => 'width:100px;',
		       ),
		       )) */ ?>
	      <?php echo $form->error($model,'expire_date'); ?>
	      </tr>
	      </table>
		</div>
	</div>
	<br>
		<div style="margin-left:20px;margin-bottom:20px;padding:px 0 0 0px; text-align:left">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Update',array('class'=>'formbut')); ?>
	</div>
		</div></div>
 </div>

<?php $this->endWidget(); ?>

</div><!-- form -->


 
 
 
</div>
</div>
</div>
    
    </td>
  </tr>
</table>
