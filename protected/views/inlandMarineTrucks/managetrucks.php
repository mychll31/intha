<?php
$this->breadcrumbs=array(
        'Inland Marine Details'=>array('/inlandMarineTrucker/manage'),
        'Member Lists',
);
?>

<style>.emp_cntntbx{min-height:0px;}</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
 <?php $this->renderPartial('left_side');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
        
    <div class="clear"></div>
    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
    <div class="clear"></div>
    <style>
    .member{
    padding: 0px;
    margin: 0px 0px 8px;
    width: 713px;
    font-size: 12px;
    letter-spacing: 0.001em;
    color: #666;
    font-weight: bold;
    border: 1px solid #E7E6E6;
    border-radius: 3px;
    background: none repeat scroll 0% 0% #F6F3F3;
    box-shadow: 0px 0px 1px #FFF inset;

    width: 458px;
    color: #40546C;
    border-right: 1px solid #E4E2E2;
    padding: 8px 0px 8px 20px;
    } 
    .totalmem{
    display: block;
    color: #778791;
    font-size: 10px;
    }
    </style>
    <?php
      $truckerlist = Students::model()->findAll();
      foreach($truckerlist as $trucker){
	$criteria = new CDbCriteria;
	$criteria->compare('truckerid',$trucker['assoc_truckerid']);
	$criteria1 = new CDbCriteria;
	$criteria1->compare('association_trucker_id',$trucker['assoc_truckerid']);
	$inlandmarine = InlandMarineTrucks::model()->findAll($criteria);
	$totalTruck = Courses::model()->count($criteria1);
	$totalmem = InlandMarineTrucks::model()->count($criteria);
	?>
	<a href="index.php?r=inlandMarineTrucks/members&id=<?php echo $trucker['id']?>"><div class="member"><?php echo $trucker['trucker'];?>
	<span class="totalmem"><?php echo $totalTruck.' - Truck(s)&nbsp;&nbsp;&nbsp;&nbsp; '.$totalmem.' - Member(s)';?></span>
	</div></a>
	<?php
      }
    ?>
</div>
</div>
</div>
    
    </td>
  </tr>
</table>
