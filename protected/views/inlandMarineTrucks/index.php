<?php
$this->breadcrumbs=array(
	'Inland Marine Trucks',
);

$this->menu=array(
	array('label'=>'Create InlandMarineTrucks', 'url'=>array('create')),
	array('label'=>'Manage InlandMarineTrucks', 'url'=>array('admin')),
);
?>

<h1>Inland Marine Trucks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
