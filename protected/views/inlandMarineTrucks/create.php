<?php
$this->breadcrumbs=array(
		'Inland Marine Details'=>array('/inlandMarineTrucker/manage'),
		'Add Member'=>array('/inlandMarineTrucks/manage'),
		'Member List'=>array('/inlandMarineTrucks/members','id'=>$_REQUEST['tid']),
		'Add',
		);

echo $this->renderPartial('_form', array('model'=>$model)); ?>
