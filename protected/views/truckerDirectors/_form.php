<div class="edit_bttns last">
    <ul>
    <li>
    <?php $tid = Students::model()->findByAttributes(array('assoc_truckerid'=>$_REQUEST['id']));?>
    <?php echo CHtml::link(Yii::t('students','<span>View Trucker</span>'), array('/students/students/assesments', 'id'=>$tid->id),array('class'=>' edit ')); ?>
    </li>
    </ul>
    </div>
	
	<div class="captionWrapper">
        <ul>
            <li><h2 class="cur">Trucker Details</h2></li>
        </ul>
	</div>

<?php $form=$this->beginWidget('CActiveForm', array(
'id'=>'trucker-directors-form',
'enableAjaxValidation'=>false,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<?php 
	if($form->errorSummary($model)){
	?>
        <div class="errorSummary">Input Error<br />
        	<span>Please fix the following error(s).</span>
        </div>
    <?php 
	}
	?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <div class="formCon">
        <div class="formConInner">
            <h3>Trucker Information</h3>
	    <table width="90%">
	      <tr>
	        <td><?php echo $form->labelEx($model,Yii::t('truckerDirectors','trucker_id')); ?></td>
		<td><?php echo $form->labelEx($model,Yii::t('truckerDirectors','prefix')); ?></td>
		<td><?php echo $form->labelEx($model,Yii::t('truckerDirectors','ext')); ?></td>

	      </tr>
	      <tr>
	      <td><?php
	      $pref = CHtml::listData(Students::model()->findAll(),'assoc_truckerid','trucker');
	      echo $form->dropDownList($model,'trucker_id',$pref,array('options'=>array($_REQUEST['id']=>array('selected'=>'true')),'empty'=>'Select Trucker')); ?>
	      <?php echo $form->error($model,'trucker_id'); ?></td>
	      <td><?php
	      $pref = CHtml::listData(EmpPrefix::model()->findAll(),'id','name');
	      echo $form->dropDownList($model,'prefix',$pref,array('empty'=>'')); ?>
	      <?php echo $form->error($model,'prefix'); ?></td>
	      <td><?php echo $form->textField($model,'ext',array('maxlength'=>255));?>
	      <?php echo $form->error($model,'ext'); ?></td>
	      </tr>
	      <tr><td>&nbsp;</td></tr>
	      <tr>
	      <td><?php echo $form->labelEx($model,Yii::t('truckerDirectors','last_name')); ?></td>
	      <td><?php echo $form->labelEx($model,Yii::t('truckerDirectors','first_name')); ?></td>
	      <td><?php echo $form->labelEx($model,Yii::t('truckerDirectors','middle_name')); ?></td>
	      </tr>
	      <td><?php echo $form->textField($model,'last_name',array('maxlength'=>255));?>
	      <?php echo $form->error($model,'last_name'); ?></td>
	      <td><?php echo $form->textField($model,'first_name',array('maxlength'=>255));?>
	      <?php echo $form->error($model,'first_name'); ?></td>
	      <td><?php echo $form->textField($model,'middle_name',array('maxlength'=>255));?>
	      <?php echo $form->error($model,'middle_name'); ?></td>
	      <tr><td>&nbsp;</td></tr>
	      <tr>
	      <td><?php echo $form->labelEx($model,Yii::t('truckerDirectors','bday')); ?></td>
	      <td><?php echo $form->labelEx($model,Yii::t('truckerDirectors','bplace')); ?></td>
	      <td><?php echo $form->labelEx($model,Yii::t('truckerDirectors','gender')); ?></td>
	      </tr>
	      <tr>
	      <td>
	      <?php
	      $date = 'yy-mm-dd';
	      $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				      'attribute'=>'bday',
				      'model'=>$model,
				      'options'=>array(
					      'showAnim'=>'fold',
					      'dateFormat'=>$date,
					      'changeMonth'=> true,
					      'changeYear'=>true,
					      'yearRange'=>'1950:2050'
					      ),
				      'htmlOptions'=>array(
					      'style' => 'width:100px;',
					      ),
				      ))
	      ?>
	      <?php echo $form->error($model,'bday'); ?></td>
	      <td><?php echo $form->textField($model,'bplace',array('maxlength'=>255));?>
	      <?php echo $form->error($model,'bplace'); ?></td>
	      <td><?php echo $form->dropDownList($model,'gender',array('M' => 'Male', 'F' => 'Female'),array('empty' =>'Select Gender')); ?>
	      <?php echo $form->error($model,'gender'); ?></td>
	      </tr>
	      <tr><td>&nbsp;</td></tr>
	      <tr>
	      <td colspan=2><?php echo $form->labelEx($model,Yii::t('truckerDirectors','address')); ?></td>
	      <td><?php echo $form->labelEx($model,Yii::t('truckerDirectors','city')); ?></td>
	      </tr>
	      <tr>
	      <td colspan=2><?php echo $form->textField($model,'address',array('maxlength'=>255));?>
	      <?php echo $form->error($model,'address'); ?></td>
	      <td><?php echo $form->textField($model,'city',array('maxlength'=>255));?>
	      <?php echo $form->error($model,'city'); ?></td>
	      </tr>
	      <tr><td>&nbsp;</td></tr>
	      <tr>
	      <td><?php echo $form->labelEx($model,Yii::t('truckerDirectors','province')); ?></td>
	      <td><?php echo $form->labelEx($model,Yii::t('truckerDirectors','zipcode')); ?></td>
	      <td></td>
	      </tr>
	      <tr>
	      <td><?php echo $form->textField($model,'zipcode',array('maxlength'=>4));?>
	      <?php echo $form->error($model,'zipcode'); ?></td>
	      <td><?php echo $form->textField($model,'province',array('maxlength'=>255));?>
	      <?php echo $form->error($model,'province'); ?></td>
	      <td></td>
	      </tr>
	      </table>
	      </div>
	      </div>



	      <div class="clear"></div>
	      <div style="padding:0px 0 0 0px; text-align:left">
	      <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit »' : 'Save',array('class'=>'formbut')); ?>
	      </div>
	      <?php $this->endWidget(); ?>
