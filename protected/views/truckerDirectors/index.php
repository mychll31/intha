<?php
$this->breadcrumbs=array(
	'Trucker Directors',
);

$this->menu=array(
	array('label'=>'Create TruckerDirectors', 'url'=>array('create')),
	array('label'=>'Manage TruckerDirectors', 'url'=>array('admin')),
);
?>

<h1>Trucker Directors</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
