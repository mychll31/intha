<?php
$this->breadcrumbs=array(
	'Trucker Directors'=>array('index'),
	$model->id,
);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    <div class="emp_cont_left">
    <?php $this->renderPartial('profileleft');?>
    
    </div>
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <br> 
    <div class="edit_bttns last">
    <ul>
     <li>
    <?php echo CHtml::link(Yii::t('students','<span>Edit</span>'), array('update', 'id'=>$_REQUEST['did']),array('class'=>' edit ')); ?>
    </li>
    </ul>
    </div>
    <div class="clear"></div>
    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
    <?php $this->renderPartial('tab');?>
    <div class="clear"></div>
    <div class="emp_cntntbx" >
    <div class="table_listbx">
    <?php $director= TruckerDirectors::model()->findByAttributes(array('id'=>$_REQUEST['did'])); ?>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody><tr class="listbxtop_hdng">
    <td colspan="2">Director Details</td>
    </tr>
    <tr>
    <td class="subhdng_nrmal"><span class="datahead">Name :</span><span class="data">
    <?php
    $p = EmpPrefix::model()->findByAttributes(array('id'=>$director->prefix));
    if($p->name !=null){echo $p->name.'. ';}else{echo '';}
    echo $director->last_name.' '.$director->first_name.' '.$director->middle_name;
    ?>
    </span></td>
    <td class="subhdng_nrmal"><span class="datahead">Gender :</span><span class="data">
    <?php
    if($director->gender=='F'){
    	echo "Female";
    }else if($director->gender=='M'){
    	echo "Male";
    }else{
    	echo "Unknown";
    }
    ?>
    </span></td>
    </tr><tr>
    <td class="subhdng_nrmal" colspan=2><span class="datahead">Address :</span><span class="data">
    <?php echo $director->address.' '.$director->city.' '.$director->province.' '.$director->zipcode;?>
    </span></td>
    </tr><tr>
    <td class="subhdng_nrmal"><span class="datahead">Birthday :</span><span class="data">
    <?php echo date('M. d, Y',strtotime($director->bday)); ?>
    </span></td>
    <td class="subhdng_nrmal"><span class="datahead">Birth Place :</span><span class="data"><?php echo $bplace; ?></span></td>
    </tr>
    </tbody></table>

    </div>
    </div>
    </div>
    </div>
    
    </div>
    </div>
   
    </td>
  </tr>
</table>
