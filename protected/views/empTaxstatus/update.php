<?php
$this->breadcrumbs=array(
	'Emp Taxstatuses'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmpTaxstatus', 'url'=>array('index')),
	array('label'=>'Create EmpTaxstatus', 'url'=>array('create')),
	array('label'=>'View EmpTaxstatus', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmpTaxstatus', 'url'=>array('admin')),
);
?>

<h1>Update EmpTaxstatus <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>