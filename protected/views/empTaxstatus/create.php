<?php
$this->breadcrumbs=array(
	'Emp Taxstatuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmpTaxstatus', 'url'=>array('index')),
	array('label'=>'Manage EmpTaxstatus', 'url'=>array('admin')),
);
?>

<h1>Create EmpTaxstatus</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>