<?php
$this->breadcrumbs=array(
	'Emp Taxstatuses',
);

$this->menu=array(
	array('label'=>'Create EmpTaxstatus', 'url'=>array('create')),
	array('label'=>'Manage EmpTaxstatus', 'url'=>array('admin')),
);
?>

<h1>Emp Taxstatuses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
