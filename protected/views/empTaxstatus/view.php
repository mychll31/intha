<?php
$this->breadcrumbs=array(
	'Emp Taxstatuses'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List EmpTaxstatus', 'url'=>array('index')),
	array('label'=>'Create EmpTaxstatus', 'url'=>array('create')),
	array('label'=>'Update EmpTaxstatus', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmpTaxstatus', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmpTaxstatus', 'url'=>array('admin')),
);
?>

<h1>View EmpTaxstatus #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
