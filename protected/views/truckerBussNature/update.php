<?php
$this->breadcrumbs=array(
	'Trucker Buss Natures'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TruckerBussNature', 'url'=>array('index')),
	array('label'=>'Create TruckerBussNature', 'url'=>array('create')),
	array('label'=>'View TruckerBussNature', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TruckerBussNature', 'url'=>array('admin')),
);
?>

<h1>Update TruckerBussNature <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>