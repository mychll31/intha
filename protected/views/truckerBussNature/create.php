<?php
$this->breadcrumbs=array(
	'Trucker Buss Natures'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TruckerBussNature', 'url'=>array('index')),
	array('label'=>'Manage TruckerBussNature', 'url'=>array('admin')),
);
?>

<h1>Create TruckerBussNature</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>