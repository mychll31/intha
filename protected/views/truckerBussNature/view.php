<?php
$this->breadcrumbs=array(
	'Trucker Buss Natures'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TruckerBussNature', 'url'=>array('index')),
	array('label'=>'Create TruckerBussNature', 'url'=>array('create')),
	array('label'=>'Update TruckerBussNature', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TruckerBussNature', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TruckerBussNature', 'url'=>array('admin')),
);
?>

<h1>View TruckerBussNature #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
