<?php
$this->breadcrumbs=array(
	'Trucker Buss Natures',
);

$this->menu=array(
	array('label'=>'Create TruckerBussNature', 'url'=>array('create')),
	array('label'=>'Manage TruckerBussNature', 'url'=>array('admin')),
);
?>

<h1>Trucker Buss Natures</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
