<?php
$this->breadcrumbs=array(
	'Request Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List RequestType', 'url'=>array('index')),
	array('label'=>'Manage RequestType', 'url'=>array('admin')),
);
?>

<h1>Create RequestType</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>