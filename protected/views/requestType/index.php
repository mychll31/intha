<?php
$this->breadcrumbs=array(
	'Request Types',
);

$this->menu=array(
	array('label'=>'Create RequestType', 'url'=>array('create')),
	array('label'=>'Manage RequestType', 'url'=>array('admin')),
);
?>

<h1>Request Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
