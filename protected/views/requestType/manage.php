<?php
$this->breadcrumbs=array(
	'Employees'=>array('/employees/'),
	'Manage Request'=>array('/requestType/'),
);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
    <?php $this->renderPartial('/requests/left_side');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <h1><?php echo Yii::t('requestType','List of Request');?></h1>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <div class="formCon">
<div class="formConInner">
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employee-leave-types-form',
	'enableAjaxValidation'=>false,
	'enableClientValidation'=>true,
)); ?>

	

	<?php /*?><?php echo $form->errorSummary($model); ?><?php */?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><?php echo $form->labelEx($model,Yii::t('requestType','name'),array('style'=>'float:left')); ?></td>
    <td><?php echo $form->textField($model,'name',array('size'=>30,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?></td>
    </tr>
  <tr>
  	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
  	<td><?php echo $form->labelEx($model,Yii::t('requestType','active'),array('style'=>'float:left')); ?>
    </td>
     <td colspan="3" class="cr_align"><?php echo $form->radioButtonList($model,'active',array('1'=>'Active','2'=>'Inactive'),array('separator'=>' ')); ?>
     <?php echo $form->error($model,'active'); ?></td>
  </tr>
</table>

	<div class="clear"></div>
<br />
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'formbut')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
</div>

<table width="90%">
	<tr><td valign=top style="padding:10px;">
<h3><?php echo Yii::t('requestType','Active Request Type');?></h3>
<div class="tableinnerlist">
<table width="100%" cellpadding="0" cellspacing="0">
<tr class="pdtab-h">
	<td style="text-align:left;">No.</td>
	<td style="text-align:left;">Request Type</td>
    <td>Actions</td>
</tr>


<?php
$active=requestType::model()->findAll("active=:x", array(':x'=>1));
$no=1;
foreach($active as $active_1)
{
   echo '<tr><td style="padding-left:10px; text-align:left;font-weight:normal;">'.$no++.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->name.'</td>';	
   echo '<td>'.CHtml::link(Yii::t('requestType','Edit'), array('update', 'id'=>$active_1->id)).'</td>';
}
?>
</table>
</div>
</td><td valign=top style="padding:10px;">
<h3><?php echo Yii::t('requestType','Inactive Request Type');?></h3>
<div class="tableinnerlist">
<table width="100%" cellpadding="0" cellspacing="0">
<tr class="pdtab-h">
	<td style="text-align:left;">No.</td>
	<td style="text-align:left;">Request Type</td>
    <td>Actions</td>
</tr>

<?php
$inactive=requestType::model()->findAll("active=:x", array(':x'=>2));
$no2 = 1;
foreach($inactive as $inactive_1)
{
   echo '<tr><td style="padding-left:10px; text-align:left;font-weight:normal">'.$no2++.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$inactive_1->name.'</td>';	
   echo '<td>'.CHtml::link(Yii::t('requestType','Edit'), array('update', 'id'=>$inactive_1->id)).'</td>';
}
?>
</table>
</td></tr>
</table>
</div>

</div>
    </td>
  </tr>
</table>