<?php
$this->breadcrumbs=array(
	'Request Types'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List RequestType', 'url'=>array('index')),
	array('label'=>'Create RequestType', 'url'=>array('create')),
	array('label'=>'View RequestType', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage RequestType', 'url'=>array('admin')),
);
?>

<h1>Update RequestType <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>