<?php
$this->breadcrumbs=array(
	'Employees'=>array('/employees/'),
	'Manage Prefixes'=>array('/empPrefix/'),
);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
    <?php $this->renderPartial('left_side');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <h1><?php echo Yii::t('empPrefix','List of Prefixes');?></h1>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <div class="formCon">
<div class="formConInner">
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employees-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
		
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><?php echo $form->labelEx($model,'policy_number'); ?></td>
    <td><?php echo $form->textField($model,'policy_number',array('maxlength'=>225)); ?>
		<?php echo $form->error($model,'policy_number'); ?></td>
	<td><?php echo $form->labelEx($model,'broker_name'); ?></td>
    <td><?php echo $form->textField($model,'broker_name',array('maxlength'=>225)); ?>
		<?php echo $form->error($model,'broker_name'); ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><?php echo $form->labelEx($model,'subs_date'); ?></td>
    <td>	<?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							
							'attribute'=>'subs_date',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
	 ?>
		<?php echo $form->error($model,'subs_date'); ?></td>
	<td><?php echo $form->labelEx($model,'broker_name'); ?></td>
    <td><?php echo $form->textField($model,'broker_name',array('maxlength'=>225)); ?>
		<?php echo $form->error($model,'broker_name'); ?></td>
  </tr>
</table>

	<div class="clear"></div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'formbut')); ?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
</div>

<table width="90%">
	<tr><td valign=top style="padding:10px;">
<h3><?php echo Yii::t('empPrefix','Active Prefix');?></h3>
<div class="tableinnerlist">
<table width="100%" cellpadding="0" cellspacing="0">
<tr class="pdtab-h">
	<td style="text-align:left;"></td>
	<td style="text-align:left;">Prefix</td>
    <td>Edit</td>
</tr>


<?php
$active=EmpPrefix::model()->findAll("active=:x", array(':x'=>1));
$no=1;
foreach($active as $active_1)
{
   echo '<tr><td style="padding-left:10px; text-align:left;font-weight:normal;">'.$no++.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->name.'</td>';	
   echo '<td>'.CHtml::link(Yii::t('empPrefix','Edit'), array('update', 'id'=>$active_1->id)).'</td>';
}
?>
</table>
</div>
</td><td valign=top style="padding:10px;">
<h3><?php echo Yii::t('empPrefix','Inactive Prefix');?></h3>
<div class="tableinnerlist">
<table width="100%" cellpadding="0" cellspacing="0">
<tr class="pdtab-h">
	<td style="text-align:left;">No.</td>
	<td style="text-align:left;">Prefix</td>
    <td>Edit</td>
</tr>

<?php
$inactive=EmpPrefix::model()->findAll("active=:x", array(':x'=>2));
$no2 = 1;
foreach($inactive as $inactive_1)
{
   echo '<tr><td style="padding-left:10px; text-align:left;font-weight:normal">'.$no2++.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$inactive_1->name.'</td>';	
   echo '<td>'.CHtml::link(Yii::t('empPrefix','Edit'), array('update', 'id'=>$inactive_1->id)).'</td>';
}
?>
</table>
</td></tr>
</table>
</div>

</div>
    </td>
  </tr>
</table>
