<?php
 $this->breadcrumbs=array(
	 'Inland Marine'=>array('/default/inlandmarine')
);
?>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="247" valign="top" id="port-left">
        	<?php $this->renderPartial('/default/left_side',array('totalCount'=>$totalCount));?>
        </td>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" width="75%" border=1>
					
					<div class="cont_right formWrapper" style="padding:20px 20px;">
					<h1>Inland Marine Details</h1>	
					<div class="pdtab_Con" style="width:97%;padding:0px !important;">
						<h3>TRUCKERS DETAILS</h3>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							  <?php		if($inlandtruckersCount!=0){ ?>
							  <tbody>
							  <tr class="pdtab-h">
								<td align="center">No.</td>
								<td align="center">Truck ID</td>
								<td align="center">Broker Name</td>
								<td align="center">Policy Number</td>
								<td align="center">Date of Subscription</td>
								<td align="center">Date of Expiration</td>
							  </tr>
							  </tbody>
							<?php	}	$no=1; 	foreach ($inlandtruckers as $t){ ?>
							  <tr>
								<td align="center" style="padding:3px 15px !important"><?php  echo $no++?></td>
								<?php $thisid=Students::model()->findByAttributes(array('assoc_truckerid'=>$t->trucker_id));?>
								<td style="padding:3px 15px !important;text-align:center"><?php echo CHtml::link(Yii::t('empLicense',$t->trucker_id), array('/students/students/inlandmarine','id'=>$thisid->id)); ?></td>
								<?php $broker_name=Inlandbrokers::model()->findByAttributes(array('id'=>$t->broker_name));?>
								<td align="center" style="padding:3px 15px !important"><?php  echo $broker_name->name?></td>
								<td><?php echo $t->policy_number;?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php	if($t->subs_date!=null)	echo date('M. d, Y',strtotime($t->subs_date));?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php	if($t->expire_date!=null) echo date('M. d, Y',strtotime($t->expire_date));?></td>
							  </tr>
							<?php	} ?>
							</table>
					<div class="pdtab_Con" style="width:97%;padding:0px !important;">
						<h3>TRUCK DETAILS</h3>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							  <?php		if($inlandtruckCount!=0){ ?>
							  <tbody>
							  <tr class="pdtab-h">
								<td align="center">No.</td>
								<td align="center">Truck ID</td>
								<td align="center">Plate Number</td>
								<td align="center">Engine Number</td>
								<td align="center">Description</td>
								<td align="center">Registered Owner</td>
								<td align="center">Date Added to IM</td>
							  </tr>
							  </tbody>
							<?php	}	$no=1; foreach ($inlandtruck as $t){ ?>
							  <tr>
								<?php $thisid=Courses::model()->findByAttributes(array('association_truck_id'=>$t->truck_id));?>
								<td align="center" style="padding:3px 15px !important"><?php  echo $no++;?></td>
								<td align="center" style="padding:3px 15px !important"><?php  echo $thisid->association_trucker_id;?></td>
								<td align="center" style="padding:3px 15px !important"><?php  echo $thisid->plate_number;?></td>
								<td align="center" style="padding:3px 15px !important"><?php  echo $thisid->truck_engine_number;?></td>
								<td align="center" style="padding:3px 15px !important"><?php  echo $thisid->description;?></td>
								<td align="center" style="padding:3px 15px !important"><?php  echo ucwords($thisid->reg_owner);?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php	if($t->subs_date!=null)	echo date('M. d, Y',strtotime($t->subs_date));?></td>
							  </tr>
							<?php	} ?>
							</table>
					</div>
                    </div>
                    </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

