<?php
                #Start Alert for Events
		#Today
                $alertEvents = 0;
                $criteria_events = new CDbCriteria;
                $criteria_events->order = 'start desc';
                $allEvents = Events::model()->findAll($criteria_events);
                foreach($allEvents as $eve){
                        if(date('Y m d',$eve->start)==date('Y m d',time())){$alertEvents++;}
                }
                #End Alert for Events

?>
<!--upgrade_div_starts-->
<div class="upgrade_bx">
	
	<div class="up_banr_firstbx">
   	  <h1>You are Using Community Edition</h1>
	  <a href="http://open-school.org/contact.php" target="_blank">upgrade to premium version!</a>
    </div>
	
</div>
<!--upgrade_div_ends-->
<?php 
$roles = Rights::getAssignedRoles(Yii::app()->user->Id); // check for single role
	?>
        <div id="othleft-sidebar">
            <!--<div class="lsearch_bar">
            <input name="" type="text" class="lsearch_bar_left" value="Search" />
            <input name="" type="button" class="sbut" />
            <div class="clear"></div>
            </div>-->
            <h1>My Account</h1>  
            <?php
            function t($message, $category = 'cms', $params = array(), $source = null, $language = null) 
            {
            	return $message;
            }
	$role=UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id));
        if($role->role==1){
            $this->widget('zii.widgets.CMenu',array(
            'encodeLabel'=>false,
            'activateItems'=>true,
            'activeCssClass'=>'list_active',
            'items'=>array(
			array('label'=>Yii::t('dashboard','Mailbox('.Yii::app()->getModule("mailbox")->getNewMsgs(Yii::app()->user->id).')<span>'.Yii::t('dashboard','All Received Messages').'</span>'), 'url'=>array('/mailbox'),
                'active'=> ((Yii::app()->controller->module->id=='mailbox' and  Yii::app()->controller->id!='news') ? true : false),'linkOptions'=>array('class'=>'inbox_ico')),
	
				array('label'=>''.t('<h1>Events</h1>'),
				'active'=> ((Yii::app()->controller->module->id=='cal') ? true : false)),
				
				array('label'=>t('Events List ( <font color=\'red\'>'.$alertEvents.'</font> ) <span>All Events</span>'), 'url'=>array('/dashboard/default/events'),
				'active'=> ((Yii::app()->controller->module->id=='dashboard') ? true : false),'linkOptions'=>array('class'=>'evntlist_ico')),
				
				array('label'=>t('Calendar<span>Schedule Events</span>'), 'url'=>array('/cal'),
				'active'=> (((Yii::app()->controller->module->id=='cal') and (Yii::app()->controller->id != 'eventsType')) ? true : false),'linkOptions'=>array('class'=>'cal_ico')),
				
				array('label'=>t('Event Types<span>Manage Event Types</span>'), 'url'=>array('/cal/eventsType'),
				'active'=> ((Yii::app()->controller->id=='eventsType') ? true : false),'linkOptions'=>array('class'=>'evnttype_ico')),
				
				array('label'=>t('Inland Marine Information<span>Manage Event Types</span>'), 'url'=>array('/inlandMarineTrucker/manage'),
				'active'=> ((Yii::app()->controller->id=='inlandMarineTrucker') ? true : false),'linkOptions'=>array('class'=>'vt_ico')),
				
				array('label'=>t('Notifications ( <font color=\'red\'>'.$totalCount.'</font> ) <span>Manage Notifications</span>'), 'url'=>array('/default/noti'),
				'active'=> ((Yii::app()->controller->action->id=='noti') ? true : false),'linkOptions'=>array('class'=>'notify_ico')),
				
            ),
            )); 
		}else{
            $this->widget('zii.widgets.CMenu',array(
            'encodeLabel'=>false,
            'activateItems'=>true,
            'activeCssClass'=>'list_active',
            'items'=>array(
			array('label'=>Yii::t('dashboard','Mailbox('.Yii::app()->getModule("mailbox")->getNewMsgs(Yii::app()->user->id).')<span>'.Yii::t('dashboard','All Received Messages').'</span>'), 'url'=>array('/mailbox'),
                'active'=> ((Yii::app()->controller->module->id=='mailbox' and  Yii::app()->controller->id!='news') ? true : false),'linkOptions'=>array('class'=>'inbox_ico')),
	
				array('label'=>''.t('<h1>Events</h1>'),
				'active'=> ((Yii::app()->controller->module->id=='cal') ? true : false)),
				
				array('label'=>t('Events List ( <font color=\'red\'>'.$alertEvents.'</font> ) <span>All Events</span>'), 'url'=>array('/dashboard/default/events'),
				'active'=> ((Yii::app()->controller->module->id=='dashboard') ? true : false),'linkOptions'=>array('class'=>'evntlist_ico')),
				
				array('label'=>t('Calendar<span>Schedule Events</span>'), 'url'=>array('/cal'),
				'active'=> (((Yii::app()->controller->module->id=='cal') and (Yii::app()->controller->id != 'eventsType')) ? true : false),'linkOptions'=>array('class'=>'cal_ico')),
				
				array('label'=>t('Inland Marine Information<span>Manage Event Types</span>'), 'url'=>array('/inlandMarineTrucker/manage'),
				'active'=> ((Yii::app()->controller->action->id=='inlandmarine') ? true : false),'linkOptions'=>array('class'=>'vt_ico')),
				
				array('label'=>t('Notifications ( <font color=\'red\'>'.$totalCount.'</font> ) <span>Manage Notifications</span>'), 'url'=>array('/default/noti'),
				'active'=> ((Yii::app()->controller->action->id=='noti') ? true : false),'linkOptions'=>array('class'=>'notify_ico')),
				
            ),
            )); 

		}
	?>
        </div>
<script type="text/javascript">
$(document).ready(function () {
	//Hide the second level menu
	$('#othleft-sidebar ul li ul').hide();            
	//Show the second level menu if an item inside it active
	$('li.list_active').parent("ul").show();
	
	$('#othleft-sidebar').children('ul').children('li').children('a').click(function () {                    
	
	if($(this).parent().children('ul').length>0){                  
		$(this).parent().children('ul').toggle();    
	}
	
	});
});
</script>
