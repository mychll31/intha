<?php
 $this->breadcrumbs=array(
	 'Notifications'=>array('/default/noti')
);
 $role=UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id));
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="247" valign="top" id="port-left">
        	<?php $this->renderPartial('/default/left_side',array('totalCount'=>$totalCount));?>
        </td>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" width="75%">
					
					<div class="cont_right formWrapper" style="padding:20px 20px;">
					<h1>Notifications</h1>					
                    <h4 style="margin:0px auto;">60 DAYS NOTICE</h4>
						<div class="pdtab_Con" style="width:97%">
							<h4>Employee Driving License ( <font color='red'><?php  echo $licCount?></font> ) </h4>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<?php if($licCount!=0){ ?>							
							<tbody>
							  <tr class="pdtab-h">
								<td align="center">#</td>
								<td align="center">Employee Number</td>
								<td align="center">License Type</td>
								<td align="center">License Number</td>
								<td align="center">Issuance Date</td>
								<td align="center">Expiration Date</td>
							  </tr>
							  </tbody>
							<?php	}	
								$no=1;
								foreach ($liclist as $t){ ?>
							  <tr>
								<?php
									$empid = Employees::model()->findByAttributes(array('id'=>$t->emp_no));
								?>
								<td style="padding:3px 15px !important;text-align:center"><?php echo $no++; ?></td>
								<?php if($role->role==1){
								?>
								<td style="padding:3px 15px !important;text-align:center"><?php echo CHtml::link(Yii::t('empLicense',$empid->association_employee_id), array('/courses/courses/view','id'=>$t->emp_no)); ?></td>
								<?php }else{ ?>
								<td style="padding:3px 15px !important;text-align:center"><?php 
								echo $empid->association_employee_id; ?></td>
								<?php } ?>
								<td align="center" style="padding:3px 15px !important"><?php echo $t->license_type;?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php echo $t->license_no;?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php echo $t->issuance_date;?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php echo $t->expiry_date;?></td>
							  </tr>
							<?php	} ?>
							</table>
						</div>
					<div class="pdtab_Con" style="width:97%">
							<h4>Truck Registration ( <font color='red'><?php  echo $truckCount;?></font> ) </h4>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<?php if($truckCount!=0){ ?>							
							<tbody>
							  <tr class="pdtab-h">
								<td align="center">#</td>
								<td align="center">Truck ID</td>
								<td align="center">Plate Number</td>
								<td align="center">Engine Number</td>
								<td align="center">Chasis Number</td>
								<td align="center">Description</td>
								<td align="center">Registered Owner</td>
								<td align="center">Date Added to IM</td>
							  </tr>
							  </tbody>
							<?php	}	$no=1;	foreach ($trucks as $t){ ?>
							  <tr>
								<td style="padding:3px 15px !important;text-align:center"><?php echo $no++; ?></td>
								<?php if($role->role==1){ ?>
								<td style="padding:3px 15px !important;text-align:center">
									<?php 
										echo CHtml::link(Yii::t('empLicense',$t->association_truck_id), array('/courses/courses/view','id'=>$t->id)); ?></td> <?php }else{ ?>
								<td style="padding:3px 15px !important;text-align:center">
									<?php 
										echo $t->association_truck_id; ?></td><?php } ?>
								<td style="padding:3px 15px !important;text-align:center"><?php  echo $t->plate_number; ?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php  echo $t->truck_engine_number; ?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php  echo $t->chasis_number; ?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php  echo $t->description; ?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php  echo $t->reg_owner; ?></td>
								<td style="padding:3px 15px !important;text-align:center">
								<?php $dateaddedtoIM = InlandMarineTrucks::model()->findByAttributes(array('truck_id'=>$t->association_truck_id));
                                        echo $dateaddedtoIM->subs_date; ?>
</td>
							  </tr>
							<?php	} ?>
							</table>
						</div>
					<div class="pdtab_Con" style="width:97%">
							<h4>Comprehensive Insurance ( <font color='red'><?php  echo $compreCount?></font> ) </h4>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<?php if($compreCount!=0){ ?>							
							<tbody>
							  <tr class="pdtab-h">
								<td align="center">#</td>
								<td align="center">Truck ID</td>
								<td align="center">Insurance Broker/Agent</td>
								<td align="center">Insurer</td>
								<td align="center">Policy Number</td>
								<td align="center">Coverage</td>
								<td align="center">Date Issued</td>
								<td align="center">Date Expiry</td>
							  </tr>
							  </tbody>
							<?php	}	$no=1;	foreach ($compreReg as $t){ ?>
							  <tr>
								<td align="center" style="padding:3px 15px !important"><?php  echo $no++; ?></td>
								<?php $truckid = Courses::model()->findByAttributes(array('association_truck_id'=>$t->truck_id)); ?>
								<?php if($role->role==1){ ?>
								<td style="padding:3px 15px !important;text-align:center"><?php echo CHtml::link(Yii::t('empLicense',$t->truck_id), array('/courses/courses/view','id'=>$truckid->id)); ?></td><?php }else{ ?>
								<td style="padding:3px 15px !important;text-align:center"><?php echo CHtml::link(Yii::t('empLicense',$t->truck_id), array('/courses/courses/view','id'=>$truckid->id)); ?></td><?php } ?>
								<td style="padding:3px 15px !important;text-align:center"><?php  echo $t->compre_broker;?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php  echo $t->compre_insurer; ?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php  echo $t->compre_policy_number; ?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php  echo $t->compre_coverage; ?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php  echo $t->compre_dateissued; ?></td>
								<td style="padding:3px 15px !important;text-align:center"><?php  echo $t->compre_dateexpire; ?></td>
							  </tr>
							<?php	} ?>
							</table>
						</div>
                    </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

