<?php 
/**
-------------------------
GNU GPL COPYRIGHT NOTICES
-------------------------
This file is part of Open-School.

Open-School is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Open-School is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Open-School.  If not, see <http://www.gnu.org/licenses/>.*/

/**
 * $Id$
 *
 * @author Open-School team <contact@Open-School.org>
 * @link http://www.Open-School.org/
 * @copyright Copyright &copy; 2009-2012 wiwo inc.
 * @Matthew George,@Rajith Ramachandran,@Arun Kumar,
 * @Anupama,@Laijesh V Kumar,@Tanuja.
 * @license http://www.Open-School.org/
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<!-- CSS main application styling. -->
    <link rel="icon" type="image/ico" href="<?php echo Yii::app()->request->baseUrl; ?>/uploadedfiles/school_logo/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style_custom.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/formstyle.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/dashboard.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/formelements.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/coda-slider-2.0.css" type="text/css" media="screen" />  
   
     <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.7.1.min.js"></script>
      <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/chart/highcharts.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/custom-form-elements.js"></script>   
   </script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.min.js"></script>

    <script>
	$(document).ready(function() {
	$("#lodrop").click(function(){
	
            	if ($("#account_drop").is(':hidden')){
                	$("#account_drop").show();
				}
            	else{
                	$("#account_drop").hide();
            	}
            return false;
       			 });
				  $('#account_drop').click(function(e) {
            		e.stopPropagation();
        			});
        		$(document).click(function() {
					if (!$("#account_drop").is(':hidden')){
            		$('#account_drop').hide();
					}
        			});	
                
});
</script>


<script>
$(document).ready(function() {
$("#book_drop").click(function(){
	
            	if ($("#bookmark").is(':hidden')){
                	$("#bookmark").show();
				}
            	else{
                	$("#bookmark").hide();
            	}
            return false;
       			 });
				  $('#bookmark').click(function(e) {
            		e.stopPropagation();
        			});
        		$(document).click(function() {
					if (!$("#bookmark").is(':hidden')){
            		$('#bookmark').hide();
					}
        			});	
                
});
</script>

<script>
$(document).ready(function() {
  $(".nav_drop_but").click(function() {
  $(".navigationbtm_wrapper_outer").slideToggle();
	});
});
</script>

<script>
<?php 
if(isset(Yii::app()->controller->module->id) and (Yii::app()->controller->module->id=='hostel'||Yii::app()->controller->module->id=='transport'||Yii::app()->controller->module->id=='library'||Yii::app()->controller->module->id=='downloads')){?>
$(document).ready(function() {
$(".navigationbtm_wrapper_outer").show();
});
<?php }?>
</script>

<script>
	/*$(function() {
		$( "#sortable1, #sortable2" ).sortable({
			connectWith:".connectedSortable",
			placeholder: "ui-state-highlight"
		}).disableSelection();
		
	});*/
</script> 
    
</head>
<title><?php $college=Configurations::model()->findByPk(1); ?><?php echo $college->config_value ; ?></title>
<body>
<?php
  $role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id));
  if(Yii::app()->controller->id=='students' and 
  	(Yii::app()->controller->action->id!='view' 
	and Yii::app()->controller->action->id!='assesments'
	and Yii::app()->controller->action->id!='fees'
	and Yii::app()->controller->action->id!='trailer'
	and Yii::app()->controller->action->id!='inlandmarine'
	and Yii::app()->controller->action->id!='insurance'
	and Yii::app()->controller->action->id!='pa'
	and Yii::app()->controller->action->id!='compre'
	and Yii::app()->controller->action->id!='ctpl'
	and Yii::app()->controller->action->id!='attachment'
	and Yii::app()->controller->action->id!='attentance')
	and $role->role!=1){
	  $this->redirect(array('/students/students/view','id'=>Yii::app()->user->id));
  }
  if($role->role!=1 and (
  	(Yii::app()->controller->module->id !='students' 
	and Yii::app()->controller->id!='students' 
	and Yii::app()->controller->id!='requests' 
	and Yii::app()->controller->module->id!='mailbox' 
	and Yii::app()->controller->module->id!='dashboard' 
	and Yii::app()->controller->module->id!='cal' 
	and Yii::app()->controller->id!='inlandMarineTrucker' 
	and Yii::app()->controller->id!='default' 
	and Yii::app()->controller->id!='empTruckerRemarks')
    )){
   $this->redirect(array('/students/students/view','id'=>Yii::app()->user->id));
  }/*
  if($role->role!=1){
    #modules
    if(Yii::app()->controller->module->id=='students' 
    or Yii::app()->controller->module->id=='mailbox'
    or Yii::app()->controller->id=='requests'){
    	if(Yii::app()->controller->module->id=='students' and (
	  Yii::app()->controller->action->id=='view'
	  or Yii::app()->controller->action->id=='attentance'
	  or Yii::app()->controller->action->id=='fees'
	  or Yii::app()->controller->action->id=='trailer'
	  or Yii::app()->controller->action->id=='inlandmarine'
	  or Yii::app()->controller->action->id=='insurance'
	  or Yii::app()->controller->action->id=='attachment'
	  or Yii::app()->controller->action->id=='assesments')){
	  echo '<br><br>this view is fine';
    	}else if(Yii::app()->controller->module->id=='requests' and 
	  Yii::app()->controller->action->id=='history')
	}else{
	  echo '<br><br>this view is not fine';
	}
     echo '<br><br>module is fine';
    }else{
     echo '<br><br>nn';
    }
  }*/
?>
<div class="wrapper">
<div id="explorer_handler"></div>
    <div class="header">
     <?php 
		 echo CHtml::ajaxLink('OPEN APP EXPLORER',array('/site/explorer'),array('update'=>'#explorer_handler'),array('id'=>'open_apps','class'=>'explorer_but'));
		 ?>
   
     <div class="lo_drop" id="account_drop">
     <div class="lo_drop_hov"></div> 
     	<div class="lo_name">
	<?php if(isset(Yii::app()->user->name)){ ?>
	<span> <?php echo ucfirst(Yii::app()->user->name); ?> </span><?php }else $this->redirect(array('site/login'));?>
            <div class="clear"></div>
        </div>
    <ul>
        	<li><?php echo CHtml::link('My Account', array('/mailbox'));?></li>
            <li><?php echo CHtml::link('Preference', array('/configurations/create'));?></a></li>
            <li> <?php echo CHtml::link('Logout', array('/user/logout'));?></li>
        </ul>
     </div>
   
	 
	  <?php $college=Configurations::model()->findByPk(1);
	  $settings=UserSettings::model()->findByAttributes(array('user_id'=>Yii::app()->user->id));
	  if($settings!=NULL)
	  {
		  $lan=$settings->language;
	  }
	  else
	  {
		  $lan='en_us';
	  }
	 Yii::app()->translate->setLanguage($lan);
	  
	 ?>
     <?php $logo=Logo::model()->findAll();?>
        	<div class="logo">
            <!--<a href="index.php"><img src="images/logo.png" alt=""  border="0" />-->
			<?php 
			if($logo!=NULL)
			{
				echo '<img src="'.$this->createUrl('/Configurations/DisplaySavedImage&id='.$logo[0]->primaryKey).'" alt="'.$logo[0]->photo_file_name.'" border="0" height="55" />';
			}
			//echo $college->photo_file_name ; ?></a> </div>
            <!--<div align="center">
          
     <a id="print_button" href="javascript:window.print();",'_blank'>print</a>
     </div>-->
            <div class="logo_right">
            
<div class="searchbx">
  
				 <form action="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=site/search" name="search" method="post">
                	<ul>
                    	<li><input class="searchbar" name="char" type="text"></li>
                        <li>
                        <input src="images/search.png" type="image" name="555" value="submit">
                        </li>
                    </ul>
                  </form>  
                </div>
                <div class="mssgbx">
                 	<div id="status-bar">
						<ul id="status-infos" style="list-style:none; padding:0px;">
							<li><a href="index.php?r=mailbox" class="mssgimg" title=" Unread Message(s)"></a></li>
							<?php
                   ##Trucker membership notifications
                $criteria_a = new CDbCriteria;
                $exp1 = date('Y-m-d',time());
                $exp2 = date('Y-m-d',(date(strtotime('+60 day'),time())));

                #Truck Registration Date
                $criteria_b = new CDbCriteria;
                $truckCount = 0;
                $criteria_b->condition=' reg_enddate >=  :expiry and reg_enddate <= :expiry1';
                $criteria_b->params[':expiry']=$exp1;
                $criteria_b->params[':expiry1']=$exp2;
                $truckCount = Courses::model()->count($criteria_b);

                #TruckerRegdate Date
                $criteria_lic = new CDbCriteria;
                $licCount = 0;
                $criteria_lic->condition=' active=1 and expiry_date>=  :expiry and expiry_date <= :expiry1';
                $criteria_lic->params[':expiry']=$exp1;
                $criteria_lic->params[':expiry1']=$exp2;
                $licCount = EmpLicense::model()->count($criteria_lic);

                #Comprehensive Insurance
                $criteria_99 = new CDbCriteria;
                $orcrCount = 0;
                $criteria_99->condition=' compre_dateexpire >=  :expiry and compre_dateexpire <= :expiry1';
                $criteria_99->params[':expiry']=$exp1;
                $criteria_99->params[':expiry1']=$exp2;
                $compreCount = Insurance::model()->count($criteria_99);

                $totalCount = $licCount + $truckCount + $compreCount;
							?>
							<li><a href="index.php?r=default/noti" title="Notification(s)"><marquee><b><?php if($totalCount > 0) echo "(".$totalCount.")";?></marquee></b></a></li>
							<li><a href="index.php?r=mailbox" class="alert" title=" Alert Message(s)"></a></li>
						</ul>
					</div>
    			</div>
                <div class="usernamebx">
                	<ul>
                    	<li><a href="#" id="lodrop">Account</a></li>
                    </ul>
                </div>
            </div>
      </div>
      <div class="navigation_wrapper_outer">
      <div class="navigation_wrapper">
      <div class="nav">
       
      <ul id="sortable1" class="connectedSortable">
        <li>
        <?php
	if(isset(Yii::app()->controller->module->id)) {
		if(Yii::app()->controller->module->id=='mailbox'||
		Yii::app()->controller->module->id=='dashboard' ||
		Yii::app()->controller->module->id=='cal'){
			echo CHtml::link(Yii::t('app','Home'), array('/mailbox'),array('class'=>'ic1 active'));
		}else{
			echo CHtml::link(Yii::t('app','Home'), array('/mailbox'),array('class'=>'ic1'));
		}
	}else if((Yii::app()->controller->id=='default')or
	(Yii::app()->controller->id=='inlandMarineTrucker')or
	(Yii::app()->controller->id=='inlandMarineTrucks')){
		echo CHtml::link(Yii::t('app','Home'), array('/mailbox'),array('class'=>'ic1 active'));
	}else{
		echo CHtml::link(Yii::t('app','Home'), array('/mailbox'),array('class'=>'ic1'));
	}
	?>
      </li>
      <li>
      <?php 
      if(Yii::app()->controller->id=='students' || 
      Yii::app()->controller->id =='studentCategories' || 
      Yii::app()->controller->id =='studentCategory'){
	      echo CHtml::link(Yii::t('app','Truckers'), array('/students/students/manage'),array('class'=>'ic2 active'));
      }else if((isset(Yii::app()->controller->id)and 
      Yii::app()->controller->id=='truckerTruckerStatus')or
      (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='truckerBussType')or
      (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='truckerBussNature')or
      (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='truckerGarage')or
      (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='TruckersOtherAssoc')or
      (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='empTruckerRemarks')or
      (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='inlandbrokers')){
	      echo CHtml::link(Yii::t('app','Truckers'), array('/students/students/manage'),array('class'=>'ic2 active'));
      }else{
	      echo CHtml::link(Yii::t('app','Truckers'), array('/students/students/manage'),array('class'=>'ic2'));
      }
     ?>
     </li>
     <?php if($role->role==1){ ?>
     <li>
     <?php 
     if(isset(Yii::app()->controller->module->id) and Yii::app()->controller->module->id=='employees'){
	     echo CHtml::link(Yii::t('app','Employees'), array('/employees/employees/index'),array('class'=>'ic3 active'));
     }else if((isset(Yii::app()->controller->id) and Yii::app()->controller->id=='employeeAddresses')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='empLicense')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='empWorkexp')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='employeeNotes')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='empDicipAction')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='empDependants')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='empAssocxp')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='empPrefix')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='empStatus')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='empPos')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='empDept')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='empTaxstatus')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='empRegion')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='empPaytype')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='empPayperiod')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='empReportingMethod')){
	     echo CHtml::link(Yii::t('app','Employees'), array('/employees/employees/index'),array('class'=>'ic3 active'));
     }else{
	     echo CHtml::link(Yii::t('app','Employees'), array('/employees/employees/index'),array('class'=>'ic3'));
     }?>
     </li>
     <li>
     <?php 
     if(isset(Yii::app()->controller->module->id) and Yii::app()->controller->module->id=='courses'){
	     echo CHtml::link(Yii::t('app','Trucks'), array('/courses/courses/managecourse'),array('class'=>'ic9 active'));
     }else if((isset(Yii::app()->controller->id) and Yii::app()->controller->id=='trailerStatus')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='truckerWheeler')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='truckerBrand')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='truckerTrailerType')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='truckerStatus')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='truckerTypeNote')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='truckersTractorType')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='truckTrailerFooter')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='truckTrailer')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='truckTrailerAxle')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='insurance')or
     (isset(Yii::app()->controller->id) and Yii::app()->controller->id=='truckNotes')){
	     echo CHtml::link(Yii::t('app','Trucks'), array('/courses/courses/managecourse'),array('class'=>'ic9 active'));
     }else{
	     echo CHtml::link(Yii::t('app','Trucks'), array('/courses/courses/managecourse'),array('class'=>'ic9'));
     }
     ?>
     </li>
     <li>
     <?php 
     if(isset(Yii::app()->controller->module->id) and Yii::app()->controller->module->id=='report'){
	     echo CHtml::link(Yii::t('app','Reports'), array('/report'),array('class'=>'ic6 active'));
     }else{
	     echo CHtml::link(Yii::t('app','Reports'), array('/report'),array('class'=>'ic6'));
     }?>
     </li>
     <?php } ?>
     <li>
     <?php 
     if(isset(Yii::app()->controller->id) and Yii::app()->controller->id=='requests'){
	echo CHtml::link(Yii::t('app','Requests'), array('/requests'),array('class'=>'ic14 active'));
     }else{
	echo CHtml::link(Yii::t('app','Requests'), array('/requests'),array('class'=>'ic14'));
     }?>
     </li>
     <?php if($role->role==1){ ?>
     <li>
     <?php 
     if(Yii::app()->controller->id=='configurations' or Yii::app()->controller->id=='subjects' or 
     Yii::app()->controller->id=='subjectName' or Yii::app()->controller->id=='user' or 
     in_array(Yii::app()->controller->id,array('admin','profile','profileField')) or Yii::app()->controller->id=='edit'){
	     echo CHtml::link(Yii::t('app','Settings'), array('/configurations/'),array('class'=>'ic8 active'));
     }else{
	     echo CHtml::link(Yii::t('app','Settings'), array('/configurations/'),array('class'=>'ic8'));
     }?>
     </li>
     <?php } ?>
     </ul>
            
        </div>	
      </div>
         <div id="jobDialog"></div>
     </div> 
    <div class="midnav">
        	<?php $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		'separator'=>'',
	)); ?>
     </div>

     
     <div class="container">
      <?php echo $content; ?>
      
	</div>
    </div>

</body>
</html>
