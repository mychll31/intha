<?php
$this->breadcrumbs=array(
	'Trucker Trailer Types'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TruckerTrailerType', 'url'=>array('index')),
	array('label'=>'Create TruckerTrailerType', 'url'=>array('create')),
	array('label'=>'View TruckerTrailerType', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TruckerTrailerType', 'url'=>array('admin')),
);
?>

<h1>Update TruckerTrailerType <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>