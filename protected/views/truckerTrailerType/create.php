<?php
$this->breadcrumbs=array(
	'Trucker Trailer Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TruckerTrailerType', 'url'=>array('index')),
	array('label'=>'Manage TruckerTrailerType', 'url'=>array('admin')),
);
?>

<h1>Create TruckerTrailerType</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>