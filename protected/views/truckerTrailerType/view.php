<?php
$this->breadcrumbs=array(
	'Trucker Trailer Types'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TruckerTrailerType', 'url'=>array('index')),
	array('label'=>'Create TruckerTrailerType', 'url'=>array('create')),
	array('label'=>'Update TruckerTrailerType', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TruckerTrailerType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TruckerTrailerType', 'url'=>array('admin')),
);
?>

<h1>View TruckerTrailerType #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
