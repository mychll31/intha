<?php
$this->breadcrumbs=array(
	'Trucker Trailer Types',
);

$this->menu=array(
	array('label'=>'Create TruckerTrailerType', 'url'=>array('create')),
	array('label'=>'Manage TruckerTrailerType', 'url'=>array('admin')),
);
?>

<h1>Trucker Trailer Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
