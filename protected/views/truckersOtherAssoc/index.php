<?php
$this->breadcrumbs=array(
	'Truckers Other Assocs',
);

$this->menu=array(
	array('label'=>'Create TruckersOtherAssoc', 'url'=>array('create')),
	array('label'=>'Manage TruckersOtherAssoc', 'url'=>array('admin')),
);
?>

<h1>Truckers Other Assocs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
