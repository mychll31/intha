<?php
$this->breadcrumbs=array(
	'Truckers Other Assocs'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TruckersOtherAssoc', 'url'=>array('index')),
	array('label'=>'Create TruckersOtherAssoc', 'url'=>array('create')),
	array('label'=>'Update TruckersOtherAssoc', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TruckersOtherAssoc', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TruckersOtherAssoc', 'url'=>array('admin')),
);
?>

<h1>View TruckersOtherAssoc #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'desc',
		'active',
	),
)); ?>
