<?php
$this->breadcrumbs=array(
	'Truckers Other Assocs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TruckersOtherAssoc', 'url'=>array('index')),
	array('label'=>'Manage TruckersOtherAssoc', 'url'=>array('admin')),
);
?>

<h1>Create TruckersOtherAssoc</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>