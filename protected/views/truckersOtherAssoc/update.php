<?php
$this->breadcrumbs=array(
	'Truckers Other Assocs'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TruckersOtherAssoc', 'url'=>array('index')),
	array('label'=>'Create TruckersOtherAssoc', 'url'=>array('create')),
	array('label'=>'View TruckersOtherAssoc', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TruckersOtherAssoc', 'url'=>array('admin')),
);
?>

<h1>Update TruckersOtherAssoc <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>