<?php
$this->breadcrumbs=array(
	'Emp License Types'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmpLicenseType', 'url'=>array('index')),
	array('label'=>'Create EmpLicenseType', 'url'=>array('create')),
	array('label'=>'View EmpLicenseType', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmpLicenseType', 'url'=>array('admin')),
);
?>

<h1>Update EmpLicenseType <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>