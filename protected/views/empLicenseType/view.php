<?php
$this->breadcrumbs=array(
	'Emp License Types'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List EmpLicenseType', 'url'=>array('index')),
	array('label'=>'Create EmpLicenseType', 'url'=>array('create')),
	array('label'=>'Update EmpLicenseType', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmpLicenseType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmpLicenseType', 'url'=>array('admin')),
);
?>

<h1>View EmpLicenseType #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
