<?php
$this->breadcrumbs=array(
	'Emp License Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmpLicenseType', 'url'=>array('index')),
	array('label'=>'Manage EmpLicenseType', 'url'=>array('admin')),
);
?>

<h1>Create EmpLicenseType</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>