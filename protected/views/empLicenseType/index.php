<?php
$this->breadcrumbs=array(
	'Emp License Types',
);

$this->menu=array(
	array('label'=>'Create EmpLicenseType', 'url'=>array('create')),
	array('label'=>'Manage EmpLicenseType', 'url'=>array('admin')),
);
?>

<h1>Emp License Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
