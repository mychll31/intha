<?php
$this->breadcrumbs=array(
	'Trucker Statuses',
);

$this->menu=array(
	array('label'=>'Create TruckerStatus', 'url'=>array('create')),
	array('label'=>'Manage TruckerStatus', 'url'=>array('admin')),
);
?>

<h1>Trucker Statuses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
