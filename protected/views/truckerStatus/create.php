<?php
$this->breadcrumbs=array(
	'Trucker Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TruckerStatus', 'url'=>array('index')),
	array('label'=>'Manage TruckerStatus', 'url'=>array('admin')),
);
?>

<h1>Create TruckerStatus</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>