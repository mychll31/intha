<?php
$this->breadcrumbs=array(
	'Trucker Statuses'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TruckerStatus', 'url'=>array('index')),
	array('label'=>'Create TruckerStatus', 'url'=>array('create')),
	array('label'=>'Update TruckerStatus', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TruckerStatus', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TruckerStatus', 'url'=>array('admin')),
);
?>

<h1>View TruckerStatus #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
