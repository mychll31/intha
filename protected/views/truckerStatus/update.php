<?php
$this->breadcrumbs=array(
	'Trucker Statuses'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TruckerStatus', 'url'=>array('index')),
	array('label'=>'Create TruckerStatus', 'url'=>array('create')),
	array('label'=>'View TruckerStatus', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TruckerStatus', 'url'=>array('admin')),
);
?>

<h1>Update TruckerStatus <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>