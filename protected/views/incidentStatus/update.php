<?php
$this->breadcrumbs=array(
	'Incident Statuses'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List IncidentStatus', 'url'=>array('index')),
	array('label'=>'Create IncidentStatus', 'url'=>array('create')),
	array('label'=>'View IncidentStatus', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage IncidentStatus', 'url'=>array('admin')),
);
?>

<h1>Update IncidentStatus <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>