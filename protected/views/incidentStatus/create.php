<?php
$this->breadcrumbs=array(
	'Incident Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List IncidentStatus', 'url'=>array('index')),
	array('label'=>'Manage IncidentStatus', 'url'=>array('admin')),
);
?>

<h1>Create IncidentStatus</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>