<?php
$this->breadcrumbs=array(
	'Incident Statuses',
);

$this->menu=array(
	array('label'=>'Create IncidentStatus', 'url'=>array('create')),
	array('label'=>'Manage IncidentStatus', 'url'=>array('admin')),
);
?>

<h1>Incident Statuses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
