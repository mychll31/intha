<?php
$this->breadcrumbs=array(
	'Trucker Garages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TruckerGarage', 'url'=>array('index')),
	array('label'=>'Manage TruckerGarage', 'url'=>array('admin')),
);
?>

<h1>Create TruckerGarage</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>