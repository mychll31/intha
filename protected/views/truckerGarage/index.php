<?php
$this->breadcrumbs=array(
	'Trucker Garages',
);

$this->menu=array(
	array('label'=>'Create TruckerGarage', 'url'=>array('create')),
	array('label'=>'Manage TruckerGarage', 'url'=>array('admin')),
);
?>

<h1>Trucker Garages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
