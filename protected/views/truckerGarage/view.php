<?php
$this->breadcrumbs=array(
	'Trucker Garages'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TruckerGarage', 'url'=>array('index')),
	array('label'=>'Create TruckerGarage', 'url'=>array('create')),
	array('label'=>'Update TruckerGarage', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TruckerGarage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TruckerGarage', 'url'=>array('admin')),
);
?>

<h1>View TruckerGarage #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
