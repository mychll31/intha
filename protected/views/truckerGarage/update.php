<?php
$this->breadcrumbs=array(
	'Trucker Garages'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TruckerGarage', 'url'=>array('index')),
	array('label'=>'Create TruckerGarage', 'url'=>array('create')),
	array('label'=>'View TruckerGarage', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TruckerGarage', 'url'=>array('admin')),
);
?>

<h1>Update TruckerGarage <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>