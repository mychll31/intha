<?php
$this->breadcrumbs=array(
	'Employee Dependants'=>array('index&id='.$_REQUEST['id']),
	$model->id,
);

$this->menu=array(
	array('label'=>'List EmpDependants', 'url'=>array('index')),
	array('label'=>'Create EmpDependants', 'url'=>array('create')),
	array('label'=>'Update EmpDependants', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmpDependants', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmpDependants', 'url'=>array('admin')),
);
?>

<h1>View EmpDependants #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'emp_id',
		'dependents',
		'pref',
		'fname',
		'lname',
		'mname',
		'ext',
		'bdate',
	),
)); ?>
