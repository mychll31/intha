<?php
$this->breadcrumbs=array(
	'Emp Dependants',
);

$this->menu=array(
	array('label'=>'Create EmpDependants', 'url'=>array('create')),
	array('label'=>'Manage EmpDependants', 'url'=>array('admin')),
);
?>

<h1>Emp Dependants</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
