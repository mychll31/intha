<?php
$this->breadcrumbs=array(
	'Emp Dependants'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmpDependants', 'url'=>array('index')),
	array('label'=>'Manage EmpDependants', 'url'=>array('admin')),
);
?>

<h1>Create EmpDependants</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>