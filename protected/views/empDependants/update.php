<?php
$this->breadcrumbs=array(
	'Employee Dependants'=>array('index&id='.$_REQUEST['id']),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmpDependants', 'url'=>array('index')),
	array('label'=>'Create EmpDependants', 'url'=>array('create')),
	array('label'=>'View EmpDependants', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmpDependants', 'url'=>array('admin')),
);
?>

<h1>Update EmpDependants <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>