<?php
$this->breadcrumbs=array(
	'Emp Marital Statuses',
);

$this->menu=array(
	array('label'=>'Create EmpMaritalStatus', 'url'=>array('create')),
	array('label'=>'Manage EmpMaritalStatus', 'url'=>array('admin')),
);
?>

<h1>Emp Marital Statuses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
