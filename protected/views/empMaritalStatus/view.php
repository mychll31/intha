<?php
$this->breadcrumbs=array(
	'Emp Marital Statuses'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List EmpMaritalStatus', 'url'=>array('index')),
	array('label'=>'Create EmpMaritalStatus', 'url'=>array('create')),
	array('label'=>'Update EmpMaritalStatus', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmpMaritalStatus', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmpMaritalStatus', 'url'=>array('admin')),
);
?>

<h1>View EmpMaritalStatus #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
