<?php
$this->breadcrumbs=array(
	'Emp Marital Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmpMaritalStatus', 'url'=>array('index')),
	array('label'=>'Manage EmpMaritalStatus', 'url'=>array('admin')),
);
?>

<h1>Create EmpMaritalStatus</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>