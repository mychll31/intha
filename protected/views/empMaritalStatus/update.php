<?php
$this->breadcrumbs=array(
	'Emp Marital Statuses'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmpMaritalStatus', 'url'=>array('index')),
	array('label'=>'Create EmpMaritalStatus', 'url'=>array('create')),
	array('label'=>'View EmpMaritalStatus', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmpMaritalStatus', 'url'=>array('admin')),
);
?>

<h1>Update EmpMaritalStatus <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>