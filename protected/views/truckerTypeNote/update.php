<?php
$this->breadcrumbs=array(
	'Trucker Type Notes'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TruckerTypeNote', 'url'=>array('index')),
	array('label'=>'Create TruckerTypeNote', 'url'=>array('create')),
	array('label'=>'View TruckerTypeNote', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TruckerTypeNote', 'url'=>array('admin')),
);
?>

<h1>Update TruckerTypeNote <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>