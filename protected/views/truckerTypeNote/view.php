<?php
$this->breadcrumbs=array(
	'Trucker Type Notes'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TruckerTypeNote', 'url'=>array('index')),
	array('label'=>'Create TruckerTypeNote', 'url'=>array('create')),
	array('label'=>'Update TruckerTypeNote', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TruckerTypeNote', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TruckerTypeNote', 'url'=>array('admin')),
);
?>

<h1>View TruckerTypeNote #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
