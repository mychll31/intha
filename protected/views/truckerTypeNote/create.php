<?php
$this->breadcrumbs=array(
	'Trucker Type Notes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TruckerTypeNote', 'url'=>array('index')),
	array('label'=>'Manage TruckerTypeNote', 'url'=>array('admin')),
);
?>

<h1>Create TruckerTypeNote</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>