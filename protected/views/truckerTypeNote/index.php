<?php
$this->breadcrumbs=array(
	'Trucker Type Notes',
);

$this->menu=array(
	array('label'=>'Create TruckerTypeNote', 'url'=>array('create')),
	array('label'=>'Manage TruckerTypeNote', 'url'=>array('admin')),
);
?>

<h1>Trucker Type Notes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
