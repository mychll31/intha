<?php
$this->breadcrumbs=array(
	'Truck Trailer Footers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TruckTrailerFooter', 'url'=>array('index')),
	array('label'=>'Manage TruckTrailerFooter', 'url'=>array('admin')),
);
?>

<h1>Create TruckTrailerFooter</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>