<?php
$this->breadcrumbs=array(
	'Truck Trailer Footers'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TruckTrailerFooter', 'url'=>array('index')),
	array('label'=>'Create TruckTrailerFooter', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('truck-trailer-footer-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Truck Trailer Footers</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'truck-trailer-footer-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'active',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
