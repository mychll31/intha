<?php
$this->breadcrumbs=array(
	'Truck Trailer Footers',
);

$this->menu=array(
	array('label'=>'Create TruckTrailerFooter', 'url'=>array('create')),
	array('label'=>'Manage TruckTrailerFooter', 'url'=>array('admin')),
);
?>

<h1>Truck Trailer Footers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
