<?php
$this->breadcrumbs=array(
	'Truck Trailer Footers'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TruckTrailerFooter', 'url'=>array('index')),
	array('label'=>'Create TruckTrailerFooter', 'url'=>array('create')),
	array('label'=>'Update TruckTrailerFooter', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TruckTrailerFooter', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TruckTrailerFooter', 'url'=>array('admin')),
);
?>

<h1>View TruckTrailerFooter #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
