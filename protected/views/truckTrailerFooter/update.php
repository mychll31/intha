<?php
$this->breadcrumbs=array(
	'Truck Trailer Footers'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TruckTrailerFooter', 'url'=>array('index')),
	array('label'=>'Create TruckTrailerFooter', 'url'=>array('create')),
	array('label'=>'View TruckTrailerFooter', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TruckTrailerFooter', 'url'=>array('admin')),
);
?>

<h1>Update TruckTrailerFooter <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>