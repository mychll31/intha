<?php
$this->breadcrumbs=array(
	'Employee Addresses',
);

$this->menu=array(
	array('label'=>'Create EmployeeAddresses', 'url'=>array('create')),
	array('label'=>'Manage EmployeeAddresses', 'url'=>array('admin')),
);
?>

<h1>Employee Addresses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
