<div class="pdtab_Con" style="width:97%">
	<?php
			$criteria = new CDbCriteria;
			$criteria->order = 'id DESC';
			$criteria->condition='employee_id = :match';
			$criteria->params = array(':match' => $_REQUEST['id']);
			$criteria->condition=$criteria->condition.' and '.'active = :active';
			$criteria->params[':active'] = 1;
			$address = EmployeeAddresses::model()->findAll($criteria);

			$ctr=0;
			$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
			if($address!=null){
	?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <tr class="pdtab-h">
                <td align="center" height="18">No.</td>
                <td align="center">Street No./ Street Address</td>
                <td align="center">Town</td>
                <td align="center">City</td>
                <td align="center">Province</td>          
                <td align="center">Zip Code</td>          
			</tr>
        </tbody>
		<?php
			}
			else{
				echo "<hr><center><h1>NOTHING FOUND!!</h1></center>";
			}
			foreach ($address as $lic){
			$ctr++;
		?>
		<tbody>
			<tr>
				<?php if($role->role==1){?>
                <td align="center"><?php echo CHtml::link(Yii::t('empLicense',$ctr), array('update', 'licid'=>$lic->id,'id'=>$_REQUEST['id'])); ?></td>
				<td align="center"><?php echo CHtml::link(Yii::t('empLicense',$lic->house_no), array('update', 'licid'=>$lic->id,'id'=>$_REQUEST['id'])); ?></td>
                <td align="center"><?php echo CHtml::link(Yii::t('empLicense',$lic->town), array('update', 'licid'=>$lic->id,'id'=>$_REQUEST['id'])); ?></td>
				<td align="center"><?php echo CHtml::link(Yii::t('empLicense',$lic->city), array('update', 'licid'=>$lic->id,'id'=>$_REQUEST['id'])); ?></td>
				<td align="center"><?php echo CHtml::link(Yii::t('empLicense',$lic->province), array('update', 'licid'=>$lic->id,'id'=>$_REQUEST['id'])); ?></td>
				<td align="center"><?php echo CHtml::link(Yii::t('empLicense',$lic->zipcode), array('update', 'licid'=>$lic->id,'id'=>$_REQUEST['id'])); ?></td>
				<?php }else{?>
				<td align="center"><?=$ctr?></td>
				<td align="center"><?=$lic->house_no?></td>
                <td align="center"><?=$lic->town?></td>
				<td align="center"><?=$lic->city?></td>
				<td align="center"><?=$lic->province?></td>
				<td align="center"><?=$lic->zipcode?></td>
				<?php }?>
			</tr>
        </tbody>
		<?php }?>
	</table>
</div>