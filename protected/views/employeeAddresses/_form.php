<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employee-addresses-form',
	'enableAjaxValidation'=>false,
)); ?>
	<br><br>
	<?php echo $form->errorSummary($model); ?>
	<?php $date = 'yy-mm-dd';?>
<div class="formCon" >
<?php echo $form->hiddenField($model,'employee_id',array('value'=>$_REQUEST['id']));?>
	<div class="formConInner">
	<h3>Personal Details</h3>
	<p class="note">Fields with <span class="required">*</span> are required.</p><br>
	<table width="75%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
		<td><?php echo $form->labelEx($model,Yii::t('empLicense','house_no')); ?></td>
		<td><?php echo $form->labelEx($model,Yii::t('empLicense','town')); ?></td>
	  </tr>
	  <tr>
	  <tr>
		<td><?php echo $form->textField($model,'house_no',array('size'=>40,'maxlength'=>255)); ?>
			<?php echo $form->error($model,'house_no'); ?></td>
		<td><?php echo $form->textField($model,'town',array('size'=>40,'maxlength'=>255)); ?>
			<?php echo $form->error($model,'town'); ?></td>
	  </tr>
	  <tr><td colspan=2>&nbsp;</td></tr>
	  <tr>
		<td><?php echo $form->labelEx($model,Yii::t('empLicense','city')); ?></td>
		<td><?php echo $form->labelEx($model,Yii::t('empLicense','province')); ?></td>
	  </tr>
	  <tr>
		<td><?php echo $form->textField($model,'city',array('size'=>40,'maxlength'=>255)); ?>
			<?php echo $form->error($model,'city'); ?></td>
		<td><?php echo $form->textField($model,'province',array('size'=>40,'maxlength'=>255)); ?>
			<?php echo $form->error($model,'province'); ?></td>
	  </tr>
	  <tr><td colspan=2>&nbsp;</td></tr>
	  <tr>
		<td><?php echo $form->labelEx($model,Yii::t('empLicense','zipcode')); ?></td>
		<td></td>
	  </tr>
	  <tr>
		<td><?php echo $form->textField($model,'zipcode',array('size'=>20,'maxlength'=>5)); ?>
			<?php echo $form->error($model,'zipcode'); ?></td>
		<td><?php echo $form->checkbox($model,'active'); ?>Active
			<?php echo $form->error($model,'active'); ?></td>
	  </tr>
	</table>
	</div>
</div>
	<div style="padding:0px 0 0 0px; text-align:left">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Add Address' : 'Update Address',array('class'=>'formbut')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->