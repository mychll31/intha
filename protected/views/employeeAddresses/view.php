<?php
$empid=Employees::model()->findByAttributes(array('association_employee_id'=>$_REQUEST['id']));
$this->breadcrumbs=array(
	'Employee'=>array('/employees'),
	$_REQUEST['id']=>array('employees/employees/view&id='.$empid->id),
	'Employee Addresses'=>array('employeeAddresses/view&id='.$_REQUEST['id']),
);
$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
      <?php $this->renderPartial('/employeeAddresses/profileleft');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
	<?php if($role->role==1){?>
	
<div class="edit_bttns" style="right: 30px;">
    <ul>
    <li><?php echo CHtml::link(Yii::t('employeeAddresses','<span>Add Addresses</span>'), array('employeeAddresses/create','id'=>$_REQUEST['id']),array('class'=>'edit last')); ?><!--<a class=" edit last" href="">Edit</a>--></li>
    </ul>
  </div>
  <?php }?>
  <h3>Address Details</h3>
<?php echo $this->renderPartial('_view', array('model'=>$model)); ?>
</div>
    </td>
  </tr>
</table>