<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ctpl_broker'); ?>
		<?php echo $form->textField($model,'ctpl_broker',array('size'=>60,'maxlength'=>225)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ctpl_insurer'); ?>
		<?php echo $form->textField($model,'ctpl_insurer',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ctpl_policy_no'); ?>
		<?php echo $form->textField($model,'ctpl_policy_no',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ctpl_coverage'); ?>
		<?php echo $form->textField($model,'ctpl_coverage',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ctpl_dateissued'); ?>
		<?php echo $form->textField($model,'ctpl_dateissued'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ctpl_dateexpire'); ?>
		<?php echo $form->textField($model,'ctpl_dateexpire'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'compre_broker'); ?>
		<?php echo $form->textField($model,'compre_broker',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'compre_insurer'); ?>
		<?php echo $form->textField($model,'compre_insurer',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'compre_policy_number'); ?>
		<?php echo $form->textField($model,'compre_policy_number',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'compre_coverage'); ?>
		<?php echo $form->textField($model,'compre_coverage',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'compre_dateissued'); ?>
		<?php echo $form->textField($model,'compre_dateissued'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'compre_dateexpire'); ?>
		<?php echo $form->textField($model,'compre_dateexpire'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pa_broker'); ?>
		<?php echo $form->textField($model,'pa_broker',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pa_insurer'); ?>
		<?php echo $form->textField($model,'pa_insurer',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pa_policy_no'); ?>
		<?php echo $form->textField($model,'pa_policy_no',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pa_coverage'); ?>
		<?php echo $form->textField($model,'pa_coverage',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pa_dateissued'); ?>
		<?php echo $form->textField($model,'pa_dateissued'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pa_dateexpire'); ?>
		<?php echo $form->textField($model,'pa_dateexpire'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'truck_id'); ?>
		<?php echo $form->textField($model,'truck_id',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trucker_id'); ?>
		<?php echo $form->textField($model,'trucker_id',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->