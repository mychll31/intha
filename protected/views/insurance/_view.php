<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ctpl_broker')); ?>:</b>
	<?php echo CHtml::encode($data->ctpl_broker); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ctpl_insurer')); ?>:</b>
	<?php echo CHtml::encode($data->ctpl_insurer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ctpl_policy_no')); ?>:</b>
	<?php echo CHtml::encode($data->ctpl_policy_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ctpl_coverage')); ?>:</b>
	<?php echo CHtml::encode($data->ctpl_coverage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ctpl_dateissued')); ?>:</b>
	<?php echo CHtml::encode($data->ctpl_dateissued); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ctpl_dateexpire')); ?>:</b>
	<?php echo CHtml::encode($data->ctpl_dateexpire); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('compre_broker')); ?>:</b>
	<?php echo CHtml::encode($data->compre_broker); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('compre_insurer')); ?>:</b>
	<?php echo CHtml::encode($data->compre_insurer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('compre_policy_number')); ?>:</b>
	<?php echo CHtml::encode($data->compre_policy_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('compre_coverage')); ?>:</b>
	<?php echo CHtml::encode($data->compre_coverage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('compre_dateissued')); ?>:</b>
	<?php echo CHtml::encode($data->compre_dateissued); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('compre_dateexpire')); ?>:</b>
	<?php echo CHtml::encode($data->compre_dateexpire); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pa_broker')); ?>:</b>
	<?php echo CHtml::encode($data->pa_broker); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pa_insurer')); ?>:</b>
	<?php echo CHtml::encode($data->pa_insurer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pa_policy_no')); ?>:</b>
	<?php echo CHtml::encode($data->pa_policy_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pa_coverage')); ?>:</b>
	<?php echo CHtml::encode($data->pa_coverage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pa_dateissued')); ?>:</b>
	<?php echo CHtml::encode($data->pa_dateissued); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pa_dateexpire')); ?>:</b>
	<?php echo CHtml::encode($data->pa_dateexpire); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('truck_id')); ?>:</b>
	<?php echo CHtml::encode($data->truck_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trucker_id')); ?>:</b>
	<?php echo CHtml::encode($data->trucker_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	*/ ?>

</div>