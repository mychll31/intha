	<div class="captionWrapper">
        <ul>
            <li><h2>Truck Details</h2></li>
            <li><h2 class="cur">Insurance Details</h2></li>
        </ul>
	</div>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'insurance-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
<div class="formCon">
<div class="formConInner">
<div>
<h3>CTPL Details</h3>
	<table width="90%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><?php echo $form->labelEx($model,'ctpl_broker'); ?></td>
			<td><?php echo $form->labelEx($model,'ctpl_insurer'); ?></td>
			<td><?php echo $form->labelEx($model,'ctpl_policy_no'); ?></td>
		</tr>
		<tr>
			<td>
			<?php echo $form->textField($model,'ctpl_broker',array('maxlength'=>225)); ?>
			<?php echo $form->error($model,'ctpl_broker'); ?>
			</td><td>
			<?php echo $form->textField($model,'ctpl_insurer',array('maxlength'=>255)); ?>
			<?php echo $form->error($model,'ctpl_insurer'); ?>
			</td><td>
			<?php echo $form->textField($model,'ctpl_policy_no',array('maxlength'=>255)); ?>
			<?php echo $form->error($model,'ctpl_policy_no'); ?>
			</td>
		</tr>
		<tr>
			<td colspan=3>&nbsp;</td>
		</tr>
		<tr>
			<td><?php echo $form->labelEx($model,'ctpl_coverage'); ?></td>
			<td><?php echo $form->labelEx($model,'ctpl_dateissued'); ?></td>
			<td><?php echo $form->labelEx($model,'ctpl_dateexpire'); ?></td>
		</tr>
		<tr>
			<td>
			<?php echo $form->textField($model,'ctpl_coverage',array('maxlength'=>255)); ?>
			<?php echo $form->error($model,'ctpl_coverage'); ?>
			</td><td>
			<?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'ctpl_dateissued',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
			?>
			<?php echo $form->error($model,'ctpl_dateissued'); ?>
			</td><td>
			<?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'ctpl_dateexpire',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
			?>
			<?php echo $form->error($model,'ctpl_dateexpire'); ?>
			</td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
		<tr><td colspan=3>&nbsp;</td></tr>
	</table>
	<h3>Compre Details</h3>
	<table width="90%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><?php echo $form->labelEx($model,'compre_broker'); ?></td>
			<td><?php echo $form->labelEx($model,'compre_insurer'); ?></td>
			<td><?php echo $form->labelEx($model,'compre_policy_number'); ?></td>
		</tr>
		<tr>
			<td>
			<?php echo $form->textField($model,'compre_broker',array('maxlength'=>255)); ?>
			<?php echo $form->error($model,'compre_broker'); ?>
			</td><td>
			<?php echo $form->textField($model,'compre_insurer',array('maxlength'=>255)); ?>
			<?php echo $form->error($model,'compre_insurer'); ?>
			</td><td>
			<?php echo $form->textField($model,'compre_policy_number',array('maxlength'=>255)); ?>
			<?php echo $form->error($model,'compre_policy_number'); ?>
			</td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
		<tr>
			<td><?php echo $form->labelEx($model,'compre_coverage'); ?></td>
			<td><?php echo $form->labelEx($model,'compre_dateissued'); ?></td>
			<td><?php echo $form->labelEx($model,'compre_dateexpire'); ?></td>
		</tr>
		<tr>
			<td>
			<?php echo $form->textField($model,'compre_coverage',array('maxlength'=>255)); ?>
			<?php echo $form->error($model,'compre_coverage'); ?>
			</td><td>
			<?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'compre_dateissued',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
			?>
			<?php echo $form->error($model,'compre_dateissued'); ?>
			</td><td>
			<?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'compre_dateexpire',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
			?>
			<?php echo $form->error($model,'compre_dateexpire'); ?>
			</td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
		<tr><td colspan=3>&nbsp;</td></tr>
	</table>
	<h3>PA Details</h3>
	<table width="90%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><?php echo $form->labelEx($model,'pa_broker'); ?></td>
			<td><?php echo $form->labelEx($model,'pa_insurer'); ?></td>
			<td><?php echo $form->labelEx($model,'pa_policy_no'); ?></td>
		</tr>
		<tr>
			<td>
			<?php echo $form->textField($model,'pa_broker',array('maxlength'=>255)); ?>
			<?php echo $form->error($model,'pa_broker'); ?>
			</td><td>
			<?php echo $form->textField($model,'pa_insurer',array('maxlength'=>255)); ?>
			<?php echo $form->error($model,'pa_insurer'); ?>
			</td><td>
			<?php echo $form->textField($model,'pa_policy_no',array('maxlength'=>255)); ?>
			<?php echo $form->error($model,'pa_policy_no'); ?>
			</td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
		<tr>
			<td><?php echo $form->labelEx($model,'pa_coverage'); ?></td>
			<td><?php echo $form->labelEx($model,'pa_dateissued'); ?></td>
			<td><?php echo $form->labelEx($model,'pa_dateexpire'); ?></td>
		</tr>
		<tr>
			<td>
			<?php echo $form->textField($model,'pa_coverage',array('maxlength'=>255)); ?>
			<?php echo $form->error($model,'pa_coverage'); ?>
			</td><td>
			<?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'pa_dateissued',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
			?>
			<?php echo $form->error($model,'pa_dateissued'); ?>
			</td><td>
			<?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'pa_dateexpire',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
			?>
			<?php echo $form->error($model,'pa_dateexpire'); ?>
			</td>
		</tr>
		<tr><td colspan=2>&nbsp;</td></tr>
		<tr>
			<td>
			<?php
				$truckdet = Courses::model()->findByAttributes(array('id'=>$_REQUEST['id']));
				echo $form->hiddenField($model,'truck_id',array('maxlength'=>255, 'value'=>$truckdet->association_truck_id)); 
			?>
			<?php echo $form->error($model,'truck_id'); ?>
			</td><td>
			<?php echo $form->hiddenField($model,'trucker_id',array('maxlength'=>255, 'value'=>$truckdet->association_trucker_id)); ?>
			<?php echo $form->error($model,'trucker_id'); ?>
			</td><td>
			<?php echo $form->hiddenField($model,'active'); ?>
			<?php echo $form->error($model,'active'); ?>
			</td>
		</tr>
	</table>
	<br><br>
	<div style="padding:0px 0 0 0px; text-align:left">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Save',array('class'=>'formbut')); ?>
	</div>
</div>
</div>
</div>
	

<?php $this->endWidget(); ?>

</div><!-- form -->