<?php
$this->breadcrumbs=array(
	'Insurances'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Insurance', 'url'=>array('index')),
	array('label'=>'Create Insurance', 'url'=>array('create')),
	array('label'=>'Update Insurance', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Insurance', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Insurance', 'url'=>array('admin')),
);
?>

<h1>View Insurance #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'ctpl_broker',
		'ctpl_insurer',
		'ctpl_policy_no',
		'ctpl_coverage',
		'ctpl_dateissued',
		'ctpl_dateexpire',
		'compre_broker',
		'compre_insurer',
		'compre_policy_number',
		'compre_coverage',
		'compre_dateissued',
		'compre_dateexpire',
		'pa_broker',
		'pa_insurer',
		'pa_policy_no',
		'pa_coverage',
		'pa_dateissued',
		'pa_dateexpire',
		'truck_id',
		'trucker_id',
		'active',
	),
)); ?>
