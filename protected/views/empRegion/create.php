<?php
$this->breadcrumbs=array(
	'Emp Regions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmpRegion', 'url'=>array('index')),
	array('label'=>'Manage EmpRegion', 'url'=>array('admin')),
);
?>

<h1>Create EmpRegion</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>