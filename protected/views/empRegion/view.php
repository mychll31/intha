<?php
$this->breadcrumbs=array(
	'Emp Regions'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List EmpRegion', 'url'=>array('index')),
	array('label'=>'Create EmpRegion', 'url'=>array('create')),
	array('label'=>'Update EmpRegion', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmpRegion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmpRegion', 'url'=>array('admin')),
);
?>

<h1>View EmpRegion #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
