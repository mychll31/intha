<?php
$this->breadcrumbs=array(
	'Emp Regions',
);

$this->menu=array(
	array('label'=>'Create EmpRegion', 'url'=>array('create')),
	array('label'=>'Manage EmpRegion', 'url'=>array('admin')),
);
?>

<h1>Emp Regions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
