<?php
$this->breadcrumbs=array(
	'Emp Regions'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmpRegion', 'url'=>array('index')),
	array('label'=>'Create EmpRegion', 'url'=>array('create')),
	array('label'=>'View EmpRegion', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmpRegion', 'url'=>array('admin')),
);
?>

<h1>Update EmpRegion <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>