<?php
$this->breadcrumbs=array(
	'Truck Trailers'=>array('/courses/courses/managetrailers'),
	'Create',
);

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="247" valign="top">

<?php $this->renderPartial('/truckerWheeler/left_side');?>

</td>
<td valign="top">
<div class="cont_right formWrapper">
<h2><?php echo Yii::t('truckerWheeler','New Trailer');?></h2>
<p class="note">Fields with <span class="required">*</span> are required.</p>
<div class="formCon">
<div class="formConInner">

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

</td></tr>
</table>
</div>

</div>
    </td>
      </tr>
      </table>

