<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trailer_type')); ?>:</b>
	<?php echo CHtml::encode($data->trailer_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trailer_plate_number')); ?>:</b>
	<?php echo CHtml::encode($data->trailer_plate_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trailer_footer')); ?>:</b>
	<?php echo CHtml::encode($data->trailer_footer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trailer_axle')); ?>:</b>
	<?php echo CHtml::encode($data->trailer_axle); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trailer_chasis_number')); ?>:</b>
	<?php echo CHtml::encode($data->trailer_chasis_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trailer_chasis_serial_number')); ?>:</b>
	<?php echo CHtml::encode($data->trailer_chasis_serial_number); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('trailer_registered_owner')); ?>:</b>
	<?php echo CHtml::encode($data->trailer_registered_owner); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mv_file_numberc')); ?>:</b>
	<?php echo CHtml::encode($data->mv_file_numberc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cr_number')); ?>:</b>
	<?php echo CHtml::encode($data->cr_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cr_date_issuedc')); ?>:</b>
	<?php echo CHtml::encode($data->cr_date_issuedc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cr_registered_owner')); ?>:</b>
	<?php echo CHtml::encode($data->cr_registered_owner); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('or_number')); ?>:</b>
	<?php echo CHtml::encode($data->or_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('or_date_issued')); ?>:</b>
	<?php echo CHtml::encode($data->or_date_issued); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('or_date_expire')); ?>:</b>
	<?php echo CHtml::encode($data->or_date_expire); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lto_field_office')); ?>:</b>
	<?php echo CHtml::encode($data->lto_field_office); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('truck_id')); ?>:</b>
	<?php echo CHtml::encode($data->truck_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	*/ ?>

</div>