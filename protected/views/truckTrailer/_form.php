
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'truck-trailer-form',
	'enableAjaxValidation'=>false,
)); ?>
<h3>Trailer Details</h3>
  <table width="90%">
    <tr>
      <td><?php echo $form->labelEx($model,'truckerid'); ?></td><td></td><td></td>
    </tr>
  <tr>
    <td><?php
	$asstrucker = CHtml::listData(Students::model()->findAll(),'assoc_truckerid','trucker');
	echo $form->dropDownList($model,'truckerid',$asstrucker,array('empty'=>'Select Trucker'));
	echo $form->error($model,'truckerid'); ?>
    </td>
    <td></td><td></td>
  </tr>
    <tr><td colspan=3>&nbsp;</td></tr>
    <tr>
      <td><?php echo $form->labelEx($model,'trailer_type'); ?></td>
      <td><?php echo $form->labelEx($model,'trailer_plate_number'); ?></td>
      <td><?php echo $form->labelEx($model,'trailer_footer'); ?></td>
    </tr>
    <tr>
     <td><?php
       $trailer = CHtml::listData(TruckerTrailerType::model()->findAll($criteria),'id','name');
       echo $form->dropDownList($model,'trailer_type',$trailer,array('empty'=>''));
       echo $form->error($model,'trailer_type'); ?></td>
     <td><?php echo $form->textField($model,'trailer_plate_number'); ?></td>
     <td><?php
      $trailerfooter = CHtml::listData(TruckTrailerFooter::model()->findAll($criteria),'id','name');
      echo $form->dropDownList($model,'trailer_footer',$trailerfooter,array('empty'=>''));
      echo $form->error($model,'trailer_footer'); ?></td>
    </tr>
    <tr><td colspan=3>&nbsp;</td></tr>
    <tr>
      <td><?php echo $form->labelEx($model,'trailer_axle'); ?></td>
      <td><?php echo $form->labelEx($model,'trailer_chasis_number'); ?></td>
      <td><?php echo $form->labelEx($model,'trailer_chasis_serial_number'); ?></td>
    </tr>
    <tr>
     <td><?php
      $traileraxle = CHtml::listData(TruckTrailerAxle::model()->findAll($criteria),'id','name');
      echo $form->dropDownList($model,'trailer_axle',$traileraxle,array('empty'=>''));
      echo $form->error($model,'trailer_axle'); ?></td>
     <td><?php echo $form->textField($model,'trailer_chasis_number'); ?></td>
     <td><?php echo $form->textField($model,'trailer_chasis_serial_number'); ?></td>
    </tr>
    <tr><td colspan=3>&nbsp;</td></tr>
    <tr>
      <td><?php echo $form->labelEx($model,'trailer_registered_owner'); ?></td>
      <td><?php echo $form->labelEx($model,'mv_file_number'); ?></td>
      <td><?php echo $form->labelEx($model,'lto_field_office'); ?></td>
    </tr>
    <tr>
      <td><?php echo $form->textField($model,'trailer_registered_owner'); ?></td>
      <td><?php echo $form->textField($model,'mv_file_number'); ?></td>
      <td><?php echo $form->textField($model,'lto_field_office'); ?></td>
    </tr>
    <tr><td colspan=3>&nbsp;</td></tr>
    <tr>
      <td><?php echo $form->labelEx($model,'cr_number'); ?></td>
      <td><?php echo $form->labelEx($model,'cr_date_issued'); ?></td>
      <td><?php echo $form->labelEx($model,'cr_registered_owner'); ?></td>
    </tr>
    <tr>
      <td><?php echo $form->textField($model,'cr_number'); ?></td>
      <td><?php
          $date = 'yy-mm-dd';
	  $this->widget('zii.widgets.jui.CJuiDatePicker', array(
		'attribute'=>'cr_date_issued',
		'model'=>$model,
		'options'=>array(
			'showAnim'=>'fold',
			'dateFormat'=>$date,
			'changeMonth'=> true,
			'changeYear'=>true,
			'yearRange'=>'1950:2050'
			),
		'htmlOptions'=>array(
			'style' => 'width:100px;',
			),
		));
	   echo $form->error($model,'cr_date_issued'); ?></td>
      <td><?php echo $form->textField($model,'cr_registered_owner'); ?></td>
    </tr>
    <tr><td colspan=3>&nbsp;</td></tr>
    <tr>
      <td><?php echo $form->labelEx($model,'or_number'); ?></td>
      <td><?php echo $form->labelEx($model,'or_date_issued'); ?></td>
      <td><?php echo $form->labelEx($model,'or_date_expire'); ?></td>
    </tr>
    <tr>
      <td><?php echo $form->textField($model,'or_number'); ?></td>
      <td><?php
          $date = 'yy-mm-dd';
	  $this->widget('zii.widgets.jui.CJuiDatePicker', array(
		'attribute'=>'or_date_issued',
		'model'=>$model,
		'options'=>array(
			'showAnim'=>'fold',
			'dateFormat'=>$date,
			'changeMonth'=> true,
			'changeYear'=>true,
			'yearRange'=>'1950:2050'
			),
		'htmlOptions'=>array(
			'style' => 'width:100px;',
			),
		));
	   echo $form->error($model,'or_date_issued'); ?></td>
      <td><?php
          $date = 'yy-mm-dd';
	  $this->widget('zii.widgets.jui.CJuiDatePicker', array(
		'attribute'=>'or_date_expire',
		'model'=>$model,
		'options'=>array(
			'showAnim'=>'fold',
			'dateFormat'=>$date,
			'changeMonth'=> true,
			'changeYear'=>true,
			'yearRange'=>'1950:2050'
			),
		'htmlOptions'=>array(
			'style' => 'width:100px;',
			),
		));
	   echo $form->error($model,'or_date_expire'); ?></td>
    </tr>
  </table>
  <br>
  <div class="row buttons">
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'formbut')); ?>
  </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
