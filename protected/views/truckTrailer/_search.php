<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trailer_type'); ?>
		<?php echo $form->textField($model,'trailer_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trailer_plate_number'); ?>
		<?php echo $form->textField($model,'trailer_plate_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trailer_footer'); ?>
		<?php echo $form->textField($model,'trailer_footer'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trailer_axle'); ?>
		<?php echo $form->textField($model,'trailer_axle'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trailer_chasis_number'); ?>
		<?php echo $form->textField($model,'trailer_chasis_number',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trailer_chasis_serial_number'); ?>
		<?php echo $form->textField($model,'trailer_chasis_serial_number',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trailer_registered_owner'); ?>
		<?php echo $form->textField($model,'trailer_registered_owner',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mv_file_numberc'); ?>
		<?php echo $form->textField($model,'mv_file_numberc',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cr_number'); ?>
		<?php echo $form->textField($model,'cr_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cr_date_issuedc'); ?>
		<?php echo $form->textField($model,'cr_date_issuedc'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cr_registered_owner'); ?>
		<?php echo $form->textField($model,'cr_registered_owner',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'or_number'); ?>
		<?php echo $form->textField($model,'or_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'or_date_issued'); ?>
		<?php echo $form->textField($model,'or_date_issued'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'or_date_expire'); ?>
		<?php echo $form->textField($model,'or_date_expire'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lto_field_office'); ?>
		<?php echo $form->textField($model,'lto_field_office',array('size'=>60,'maxlength'=>1000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'truck_id'); ?>
		<?php echo $form->textField($model,'truck_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->