<?php
$this->breadcrumbs=array(
	'Truck Trailers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TruckTrailer', 'url'=>array('index')),
	array('label'=>'Create TruckTrailer', 'url'=>array('create')),
	array('label'=>'Update TruckTrailer', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TruckTrailer', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TruckTrailer', 'url'=>array('admin')),
);
?>

<h1>View TruckTrailer #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'trailer_type',
		'trailer_plate_number',
		'trailer_footer',
		'trailer_axle',
		'trailer_chasis_number',
		'trailer_chasis_serial_number',
		'trailer_registered_owner',
		'mv_file_numberc',
		'cr_number',
		'cr_date_issuedc',
		'cr_registered_owner',
		'or_number',
		'or_date_issued',
		'or_date_expire',
		'lto_field_office',
		'truck_id',
		'active',
	),
)); ?>
