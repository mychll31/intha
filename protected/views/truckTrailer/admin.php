<?php
$this->breadcrumbs=array(
	'Truck Trailers'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TruckTrailer', 'url'=>array('index')),
	array('label'=>'Create TruckTrailer', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('truck-trailer-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Truck Trailers</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'truck-trailer-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'trailer_type',
		'trailer_plate_number',
		'trailer_footer',
		'trailer_axle',
		'trailer_chasis_number',
		/*
		'trailer_chasis_serial_number',
		'trailer_registered_owner',
		'mv_file_numberc',
		'cr_number',
		'cr_date_issuedc',
		'cr_registered_owner',
		'or_number',
		'or_date_issued',
		'or_date_expire',
		'lto_field_office',
		'truck_id',
		'active',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
