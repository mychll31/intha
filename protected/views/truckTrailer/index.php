<?php
$this->breadcrumbs=array(
	'Truck Trailers',
);

$this->menu=array(
	array('label'=>'Create TruckTrailer', 'url'=>array('create')),
	array('label'=>'Manage TruckTrailer', 'url'=>array('admin')),
);
?>

<h1>Truck Trailers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
