<?php
$this->breadcrumbs=array(
	'Trailer Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TrailerStatus', 'url'=>array('index')),
	array('label'=>'Manage TrailerStatus', 'url'=>array('admin')),
);
?>

<h1>Create TrailerStatus</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>