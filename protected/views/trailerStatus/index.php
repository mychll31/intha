<?php
$this->breadcrumbs=array(
	'Trailer Statuses',
);

$this->menu=array(
	array('label'=>'Create TrailerStatus', 'url'=>array('create')),
	array('label'=>'Manage TrailerStatus', 'url'=>array('admin')),
);
?>

<h1>Trailer Statuses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
