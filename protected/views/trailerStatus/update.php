<?php
$this->breadcrumbs=array(
	'Trailer Statuses'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TrailerStatus', 'url'=>array('index')),
	array('label'=>'Create TrailerStatus', 'url'=>array('create')),
	array('label'=>'View TrailerStatus', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TrailerStatus', 'url'=>array('admin')),
);
?>

<h1>Update TrailerStatus <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>