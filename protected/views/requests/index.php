<?php
$this->breadcrumbs=array(
	'Requests',
);
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
	
    <?php 
		$this->renderPartial('left_side');
	?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <h1>Requests</h1>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <div class="formCon">
<div class="formConInner">
<div class="form">

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

</div><!-- form -->
</div>
</div>

</div>
</div>
    </td>
  </tr>
</table>