<?php
$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
    <?php 
	if($role->role==1){
		$this->renderPartial('left_side');
	}else{
		$this->renderPartial('left_side2');
	}
	?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <h1><?php echo Yii::t('requestController','Request');?></h1>
    <div class="formCon">
<div class="formConInner">
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'requests-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
<style>td{vertical-align:top;}</style>
	<?php #echo $form->errorSummary($model); ?>
<table width="80%">
	<tr>
	<td>
	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php
				$date = 'yy-mm-dd';
				$this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'attribute'=>'date',
					'model'=>$model,
						'options'=>array(
							'showAnim'=>'fold',
							'dateFormat'=>$date,
							'changeMonth'=> true,
								'changeYear'=>true,
								'yearRange'=>'1950:2050'
						),
					'htmlOptions'=>array(
						'style' => 'width:100px;',
					),
				))
			?>
		<?php echo $form->error($model,'date'); ?>
	</div>
	</td><td>
	<div class="row">
		<?php echo $form->labelEx($model,'trucker_id'); ?>
		<?php
			$truckerid = CHtml::listData(Students::model()->findAll(),'assoc_truckerid','trucker');
			echo $form->dropDownList($model,'trucker_id',$truckerid,array('empty'=>'')); ?>	
		<?php echo $form->error($model,'trucker_id'); ?>
	</div>
	</td>
		<?php $form->labelEx($model,'requestor'); ?>
		<?php $form->textField($model,'requestor',array('value'=>Yii::app()->user->id)); ?>
		<?php $form->error($model,'requestor'); ?>
	<td>
	<div class="row">
		<?php echo $form->labelEx($model,'type_request'); ?>
		<?php
			$criteria_a = new CDbCriteria;
			$criteria_a->compare('active',1);
			$request = CHtml::listData(RequestType::model()->findAll($criteria_a),'id','name');
			echo $form->dropDownList($model,'type_request',$request,array('empty'=>'')); ?>		
		<?php echo $form->error($model,'type_request'); ?>
	</div>
	</td>
	<td colspan=2>
		<?php echo $form->labelEx($model,'notes'); ?>
		<?php echo $form->textArea($model,'notes'); ?>
		<?php echo $form->error($model,'notes'); ?>
	</td>
	</tr>
	<tr><td colspan=2>&nbsp;</td></tr>
	<tr><td colspan=2><style>
		.addbutton{
			background-color: rgb(226, 146, 27) !important;
			color: rgb(97, 80, 12);
			background-color: oldlace;
			padding: 0px 5px;
			border-radius: 30px;
			font-size: 15pt;
			border: 1px solid #857710;
			font-weight: bolder;
		}
		.addbutton:hover{
			background-color: rgb(243, 169, 58) !important;
		}
		</style>
		<div id="file_container">
			<input name="file[]" type="file"  /><br />
		  </div>
		<br><a class="addbutton" href="javascript:void(0);" onClick="add_file_field();">+</a><br />
		</td></tr>
	<tr><td colspan=2>&nbsp;</td></tr>
	<tr><td colspan=2>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create Request' : 'Save',array('class'=>'formbut')); ?>
	</div>
	</td>
	</tr>
</table>
<?php $this->endWidget(); ?>
</div><!-- form -->
</div>
</div>

</div>
</div>
    </td>
  </tr>
</table>

<script>
	function add_file_field(){
		var container=document.getElementById('file_container');
		var file_field=document.createElement('input');
		file_field.name='file[]';
		file_field.type='file';
		container.appendChild(file_field);
		var br_field=document.createElement('br');
		container.appendChild(br_field);
	}
</script>