<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trucker_id'); ?>
		<?php echo $form->textField($model,'trucker_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'requestor'); ?>
		<?php echo $form->textField($model,'requestor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type_request'); ?>
		<?php echo $form->textField($model,'type_request'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->