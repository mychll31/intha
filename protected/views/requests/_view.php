<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trucker_id')); ?>:</b>
	<?php echo CHtml::encode($data->trucker_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('requestor')); ?>:</b>
	<?php echo CHtml::encode($data->requestor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type_request')); ?>:</b>
	<?php echo CHtml::encode($data->type_request); ?>
	<br />


</div>