<?php
$this->breadcrumbs=array(
	'Requests'=>array('index'),
	$model->id,
);

$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
    <?php 
	if($role->role==1){
		$this->renderPartial('left_side');
	}else{
		$this->renderPartial('left_side2');
	}
	?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
	<br>
	    <div class="table_listbx">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr class="listbxtop_hdng"><td><?php echo Yii::t('students','Request Details');?></td></tr>
			<tr><td class="subhdng_nrmal"><span class="datahead">Date :</span><span class="data"><?=date('M. d, Y',strtotime($model->date))?></span></td></tr>
			<tr>
			  <?php $trucker=Students::model()->findByAttributes(array('assoc_truckerid'=>$model->trucker_id)); ?>
			  <td class="subhdng_nrmal"><span class="datahead">Trucker :</span><span class="data"><?=$trucker->trucker?></span></td>
			</tr>
			  <?php $profile=Profiles::model()->findByAttributes(array('user_id'=>$model->requestor)); ?>
			<tr><td class="subhdng_nrmal"><span class="datahead">Requester :</span><span class="data"><?=ucwords($profile->firstname.' '.$profile->lastname)?></span></td></tr>
			  <?php $requestT=RequestType::model()->findByAttributes(array('id'=>$model->type_request)); ?>
			<tr><td class="subhdng_nrmal"><span class="datahead">Request Type :</span><span class="data"><?=$requestT->name?></span></td></tr>
			<tr><td class="subhdng_nrmal"><span class="datahead">Note :</span><span class="data"><?=$model->notes?></span></td></tr>
			<tr><td class="subhdng_nrmal"><span class="datahead">Last Granted Date :</span><span class="data"><?=$model->lastgranteddate?></span></td></tr>
			
			<?php
			$form=$this->beginWidget('CActiveForm', array(
				'id'=>'requests-form',
				'enableAjaxValidation'=>false,
				));
				
				$profile=Profiles::model()->findByAttributes(array('user_id'=>$model->lastgrantedby));
				if($model->lastgrantedby==0 and $role->role==1){
					echo '<tr><td class="subhdng_nrmal" valign="top"><span class="datahead">Remarks :</span><span class="data">';
					echo $form->textArea($model,'remarks').'</span>';
					echo $form->error($model,'remarks');
					echo '<span class="data">'.CHtml::submitButton($model->isNewRecord ? 'Create Request' : 'Grant Request',array('class'=>'formbut')).'</span></td></tr>';
				}else{
					echo '<tr><td class="subhdng_nrmal" valign="top"><span class="datahead">Last Granted Date :</span><span class="data">'.ucwords($profile->firstname.' '.$profile->lastname).'</span></td></tr>';
					echo '<tr><td class="subhdng_nrmal" valign="top"><span class="datahead">Remarks :</span><span class="data">'.ucwords($model->remarks).'</span></td></tr>';
				}
			$this->endWidget(); ?>
		  </table>
		</div>
	  </div>
</div>
</div>
    </td>
  </tr>
</table>
