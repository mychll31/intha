<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_id')); ?>:</b>
	<?php echo CHtml::encode($data->emp_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('co_name')); ?>:</b>
	<?php echo CHtml::encode($data->co_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('co_add')); ?>:</b>
	<?php echo CHtml::encode($data->co_add); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('co_province')); ?>:</b>
	<?php echo CHtml::encode($data->co_province); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('co_city')); ?>:</b>
	<?php echo CHtml::encode($data->co_city); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('co_zip')); ?>:</b>
	<?php echo CHtml::encode($data->co_zip); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('position')); ?>:</b>
	<?php echo CHtml::encode($data->position); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('co_contact')); ?>:</b>
	<?php echo CHtml::encode($data->co_contact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('head')); ?>:</b>
	<?php echo CHtml::encode($data->head); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('in_date')); ?>:</b>
	<?php echo CHtml::encode($data->in_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ex_date')); ?>:</b>
	<?php echo CHtml::encode($data->ex_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reason')); ?>:</b>
	<?php echo CHtml::encode($data->reason); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notes')); ?>:</b>
	<?php echo CHtml::encode($data->notes); ?>
	<br />

	*/ ?>

</div>