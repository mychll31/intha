<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'emp-workexp-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'emp_id'); ?>
		<?php echo $form->textField($model,'emp_id'); ?>
		<?php echo $form->error($model,'emp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'co_name'); ?>
		<?php echo $form->textField($model,'co_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'co_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'co_add'); ?>
		<?php echo $form->textField($model,'co_add',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'co_add'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'co_province'); ?>
		<?php echo $form->textField($model,'co_province',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'co_province'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'co_city'); ?>
		<?php echo $form->textField($model,'co_city',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'co_city'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'co_zip'); ?>
		<?php echo $form->textField($model,'co_zip'); ?>
		<?php echo $form->error($model,'co_zip'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'position'); ?>
		<?php echo $form->textField($model,'position',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'position'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'co_contact'); ?>
		<?php echo $form->textField($model,'co_contact'); ?>
		<?php echo $form->error($model,'co_contact'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'head'); ?>
		<?php echo $form->textField($model,'head',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'head'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'in_date'); ?>
		<?php echo $form->textField($model,'in_date'); ?>
		<?php echo $form->error($model,'in_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ex_date'); ?>
		<?php echo $form->textField($model,'ex_date'); ?>
		<?php echo $form->error($model,'ex_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reason'); ?>
		<?php echo $form->textField($model,'reason',array('size'=>60,'maxlength'=>10000)); ?>
		<?php echo $form->error($model,'reason'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'notes'); ?>
		<?php echo $form->textField($model,'notes',array('size'=>60,'maxlength'=>10000)); ?>
		<?php echo $form->error($model,'notes'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->