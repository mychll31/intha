<?php
$this->breadcrumbs=array(
	'Employees'=>array('/employees/'),
	'Manage Prefixes'=>array('/empWorkexp/'),
);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
    <?php $this->renderPartial('/empWorkexp/profileleft');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <h1><?php echo Yii::t('empWorkexp','Work Experiences');?></h1>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <div class="formCon">
<div class="formConInner">
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'emp-workexp-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php #echo $form->errorSummary($model); ?>
		<?php #echo $form->labelEx($model,'emp_id'); ?>
		<?php echo $form->hiddenField($model,'emp_id',array('value'=>$_REQUEST['id'])); ?>
		<?php echo $form->error($model,'emp_id'); ?>
	
<table width="90%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?php echo $form->labelEx($model,'co_name'); ?></td>
		<td><?php echo $form->labelEx($model,'co_add'); ?></td>
		<td><?php echo $form->labelEx($model,'co_province'); ?></td>
	</tr>
	<tr>
		<td><?php echo $form->textField($model,'co_name',array('size'=>30,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'co_name'); ?></td>
		<td><?php echo $form->textField($model,'co_add',array('size'=>30,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'co_add'); ?></td>
		<td><?php echo $form->textField($model,'co_province',array('size'=>30,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'co_province'); ?></td>
	</tr>
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $form->labelEx($model,'co_city'); ?></td>
		<td><?php echo $form->labelEx($model,'co_zip'); ?></td>
		<td><?php echo $form->labelEx($model,'co_contact'); ?></td>
	</tr>
	<tr>
		<td><?php echo $form->textField($model,'co_city',array('size'=>30,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'co_city'); ?></td>
		<td><?php echo $form->textField($model,'co_zip'); ?>
		<?php echo $form->error($model,'co_zip'); ?></td>
		<td><?php echo $form->textField($model,'co_contact',array('size'=>30,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'co_contact'); ?></td>
	</tr>
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $form->labelEx($model,'position'); ?></td>
		<td><?php echo $form->labelEx($model,'head'); ?></td>
		<td></td>
	</tr>
	<tr>
		<td><?php echo $form->textField($model,'position'); ?>
		<?php echo $form->error($model,'position'); ?></td>
		<td><?php echo $form->textField($model,'head',array('size'=>30,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'head'); ?></td>
		<td></td>
	</tr>
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $form->labelEx($model,'in_date'); ?></td>
		<td><?php echo $form->labelEx($model,'ex_date'); ?></td>
		<td></td>
	</tr>
	<tr>
		<td><?php
				$date = 'yy-mm-dd';
				$this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'attribute'=>'in_date',
					'model'=>$model,
						'options'=>array(
							'showAnim'=>'fold',
							'dateFormat'=>$date,
							'changeMonth'=> true,
								'changeYear'=>true,
								'yearRange'=>'1950:2050'
						),
					'htmlOptions'=>array(
						'style' => 'width:100px;',
					),
				))
			?>
		<?php echo $form->error($model,'in_date'); ?></td>
		<td><?php
				$date = 'yy-mm-dd';
				$this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'attribute'=>'ex_date',
					'model'=>$model,
						'options'=>array(
							'showAnim'=>'fold',
							'dateFormat'=>$date,
							'changeMonth'=> true,
								'changeYear'=>true,
								'yearRange'=>'1950:2050'
						),
					'htmlOptions'=>array(
						'style' => 'width:100px;',
					),
				))
			?>
		<?php echo $form->error($model,'ex_date'); ?></td>
		<td></td>
	</tr>
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $form->labelEx($model,'reason'); ?></td>
		<td><?php echo $form->labelEx($model,'notes'); ?></td>
		<td></td>
	</tr>
	<tr>
		<td><?php echo $form->textArea($model,'reason',array('maxlength'=>10000,'rows'=>5, 'cols'=>20)); ?>
		<?php echo $form->error($model,'reason'); ?></td>
		<td><?php echo $form->textArea($model,'notes',array('maxlength'=>10000,'rows'=>5, 'cols'=>20)); ?>
		<?php echo $form->error($model,'notes'); ?></td>
		<td></td>
	</tr>
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
</table>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update',array('class'=>'formbut')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
</div>

<h3><?php echo Yii::t('empWorkexp','List of Working Experience');?></h3>
<div class="tableinnerlist">
<table width="100%" cellpadding="0" cellspacing="0">
<tr class="pdtab-h">
	<td style="text-align:left;">No.</td>
	<td style="text-align:left;">Company Name</td>
	<td style="text-align:left;">Company Address</td>
	<td style="text-align:left;">Position</td>
	<td style="text-align:left;">Inclusive Date</td>
	<td style="text-align:left;">Exclusive Date</td>
    <td>Actions</td>
</tr>


<?php
$active=EmpWorkexp::model()->findAll("emp_id=:x", array(':x'=>$_REQUEST['id']));
$no=1;
foreach($active as $active_1)
{
   echo '<tr><td style="padding-left:10px; text-align:left;font-weight:normal;">'.$no++.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->co_name.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->co_add.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->position.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->in_date.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->ex_date.'</td>';	
   echo '<td>'.CHtml::link(Yii::t('empWorkexp','Edit'), array('update','eid'=>$active_1->id,'id'=>$active_1->emp_id)).'&nbsp;&nbsp;&nbsp;'.CHtml::link(Yii::t('empWorkexp','View'), array('update','eid'=>$active_1->id,'id'=>$active_1->emp_id)).'</td>';
}
?>
</table>
</div>

</div>

</div>
    </td>
  </tr>
</table>