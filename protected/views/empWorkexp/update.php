<?php
$this->breadcrumbs=array(
	'Emp Workexps'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmpWorkexp', 'url'=>array('index')),
	array('label'=>'Create EmpWorkexp', 'url'=>array('create')),
	array('label'=>'View EmpWorkexp', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmpWorkexp', 'url'=>array('admin')),
);
?>

<h1>Update EmpWorkexp <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>