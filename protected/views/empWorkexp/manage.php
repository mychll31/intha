<?php
$empid=Employees::model()->findByAttributes(array('id'=>$_REQUEST['id']));
$this->breadcrumbs=array(
        'Employee'=>array('/employees'),
        $empid->association_employee_id=>array('/employees/employees/view','id'=>$_REQUEST['id']),
	'Employee Work Experience'
);
$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
    <?php $this->renderPartial('/employeeAddresses/profileleft');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
	<?php if($role->role==1){?>
	<h3><?php echo Yii::t('empWorkexp','Work Experiences');?></h3>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <div class="formCon">
<div class="formConInner">
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'emp-workexp-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php #echo $form->errorSummary($model); ?>
		<?php #echo $form->labelEx($model,'emp_id'); ?>
		<?php echo $form->hiddenField($model,'emp_id',array('value'=>$_REQUEST['id'])); ?>
		<?php echo $form->error($model,'emp_id'); ?>
	
<table width="90%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?php echo $form->labelEx($model,'co_name'); ?></td>
		<td><?php echo $form->labelEx($model,'co_add'); ?></td>
		<td><?php echo $form->labelEx($model,'co_province'); ?></td>
	</tr>
	<tr>
		<td><?php echo $form->textField($model,'co_name',array('maxlength'=>255)); ?>
		<?php echo $form->error($model,'co_name'); ?></td>
		<td><?php echo $form->textField($model,'co_add',array('maxlength'=>255)); ?>
		<?php echo $form->error($model,'co_add'); ?></td>
		<td><?php echo $form->textField($model,'co_province',array('maxlength'=>255)); ?>
		<?php echo $form->error($model,'co_province'); ?></td>
	</tr>
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $form->labelEx($model,'co_city'); ?></td>
		<td><?php echo $form->labelEx($model,'co_zip'); ?></td>
		<td><?php echo $form->labelEx($model,'co_contact'); ?></td>
	</tr>
	<tr>
		<td><?php echo $form->textField($model,'co_city',array('maxlength'=>255)); ?>
		<?php echo $form->error($model,'co_city'); ?></td>
		<td><?php echo $form->textField($model,'co_zip'); ?>
		<?php echo $form->error($model,'co_zip'); ?></td>
		<td><?php echo $form->textField($model,'co_contact',array('maxlength'=>255)); ?>
		<?php echo $form->error($model,'co_contact'); ?></td>
	</tr>
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $form->labelEx($model,'position'); ?></td>
		<td><?php echo $form->labelEx($model,'head'); ?></td>
		<td></td>
	</tr>
	<tr>
		<td><?php echo $form->textField($model,'position'); ?>
		<?php echo $form->error($model,'position'); ?></td>
		<td><?php echo $form->textField($model,'head',array('maxlength'=>255)); ?>
		<?php echo $form->error($model,'head'); ?></td>
		<td class="cr_align" style="vertical-align:middle !important;">
		<?php echo $form->radioButtonList($model,'current',array('1'=>'Current','0'=>'Previous'),array('separator'=>' ')); ?>
		<?php echo $form->error($model,'current'); ?></td>
	</tr>
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $form->labelEx($model,'in_date'); ?></td>
		<td><?php echo $form->labelEx($model,'ex_date'); ?></td>
		<td></td>
	</tr>
	<tr>
		<td><?php
				$date = 'yy-mm-dd';
				$this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'attribute'=>'in_date',
					'model'=>$model,
						'options'=>array(
							'showAnim'=>'fold',
							'dateFormat'=>$date,
							'changeMonth'=> true,
								'changeYear'=>true,
								'yearRange'=>'1950:2050'
						),
					'htmlOptions'=>array(
						'style' => 'width:100px;',
					),
				))
			?>
		<?php echo $form->error($model,'in_date'); ?></td>
		<td><?php
				$date = 'yy-mm-dd';
				$this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'attribute'=>'ex_date',
					'model'=>$model,
						'options'=>array(
							'showAnim'=>'fold',
							'dateFormat'=>$date,
							'changeMonth'=> true,
								'changeYear'=>true,
								'yearRange'=>'1950:2050'
						),
					'htmlOptions'=>array(
						'style' => 'width:100px;',
					),
				))
			?>
		<?php echo $form->error($model,'ex_date'); ?></td>
		<td></td>
	</tr>
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $form->labelEx($model,'reason'); ?></td>
		<td><?php echo $form->labelEx($model,'notes'); ?></td>
		<td></td>
	</tr>
	<tr>
		<td><?php echo $form->textArea($model,'reason',array('maxlength'=>10000,'rows'=>5, 'cols'=>20)); ?>
		<?php echo $form->error($model,'reason'); ?></td>
		<td><?php echo $form->textArea($model,'notes',array('maxlength'=>10000,'rows'=>5, 'cols'=>20)); ?>
		<?php echo $form->error($model,'notes'); ?></td>
		<td></td>
	</tr>
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
</table>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update',array('class'=>'formbut')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
</div>
<?php }
$criteria = new CDbCriteria;
$criteria->condition='emp_id=:x';
$criteria->params = array(':x'=>$_REQUEST['id']);
$criteria->order = 'ex_date DESC';
$active=EmpWorkexp::model()->findAll($criteria);
$no=1;
if($active!=null){
?>
<h3><?php echo Yii::t('empWorkexp','List of Working Experience');?></h3>
<div class="tableinnerlist">
<table width="100%" cellpadding="0" cellspacing="0">
<tr class="pdtab-h">
	<td style="text-align:left;">No.</td>
	<td style="text-align:left;">Company Name</td>
	<td style="text-align:left;">Company Address</td>
	<td style="text-align:left;">Position</td>
	<td style="text-align:left;">Inclusive Date</td>
	<td style="text-align:left;">Exclusive Date</td>
	<?php if($role->role==1){?>
    <td>Actions</td>
	<?php }?>
</tr>
<?php
}
foreach($active as $active_1)
{
if($active_1->current == 1){
 echo '<tbody style="background-color:rgba(255, 192, 203, 0.33);">';
}else{
 echo '<tbody>';
}
   echo '<tr><td style="padding-left:10px; text-align:left;font-weight:normal;">'.$no++.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->co_name.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">';
   if($active_1->co_city!=null){echo $active_1->co_city.', ';}else{echo '';}
   if($active_1->co_province!=null){echo $active_1->co_province;}else{echo '';}
   echo '</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->position.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->in_date.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->ex_date.'</td>';	
   if($role->role==1){
   echo '<td>'.CHtml::link(Yii::t('empWorkexp','Edit'), array('update','eid'=>$active_1->id,'id'=>$active_1->emp_id)).'</td>';
   }
}
?>
</tbody>
</table>
</div>

</div>

</div>
    </td>
  </tr>
</table>
