<?php
$this->breadcrumbs=array(
	'Emp Workexps'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List EmpWorkexp', 'url'=>array('index')),
	array('label'=>'Create EmpWorkexp', 'url'=>array('create')),
	array('label'=>'Update EmpWorkexp', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmpWorkexp', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmpWorkexp', 'url'=>array('admin')),
);
?>

<h1>View EmpWorkexp #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'emp_id',
		'co_name',
		'co_add',
		'co_province',
		'co_city',
		'co_zip',
		'position',
		'co_contact',
		'head',
		'in_date',
		'ex_date',
		'reason',
		'notes',
	),
)); ?>
