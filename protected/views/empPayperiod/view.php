<?php
$this->breadcrumbs=array(
	'Emp Payperiods'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List EmpPayperiod', 'url'=>array('index')),
	array('label'=>'Create EmpPayperiod', 'url'=>array('create')),
	array('label'=>'Update EmpPayperiod', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmpPayperiod', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmpPayperiod', 'url'=>array('admin')),
);
?>

<h1>View EmpPayperiod #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
