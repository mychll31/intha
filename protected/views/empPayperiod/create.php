<?php
$this->breadcrumbs=array(
	'Emp Payperiods'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmpPayperiod', 'url'=>array('index')),
	array('label'=>'Manage EmpPayperiod', 'url'=>array('admin')),
);
?>

<h1>Create EmpPayperiod</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>