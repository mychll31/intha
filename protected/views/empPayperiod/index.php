<?php
$this->breadcrumbs=array(
	'Emp Payperiods',
);

$this->menu=array(
	array('label'=>'Create EmpPayperiod', 'url'=>array('create')),
	array('label'=>'Manage EmpPayperiod', 'url'=>array('admin')),
);
?>

<h1>Emp Payperiods</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
