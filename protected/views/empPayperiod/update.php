<?php
$this->breadcrumbs=array(
	'Emp Payperiods'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmpPayperiod', 'url'=>array('index')),
	array('label'=>'Create EmpPayperiod', 'url'=>array('create')),
	array('label'=>'View EmpPayperiod', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmpPayperiod', 'url'=>array('admin')),
);
?>

<h1>Update EmpPayperiod <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>