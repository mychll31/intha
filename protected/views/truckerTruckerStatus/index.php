<?php
$this->breadcrumbs=array(
	'Trucker Trucker Statuses',
);

$this->menu=array(
	array('label'=>'Create TruckerTruckerStatus', 'url'=>array('create')),
	array('label'=>'Manage TruckerTruckerStatus', 'url'=>array('admin')),
);
?>

<h1>Trucker Trucker Statuses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
