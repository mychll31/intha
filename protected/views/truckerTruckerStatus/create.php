<?php
$this->breadcrumbs=array(
	'Trucker Trucker Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TruckerTruckerStatus', 'url'=>array('index')),
	array('label'=>'Manage TruckerTruckerStatus', 'url'=>array('admin')),
);
?>

<h1>Create TruckerTruckerStatus</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>