<?php
$this->breadcrumbs=array(
	'Trucker Trucker Statuses'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TruckerTruckerStatus', 'url'=>array('index')),
	array('label'=>'Create TruckerTruckerStatus', 'url'=>array('create')),
	array('label'=>'View TruckerTruckerStatus', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TruckerTruckerStatus', 'url'=>array('admin')),
);
?>

<h1>Update TruckerTruckerStatus <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>