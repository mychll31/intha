<?php
$empid=Employees::model()->findByAttributes(array('id'=>$_REQUEST['id']));
$this->breadcrumbs=array(
	'Employee'=>array('/employees'),
	$empid->association_employee_id=>array('/employees/employees/view','id'=>$_REQUEST['id']),
	'Employee Notes',
);
$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
    <?php $this->renderPartial('/employeeAddresses/profileleft');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
	<?php $pref = EmpPrefix::model()->findByAttributes(array('id'=>$empid->prefix)); ?>
	<h1 ><?php echo Yii::t('employees','');?>
	<?php 
		if($pref!=NULL){$pname= $pref->name.'. ';}else{$pname='';}
		if($empid->lastname !=null){$lname =$empid->lastname.', ';}else{$lname='';}
		if($empid->firstname !=null){$fname =$empid->firstname.' ';}else{$fname='';}
		if($empid->middlename !=null){$mname =$empid->middlename.' ';}else{$mname='';}
		echo ucwords($pname.$lname.$fname.$mname);
	?>
	</h1>
	<?php if($role->role==1){?>
    <h3><?php echo Yii::t('employeeNotes','Notes');?></h3>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <div class="formCon">
<div class="formConInner">
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'emp-workexp-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php #echo $form->errorSummary($model); ?>
		<?php #echo $form->labelEx($model,'emp_id'); ?>
		<?php echo $form->hiddenField($model,'emp_id',array('value'=>$_REQUEST['id'])); ?>
		<?php echo $form->error($model,'emp_id'); ?>
	
<table width="90%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?php echo $form->labelEx($model,'type'); ?></td>
		<td><?php echo $form->labelEx($model,'date_ofnote'); ?></td>
		<td><?php echo $form->labelEx($model,'reportedby'); ?></td>
	</tr>
	<tr>
		<td><?php $notetyp = CHtml::listData(TruckerTypeNote::model()->findAll("active=:x", array(':x'=>1)),'id','name');
			echo $form->dropDownList($model,'type',$notetyp,array('empty'=>''));?>
		<?php echo $form->error($model,'type'); ?></td>
		<td><?php
				$date = 'yy-mm-dd';
				$this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'attribute'=>'date_ofnote',
					'model'=>$model,
						'options'=>array(
							'showAnim'=>'fold',
							'dateFormat'=>$date,
							'changeMonth'=> true,
								'changeYear'=>true,
								'yearRange'=>'1950:2050'
						),
					'htmlOptions'=>array(
						'style' => 'width:100px;',
					),
				))
			?>
		<?php echo $form->error($model,'date_ofnote'); ?></td>
		<td><?php echo $form->textField($model,'reportedby',array('size'=>30,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'reportedby'); ?></td>
	</tr>
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $form->labelEx($model,'details'); ?></td>
		<td valign="top"><?php echo $form->labelEx($model,'datereportedby'); ?></td>
		<td></td>
	</tr>
	<tr>
		<td><?php echo $form->textArea($model,'details',array('maxlength'=>10000,'rows'=>5, 'cols'=>20)); ?>
		<?php echo $form->error($model,'details'); ?></td>
		<td valign="top"><?php
				$date = 'yy-mm-dd';
				$this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'attribute'=>'datereportedby',
					'model'=>$model,
						'options'=>array(
							'showAnim'=>'fold',
							'dateFormat'=>$date,
							'changeMonth'=> true,
								'changeYear'=>true,
								'yearRange'=>'1950:2050'
						),
					'htmlOptions'=>array(
						'style' => 'width:100px;',
					),
				))
			?>
		<?php echo $form->error($model,'datereportedby'); ?></td>
		<td></td>
	</tr>
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
</table>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update',array('class'=>'formbut')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
</div>
<?php }
$criteria = new CDbCriteria;
$criteria->condition='emp_id=:x';
$criteria->params = array(':x'=>$_REQUEST['id']);
$criteria->order = 'date_ofnote DESC';

$active=EmployeeNotes::model()->findAll($criteria);
$no=1;
if($active!=null){
?>
<h3><?php echo Yii::t('employeeNotes','List of Notes');?></h3>
<div class="tableinnerlist">
<table width="100%" cellpadding="0" cellspacing="0">
<tr class="pdtab-h">
	<td style="text-align:left;">No.</td>
	<td style="text-align:left;">Type of Note</td>
	<td style="text-align:left;">Date of Note</td>
	<td style="text-align:left;">Reported By</td>
	<td style="text-align:left;">Date Reported By</td>
    <td>Actions</td>
</tr>
<?php
}
foreach($active as $active_1)
{
   $typeofnote = TruckerTypeNote::model()->findByAttributes(array('id'=>$active_1->type));
   echo '<tr><td style="padding-left:10px; text-align:left;font-weight:normal;">'.$no++.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$typeofnote->name.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->date_ofnote.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->reportedby.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->datereportedby.'</td>';
   if($role->role==1){
   echo '<td>'.CHtml::link(Yii::t('employeeNotes','Edit'), array('update','id'=>$active_1->emp_id,'eid'=>$active_1->id)).'&nbsp;&nbsp;&nbsp;'.CHtml::link(Yii::t('employeeNotes','View'), array('view','id'=>$active_1->emp_id,'eid'=>$active_1->id));
   echo '</td>';
   }else{
   echo '<td>'.CHtml::link(Yii::t('employeeNotes','View'), array('view','id'=>$active_1->emp_id,'eid'=>$active_1->id));
   echo '</td>';}
}
?> 

</table>
</div>
</div>
</div>
    </td>
  </tr>
</table>
<?php
/*
$this->beginWidget('zii.widgets.jui.CJuiDialog',
			array(
			'id'=>'mymodal',
			'options'=>array(
				'title'=>'errr',
				'autoOpen'=>'false',
				'modal'=>true,
				'buttons'=>array('OK'=>'js:function(){$(this).dialog("close")}')
			))
   );
   echo 'i am a modal';
   $this->endWidget('zii.widgets.jui.CJuiDialog');

   */
?>
