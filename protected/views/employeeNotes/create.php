<?php
$this->breadcrumbs=array(
	'Employee Notes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmployeeNotes', 'url'=>array('index')),
	array('label'=>'Manage EmployeeNotes', 'url'=>array('admin')),
);
?>

<h1>Create EmployeeNotes</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>