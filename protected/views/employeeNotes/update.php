<?php
$this->breadcrumbs=array(
	'Employee Notes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmployeeNotes', 'url'=>array('index')),
	array('label'=>'Create EmployeeNotes', 'url'=>array('create')),
	array('label'=>'View EmployeeNotes', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmployeeNotes', 'url'=>array('admin')),
);
?>

<h1>Update EmployeeNotes <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>