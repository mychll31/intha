<?php
$this->breadcrumbs=array(
	'Employee'=>array('/employees'),
	$_REQUEST['id']=>array('employees/employees/view&id='.$_REQUEST['id']),
	'Employee Notes'=>array('employeeNotes/index&id='.$_REQUEST['id']),
);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
    <?php $this->renderPartial('/employeeNotes/profileleft');?>
    
    </td>
    <td valign="top">
    
	<div class="cont_right formWrapper">
    
	<div class="edit_bttns">
    <ul>
    <li><?php echo CHtml::link(Yii::t('employeeNotes','<span>Back to Notes</span>'), array('index', 'id'=>$_REQUEST['id']),array('class'=>'edit last')); ?></li>
    </ul>
    </div>
	
	<h1><?php echo Yii::t('employeeNotes','Notes');?></h1>
    <div class="formCon">
<div class="formConInner">
<div class="form">
<table width="70%" clas='geof'>
	<tr>
		<td><b>TYPE OF NOTE: </b></td><td style="border-bottom:1px solid #ccc;">
		<?php
		$notetype = TruckerTypeNote::model()->findByAttributes(array('id'=>$model->type));
		echo $notetype->name;
		?></td>
		<td><b>DATE OF NOTE: </b></td><td style="border-bottom:1px solid #ccc;"><?php echo date('M d,Y',strtotime($model->date_ofnote));?></td>
	</tr>
	<tr><td colspan=4>&nbsp;</td></tr>
	<tr>
		<td><b>REPORTED BY:</b></td><td style="border-bottom:1px solid #ccc;"><?php echo $model->reportedby;?></td>
		<td><b>DATE REPORTED BY:</b></td><td style="border-bottom:1px solid #ccc;"><?php echo date('M d,Y',strtotime($model->datereportedby));?></td>
	</tr>
	<tr><td colspan=4>&nbsp;</td></tr>
	<tr>
		<td colspan=4><b>DETAILS</b></td>
	</tr>
	<tr><td colspan=4 style="background-color:#fff;"><br><?php echo $model->details;?></td></tr>
	<tr>
		<td colspan=4></td>
	</tr>
</table>
</div><!-- form -->
</div>
</div>