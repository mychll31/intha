<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_id')); ?>:</b>
	<?php echo CHtml::encode($data->emp_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('details')); ?>:</b>
	<?php echo CHtml::encode($data->details); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_ofnote')); ?>:</b>
	<?php echo CHtml::encode($data->date_ofnote); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reportedby')); ?>:</b>
	<?php echo CHtml::encode($data->reportedby); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datereportedby')); ?>:</b>
	<?php echo CHtml::encode($data->datereportedby); ?>
	<br />


</div>