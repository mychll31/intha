<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employee-notes-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'emp_id'); ?>
		<?php echo $form->textField($model,'emp_id'); ?>
		<?php echo $form->error($model,'emp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->textField($model,'type'); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'details'); ?>
		<?php echo $form->textField($model,'details',array('size'=>60,'maxlength'=>10000)); ?>
		<?php echo $form->error($model,'details'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_ofnote'); ?>
		<?php echo $form->textField($model,'date_ofnote'); ?>
		<?php echo $form->error($model,'date_ofnote'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reportedby'); ?>
		<?php echo $form->textField($model,'reportedby',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'reportedby'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datereportedby'); ?>
		<?php echo $form->textField($model,'datereportedby'); ?>
		<?php echo $form->error($model,'datereportedby'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->