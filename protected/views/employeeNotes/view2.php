<?php
$this->breadcrumbs=array(
	'Employee Notes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List EmployeeNotes', 'url'=>array('index')),
	array('label'=>'Create EmployeeNotes', 'url'=>array('create')),
	array('label'=>'Update EmployeeNotes', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmployeeNotes', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmployeeNotes', 'url'=>array('admin')),
);
?>

<h1>View EmployeeNotes #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'emp_id',
		'type',
		'details',
		'date_ofnote',
		'reportedby',
		'datereportedby',
	),
)); ?>
