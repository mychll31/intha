<?php
$this->breadcrumbs=array(
	'Employee Notes',
);

$this->menu=array(
	array('label'=>'Create EmployeeNotes', 'url'=>array('create')),
	array('label'=>'Manage EmployeeNotes', 'url'=>array('admin')),
);
?>

<h1>Employee Notes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
