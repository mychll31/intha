<?php
$this->breadcrumbs=array(
	'Truck Trailer Axles'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TruckTrailerAxle', 'url'=>array('index')),
	array('label'=>'Create TruckTrailerAxle', 'url'=>array('create')),
	array('label'=>'Update TruckTrailerAxle', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TruckTrailerAxle', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TruckTrailerAxle', 'url'=>array('admin')),
);
?>

<h1>View TruckTrailerAxle #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
