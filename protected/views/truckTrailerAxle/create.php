<?php
$this->breadcrumbs=array(
	'Truck Trailer Axles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TruckTrailerAxle', 'url'=>array('index')),
	array('label'=>'Manage TruckTrailerAxle', 'url'=>array('admin')),
);
?>

<h1>Create TruckTrailerAxle</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>