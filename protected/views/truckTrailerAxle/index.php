<?php
$this->breadcrumbs=array(
	'Truck Trailer Axles',
);

$this->menu=array(
	array('label'=>'Create TruckTrailerAxle', 'url'=>array('create')),
	array('label'=>'Manage TruckTrailerAxle', 'url'=>array('admin')),
);
?>

<h1>Truck Trailer Axles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
