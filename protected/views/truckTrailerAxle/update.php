<?php
$this->breadcrumbs=array(
	'Truck Trailer Axles'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TruckTrailerAxle', 'url'=>array('index')),
	array('label'=>'Create TruckTrailerAxle', 'url'=>array('create')),
	array('label'=>'View TruckTrailerAxle', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TruckTrailerAxle', 'url'=>array('admin')),
);
?>

<h1>Update TruckTrailerAxle <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>