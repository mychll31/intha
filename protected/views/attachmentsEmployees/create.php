<?php
$this->breadcrumbs=array(
	'Attachments Employees'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AttachmentsEmployees', 'url'=>array('index')),
	array('label'=>'Manage AttachmentsEmployees', 'url'=>array('admin')),
);
?>

<h1>Create AttachmentsEmployees</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>