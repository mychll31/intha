<?php
$this->breadcrumbs=array(
	'Attachments Employees'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AttachmentsEmployees', 'url'=>array('index')),
	array('label'=>'Create AttachmentsEmployees', 'url'=>array('create')),
	array('label'=>'Update AttachmentsEmployees', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AttachmentsEmployees', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AttachmentsEmployees', 'url'=>array('admin')),
);
?>

<h1>View AttachmentsEmployees #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'truckers_id',
		'filename',
		'fileext',
		'fileloc',
		'modified_date',
		'deleted',
	),
)); ?>
