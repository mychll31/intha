<?php
$this->breadcrumbs=array(
	'Attachments Employees',
);

$this->menu=array(
	array('label'=>'Create AttachmentsEmployees', 'url'=>array('create')),
	array('label'=>'Manage AttachmentsEmployees', 'url'=>array('admin')),
);
?>

<h1>Attachments Employees</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
