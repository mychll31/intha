<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'attachments-employees-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'truckers_id'); ?>
		<?php echo $form->textField($model,'truckers_id'); ?>
		<?php echo $form->error($model,'truckers_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'filename'); ?>
		<?php echo $form->textField($model,'filename',array('size'=>60,'maxlength'=>225)); ?>
		<?php echo $form->error($model,'filename'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fileext'); ?>
		<?php echo $form->textField($model,'fileext',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'fileext'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fileloc'); ?>
		<?php echo $form->textField($model,'fileloc',array('size'=>60,'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'fileloc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'modified_date'); ?>
		<?php echo $form->textField($model,'modified_date'); ?>
		<?php echo $form->error($model,'modified_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deleted'); ?>
		<?php echo $form->textField($model,'deleted'); ?>
		<?php echo $form->error($model,'deleted'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->