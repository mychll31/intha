<?php
$this->breadcrumbs=array(
	'Attachments Employees'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AttachmentsEmployees', 'url'=>array('index')),
	array('label'=>'Create AttachmentsEmployees', 'url'=>array('create')),
	array('label'=>'View AttachmentsEmployees', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AttachmentsEmployees', 'url'=>array('admin')),
);
?>

<h1>Update AttachmentsEmployees <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>