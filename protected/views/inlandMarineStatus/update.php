<?php
$this->breadcrumbs=array(
	'Inland Marine Statuses'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List InlandMarineStatus', 'url'=>array('index')),
	array('label'=>'Create InlandMarineStatus', 'url'=>array('create')),
	array('label'=>'View InlandMarineStatus', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage InlandMarineStatus', 'url'=>array('admin')),
);
?>

<h1>Update InlandMarineStatus <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>