<?php
$this->breadcrumbs=array(
	'Inland Marine Statuses',
);

$this->menu=array(
	array('label'=>'Create InlandMarineStatus', 'url'=>array('create')),
	array('label'=>'Manage InlandMarineStatus', 'url'=>array('admin')),
);
?>

<h1>Inland Marine Statuses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
