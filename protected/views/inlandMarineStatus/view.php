<?php
$this->breadcrumbs=array(
	'Inland Marine Statuses'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List InlandMarineStatus', 'url'=>array('index')),
	array('label'=>'Create InlandMarineStatus', 'url'=>array('create')),
	array('label'=>'Update InlandMarineStatus', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete InlandMarineStatus', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InlandMarineStatus', 'url'=>array('admin')),
);
?>

<h1>View InlandMarineStatus #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
