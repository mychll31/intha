<?php
$this->breadcrumbs=array(
	'Inland Marine Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List InlandMarineStatus', 'url'=>array('index')),
	array('label'=>'Manage InlandMarineStatus', 'url'=>array('admin')),
);
?>

<h1>Create InlandMarineStatus</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>