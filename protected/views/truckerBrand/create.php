<?php
$this->breadcrumbs=array(
	'Trucker Brands'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TruckerBrand', 'url'=>array('index')),
	array('label'=>'Manage TruckerBrand', 'url'=>array('admin')),
);
?>

<h1>Create TruckerBrand</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>