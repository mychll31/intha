<?php
$this->breadcrumbs=array(
	'Trucker Brands'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TruckerBrand', 'url'=>array('index')),
	array('label'=>'Create TruckerBrand', 'url'=>array('create')),
	array('label'=>'Update TruckerBrand', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TruckerBrand', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TruckerBrand', 'url'=>array('admin')),
);
?>

<h1>View TruckerBrand #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
