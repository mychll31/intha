<?php
$this->breadcrumbs=array(
	'Trucker Brands',
);

$this->menu=array(
	array('label'=>'Create TruckerBrand', 'url'=>array('create')),
	array('label'=>'Manage TruckerBrand', 'url'=>array('admin')),
);
?>

<h1>Trucker Brands</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
