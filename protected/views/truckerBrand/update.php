<?php
$this->breadcrumbs=array(
	'Trucker Brands'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TruckerBrand', 'url'=>array('index')),
	array('label'=>'Create TruckerBrand', 'url'=>array('create')),
	array('label'=>'View TruckerBrand', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TruckerBrand', 'url'=>array('admin')),
);
?>

<h1>Update TruckerBrand <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>