<?php
$this->breadcrumbs=array(
	'Emp Licenses',
);

$this->menu=array(
	array('label'=>'Create EmpLicense', 'url'=>array('create')),
	array('label'=>'Manage EmpLicense', 'url'=>array('admin')),
);
?>

<h1>Emp Licenses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
