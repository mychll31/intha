<?php $role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); ?>
<div class="pdtab_Con" style="width:97%">
	<?php
		$criteria = new CDbCriteria;
		$criteria->order = 'expiry_date DESC';
		$criteria->condition='emp_no = :match';
		$criteria->params = array(':match' => $_REQUEST['id']);
		$license = EmpLicense::model()->findAll($criteria);			
		$ctr=0;
		if($license!=null){
	?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <tr class="pdtab-h">
                <td align="center" height="18">No.</td>
                <td align="center">License Type</td>
                <td align="center">License Number</td>
                <td align="center">Issuance Date</td>
                <td align="center">Expiry Date</td>          
		<?php if($role->role==1){?>
                <td align="center">Action</td>          
		<?php }?>
			</tr>
        </tbody>
		<?php
			}
			else{
				echo "<hr><center><h1>NOTHING FOUND!!</h1></center>";
			}
			foreach ($license as $lic){
			$ctr++;
		?>
		<tbody>
			<tr>
			<td align="center"><?=$ctr?></td>
			<?php $lice_type = EmpLicenseType::model()->findByAttributes(array('id'=>$lic->license_type)); ?>
			<td align="center"><?php echo $lice_type->name; ?></td>
			<td align="center"><?php echo $lic->license_no; ?></td>
			<td align="center"><?php echo $lic->issuance_date; ?></td>
			<td align="center"><?php echo $lic->expiry_date; ?></td>
			<?php $lice_type = EmpLicenseType::model()->findByAttributes(array('id'=>$lic->license_type)); ?>
			<?php if($role->role==1){?>
				<td align="center"><?php echo CHtml::link(Yii::t('empLicense','Update'), array('update', 'licid'=>$lic->id,'id'=>$_REQUEST['id'])); ?></td>
			<?php }?>
			</tr>
        </tbody>
		<?php }?>
	</table>
</div>
