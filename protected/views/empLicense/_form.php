<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'emp-license-form',
	'enableAjaxValidation'=>false,
)); ?>
<br>
	<?php echo $form->errorSummary($model); ?>
	<?php $date = 'yy-mm-dd';?>
<div class="formCon" >
<?php echo $form->hiddenField($model,'emp_no',array('value'=>$_REQUEST['id']));?>
	<div class="formConInner">
	<h3>License Details</h3>
	<p class="note">Fields with <span class="required">*</span> are required.</p><br>
	<table width="85%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
		<td><?php echo $form->labelEx($model,Yii::t('empLicense','license_type')); ?></td>
		<td><?php echo $form->labelEx($model,Yii::t('empLicense','license_no')); ?></td>
		<td><?php echo $form->labelEx($model,Yii::t('empLicense','issuance_date')); ?></td>
		<td><?php echo $form->labelEx($model,Yii::t('empLicense','expiry_date')); ?></td>
	  </tr>
	  <tr>
	  <tr>
		<td><?php
			$lictype = CHtml::listData(EmpLicenseType::model()->findAll(),'id','name');
			echo $form->dropDownList($model,'license_type',$lictype,array('empty'=>'License Type')); ?>
			<?php echo $form->error($model,'license_type'); ?></td>
		<td><?php echo $form->textField($model,'license_no',array('size'=>30,'maxlength'=>255)); ?>
			<?php echo $form->error($model,'license_no'); ?></td>
		<td>
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'attribute'=>'issuance_date',
				'model'=>$model,
				'options'=>array(
					'showAnim'=>'fold',
					'dateFormat'=>$date,
					'changeMonth'=> true,
					'changeYear'=>true,
					'yearRange'=>'1950:2050'
				),
				'htmlOptions'=>array(
					'style' => 'width:100px;',
					),
				))
		?>
		<?php echo $form->error($model,'issuance_date'); ?>
		</td>
		<td>
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'attribute'=>'expiry_date',
				'model'=>$model,
				'options'=>array(
					'showAnim'=>'fold',
					'dateFormat'=>$date,
					'changeMonth'=> true,
					'changeYear'=>true,
					'yearRange'=>'1950:2050'
				),
				'htmlOptions'=>array(
					'style' => 'width:100px;',
					),
				))
		?>
		<?php echo $form->error($model,'expiry_date'); ?>
		</td>
	  </tr>
	</table>
	</div>
</div>
	<div style="padding:0px 0 0 0px; text-align:left">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Add License' : 'Update License',array('class'=>'formbut')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->