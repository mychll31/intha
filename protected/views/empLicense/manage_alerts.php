<?php
$this->breadcrumbs=array(
	'Employee'=>array('/employees'),
	$_REQUEST['id']=>array('employees/employees/view&id='.$_REQUEST['id']),
	'Employee License'=>array('employeeAddresses/view&id='.$_REQUEST['id']),
);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
      <?php $this->renderPartial('/empLicense/profileleft1');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
<?php echo $this->renderPartial('_managealerts', array('model'=>$model,
		'list'=>$list,
		'pages' => $pages,
		'item_count'=>$item_count,
		'page_size'=>Yii::app()->params['listPerPage'],)); ?>
</div>
    </td>
  </tr>
</table>