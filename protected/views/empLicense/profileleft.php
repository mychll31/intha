 <div class="emp_cont_left">
    <div class="empleftbx">
<div class="empimgbx" style="height:128px;">
    <ul>
    <li>
   <?php
   $employee=Employees::model()->findByAttributes(array('id'=>$_REQUEST['id']));
	 if($employee->photo_file_name){ 
	 echo '<img  src="'.$this->createUrl('employees/employees/DisplaySavedImage&id='.$employee->primaryKey).'" alt="'.$employee->photo_file_name.'" width="170" height="140" />';
   // echo '<img class="imgbrder" src="'.$this->createUrl('DisplaySavedImage&id='.$employee->primaryKey).'" alt="'.$employee->photo_file_name.'" width="170" height="140" />';
	 }else{
		echo '<img class="imgbrder" src="images/super_avatar.png" alt='.$employee->firstname.' width="170" height="140" />'; 
	 }
	 ?>
     </li>
    <li class="img_text">
    	<div style="line-height:9px; margin:20px 0px 5px 0px; font-size:14px;line-height: 22px;"><?php echo CHtml::link(ucfirst($employee->firstname).'&nbsp;'.ucfirst($employee->lastname), array('/employees/employees/view','id'=>$_REQUEST['id']));?></div>
        <a style="font-size:12px; color:#C30; padding-top:6px; display:block" href="#"><?php echo $employee->other_email; ?></a>
    </li>
    </ul>
    </div>
    <div class="clear"></div>
	<br><br>
    <div id="othleft-sidebar">
        <h1><?php echo Yii::t('employees','Manage Details');?></h1>   
            <?php

			$this->widget('zii.widgets.CMenu',array(
			'encodeLabel'=>false,
			'activateItems'=>true,
			'activeCssClass'=>'list_active',
			'items'=>array(
					array('label'=>''.Yii::t('employees','Manage Address').'<span>'.Yii::t('employees','Employee Address Details').'</span>', 'url'=>array('/employeeAddresses/view','id'=>ucfirst($employee->association_employee_id)) ,'linkOptions'=>array('class'=>'lbook_ico'),
                        'active'=> (Yii::app()->controller->id=='employeeAddresses')
					    ),                  
					array('label'=>''.Yii::t('employees','Manage License').'<span>'.Yii::t('employees','Employee License Details').'</span>',  'url'=>array('/empLicense/view','id'=>ucfirst($employee->association_employee_id)),'linkOptions'=>array('class'=>'sl_ico' ),'active'=> (Yii::app()->controller->id=='empLicense'), 'itemOptions'=>array('id'=>'menu_1') 
					       ),
						   
					array('label'=>''.'<h1>'.Yii::t('employees','Work Experience').'</h1>'), 
						array('label'=>Yii::t('employees','Add Work Experience').'<span>'.Yii::t('employees','').'</span>', 'url'=>array('/empWorkexp/index&id='.$_REQUEST['id']),'linkOptions'=>array('class'=>'abook_ico'),'active'=> (Yii::app()->controller->id=='empWorkexp')),
						array('label'=>Yii::t('employees','Notes').'<span>'.Yii::t('employees','').'</span>', 'url'=>array('/employeeNotes/index&id='.$_REQUEST['id']),'linkOptions'=>array('class'=>'abook_ico'),'active'=> (Yii::app()->controller->id=='employeeNotes')),
						array('label'=>Yii::t('employees','Disciplinary Actions').'<span>'.Yii::t('employees','').'</span>', 'url'=>array('/empDicipAction/index&id='.$_REQUEST['id']),'linkOptions'=>array('class'=>'abook_ico'),'active'=> (Yii::app()->controller->id=='empDicipAction')),
						array('label'=>Yii::t('employees','Dependants').'<span>'.Yii::t('employees','').'</span>', 'url'=>array('/empDependants/index&id='.$_REQUEST['id']),'linkOptions'=>array('class'=>'abook_ico'),'active'=> (Yii::app()->controller->id=='empDependants')),
				),
			)); ?>
		
		</div>
 
    <div class="clear"></div>
    <!--<div class="left_emp_navbx">
    <div class="left_emp_nav">
    <h2>Your Search</h2>
    <ul>
    <li><a class="icon_emp" href="#">Profile</a></li>
    <li><a href="#">Delete</a></li>
    <li><span class="activearrow"></span><a class="active" href="#">Leaves <span class="active"></span></a></li>
    <li><a class="last" href="#">More</a></li>
    </ul>
    </div>
    <div class="clear"></div>
    <div class="left_emp_btn"><a class="arrowsml" href="#">Saved Searches</a></div>
    </div>-->
    </div>
    
    </div>
