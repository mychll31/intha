<?php
$empid=Employees::model()->findByAttributes(array('association_employee_id'=>$_REQUEST['id']));
$this->breadcrumbs=array(
	'Employee'=>array('/employees'),
	$_REQUEST['id']=>array('employees/employees/view&id='.$empid->id),
	'Employee License'
);
$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
      <?php $this->renderPartial('/employeeAddresses/profileleft');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
	<?php $pref = EmpPrefix::model()->findByAttributes(array('id'=>$empid->prefix)); ?>
	<h1 ><?php echo Yii::t('employees','');?>
	<?php 
		if($pref!=NULL){$pname= $pref->name.'. ';}else{$pname='';}
		if($empid->lastname !=null){$lname =$empid->lastname.', ';}else{$lname='';}
		if($empid->firstname !=null){$fname =$empid->firstname.' ';}else{$fname='';}
		if($empid->middlename !=null){$mname =$empid->middlename.' ';}else{$mname='';}
		echo ucwords($pname.$lname.$fname.$mname);
	?>
	</h1>
	<?php if($role->role==1){?>
	<div class="edit_bttns" style="right: 30px;">
    <ul>
    <li><?php echo CHtml::link(Yii::t('empLicense','<span>Add License</span>'), array('empLicense/create','id'=>$_REQUEST['id']),array('class'=>'edit last')); ?><!--<a class=" edit last" href="">Edit</a>--></li>
    </ul>
  </div>
  <?php }?>
  <h3>License Details</h3>
<?php echo $this->renderPartial('_view', array('model'=>$model)); ?>
</div>
    </td>
  </tr>
</table>
