<!--upgrade_div_starts-->
<div class="upgrade_bx">
	<div class="up_banr_imgbx"><a href="http://open-school.org/contact.php" target="_blank"><img src="http://tryopenschool.com/images/promo_bnnr_innerpage.png" width="231" height="200" /></a></div>
	<div class="up_banr_firstbx">
   	  <h1>You are Using Community Edition</h1>
	  <a href="http://open-school.org/contact.php" target="_blank">upgrade to premium version!</a>
    </div>
	
</div>
<!--upgrade_div_ends-->
<div id="othleft-sidebar">
             <!--<div class="lsearch_bar">
             	<input type="text" value="Search" class="lsearch_bar_left" name="">
                <input type="button" class="sbut" name="">
                <div class="clear"></div>
  </div>-->    <h1><?php echo Yii::t('employees','Manage Employee');?></h1>   
                    <?php
			function t($message, $category = 'cms', $params = array(), $source = null, $language = null) 
{
    return $message;
}

			$this->widget('zii.widgets.CMenu',array(
			'encodeLabel'=>false,
			'activateItems'=>true,
			'activeCssClass'=>'list_active',
			'items'=>array(
						array('label'=>''.Yii::t('empLicense','List Employees').'<span>'.Yii::t('empLicense','All Employee Details').'</span>', 'url'=>array('/employees/employees/manage') ,'linkOptions'=>array('class'=>'lbook_ico'),'active'=> (Yii::app()->controller->id=='employees' and Yii::app()->controller->action->id=='manage')),  
						array('label'=>''.Yii::t('empLicense','Add Employee').'<span>'.Yii::t('empLicense','Add New Employee Details').'</span>',  'url'=>array('/employees/employees/create'),'linkOptions'=>array('class'=>'sl_ico' ),'active'=> (Yii::app()->controller->id=='employees' and (Yii::app()->controller->action->id=='create' or Yii::app()->controller->action->id=='create2')), 'itemOptions'=>array('id'=>'menu_1')),
					
					array('label'=>''.'<h1>'.Yii::t('empLicense','Manage Employee License').'</h1>'), 
						array('label'=>Yii::t('empLicense','List of Employee License').'<span>'.Yii::t('empLicense','All Employee Licenses').'</span>', 'url'=>array('/empLicense/manage'),'linkOptions'=>array('class'=>'lbook_ico'),'active'=> (Yii::app()->controller->id=='empLicense' and (Yii::app()->controller->action->id=='manage')) or (Yii::app()->controller->id=='empLicense' and (Yii::app()->controller->action->id=='managealerts'))),
						array('label'=>Yii::t('empLicense','Add Employee License').'<span>'.Yii::t('empLicense','New Employee Licenses').'</span>', 'url'=>array('/empLicense/createall'),'linkOptions'=>array('class'=>'sl_ico'),'active'=> (Yii::app()->controller->id=='empLicense' and (Yii::app()->controller->action->id=='createall'))),
						 /*array('label'=>''.'<h1>'.Yii::t('employees','Attendance Management').'</h1>'),
						   array('label'=>Yii::t('employees','Attendance Register').'<span>'.Yii::t('employees','Manage Employee Attendance').'</span>', 'url'=>array('/employees/employeeAttendances'),'active'=>(Yii::app()->controller->id=='employeeAttendances' ? true : false),'linkOptions'=>array('class'=>'ar_ico')),
						 */ 
						array('label'=>''.'<h1>'.Yii::t('employees','Employee Settings').'</h1>'),
						array('label'=>Yii::t('employees','Manage Prefixes').'<span>'.Yii::t('employees','All Prefixes').'</span>', 'url'=>array('/empPrefix/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='empPrefix')),
						array('label'=>Yii::t('employees','Manage Employment Status').'<span>'.Yii::t('employees','All Employment Status').'</span>', 'url'=>array('/empStatus/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='empStatus')),
						array('label'=>Yii::t('employees','Manage Positions').'<span>'.Yii::t('employees','All Positions').'</span>', 'url'=>array('/empPos/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='empPos')),
						array('label'=>Yii::t('employees','Manage Department').'<span>'.Yii::t('employees','All Departments').'</span>', 'url'=>array('/empDept/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='empDept')),
						array('label'=>Yii::t('employees','Manage Marital Status').'<span>'.Yii::t('employees','All Marital Status').'</span>', 'url'=>array('/empTaxstatus/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='empTaxstatus')),
						array('label'=>Yii::t('employees','Manage Region').'<span>'.Yii::t('employees','All Regions').'</span>', 'url'=>array('/empRegion/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='empRegion')),
						array('label'=>Yii::t('employees','Manage Pay Type').'<span>'.Yii::t('employees','All Pay Type').'</span>', 'url'=>array('/empPaytype/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='empPaytype')),
						array('label'=>Yii::t('employees','Manage Pay Period').'<span>'.Yii::t('employees','All Pay Period').'</span>', 'url'=>array('/empPayperiod/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='empPayperiod')),
						array('label'=>Yii::t('employees','Manage Method').'<span>'.Yii::t('employees','All Reporting Method').'</span>', 'url'=>array('/empReportingMethod/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='empReportingMethod')),
							
				),
			)); ?>
		
		</div>
        <script type="text/javascript">

	$(document).ready(function () {
            //Hide the second level menu
            $('#othleft-sidebar ul li ul').hide();            
            //Show the second level menu if an item inside it active
            $('li.list_active').parent("ul").show();
            
            $('#othleft-sidebar').children('ul').children('li').children('a').click(function () {                    
                
                 if($(this).parent().children('ul').length>0){                  
                    $(this).parent().children('ul').toggle();    
                 }
                 
            });
          
            
        });

    </script>
