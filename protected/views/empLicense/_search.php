<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'emp_no'); ?>
		<?php echo $form->textField($model,'emp_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'license_type'); ?>
		<?php echo $form->textField($model,'license_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'license_no'); ?>
		<?php echo $form->textField($model,'license_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'issuance_date'); ?>
		<?php echo $form->textField($model,'issuance_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'expiry_date'); ?>
		<?php echo $form->textField($model,'expiry_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->