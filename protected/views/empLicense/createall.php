<?php
$this->breadcrumbs=array(
	'Employee'=>array('/employees'),
	'Employee License'=>array('employeeAddresses/view&id='.$_REQUEST['id']),
	'Create',
);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
      <?php $this->renderPartial('/empLicense/profileleft1');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
<?php echo $this->renderPartial('_formall', array('model'=>$model)); ?>
</div>
    </td>
  </tr>
</table>