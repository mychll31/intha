<?php
$this->breadcrumbs=array(
	'User Customer Roles',
);

$this->menu=array(
	array('label'=>'Create UserCustomerRole', 'url'=>array('create')),
	array('label'=>'Manage UserCustomerRole', 'url'=>array('admin')),
);
?>

<h1>User Customer Roles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
