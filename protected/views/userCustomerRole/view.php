<?php
$this->breadcrumbs=array(
	'User Customer Roles'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UserCustomerRole', 'url'=>array('index')),
	array('label'=>'Create UserCustomerRole', 'url'=>array('create')),
	array('label'=>'Update UserCustomerRole', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete UserCustomerRole', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UserCustomerRole', 'url'=>array('admin')),
);
?>

<h1>View UserCustomerRole #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user',
		'role',
	),
)); ?>
