<?php
$this->breadcrumbs=array(
	'User Customer Roles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserCustomerRole', 'url'=>array('index')),
	array('label'=>'Manage UserCustomerRole', 'url'=>array('admin')),
);
?>

<h1>Create UserCustomerRole</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>