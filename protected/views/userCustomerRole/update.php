<?php
$this->breadcrumbs=array(
	'User Customer Roles'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserCustomerRole', 'url'=>array('index')),
	array('label'=>'Create UserCustomerRole', 'url'=>array('create')),
	array('label'=>'View UserCustomerRole', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage UserCustomerRole', 'url'=>array('admin')),
);
?>

<h1>Update UserCustomerRole <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>