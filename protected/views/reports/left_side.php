<?php Yii::app()->clientScript->registerCoreScript('jquery');

         //IMPORTANT about Fancybox.You can use the newest 2.0 version or the old one
        //If you use the new one,as below,you can use it for free only for your personal non-commercial site.For more info see
		//If you decide to switch back to fancybox 1 you must do a search and replace in index view file for "beforeClose" and replace with 
		//"onClosed"
        // http://fancyapps.com/fancybox/#license
          // FancyBox2
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js_plugins/fancybox2/jquery.fancybox.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/js_plugins/fancybox2/jquery.fancybox.css', 'screen');
         // FancyBox
         //Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js_plugins/fancybox/jquery.fancybox-1.3.4.js', CClientScript::POS_HEAD);
         // Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js_plugins/fancybox/jquery.fancybox-1.3.4.css','screen');
        //JQueryUI (for delete confirmation  dialog)
         Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js_plugins/jqui1812/js/jquery-ui-1.8.12.custom.min.js', CClientScript::POS_HEAD);
         Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js_plugins/jqui1812/css/dark-hive/jquery-ui-1.8.12.custom.css','screen');
          ///JSON2JS
         Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js_plugins/json2/json2.js');
       

           //jqueryform js
               Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js_plugins/ajaxform/jquery.form.js', CClientScript::POS_HEAD);
              Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js_plugins/ajaxform/form_ajax_binding.js', CClientScript::POS_HEAD);
              Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js_plugins/ajaxform/client_val_form.css','screen');  ?>
              
              



<!--upgrade_div_starts-->
<div class="upgrade_bx">
	<div class="up_banr_imgbx"><a href="http://open-school.org/contact.php" target="_blank"><img src="http://tryopenschool.com/images/promo_bnnr_innerpage.png" width="231" height="200" /></a></div>
	<div class="up_banr_firstbx">
   	  <h1>You are Using Community Edition</h1>
	  <a href="http://open-school.org/contact.php" target="_blank">upgrade to premium version!</a>
    </div>
	
</div>
<!--upgrade_div_ends-->

<div id="othleft-sidebar">
<!--<div class="lsearch_bar">
             	<input type="text" value="Search" class="lsearch_bar_left" name="">
                <input type="button" class="sbut" name="">
                <div class="clear"></div>
  </div> --> <h1><?php echo Yii::t('Courses','Manage Trucks');?></h1>
                    
                    <?php
			function t($message, $category = 'cms', $params = array(), $source = null, $language = null) 
{
    return $message;
}

			$this->widget('zii.widgets.CMenu',array(
			'encodeLabel'=>false,
			'activateItems'=>true,
			'activeCssClass'=>'list_active',
			'items'=>array(
					
                                                         
					#array('label'=>''.Yii::t('Courses','General Settings').'<span>'.Yii::t('Courses','Manage Configurations').'</span>',  'url'=>array('/configurations/index'), 'linkOptions'=>array('class'=>'gs_ico' ), 'itemOptions'=>array('id'=>'menu_1') ),
						   
					
					      
						array('label'=>Yii::t('Courses','List of Trucks').'<span>'.Yii::t('Courses','All Trucks Details').'</span>', 'url'=>array('/courses/courses/managecourse'), 'active'=> ((Yii::app()->controller->id=='courses') and (in_array(Yii::app()->controller->action->id,array('managecourse'))) ? true : false),'linkOptions'=>array('class'=>'lbook_ico' )),
						
						
						array('label'=>Yii::t('Courses','Add Truck').'<span>'.Yii::t('Courses','Add New Truck Details').'</span>', 'url'=>array('/courses/courses/create'),
							'active'=> ((Yii::app()->controller->id=='courses') && (in_array(Yii::app()->controller->action->id,array('update','view','admin','index','create'))) ? true : false),'linkOptions'=>array('class'=>'abook_ico' )                                                                                           
						      ),
							 // array('label'=>t('Create Batches'), 'url'=>'#',
							//'active'=> ((Yii::app()->controller->id=='beterm') && (in_array(Yii::app()->controller->action->id,array('update','view','admin','index'))) ? true : false)                                                                                           
						     // ),
						                                                                                    
		
					   
					
						
						/*array('label'=>''.t('Batch-Settings<span>Manage your Dashboard</span>'), 'url'=>'javascript:void(0);','linkOptions'=>array('id'=>'menu_3','class'=>'menu_3'), 'itemOptions'=>array('id'=>'menu_3'),
					       'items'=>array(
						
						array('label'=>t('Set Week days'), 'url'=>array('/courses/weekdays&id='),
								'active'=> ((Yii::app()->controller->id=='weekdays') && (in_array(Yii::app()->controller->action->id,array('update','view','admin','index'))) ? true : false)),
						array('label'=>t('Set Class Timings'), 'url'=>array('/courses/classTimings'),
								'active'=> ((Yii::app()->controller->id=='classTimings') && (in_array(Yii::app()->controller->action->id,array('update','view','admin','index'))) ? true : false)),
						array('label'=>t('Create/View Time Table'), 'url'=>array('#'),),
						
					  
					    )),*/

					array('label'=>''.'<h1>'.Yii::t('employees','Trucks Settings').'</h1>'),
						array('label'=>Yii::t('employees','Manage Wheeler').'<span>'.Yii::t('employees','All Wheelers').'</span>', 'url'=>array('/truckerWheeler/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='truckerWheeler')),
						array('label'=>Yii::t('employees','Manage Brand').'<span>'.Yii::t('employees','All Brands').'</span>', 'url'=>array('/truckerBrand/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='truckerBrand')),
						array('label'=>Yii::t('employees','Manage Trailer Type').'<span>'.Yii::t('employees','All Trailer Type').'</span>', 'url'=>array('/truckerTrailerType/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='truckerTrailerType')),
						array('label'=>Yii::t('employees','Manage Truck Status').'<span>'.Yii::t('employees','All Status').'</span>', 'url'=>array('/truckerStatus/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='truckerStatus')),
						array('label'=>Yii::t('employees','Manage Type of Note').'<span>'.Yii::t('employees','All Notes').'</span>', 'url'=>array('/truckerTypeNote/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='truckerTypeNote')),
						
						
						array('label'=>Yii::t('employees','Manage Tractor Type').'<span>'.Yii::t('employees','All Types of Tractor').'</span>', 'url'=>array('/truckersTractorType/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='truckersTractorType')),
						array('label'=>Yii::t('employees','Manage Trailer Footer').'<span>'.Yii::t('employees','All Types of Trailer Footer').'</span>', 'url'=>array('/truckTrailerFooter/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='truckTrailerFooter')),
						array('label'=>Yii::t('employees','Manage Trailer Axle').'<span>'.Yii::t('employees','All Types Trailer Axle').'</span>', 'url'=>array('/truckTrailerAxle/'),'linkOptions'=>array('class'=>'mp_ico'),'active'=> (Yii::app()->controller->id=='truckTrailerAxle')),
							
				),
			)); ?>
		<div id="subject-name-ajax-grid"></div>
        
        
       
        

 <script type="text/javascript">

	$(document).ready(function () {
            //Hide the second level menu
            $('#othleft-sidebar ul li ul').hide();            
            //Show the second level menu if an item inside it active
            $('li.list_active').parent("ul").show();
            
            $('#othleft-sidebar').children('ul').children('li').children('a').click(function () {                    
                
                 if($(this).parent().children('ul').length>0){                  
                    $(this).parent().children('ul').toggle();    
                 }
                 
            });
          
            
        });
		
		
		//CREATE 
    
    $('#add_subject-name-ajax').bind('click', function() {
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->request->baseUrl;?>/index.php?r=subjectNameAjax/returnForm",
            data:{"YII_CSRF_TOKEN":"<?php echo Yii::app()->request->csrfToken;?>"},
                beforeSend : function() {
                    $("#subject-name-ajax-grid").addClass("ajax-sending");
                },
                complete : function() {
                    $("#subject-name-ajax-grid").removeClass("ajax-sending");
                },
            success: function(data) {
                $.fancybox(data,
                        {    "transitionIn"      : "elastic",
                            "transitionOut"   : "elastic",
                            "speedIn"                : 600,
                            "speedOut"            : 200,
                            "overlayShow"     : false,
                            "hideOnContentClick": false,
							"afterClose":    function() {
								window.location.reload();
								} 
							
							
							
                             //onclosed function
                        });//fancybox
            } //success
        });//ajax
        return false;
    });//bind
	
    </script>
    
   