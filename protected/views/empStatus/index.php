<?php
$this->breadcrumbs=array(
	'Emp Statuses',
);

$this->menu=array(
	array('label'=>'Create EmpStatus', 'url'=>array('create')),
	array('label'=>'Manage EmpStatus', 'url'=>array('admin')),
);
?>

<h1>Emp Statuses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
