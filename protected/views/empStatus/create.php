<?php
$this->breadcrumbs=array(
	'Emp Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmpStatus', 'url'=>array('index')),
	array('label'=>'Manage EmpStatus', 'url'=>array('admin')),
);
?>

<h1>Create EmpStatus</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>