<?php
$this->breadcrumbs=array(
	'Emp Statuses'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmpStatus', 'url'=>array('index')),
	array('label'=>'Create EmpStatus', 'url'=>array('create')),
	array('label'=>'View EmpStatus', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmpStatus', 'url'=>array('admin')),
);
?>

<h1>Update EmpStatus <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>