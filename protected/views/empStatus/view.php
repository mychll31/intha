<?php
$this->breadcrumbs=array(
	'Emp Statuses'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List EmpStatus', 'url'=>array('index')),
	array('label'=>'Create EmpStatus', 'url'=>array('create')),
	array('label'=>'Update EmpStatus', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmpStatus', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmpStatus', 'url'=>array('admin')),
);
?>

<h1>View EmpStatus #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
