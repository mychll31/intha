<?php
$this->breadcrumbs=array(
	'Employees Remarks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List EmployeesRemarks', 'url'=>array('index')),
	array('label'=>'Create EmployeesRemarks', 'url'=>array('create')),
	array('label'=>'Update EmployeesRemarks', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmployeesRemarks', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmployeesRemarks', 'url'=>array('admin')),
);
?>

<h1>View EmployeesRemarks #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'dateencoded',
		'datereminder',
		'description',
		'remarks',
		'emp_no',
		'createdby',
		'datecreatedby',
		'datemodified',
	),
)); ?>
