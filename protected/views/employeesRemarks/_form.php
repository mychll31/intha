<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employees-remarks-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'dateencoded'); ?>
		<?php echo $form->textField($model,'dateencoded'); ?>
		<?php echo $form->error($model,'dateencoded'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datereminder'); ?>
		<?php echo $form->textField($model,'datereminder'); ?>
		<?php echo $form->error($model,'datereminder'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>10000)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textField($model,'remarks'); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'emp_no'); ?>
		<?php echo $form->textField($model,'emp_no',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'emp_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'createdby'); ?>
		<?php echo $form->textField($model,'createdby',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'createdby'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datecreatedby'); ?>
		<?php echo $form->textField($model,'datecreatedby'); ?>
		<?php echo $form->error($model,'datecreatedby'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datemodified'); ?>
		<?php echo $form->textField($model,'datemodified'); ?>
		<?php echo $form->error($model,'datemodified'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->