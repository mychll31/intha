<?php
$this->breadcrumbs=array(
	'Employees Remarks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmployeesRemarks', 'url'=>array('index')),
	array('label'=>'Create EmployeesRemarks', 'url'=>array('create')),
	array('label'=>'View EmployeesRemarks', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmployeesRemarks', 'url'=>array('admin')),
);
?>

<h1>Update EmployeesRemarks <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>