<?php
$this->breadcrumbs=array(
	'Employees Remarks',
);

$this->menu=array(
	array('label'=>'Create EmployeesRemarks', 'url'=>array('create')),
	array('label'=>'Manage EmployeesRemarks', 'url'=>array('admin')),
);
?>

<h1>Employees Remarks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
