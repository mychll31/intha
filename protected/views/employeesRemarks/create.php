<?php
$this->breadcrumbs=array(
	'Employees Remarks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmployeesRemarks', 'url'=>array('index')),
	array('label'=>'Manage EmployeesRemarks', 'url'=>array('admin')),
);
?>

<h1>Create EmployeesRemarks</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>