<?php
$this->breadcrumbs=array(
	'Truckers Tractor Types'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TruckersTractorType', 'url'=>array('index')),
	array('label'=>'Create TruckersTractorType', 'url'=>array('create')),
	array('label'=>'Update TruckersTractorType', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TruckersTractorType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TruckersTractorType', 'url'=>array('admin')),
);
?>

<h1>View TruckersTractorType #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
