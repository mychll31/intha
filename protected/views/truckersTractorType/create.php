<?php
$this->breadcrumbs=array(
	'Truckers Tractor Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TruckersTractorType', 'url'=>array('index')),
	array('label'=>'Manage TruckersTractorType', 'url'=>array('admin')),
);
?>

<h1>Create TruckersTractorType</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>