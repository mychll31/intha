<?php
$this->breadcrumbs=array(
	'Truckers Tractor Types',
);

$this->menu=array(
	array('label'=>'Create TruckersTractorType', 'url'=>array('create')),
	array('label'=>'Manage TruckersTractorType', 'url'=>array('admin')),
);
?>

<h1>Truckers Tractor Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
