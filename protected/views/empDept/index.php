<?php
$this->breadcrumbs=array(
	'Emp Depts',
);

$this->menu=array(
	array('label'=>'Create EmpDept', 'url'=>array('create')),
	array('label'=>'Manage EmpDept', 'url'=>array('admin')),
);
?>

<h1>Emp Depts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
