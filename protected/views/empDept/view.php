<?php
$this->breadcrumbs=array(
	'Emp Depts'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List EmpDept', 'url'=>array('index')),
	array('label'=>'Create EmpDept', 'url'=>array('create')),
	array('label'=>'Update EmpDept', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmpDept', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmpDept', 'url'=>array('admin')),
);
?>

<h1>View EmpDept #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
