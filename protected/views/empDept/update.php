<?php
$this->breadcrumbs=array(
	'Emp Depts'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmpDept', 'url'=>array('index')),
	array('label'=>'Create EmpDept', 'url'=>array('create')),
	array('label'=>'View EmpDept', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmpDept', 'url'=>array('admin')),
);
?>

<h1>Update EmpDept <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>