<?php
$this->breadcrumbs=array(
	'Emp Depts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmpDept', 'url'=>array('index')),
	array('label'=>'Manage EmpDept', 'url'=>array('admin')),
);
?>

<h1>Create EmpDept</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>