<?php
$this->breadcrumbs=array(
	'Emp Paytypes',
);

$this->menu=array(
	array('label'=>'Create EmpPaytype', 'url'=>array('create')),
	array('label'=>'Manage EmpPaytype', 'url'=>array('admin')),
);
?>

<h1>Emp Paytypes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
