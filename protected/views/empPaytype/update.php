<?php
$this->breadcrumbs=array(
	'Emp Paytypes'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmpPaytype', 'url'=>array('index')),
	array('label'=>'Create EmpPaytype', 'url'=>array('create')),
	array('label'=>'View EmpPaytype', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmpPaytype', 'url'=>array('admin')),
);
?>

<h1>Update EmpPaytype <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>