<?php
$this->breadcrumbs=array(
	'Emp Paytypes'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List EmpPaytype', 'url'=>array('index')),
	array('label'=>'Create EmpPaytype', 'url'=>array('create')),
	array('label'=>'Update EmpPaytype', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmpPaytype', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmpPaytype', 'url'=>array('admin')),
);
?>

<h1>View EmpPaytype #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
