<?php
$this->breadcrumbs=array(
	'Emp Paytypes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmpPaytype', 'url'=>array('index')),
	array('label'=>'Manage EmpPaytype', 'url'=>array('admin')),
);
?>

<h1>Create EmpPaytype</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>