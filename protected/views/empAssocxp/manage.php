<?php
$empid=Employees::model()->findByAttributes(array('id'=>$_REQUEST['id']));
$this->breadcrumbs=array(
        'Employee'=>array('/employees'),
        $empid->association_employee_id=>array('/employees/employees/view','id'=>$_REQUEST['id']),
	'Experience with Association'
);
$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
    <?php $this->renderPartial('/employeeAddresses/profileleft');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <?php if($role->role==1){?>
	<h3><?php echo Yii::t('empAssocxp','Work Experience with Association');?></h3>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    
	<div class="formCon">

	<div class="formConInner">
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'emp-assocxp-form',
	'enableAjaxValidation'=>false,
)); ?>

	<style>td{vertical-align:top;}</style>
	<?php
		$employee_id= Employees::model()->findByAttributes(array('id'=>$_REQUEST['id']));
	?>
	<?php echo $form->hiddenField($model,'emp_id',array('value'=>$employee_id->association_employee_id)); ?>
	<?php echo $form->error($model,'emp_id'); ?>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td style="vertical-align:middle !important;"><?php echo $form->labelEx($model,'assoc_truckerid'); ?></td>
		<td><?php 
		      if($_REQUEST['trucker']==null){
			$truckers = CHtml::listData(Students::model()->findAll(),'assoc_truckerid','trucker');
			echo $form->dropDownList($model,'assoc_truckerid',$truckers,array('empty'=>''),array('options'=>array('E-00002'=>array('selected'=>true))));
			}else{
			echo $form->dropdownList(
			  $model,
			  'assoc_truckerid',
			  CHtml::listData(Students::model()->findAll(),'assoc_truckerid','trucker'),
			  array(
			    'options'=>array($_REQUEST['trucker']=>array('selected'=>true))
			  )
			);
			}
			?>
			<?php echo $form->error($model,'assoc_truckerid'); ?>
		</td>
		<td style="vertical-align:middle !important;"><?php echo $form->labelEx($model,'emp_number'); ?></td>
		<td><?php echo $form->textField($model,'emp_number',array('size'=>25)); ?>
			<?php echo $form->error($model,'emp_number'); ?></td>
	</tr>
	<tr><td colspan=4>&nbsp;</td></tr>
	<tr>
		<td  style="vertical-align:middle !important;"><?php echo $form->labelEx($model,'inc_date'); ?></td>
		<td><?php
				$date = 'yy-mm-dd';
				$this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'attribute'=>'inc_date',
					'model'=>$model,
						'options'=>array(
							'showAnim'=>'fold',
							'dateFormat'=>$date,
							'changeMonth'=> true,
								'changeYear'=>true,
								'yearRange'=>'1950:2050'
						),
					'htmlOptions'=>array(
						'style' => 'width:100px;',
					),
				))
			?>
		<?php echo $form->error($model,'inc_date'); ?></td>
		<td style="vertical-align:middle !important;"><?php echo $form->labelEx($model,'exc_date'); ?></td>
		<td><?php
				$this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'attribute'=>'exc_date',
					'model'=>$model,
						'options'=>array(
							'showAnim'=>'fold',
							'dateFormat'=>$date,
							'changeMonth'=> true,
								'changeYear'=>true,
								'yearRange'=>'1950:2050'
						),
					'htmlOptions'=>array(
						'style' => 'width:100px;',
					),
				))
			?>
		<?php echo $form->error($model,'exc_date'); ?></td>
	</tr>
	<tr><td colspan=4>&nbsp;</td></tr>
	<tr>
		<td style="vertical-align:middle !important;"><?php echo $form->labelEx($model,'position'); ?></td>
		<td><?php 
			$positions = CHtml::listData(EmpPos::model()->findAll(),'id','name');
			echo $form->dropDownList($model,'position',$positions,array('empty'=>''));?>
			<?php echo $form->error($model,'position'); ?></td>
		<td style="vertical-align:middle !important;"><?php echo $form->labelEx($model,'emp_status'); ?></td>
		<td><?php 
			$empstat = CHtml::listData(EmpStatus::model()->findAll(),'id','name');
			echo $form->dropDownList($model,'emp_status',$empstat,array('empty'=>''));?>
			<?php echo $form->error($model,'emp_status'); ?></td>
	</tr>
	<tr><td colspan=4>&nbsp;</td></tr>
	<tr>
		<td style="vertical-align:middle !important;"><?php echo $form->labelEx($model,'reason_forleaving'); ?></td>
		<td><?php echo $form->textArea($model,'reason_forleaving'); ?>
			<?php echo $form->error($model,'reason_forleaving'); ?></td>
		<td style="vertical-align:middle !important;"><?php echo $form->labelEx($model,Yii::t('empDept','current'),array('style'=>'float:left')); ?>
		<td class="cr_align" style="vertical-align:middle !important;">
		<?php 
		if($_REQUEST['trucker']==null){
		}else{
		$model->current=1;
		}
		echo $form->radioButtonList($model,'current',array('1'=>'Current','0'=>'Previous'),array('separator'=>' ')); 
		?>
			<?php echo $form->error($model,'current'); ?></td>
	</tr>
	<tr><td colspan=4>&nbsp;</td></tr>
</table>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update',array('class'=>'formbut')); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
</div>
</div>
<style>
.linka {
background-color: rgb(11, 131, 11);
color: white;
padding: 8px 18px;
border-radius: 8px;
border: 1px solid rgb(4, 126, 4);
}
a.linka:hover {
background-color: rgba(0, 128, 0, 0.72);
}
</style>
<?php 
$curtrucker = Employees::model()->findByAttributes(array('id'=>$_REQUEST['id']));
$criteria1 = new CDbCriteria;
$criteria1->condition='emp_id=\''.$employee_id->association_employee_id.'\'';
$criteria1->condition= $criteria1->condition.' and assoc_truckerid=\''.$curtrucker->trucker_id.'\'';
$empass=EmpAssocxp::model()->count($criteria1);
$truckername =Students::model()->findByAttributes(array('assoc_truckerid'=>$curtrucker->trucker_id));

  if($empass==0 and $curtrucker!= null){
    echo CHtml::link('Add '.$truckername->trucker.' in Work Experience',array('index','id'=>$_REQUEST['id'],'trucker'=>$curtrucker->trucker_id),array('class'=>'linka')); 
  }
}
$criteria = new CDbCriteria;
$criteria->condition='emp_id=:x';
$criteria->params = array(':x'=>$employee_id->association_employee_id);
$criteria->order = 'exc_date DESC';
$active=EmpAssocxp::model()->findAll($criteria);
$no=1;
if($active !=null){
?>
<h3><?php echo Yii::t('empAssocxp','List of Work Experience with Association');?></h3>
<div class="tableinnerlist">
<table width="100%" cellpadding="0" cellspacing="0">
<tr class="pdtab-h">
	<td style="text-align:left;">No.</td>
	<td style="text-align:left;">Trucker Name</td>
	<td style="text-align:left;">Employee No.</td>
	<td style="text-align:left;">Position</td>
	<td style="text-align:left;">Date</td>
	<!--td style="text-align:left;">Reason for Leaving</td-->
	<td style="text-align:left;">Status</td>
	<?php if($role->role==1){?>
    <td>Actions</td>
	<?php }?>
</tr>
<?php
}
foreach($active as $active_1)
{
   echo '<tr';
   if($active_1->current==1){echo ' style="background-color:rgba(255, 192, 203, 0.33);"';}
   echo '><td rowspan=2 style="padding-left:10px; text-align:left;font-weight:normal;">'.$no++.'</td>';
   $truckername= Students::model()->findByAttributes(array('assoc_truckerid'=>$active_1->assoc_truckerid));
   echo '<td style="padding-left:10px; text-align:left;">'.$truckername->trucker.'</td>';
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->emp_number.'</td>';
   $pos=EmpPos::model()->findByAttributes(array('id'=>$active_1->position));
   echo '<td style="padding-left:10px; text-align:left;">'.$pos->name.'</td>';
   echo '<td style="padding-left:10px; text-align:left;">'.date('M. d, Y',strtotime($active_1->inc_date)).' - ';
   if($active_1->current == 1){ echo 'Present';}else{ echo date('M. d, Y',strtotime($active_1->exc_date));}
   echo '</td>';
   #echo '<td style="padding-left:10px; text-align:left;">'.$active_1->reason_forleaving.'</td>';
	$empstats=EmpStatus::model()->findByAttributes(array('id'=>$active_1->emp_status));
   echo '<td style="padding-left:10px; text-align:left;">'.$empstats->name.'</td>';
	if($role->role==1){   
   echo '<td>'.CHtml::link(Yii::t('empAssocxp','Edit'), array('update','id'=>$_REQUEST['id'],'eid'=>$active_1->id));
   echo '</td>';}
   echo '<tr';
   if($active_1->current==1){echo ' style="background-color:rgba(255, 192, 203, 0.33);"';}
   echo '><td  colspan=6 style="padding-left:10px; text-align:left;font-weight:normal;">
		<b>Reason : </b>'.$active_1->reason_forleaving.'
   </td>';
}
?> 
</table>
</div>
</div>
</div>
    </td>
  </tr>
</table>
