<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'employeetable_id'); ?>
		<?php echo $form->textField($model,'employeetable_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'assoc_truckerid'); ?>
		<?php echo $form->textField($model,'assoc_truckerid',array('size'=>22,'maxlength'=>22)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'emp_id'); ?>
		<?php echo $form->textField($model,'emp_id',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'inc_date'); ?>
		<?php echo $form->textField($model,'inc_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'exc_date'); ?>
		<?php echo $form->textField($model,'exc_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reason_forleaving'); ?>
		<?php echo $form->textField($model,'reason_forleaving',array('size'=>60,'maxlength'=>10000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'emp_number'); ?>
		<?php echo $form->textField($model,'emp_number',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'current'); ?>
		<?php echo $form->textField($model,'current'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->