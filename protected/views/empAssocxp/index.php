<?php
$this->breadcrumbs=array(
	'Emp Assocxps',
);

$this->menu=array(
	array('label'=>'Create EmpAssocxp', 'url'=>array('create')),
	array('label'=>'Manage EmpAssocxp', 'url'=>array('admin')),
);
?>

<h1>Emp Assocxps</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
