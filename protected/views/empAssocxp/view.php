<?php
$this->breadcrumbs=array(
	'Emp Assocxps'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List EmpAssocxp', 'url'=>array('index')),
	array('label'=>'Create EmpAssocxp', 'url'=>array('create')),
	array('label'=>'Update EmpAssocxp', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmpAssocxp', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmpAssocxp', 'url'=>array('admin')),
);
?>

<h1>View EmpAssocxp #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'employeetable_id',
		'assoc_truckerid',
		'emp_id',
		'inc_date',
		'exc_date',
		'reason_forleaving',
		'emp_number',
		'current',
	),
)); ?>
