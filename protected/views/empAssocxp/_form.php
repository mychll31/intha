<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'emp-assocxp-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'employeetable_id'); ?>
		<?php echo $form->textField($model,'employeetable_id'); ?>
		<?php echo $form->error($model,'employeetable_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'assoc_truckerid'); ?>
		<?php echo $form->textField($model,'assoc_truckerid',array('size'=>22,'maxlength'=>22)); ?>
		<?php echo $form->error($model,'assoc_truckerid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'emp_id'); ?>
		<?php echo $form->textField($model,'emp_id',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'emp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'inc_date'); ?>
		<?php echo $form->textField($model,'inc_date'); ?>
		<?php echo $form->error($model,'inc_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'exc_date'); ?>
		<?php echo $form->textField($model,'exc_date'); ?>
		<?php echo $form->error($model,'exc_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reason_forleaving'); ?>
		<?php echo $form->textField($model,'reason_forleaving',array('size'=>60,'maxlength'=>10000)); ?>
		<?php echo $form->error($model,'reason_forleaving'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'emp_number'); ?>
		<?php echo $form->textField($model,'emp_number',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'emp_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'current'); ?>
		<?php echo $form->textField($model,'current'); ?>
		<?php echo $form->error($model,'current'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->