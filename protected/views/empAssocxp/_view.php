<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('employeetable_id')); ?>:</b>
	<?php echo CHtml::encode($data->employeetable_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('assoc_truckerid')); ?>:</b>
	<?php echo CHtml::encode($data->assoc_truckerid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_id')); ?>:</b>
	<?php echo CHtml::encode($data->emp_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inc_date')); ?>:</b>
	<?php echo CHtml::encode($data->inc_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exc_date')); ?>:</b>
	<?php echo CHtml::encode($data->exc_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reason_forleaving')); ?>:</b>
	<?php echo CHtml::encode($data->reason_forleaving); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_number')); ?>:</b>
	<?php echo CHtml::encode($data->emp_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('current')); ?>:</b>
	<?php echo CHtml::encode($data->current); ?>
	<br />

	*/ ?>

</div>