<?php
$this->breadcrumbs=array(
	'Emp Assocxps'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmpAssocxp', 'url'=>array('index')),
	array('label'=>'Create EmpAssocxp', 'url'=>array('create')),
	array('label'=>'View EmpAssocxp', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmpAssocxp', 'url'=>array('admin')),
);
?>

<h1>Update EmpAssocxp <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>