<?php
$this->breadcrumbs=array(
	'Inlandbrokers'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Inlandbrokers', 'url'=>array('index')),
	array('label'=>'Create Inlandbrokers', 'url'=>array('create')),
	array('label'=>'View Inlandbrokers', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Inlandbrokers', 'url'=>array('admin')),
);
?>

<h1>Update Inlandbrokers <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>