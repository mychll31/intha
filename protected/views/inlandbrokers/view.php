<?php
$this->breadcrumbs=array(
	'Inlandbrokers'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Inlandbrokers', 'url'=>array('index')),
	array('label'=>'Create Inlandbrokers', 'url'=>array('create')),
	array('label'=>'Update Inlandbrokers', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Inlandbrokers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Inlandbrokers', 'url'=>array('admin')),
);
?>

<h1>View Inlandbrokers #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'status',
	),
)); ?>
