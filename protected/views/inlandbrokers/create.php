<?php
$this->breadcrumbs=array(
	'Inlandbrokers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Inlandbrokers', 'url'=>array('index')),
	array('label'=>'Manage Inlandbrokers', 'url'=>array('admin')),
);
?>

<h1>Create Inlandbrokers</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>