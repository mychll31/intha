<?php
$this->breadcrumbs=array(
	'Inlandbrokers',
);

$this->menu=array(
	array('label'=>'Create Inlandbrokers', 'url'=>array('create')),
	array('label'=>'Manage Inlandbrokers', 'url'=>array('admin')),
);
?>

<h1>Inlandbrokers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
