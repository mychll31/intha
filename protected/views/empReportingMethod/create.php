<?php
$this->breadcrumbs=array(
	'Emp Reporting Methods'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmpReportingMethod', 'url'=>array('index')),
	array('label'=>'Manage EmpReportingMethod', 'url'=>array('admin')),
);
?>

<h1>Create EmpReportingMethod</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>