<?php
$this->breadcrumbs=array(
	'Emp Reporting Methods'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List EmpReportingMethod', 'url'=>array('index')),
	array('label'=>'Create EmpReportingMethod', 'url'=>array('create')),
	array('label'=>'Update EmpReportingMethod', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmpReportingMethod', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmpReportingMethod', 'url'=>array('admin')),
);
?>

<h1>View EmpReportingMethod #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
