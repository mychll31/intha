<?php
$this->breadcrumbs=array(
	'Emp Reporting Methods'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmpReportingMethod', 'url'=>array('index')),
	array('label'=>'Create EmpReportingMethod', 'url'=>array('create')),
	array('label'=>'View EmpReportingMethod', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmpReportingMethod', 'url'=>array('admin')),
);
?>

<h1>Update EmpReportingMethod <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>