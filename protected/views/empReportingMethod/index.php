<?php
$this->breadcrumbs=array(
	'Emp Reporting Methods',
);

$this->menu=array(
	array('label'=>'Create EmpReportingMethod', 'url'=>array('create')),
	array('label'=>'Manage EmpReportingMethod', 'url'=>array('admin')),
);
?>

<h1>Emp Reporting Methods</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
