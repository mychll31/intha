<?php
$this->breadcrumbs=array(
	'Emp Poses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmpPos', 'url'=>array('index')),
	array('label'=>'Manage EmpPos', 'url'=>array('admin')),
);
?>

<h1>Create EmpPos</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>