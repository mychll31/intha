<?php
$this->breadcrumbs=array(
	'Emp Poses',
);

$this->menu=array(
	array('label'=>'Create EmpPos', 'url'=>array('create')),
	array('label'=>'Manage EmpPos', 'url'=>array('admin')),
);
?>

<h1>Emp Poses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
