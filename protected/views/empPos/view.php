<?php
$this->breadcrumbs=array(
	'Emp Poses'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List EmpPos', 'url'=>array('index')),
	array('label'=>'Create EmpPos', 'url'=>array('create')),
	array('label'=>'Update EmpPos', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmpPos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmpPos', 'url'=>array('admin')),
);
?>

<h1>View EmpPos #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
