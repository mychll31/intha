<?php
$this->breadcrumbs=array(
	'Emp Poses'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmpPos', 'url'=>array('index')),
	array('label'=>'Create EmpPos', 'url'=>array('create')),
	array('label'=>'View EmpPos', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmpPos', 'url'=>array('admin')),
);
?>

<h1>Update EmpPos <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>