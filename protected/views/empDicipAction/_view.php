<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_id')); ?>:</b>
	<?php echo CHtml::encode($data->emp_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('incident')); ?>:</b>
	<?php echo CHtml::encode($data->incident); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reportedbytrucker')); ?>:</b>
	<?php echo CHtml::encode($data->reportedbytrucker); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('incireportedby')); ?>:</b>
	<?php echo CHtml::encode($data->incireportedby); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actiontaken')); ?>:</b>
	<?php echo CHtml::encode($data->actiontaken); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	*/ ?>

</div>