<?php
$this->breadcrumbs=array(
	'Emp Dicip Actions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmpDicipAction', 'url'=>array('index')),
	array('label'=>'Manage EmpDicipAction', 'url'=>array('admin')),
);
?>

<h1>Create EmpDicipAction</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>