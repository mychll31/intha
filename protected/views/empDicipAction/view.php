<?php
$empid=Employees::model()->findByAttributes(array('id'=>$_REQUEST['id']));
$this->breadcrumbs=array(
        'Employee'=>array('/employees'),
        $empid->association_employee_id=>array('/employees/employees/view','id'=>$_REQUEST['id']),
        'Diciplinary Actions',
);
$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="247" valign="top">

<?php $this->renderPartial('/employeeAddresses/profileleft');?>

</td>
<td valign="top">
<div class="cont_right formWrapper">
<div class="edit_bttns last">
<ul>
<li>
<?php echo CHtml::link(Yii::t('students','<span>Back</span>'), array('index', 'id'=>$_REQUEST['id']),array('class'=>' edit ')); ?>
</li>
</ul>
</div>
<br>
<div class="emp_cntntbx">
<div class="table_listbx">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr class="listbxtop_hdng">
<td>Disciplinary Actions</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody></table>
<table width="100%">
<tbody>
<tr>
<td class="subhdng_nrmal"><span class="datahead">Date :</span><span class="data"><?php echo $model->date;?></span></td>
<td colspan=2 class="subhdng_nrmal"><span class="datahead">Incident :</span><span class="data"><?php echo $model->incident;?></span></td>
</tr>
<tr>
<td class="subhdng_nrmal"><span class="datahead">Incident Reported by :</span><span class="data"><?php echo $model->incireportedby;?></span></td>
<td class="subhdng_nrmal"><span class="datahead">Action Taken :</span><span class="data"><?php echo $model->actiontaken;?></span></td>
<?php $istat = IncidentStatus::model()->findByAttributes(array('id'=>$model->status))?>
<td class="subhdng_nrmal"><span class="datahead">Status :</span><span class="data"><?php echo $istat->name;?></span></td>
</tr>
</tbody></table> 
</div>
</div>
</td>
</tr>
</table>
