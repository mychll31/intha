<?php
$this->breadcrumbs=array(
	'Emp Dicip Actions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmpDicipAction', 'url'=>array('index')),
	array('label'=>'Create EmpDicipAction', 'url'=>array('create')),
	array('label'=>'View EmpDicipAction', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmpDicipAction', 'url'=>array('admin')),
);
?>

<h1>Update EmpDicipAction <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>