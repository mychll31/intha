<?php
$this->breadcrumbs=array(
	'Emp Dicip Actions',
);

$this->menu=array(
	array('label'=>'Create EmpDicipAction', 'url'=>array('create')),
	array('label'=>'Manage EmpDicipAction', 'url'=>array('admin')),
);
?>

<h1>Emp Dicip Actions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
