<?php
$empid=Employees::model()->findByAttributes(array('id'=>$_REQUEST['id']));
$this->breadcrumbs=array(
        'Employee'=>array('/employees'),
        $empid->association_employee_id=>array('/employees/employees/view','id'=>$_REQUEST['id']),
        'Diciplinary Actions',
);
$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="247" valign="top">

<?php $this->renderPartial('/employeeAddresses/profileleft');?>

</td>
<td valign="top">
<div class="cont_right formWrapper">
<?php if($role->role==1){?>
<h3><?php echo Yii::t('empDicipAction','Disciplinary Actions');?></h3>
<p class="note">Fields with <span class="required">*</span> are required.</p>
<div class="formCon">
<div class="formConInner">
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
'id'=>'emp-workexp-form',
'enableAjaxValidation'=>false,
)); ?>

<?php #echo $form->errorSummary($model); ?>
	<?php #echo $form->labelEx($model,'emp_id'); ?>
	<?php echo $form->hiddenField($model,'emp_id',array('value'=>$_REQUEST['id'])); ?>
	<?php echo $form->error($model,'emp_id'); ?>

<table width="50%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="middle"><?php echo $form->labelEx($model,'date'); ?></td>
	<td><?php
			$date = 'yy-mm-dd';
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'attribute'=>'date',
				'model'=>$model,
					'options'=>array(
						'showAnim'=>'fold',
						'dateFormat'=>$date,
						'changeMonth'=> true,
							'changeYear'=>true,
							'yearRange'=>'1950:2050'
					),
				'htmlOptions'=>array(
					'style' => 'width:100px;',
				),
			))
		?>
	<?php echo $form->error($model,'date'); ?></td>
</tr>
<tr><td colspan=2>&nbsp;</td></tr>
<tr>
	<td valign="top"><?php echo $form->labelEx($model,'incident'); ?></td>
	<td><?php echo $form->textArea($model,'incident',array('maxlength'=>10000,'rows'=>5, 'cols'=>20)); ?>
	<?php echo $form->error($model,'incident'); ?></td>
</tr>
<tr>
	<td colspan=2>&nbsp;</td>
</tr>
<tr>
	<td valign="middle"><?php echo $form->labelEx($model,'incireportedby'); ?></td>
	<td><?php echo $form->textField($model,'incireportedby',array('maxlength'=>255)); ?>
	<?php echo $form->error($model,'incireportedby'); ?></td>
</tr>
<tr>
	<td colspan=2>&nbsp;</td>
</tr>
<tr>
	<td valign="top"><?php echo $form->labelEx($model,'actiontaken'); ?></td>
	<td><?php echo $form->textArea($model,'actiontaken',array('maxlength'=>255)); ?>
	<?php echo $form->error($model,'actiontaken'); ?></td>
</tr>
<tr>
	<td colspan=2>&nbsp;</td>
</tr>
<tr>
	<td valign="middle"><?php echo $form->labelEx($model,'status'); ?></td>
	<td><?php 
		$criteria = new CDbCriteria;
		$criteria->compare('active',1);
		$istat = CHtml::listData(IncidentStatus::model()->findAll($criteria),'id','name');
		echo $form->dropDownList($model,'status',$istat,array('empty'=>''));
	?>
	<?php echo $form->error($model,'status'); ?></td>
</tr>
<tr>
	<td colspan=2>&nbsp;</td>
</tr>
<tr>
	<td colspan=2>&nbsp;</td>
</tr>
</table>

<div class="row buttons">
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update',array('class'=>'formbut')); ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
</div>
<?php }
$criteria = new CDbCriteria;
$criteria->condition='emp_id=:x';
$criteria->params = array(':x'=>$_REQUEST['id']);
/*
$criteria->condition=$criteria->condition . ' and reportedbytrucker=:x';
$criteria->params = array(':x'=>Yii::app()->user->id);
*/
$criteria->order = 'date DESC';
$active=EmpDicipAction::model()->findAll($criteria);
$no=1;
if($active!=null){
?>
<h3><?php echo Yii::t('empDicipAction','List of Disciplinary Actions');?></h3>
<div class="tableinnerlist">
<table width="70%" cellpadding="0" cellspacing="0">
<tr class="pdtab-h">
<td style="text-align:left;">No.</td>
<td style="text-align:left;">Date</td>
<td style="text-align:left;">Reported By</td>
<td style="text-align:left;">Status</td>
<td>Actions</td>
</tr>
<?php
}
foreach($active as $active_1)
{
   echo '<tr><td style="padding-left:10px; text-align:left;font-weight:normal;">'.$no++.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->date.'</td>';	
   echo '<td style="padding-left:10px; text-align:left;">'.$active_1->incireportedby.'</td>';	
   $istat = IncidentStatus::model()->findByAttributes(array('id'=>$active_1->status));
   echo '<td style="padding-left:10px; text-align:left;">'.$istat->name.'</td>';	
   if($role->role==1){
   echo '<td>'.CHtml::link(Yii::t('empDicipAction','Edit'), array('update','id'=>$active_1->emp_id,'eid'=>$active_1->id)).'&nbsp;&nbsp;&nbsp;'.CHtml::link(Yii::t('empDicipAction','View'), array('view','id'=>$active_1->emp_id,'eid'=>$active_1->id));
   }else{
   echo '<td>'.CHtml::link(Yii::t('empDicipAction','View'), array('view','id'=>$active_1->emp_id,'eid'=>$active_1->id));
   }
   echo '</td>';
}
?> 

</table>
</div>
</div>
</div>
    </td>
  </tr>
</table>
