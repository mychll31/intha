<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'emp-dicip-action-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

		<?php echo $form->labelEx($model,'emp_id'); ?>
		<?php echo $form->textField($model,'emp_id'); ?>
		<?php echo $form->error($model,'emp_id'); ?>

		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>

		<?php echo $form->labelEx($model,'incident'); ?>
		<?php echo $form->textField($model,'incident',array('size'=>60,'maxlength'=>10000)); ?>
		<?php echo $form->error($model,'incident'); ?>

		<?php echo $form->labelEx($model,'reportedbytrucker'); ?>
		<?php echo $form->textField($model,'reportedbytrucker'); ?>
		<?php echo $form->error($model,'reportedbytrucker'); ?>

		<?php echo $form->labelEx($model,'incireportedby'); ?>
		<?php echo $form->textField($model,'incireportedby',array('size'=>60,'maxlength'=>225)); ?>
		<?php echo $form->error($model,'incireportedby'); ?>

		<?php echo $form->labelEx($model,'actiontaken'); ?>
		<?php echo $form->textField($model,'actiontaken',array('size'=>60,'maxlength'=>10000)); ?>
		<?php echo $form->error($model,'actiontaken'); ?>

		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>

		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>

<?php $this->endWidget(); ?>

