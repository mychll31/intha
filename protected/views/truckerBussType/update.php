<?php
$this->breadcrumbs=array(
	'Trucker Buss Types'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TruckerBussType', 'url'=>array('index')),
	array('label'=>'Create TruckerBussType', 'url'=>array('create')),
	array('label'=>'View TruckerBussType', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TruckerBussType', 'url'=>array('admin')),
);
?>

<h1>Update TruckerBussType <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>