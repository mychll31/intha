<?php
$this->breadcrumbs=array(
	'Trucker Buss Types',
);

$this->menu=array(
	array('label'=>'Create TruckerBussType', 'url'=>array('create')),
	array('label'=>'Manage TruckerBussType', 'url'=>array('admin')),
);
?>

<h1>Trucker Buss Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
