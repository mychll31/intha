<?php
$this->breadcrumbs=array(
	'Truckers Attachments'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TruckersAttachments', 'url'=>array('index')),
	array('label'=>'Manage TruckersAttachments', 'url'=>array('admin')),
);
?>

<h1>Create TruckersAttachments</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>