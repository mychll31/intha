<?php
$this->breadcrumbs=array(
	'Truckers Attachments',
);

$this->menu=array(
	array('label'=>'Create TruckersAttachments', 'url'=>array('create')),
	array('label'=>'Manage TruckersAttachments', 'url'=>array('admin')),
);
?>

<h1>Truckers Attachments</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
