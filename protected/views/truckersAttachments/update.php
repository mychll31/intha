<?php
$this->breadcrumbs=array(
	'Truckers Attachments'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TruckersAttachments', 'url'=>array('index')),
	array('label'=>'Create TruckersAttachments', 'url'=>array('create')),
	array('label'=>'View TruckersAttachments', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TruckersAttachments', 'url'=>array('admin')),
);
?>

<h1>Update TruckersAttachments <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>