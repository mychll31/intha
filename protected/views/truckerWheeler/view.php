<?php
$this->breadcrumbs=array(
	'Trucker Wheelers'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TruckerWheeler', 'url'=>array('index')),
	array('label'=>'Create TruckerWheeler', 'url'=>array('create')),
	array('label'=>'Update TruckerWheeler', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TruckerWheeler', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TruckerWheeler', 'url'=>array('admin')),
);
?>

<h1>View TruckerWheeler #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'active',
	),
)); ?>
