<?php
$this->breadcrumbs=array(
	'Trucker Wheelers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TruckerWheeler', 'url'=>array('index')),
	array('label'=>'Manage TruckerWheeler', 'url'=>array('admin')),
);
?>

<h1>Create TruckerWheeler</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>