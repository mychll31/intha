<?php
$this->breadcrumbs=array(
	'Trucker Wheelers'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TruckerWheeler', 'url'=>array('index')),
	array('label'=>'Create TruckerWheeler', 'url'=>array('create')),
	array('label'=>'View TruckerWheeler', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TruckerWheeler', 'url'=>array('admin')),
);
?>

<h1>Update TruckerWheeler <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>