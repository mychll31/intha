<?php
$this->breadcrumbs=array(
	'Trucker Wheelers',
);

$this->menu=array(
	array('label'=>'Create TruckerWheeler', 'url'=>array('create')),
	array('label'=>'Manage TruckerWheeler', 'url'=>array('admin')),
);
?>

<h1>Trucker Wheelers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
