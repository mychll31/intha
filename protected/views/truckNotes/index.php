<?php
$this->breadcrumbs=array(
	'Truck Notes',
);

$this->menu=array(
	array('label'=>'Create TruckNotes', 'url'=>array('create')),
	array('label'=>'Manage TruckNotes', 'url'=>array('admin')),
);
?>

<h1>Truck Notes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
