<?php
$this->breadcrumbs=array(
	'Truck Notes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TruckNotes', 'url'=>array('index')),
	array('label'=>'Create TruckNotes', 'url'=>array('create')),
	array('label'=>'Update TruckNotes', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TruckNotes', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TruckNotes', 'url'=>array('admin')),
);
?>

<h1>View TruckNotes #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'truck_id',
		'type',
		'details',
		'date',
		'reportedby',
		'active',
		'datereported',
		'createdby',
		'datecreatedby',
		'updatedby',
		'dateupdatedby',
	),
)); ?>
