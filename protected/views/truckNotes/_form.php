<p class="note">Fields with <span class="required">*</span> are required.</p>
<div class="formCon">
<div class="formConInner">

<?php 
  $criteria = new CDbCriteria;
  $criteria->compare('active',1);
  $truckervalue =Courses::model()->findByAttributes(array('id'=>$_REQUEST['id']));
  $form=$this->beginWidget('CActiveForm', array(
	'id'=>'truck-notes-form',
	'enableAjaxValidation'=>false,
  )); 
?>
<?php echo $form->hiddenField($model,'truck_id',array('maxlength'=>255,'value'=>$truckervalue->association_truck_id)); ?>
<?php echo $form->error($model,'truck_id'); ?>

  <table>
    <tr>
      <td><?php echo $form->labelEx($model,'type'); ?></td>
      <td><?php echo $form->labelEx($model,'date'); ?></td>
    </tr>
    <tr>
      <td><?php
      		$notestat = CHtml::listData(TruckerTypeNote::model()->findAll($criteria),'id','name');
                echo $form->dropDownList($model,'type',$notestat,array('empty'=>''));
      		echo $form->error($model,'type'); ?></td>
      <td><?php 
          $date = 'yy-mm-dd';
	  $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			    'attribute'=>'date',
			    'model'=>$model,
			    'options'=>array(
				    'showAnim'=>'fold',
				    'dateFormat'=>$date,
				    'changeMonth'=> true,
				    'changeYear'=>true,
				    'yearRange'=>'1950:2050'
				    ),
			    'htmlOptions'=>array(
				    'style' => 'width: 75% !important;',
				    ),
			    ));
	  echo $form->error($model,'date'); ?></td>
    </tr>
    <tr><td colspan=2>&nbsp;</td></tr>
    <tr>
      <td><?php echo $form->labelEx($model,'reportedby'); ?></td>
      <td><?php echo $form->labelEx($model,'datereported'); ?></td>
    </tr>
    <tr>
      <td><?php echo $form->textField($model,'reportedby'); echo $form->error($model,'reportedby'); ?></td>
      <td><?php 
          $date = 'yy-mm-dd';
	  $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			    'attribute'=>'datereported',
			    'model'=>$model,
			    'options'=>array(
				    'showAnim'=>'fold',
				    'dateFormat'=>$date,
				    'changeMonth'=> true,
				    'changeYear'=>true,
				    'yearRange'=>'1950:2050'
				    ),
			    'htmlOptions'=>array(
				    'style' => 'width: 75% !important;',
				    ),
			    ));
	  echo $form->error($model,'datereported'); ?></td>
    </tr>
    <tr><td colspan=2>&nbsp;</td></tr>
    <tr>
      <td colspan=2><?php echo $form->labelEx($model,'details'); ?></td>
    </tr>
    <tr>
      <td colspan=2><?php echo $form->textArea($model,'details'); echo $form->error($model,'details'); ?></td>
    </tr>
    <tr><td colspan=2>&nbsp;</td></tr>
  </table>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'formbut')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

</div>
