<?php $this->breadcrumbs=array(
	'Trucker'=>array('students/students/view','id'=>$_REQUEST['id']),
	'Employees List'=>array('students/students/attentance','id'=>$_REQUEST['id']),
	'Remarks',);
$employee= Employees::model()->findByAttributes(array('association_employee_id'=>$_REQUEST['eid']));
$criteria = new CDbCriteria;
$criteria->compare('emp_no',$_REQUEST['eid']);
$criteria->order = 'dateencoded DESC';
$model= EmpTruckerRemarks::model()->findAll($criteria);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
     <?php $this->renderPartial('profileleft');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <div class="clear"></div>
	<div class="edit_bttns last">
	    <ul>
	    <li><?php echo CHtml::link(Yii::t('students','<span>Back to Employee</span>'), array('students/students/attentance', 'id'=>$_REQUEST['id']),array('class'=>' edit ')); ?></li>
	    </ul>
	</div>
	<h2>
	<?php
	$pref = EmpPrefix::model()->findByAttributes(array('id'=>$employee->prefix));
	if($pref!=NULL){$pname= $pref->name.'. ';}else{$pname='';}
	if($employee->lastname !=null){$lname =$employee->lastname.' ';}else{$lname='';}
	if($employee->firstname !=null){$fname =$employee->firstname.' ';}else{$fname='';}
	if($employee->middlename !=null){$mname =substr($employee->middlename,1,1).'. ';}else{$mname='';}
	echo ucwords($pname.$fname.$mname.$lname);
	?></h2>
	<div class="table_listbx">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr class="listbxtop_hdng">
	<td colspan=3><?php echo Yii::t('students','Employee Remarks');?></td>
	</tr>
	<?php foreach($model as $model){ ?>
	<tr>
	<td class="subhdng_nrmal"><span class="datahead">Date Encoded :</span>
	  <span class="data"><?php echo $model->dateencoded; ?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Date Reminder :</span>
	  <span class="data"><?php echo $model->datereminder; ?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Remarks :</span>
	  <span class="data"><?php 
	  	$rem= EmployeeCaseRemarks::model()->findByAttributes(array('id'=>$model->remarks));
		echo $rem->name; ?>
	  </span></td>
	</tr>
	<tr>
	<td colspan=3 class="subhdng_nrmal"><span class="datahead">Description</span>
	  <span class="data"><pre><?php echo $model->description;?></pre>
		<br><?php
		if($model->modifiedby!=null){
		  $profile= Profiles::model()->findByAttributes(array('user_id'=>$model->modifiedby));
		  echo '<font color="green"> Modified By: <b>'.$profile->firstname.' '.$profile->lastname.'</b>&nbsp;&nbsp;&nbsp;Date: <b>'.$model->datemodified.'</b>&nbsp;';
		}
		?>
		<a href="index.php?r=empTruckerRemarks/update&id=<?php echo $_REQUEST['id']; ?>&eid=<?php echo $model->id; ?> "title='Edit Remarks'><img src="images/edit.png" /></a>
		&nbsp;&nbsp;
		<a href="index.php?r=empTruckerRemarks/view&id=<?php echo $_REQUEST['id']; ?>&eid=<?php echo $model->id; ?> "title='View Attachments'><img src="images/mcb-bg-h.png" /></a>
	  </span></td>
	</tr>
	<?php } ?>
	</table>

    </td>
  </tr>
</table>
 
