<p class="note">Fields with <span class="required">*</span> are required.</p>
<div class="formCon">
<div class="formConInner">
<div class="form">
<?php if($err!=null){ ?>
<div class="errorSummary"><?php echo $err;?></div>
<?php } ?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'emp-trucker-remarks-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<table>
  <tr>
    <td><?php echo $form->labelEx($model,'dateencoded'); ?></td>
    <td><?php echo $form->labelEx($model,'datereminder'); ?></td>
    <td><?php echo $form->labelEx($model,'remarks'); ?></td>
  </tr>
  <tr>
    <td>
    <?php
    $date = 'yy-mm-dd';
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
    	'attribute'=>'dateencoded',
	'model'=>$model,
	'options'=>array(
		'showAnim'=>'fold',
		'dateFormat'=>$date,
		'changeMonth'=> true,
		'changeYear'=>true,
		'yearRange'=>'1950:2050'),
			'htmlOptions'=>array(
				'style' => 'width: 75% !important;',
			),
	));?>
	<?php echo $form->error($model,'dateencoded'); ?>
    </td>
    <td>
    <?php
    $date = 'yy-mm-dd';
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
    	'attribute'=>'datereminder',
	'model'=>$model,
	'options'=>array(
		'showAnim'=>'fold',
		'dateFormat'=>$date,
		'changeMonth'=> true,
		'changeYear'=>true,
		'yearRange'=>'1950:2050'),
			'htmlOptions'=>array(
				'style' => 'width: 75% !important;',
			),
	));?>
    <?php echo $form->error($model,'datereminder'); ?>
    </td>
    <td>
    <?php
    $criteria_a = new CDbCriteria;
    $criteria_a->compare('active',1);
    $rem = CHtml::listData(EmployeeCaseRemarks::model()->findAll($criteria_a),'id','name');
    echo $form->dropDownList($model,'remarks',$rem,array('empty'=>'Select Remarks')); ?>
    <?php echo $form->error($model,'remarks'); ?>
    </td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  <tr>
  <td colspan=3><?php echo $form->labelEx($model,'description'); ?></td>
  </tr>
  <tr>
    <td colspan=3>
		<pre><?php echo $form->textArea($model,'description',array('style'=>'width:97% !important','rows'=>7,'maxlength'=>10000)); ?></pre>
		<?php echo $form->error($model,'description'); ?>
    </td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  <tr>
    <td colspan=3>
    <br>
    <style>
    .addbutton{
	    background-color: rgb(226, 146, 27) !important;
	    color: rgb(97, 80, 12);
	    background-color: oldlace;
	    padding: 0px 5px;
	    border-radius: 30px;
	    font-size: 15pt;
	    border: 1px solid #857710;
	    font-weight: bolder;
    }
   .addbutton:hover{
	background-color: rgb(243, 169, 58) !important;
    }
</style>
<div id="file_container">
<input name="ufile[]" type="file"  /><br />
</div>
<br><a class="addbutton" href="javascript:void(0);" onClick="add_file_field();">+</a><br />
    </td>
  </tr>
</table>
<br>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'formbut')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
</div>
<script>
function add_file_field(){
	var container=document.getElementById('file_container');
	var file_field=document.createElement('input');
	file_field.name='ufile[]';
	file_field.type='file';
	container.appendChild(file_field);
	var br_field=document.createElement('br');
	container.appendChild(br_field);
}
</script>

