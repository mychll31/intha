<?php
$this->breadcrumbs=array(
	'Emp Trucker Remarks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List EmpTruckerRemarks', 'url'=>array('index')),
	array('label'=>'Create EmpTruckerRemarks', 'url'=>array('create')),
	array('label'=>'Update EmpTruckerRemarks', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmpTruckerRemarks', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmpTruckerRemarks', 'url'=>array('admin')),
);
?>

<h1>View EmpTruckerRemarks #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'dateencoded',
		'datereminder',
		'description',
		'remarks',
		'emp_no',
		'createdby',
		'datecreatedby',
		'datemodified',
	),
)); ?>
