<?php
$this->breadcrumbs=array(
	'Emp Trucker Remarks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmpTruckerRemarks', 'url'=>array('index')),
	array('label'=>'Manage EmpTruckerRemarks', 'url'=>array('admin')),
);
?>

<h1>Create EmpTruckerRemarks</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>