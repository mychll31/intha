<?php
$this->breadcrumbs=array(
	'Emp Trucker Remarks',
);

$this->menu=array(
	array('label'=>'Create EmpTruckerRemarks', 'url'=>array('create')),
	array('label'=>'Manage EmpTruckerRemarks', 'url'=>array('admin')),
);
?>

<h1>Emp Trucker Remarks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
