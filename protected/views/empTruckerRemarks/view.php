<?php $this->breadcrumbs=array(
	'Trucker'=>array('students/students/view','id'=>$_REQUEST['id']),
	'Employees List'=>array('students/students/attentance','id'=>$_REQUEST['id']),
	'Remarks'=>array('remarks','id'=>$_REQUEST['id'],'eid'=>$_REQUESt['eid']),
	'View',); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
     <?php $this->renderPartial('profileleft');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <div class="clear"></div>
	<div class="edit_bttns last">
	    <ul>
	    <li><?php echo CHtml::link(Yii::t('students','<span>Back to Employee</span>'), array('students/students/attentance', 'id'=>$_REQUEST['id']),array('class'=>' edit ')); ?></li>
	    <li><?php echo CHtml::link(Yii::t('students','<span>Edit Remarks</span>'), array('update', 'id'=>$_REQUEST['id'],'eid'=>$model->id),array('class'=>' edit ')); ?></li>
	    </ul>
	</div>
	<br><br><br>
	<div class="table_listbx">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr class="listbxtop_hdng">
	<td colspan=3><?php echo Yii::t('students','Employee Remarks');?></td>
	</tr>
	<tr>
	<td class="subhdng_nrmal"><span class="datahead">Date Encoded :</span>
	  <span class="data"><?php echo $model->dateencoded; ?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Date Reminder :</span>
	  <span class="data"><?php echo $model->datereminder; ?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Remarks :</span>
	  <span class="data"><?php 
	  	$rem= EmployeeCaseRemarks::model()->findByAttributes(array('id'=>$model->remarks));
		echo $rem->name; ?>
	  </span></td>
	</tr>
	<tr>
	<td colspan=3 class="subhdng_nrmal"><span class="datahead">Description</span>
	  <span class="data"><pre><?php echo $model->description;?></pre>
	  <br><?php
	  if($model->modifiedby!=null){
		  $profile= Profiles::model()->findByAttributes(array('user_id'=>$model->modifiedby));
		  echo '<font color="green"> Modified By: <b>'.$profile->firstname.' '.$profile->lastname.'</b>&nbsp;&nbsp;&nbsp;Date: <b>'.$model->datemodified.'</b>&nbsp;';
	  }
	  ?>
	  </span></td>
	</tr>
	</table>
	<?php
	$criteria = new CDbCriteria;
	$criteria->compare('rem_id',$_REQUEST['eid']);
	$criteria->order = 'createddate DESC';
	$atts= RemarksEmployeeAttachment::model()->findAll($criteria);
	?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr class="listbxtop_hdng">
	<td colspan=3><?php echo Yii::t('students','Employee Attachments');?></td>
	</tr>
	<?php 
	  $no=1;
	  foreach($atts as $att){ ?>
	<tr>
	<td class="subhdng_nrmal" colspan=2><span class="datahead"><?php echo $no++; ?>.</span>
	  <span class="data"><a href="<?php echo $att->fileloc.'.'.$att->fileext;?>" title="Click to Download">
	  <?php echo $att->fileloc.'.'.$att->fileext;?></a></span></td>
	<td class="subhdng_nrmal"><span class="datahead"></span>
	  <span class="data">
	  <?php
	  echo CHtml::button('Remove',
			  array(
				  'submit'=>array('empTruckerRemarks/remove','id'=>$_REQUEST['id'],'eid'=>$_REQUEST['eid'],'aid'=>$att->id),'confirm'=>'are you sure?','class'=>'remove'
			       )
			  );
	  ?>
	  </span></td>
	</tr>
	<?php } ?>
	</table>

    </td>
  </tr>
</table>
 <style>
 .remove{
  margin: 0px;
  font-size: 12px;
  display: block;
  color: #fff;
  font-weight: bold;
  background: #5F5A5A;
  padding: 2px 7px;
 }
 .remove:hover{
  background: #000 ;
 }
 </style>
