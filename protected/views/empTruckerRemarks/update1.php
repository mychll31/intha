<?php
$this->breadcrumbs=array(
	'Emp Trucker Remarks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmpTruckerRemarks', 'url'=>array('index')),
	array('label'=>'Create EmpTruckerRemarks', 'url'=>array('create')),
	array('label'=>'View EmpTruckerRemarks', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmpTruckerRemarks', 'url'=>array('admin')),
);
?>

<h1>Update EmpTruckerRemarks <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>