<?php

/**
 * This is the model class for table "inland_marine_trucks".
 *
 * The followings are the available columns in table 'inland_marine_trucks':
 * @property integer $id
 * @property string $truck_id
 * @property string $broker_name
 * @property string $subs_date
 * @property string $expire_date
 * @property integer $status
 */
class InlandMarineTrucks extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return InlandMarineTrucks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inland_marine_trucks';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status', 'numerical', 'integerOnly'=>true),
			array('truck_id, broker_name', 'length', 'max'=>225),
			array('subs_date, expire_date', 'safe'),
			array('status,subs_date','required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, truck_id, broker_name, subs_date, expire_date, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'truck_id' => 'Truck',
			'broker_name' => 'Broker Name',
			'subs_date' => 'Date Add',
			'expire_date' => 'Expiration Date',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('truck_id',$this->truck_id,true);
		$criteria->compare('broker_name',$this->broker_name,true);
		$criteria->compare('subs_date',$this->subs_date,true);
		$criteria->compare('expire_date',$this->expire_date,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
