<?php

/**
 * This is the model class for table "remarks_employee_attachment".
 *
 * The followings are the available columns in table 'remarks_employee_attachment':
 * @property integer $id
 * @property integer $rem_id
 * @property string $filename
 * @property string $fileext
 * @property string $fileloc
 * @property integer $createdby
 * @property string $createddate
 */
class RemarksEmployeeAttachment extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return RemarksEmployeeAttachment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'remarks_employee_attachment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rem_id, createdby', 'numerical', 'integerOnly'=>true),
			array('filename', 'length', 'max'=>225),
			array('fileext', 'length', 'max'=>10),
			array('fileloc', 'length', 'max'=>1000),
			array('createddate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, rem_id, filename, fileext, fileloc, createdby, createddate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'rem_id' => 'Rem',
			'filename' => 'Filename',
			'fileext' => 'Fileext',
			'fileloc' => 'Fileloc',
			'createdby' => 'Createdby',
			'createddate' => 'Createddate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('rem_id',$this->rem_id);
		$criteria->compare('filename',$this->filename,true);
		$criteria->compare('fileext',$this->fileext,true);
		$criteria->compare('fileloc',$this->fileloc,true);
		$criteria->compare('createdby',$this->createdby);
		$criteria->compare('createddate',$this->createddate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}