<?php

/**
 * This is the model class for table "inland_marine_trucker".
 *
 * The followings are the available columns in table 'inland_marine_trucker':
 * @property integer $id
 * @property string $trucker_id
 * @property string $policy_number
 * @property integer $broker_name
 * @property string $subs_date
 * @property string $expire_date
 * @property integer $status
 */
class InlandMarineTrucker extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return InlandMarineTrucker the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inland_marine_trucker';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('broker_name, status', 'numerical', 'integerOnly'=>true),
			array('trucker_id', 'length', 'max'=>225),
			array('policy_number', 'length', 'max'=>255),
			array('subs_date, expire_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, trucker_id, policy_number, broker_name, subs_date, expire_date, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'trucker_id' => 'Trucker',
			'policy_number' => 'Policy Number',
			'broker_name' => 'Broker Name',
			'subs_date' => 'Subscription Date',
			'expire_date' => 'Expiration Date',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('trucker_id',$this->trucker_id,true);
		$criteria->compare('policy_number',$this->policy_number,true);
		$criteria->compare('broker_name',$this->broker_name);
		$criteria->compare('subs_date',$this->subs_date,true);
		$criteria->compare('expire_date',$this->expire_date,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}