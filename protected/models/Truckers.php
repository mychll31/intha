<?php

/**
 * This is the model class for table "truckers".
 *
 * The followings are the available columns in table 'truckers':
 * @property integer $id
 * @property integer $association_trucker_id
 * @property string $date_of_association_membership
 * @property string $expiration_of_association_membership
 * @property string $trucker
 * @property integer $trucker_status_id
 * @property integer $business_type_id
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $nick_name
 * @property string $prefix_name
 * @property string $business_registration
 * @property string $mayors_permit
 * @property integer $sss
 * @property integer $philhealth
 * @property integer $pagibig
 * @property integer $bir_tin
 * @property integer $telephone_number
 * @property integer $fax_number
 * @property string $website
 * @property string $email_address
 */
class Truckers extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Truckers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'truckers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('association_trucker_id, trucker_status_id, business_type_id, sss, philhealth, pagibig, bir_tin, telephone_number, fax_number', 'numerical', 'integerOnly'=>true),
			array('trucker, first_name, last_name, middle_name, nick_name, website, email_address', 'length', 'max'=>255),
			array('prefix_name', 'length', 'max'=>10),
			array('date_of_association_membership, expiration_of_association_membership, business_registration, mayors_permit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, association_trucker_id, date_of_association_membership, expiration_of_association_membership, trucker, trucker_status_id, business_type_id, first_name, last_name, middle_name, nick_name, prefix_name, business_registration, mayors_permit, sss, philhealth, pagibig, bir_tin, telephone_number, fax_number, website, email_address', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'association_trucker_id' => 'Association Trucker',
			'date_of_association_membership' => 'Date Of Association Membership',
			'expiration_of_association_membership' => 'Expiration Of Association Membership',
			'trucker' => 'Trucker',
			'trucker_status_id' => 'Trucker Status',
			'business_type_id' => 'Business Type',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'middle_name' => 'Middle Name',
			'nick_name' => 'Nick Name',
			'prefix_name' => 'Prefix Name',
			'business_registration' => 'Business Registration',
			'mayors_permit' => 'Mayors Permit',
			'sss' => 'Sss',
			'philhealth' => 'Philhealth',
			'pagibig' => 'Pagibig',
			'bir_tin' => 'Bir Tin',
			'telephone_number' => 'Telephone Number',
			'fax_number' => 'Fax Number',
			'website' => 'Website',
			'email_address' => 'Email Address',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('association_trucker_id',$this->association_trucker_id);
		$criteria->compare('date_of_association_membership',$this->date_of_association_membership,true);
		$criteria->compare('expiration_of_association_membership',$this->expiration_of_association_membership,true);
		$criteria->compare('trucker',$this->trucker,true);
		$criteria->compare('trucker_status_id',$this->trucker_status_id);
		$criteria->compare('business_type_id',$this->business_type_id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('nick_name',$this->nick_name,true);
		$criteria->compare('prefix_name',$this->prefix_name,true);
		$criteria->compare('business_registration',$this->business_registration,true);
		$criteria->compare('mayors_permit',$this->mayors_permit,true);
		$criteria->compare('sss',$this->sss);
		$criteria->compare('philhealth',$this->philhealth);
		$criteria->compare('pagibig',$this->pagibig);
		$criteria->compare('bir_tin',$this->bir_tin);
		$criteria->compare('telephone_number',$this->telephone_number);
		$criteria->compare('fax_number',$this->fax_number);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('email_address',$this->email_address,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}