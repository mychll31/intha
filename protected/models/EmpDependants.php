<?php

/**
 * This is the model class for table "emp_dependants".
 *
 * The followings are the available columns in table 'emp_dependants':
 * @property integer $id
 * @property integer $emp_id
 * @property string $dependents
 * @property integer $pref
 * @property string $fname
 * @property string $lname
 * @property string $mname
 * @property string $ext
 * @property string $bdate
 */
class EmpDependants extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return EmpDependants the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'emp_dependants';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_id, pref', 'numerical', 'integerOnly'=>true),
			array('dependents', 'length', 'max'=>225),
			array('fname, lname, mname', 'length', 'max'=>255),
			array('ext', 'length', 'max'=>100),
			array('bdate', 'safe'),
			array('emp_id, fname, lname, mname, bdate','required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, emp_id, dependents, pref, fname, lname, mname, ext, bdate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'emp_id' => 'Emp',
			'dependents' => 'Relation',
			'pref' => 'Prefix',
			'fname' => 'Firstname',
			'lname' => 'Lastname',
			'mname' => 'Middlename',
			'ext' => 'Extension',
			'bdate' => 'Birthdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('emp_id',$this->emp_id);
		$criteria->compare('dependents',$this->dependents,true);
		$criteria->compare('pref',$this->pref);
		$criteria->compare('fname',$this->fname,true);
		$criteria->compare('lname',$this->lname,true);
		$criteria->compare('mname',$this->mname,true);
		$criteria->compare('ext',$this->ext,true);
		$criteria->compare('bdate',$this->bdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}