<?php

/**
 * This is the model class for table "mailbox_conversation".
 *
 * The followings are the available columns in table 'mailbox_conversation':
 * @property string $conversation_id
 * @property integer $initiator_id
 * @property integer $interlocutor_id
 * @property string $subject
 * @property integer $bm_read
 * @property integer $bm_deleted
 * @property string $modified
 * @property string $is_system
 * @property integer $initiator_del
 * @property integer $interlocutor_del
 */
class MailboxConversation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return MailboxConversation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mailbox_conversation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('initiator_id, interlocutor_id, modified', 'required'),
			array('initiator_id, interlocutor_id, bm_read, bm_deleted, initiator_del, interlocutor_del', 'numerical', 'integerOnly'=>true),
			array('subject', 'length', 'max'=>100),
			array('modified', 'length', 'max'=>10),
			array('is_system', 'length', 'max'=>3),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('conversation_id, initiator_id, interlocutor_id, subject, bm_read, bm_deleted, modified, is_system, initiator_del, interlocutor_del', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'conversation_id' => 'Conversation',
			'initiator_id' => 'Initiator',
			'interlocutor_id' => 'Interlocutor',
			'subject' => 'Subject',
			'bm_read' => 'Bm Read',
			'bm_deleted' => 'Bm Deleted',
			'modified' => 'Modified',
			'is_system' => 'Is System',
			'initiator_del' => 'Initiator Del',
			'interlocutor_del' => 'Interlocutor Del',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('conversation_id',$this->conversation_id,true);
		$criteria->compare('initiator_id',$this->initiator_id);
		$criteria->compare('interlocutor_id',$this->interlocutor_id);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('bm_read',$this->bm_read);
		$criteria->compare('bm_deleted',$this->bm_deleted);
		$criteria->compare('modified',$this->modified,true);
		$criteria->compare('is_system',$this->is_system,true);
		$criteria->compare('initiator_del',$this->initiator_del);
		$criteria->compare('interlocutor_del',$this->interlocutor_del);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}