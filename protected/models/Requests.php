<?php

/**
 * This is the model class for table "requests".
 *
 * The followings are the available columns in table 'requests':
 * @property integer $id
 * @property string $date
 * @property integer $trucker_id
 * @property integer $requestor
 * @property integer $type_request
 */
class Requests extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Requests the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'requests';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			#array('trucker_id, requestor, type_request', 'numerical', 'integerOnly'=>true),
			array('date', 'safe'),
			#array('trucker_id, requestor, type_request,date','required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, date, trucker_id, requestor, type_request,remarks', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'trucker_id' => 'Trucker',
			'requestor' => 'Requestor',
			'type_request' => 'Type of request',
			'notes' => 'Notes',
			'remarks' => 'Remarks',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('trucker_id',$this->trucker_id);
		$criteria->compare('requestor',$this->requestor);
		$criteria->compare('type_request',$this->type_request);
		$criteria->compare('remarks',$this->remarks);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}