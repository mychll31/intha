<?php

/**
 * This is the model class for table "emp_assocxp".
 *
 * The followings are the available columns in table 'emp_assocxp':
 * @property integer $id
 * @property integer $employeetable_id
 * @property string $assoc_truckerid
 * @property string $emp_id
 * @property string $inc_date
 * @property string $exc_date
 * @property string $reason_forleaving
 * @property string $emp_number
 * @property integer $current
 */
class EmpAssocxp extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return EmpAssocxp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'emp_assocxp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('employeetable_id, current', 'numerical', 'integerOnly'=>true),
			array('assoc_truckerid', 'length', 'max'=>22),
			array('emp_id, emp_number', 'length', 'max'=>200),
			array('reason_forleaving', 'length', 'max'=>10000),
			array('assoc_truckerid,emp_id,inc_date,emp_status', 'required'),
			array('inc_date, exc_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, emp_status, position,employeetable_id, assoc_truckerid, emp_id, inc_date, exc_date, reason_forleaving, emp_number, current', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'employeetable_id' => 'Employeetable',
			'assoc_truckerid' => 'Trucker',
			'emp_id' => 'Employee ID',
			'inc_date' => 'Inclusive Date',
			'exc_date' => 'Exclusive Date',
			'reason_forleaving' => 'Reason for Leaving',
			'emp_number' => 'Employee Number',
			'current' => 'Current',
			'emp_status' => 'Employment Status',
			'position' => 'Position',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('employeetable_id',$this->employeetable_id);
		$criteria->compare('assoc_truckerid',$this->assoc_truckerid,true);
		$criteria->compare('emp_id',$this->emp_id,true);
		$criteria->compare('inc_date',$this->inc_date,true);
		$criteria->compare('exc_date',$this->exc_date,true);
		$criteria->compare('reason_forleaving',$this->reason_forleaving,true);
		$criteria->compare('emp_number',$this->emp_number,true);
		$criteria->compare('current',$this->current);
		$criteria->compare('emp_status',$this->emp_status);
		$criteria->compare('position',$this->position);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
