<?php

/**
 * This is the model class for table "truck_trailer".
 *
 * The followings are the available columns in table 'truck_trailer':
 * @property integer $id
 * @property integer $trailer_type
 * @property integer $trailer_plate_number
 * @property integer $trailer_footer
 * @property integer $trailer_axle
 * @property string $trailer_chasis_number
 * @property string $trailer_chasis_serial_number
 * @property string $trailer_registered_owner
 * @property string $mv_file_number
 * @property integer $cr_number
 * @property string $cr_date_issued
 * @property string $cr_registered_owner
 * @property integer $or_number
 * @property string $or_date_issued
 * @property string $or_date_expire
 * @property string $lto_field_office
 * @property string $truckerid
 * @property integer $active
 */
class TruckTrailer extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return TruckTrailer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'truck_trailer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('trailer_type, trailer_plate_number, trailer_footer, trailer_axle, cr_number, or_number, active', 'numerical', 'integerOnly'=>true),
			array('trailer_chasis_number, trailer_chasis_serial_number, trailer_registered_owner, mv_file_number, cr_registered_owner', 'length', 'max'=>255),
			array('lto_field_office', 'length', 'max'=>1000),
			array('trailer_plate_number', 'unique'),
			array('truckerid,trailer_plate_number', 'required'),
			array('cr_date_issued, or_date_issued, or_date_expire', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, trailer_type, trailer_plate_number, trailer_footer, trailer_axle, trailer_chasis_number, trailer_chasis_serial_number, trailer_registered_owner, mv_file_number, cr_number, cr_date_issued, cr_registered_owner, or_number, or_date_issued, or_date_expire, lto_field_office, truckerid, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'trailer_type' => 'Type',
			'trailer_plate_number' => 'Plate Number',
			'trailer_footer' => 'Footer',
			'trailer_axle' => 'Axle',
			'trailer_chasis_number' => 'Chasis Number',
			'trailer_chasis_serial_number' => 'Chasis Serial Number',
			'trailer_registered_owner' => 'Registered Owner',
			'mv_file_number' => 'Mv File Number',
			'cr_number' => 'CR Number',
			'cr_date_issued' => 'CR Date Issued',
			'cr_registered_owner' => 'CR Registered Owner',
			'OR_number' => 'OR Number',
			'or_date_issued' => 'OR Date Issued',
			'or_date_expire' => 'OR Date Expire',
			'lto_field_office' => 'Lto Field Office',
			'truckerid' => 'Trucker',
			'active' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('trailer_type',$this->trailer_type);
		$criteria->compare('trailer_plate_number',$this->trailer_plate_number);
		$criteria->compare('trailer_footer',$this->trailer_footer);
		$criteria->compare('trailer_axle',$this->trailer_axle);
		$criteria->compare('trailer_chasis_number',$this->trailer_chasis_number,true);
		$criteria->compare('trailer_chasis_serial_number',$this->trailer_chasis_serial_number,true);
		$criteria->compare('trailer_registered_owner',$this->trailer_registered_owner,true);
		$criteria->compare('mv_file_number',$this->mv_file_number,true);
		$criteria->compare('cr_number',$this->cr_number);
		$criteria->compare('cr_date_issued',$this->cr_date_issued,true);
		$criteria->compare('cr_registered_owner',$this->cr_registered_owner,true);
		$criteria->compare('or_number',$this->or_number);
		$criteria->compare('or_date_issued',$this->or_date_issued,true);
		$criteria->compare('or_date_expire',$this->or_date_expire,true);
		$criteria->compare('lto_field_office',$this->lto_field_office,true);
		$criteria->compare('truckerid',$this->truckerid);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
