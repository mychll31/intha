<?php

/**
 * This is the model class for table "students".
 *
 * The followings are the available columns in table 'students':
 * @property integer $id
 * @property string $assoc_truckerid
 * @property string $membership_date
 * @property string $trucker
 * @property string $owner
 * @property integer $batch_id
 * @property string $membership_expiration
 * @property string $trucker_status
 * @property string $buss_type
 * @property string $website
 * @property string $religion
 * @property string $buss_reg
 * @property string $mayors_permit
 * @property string $sss
 * @property string $ph
 * @property string $pagibig
 * @property integer $bir
 * @property string $tel_no
 * @property string $fax
 * @property string $email
 * @property integer $is_sms_enabled
 * @property string $photo_file_name
 * @property string $photo_content_type
 * @property string $photo_data
 * @property string $status_description
 * @property integer $is_active
 * @property integer $is_deleted
 * @property string $created_at
 * @property string $updated_at
 * @property integer $has_paid_fees
 * @property integer $photo_file_size
 */
 
class Students extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Students the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public $status;
	public $dobrange;
	public $admissionrange;
	public $task_type;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'students';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('buss_add_zip, garage_zip, bir, is_sms_enabled, is_active, is_deleted, has_paid_fees, photo_file_size, tel_no, fax', 'numerical', 'integerOnly'=>true),
			array('assoc_truckerid, trucker, owner, trucker_status, buss_nature, tel_no, email, buss_type, buss_add_add, membership_date, membership_expiration,buss_reg,mayors_permit,tel_no,email,rep_lname,rep_fname,rep_desig,rep_email,rep_mobile,alter_fname,alter_lname,alter_desig,alter_email,alter_mobile', 'required',),
			array('assoc_truckerid','unique'),
			array('email','check'),
			array('assoc_truckerid, trucker, owner, trucker_status, buss_type, website, buss_reg, mayors_permit, sss, ph, email, photo_file_name, photo_content_type, status_description', 'length', 'max'=>255),
			array('membership_date, membership_expiration, created_at, updated_at', 'safe'),			
			array('email,rep_email,alter_email','email'),
			/*
			array(
				'membership_expiration',
				'compare',
				'compareAttribute'=>'created_at',
				'operator'=>'>', 
				'allowEmpty'=>false , 
				'message'=>'{attribute} must be less than "{compareValue}".'
			  ),
			*/
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('photo_data', 'file', 'types'=>'jpg, gif, png', 'allowEmpty' => true),
			array('id, membership_date, trucker, owner, batch_id, membership_expiration, trucker_status, buss_type, website, buss_reg, mayors_permit, sss, ph, pagibig, bir, tel_no, fax, email, is_sms_enabled, photo_file_name, photo_content_type, photo_data, status_description, is_active, is_deleted, created_at, updated_at, has_paid_fees, photo_file_size', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	
	public function check($attribute,$params)
    {
		if(Yii::app()->controller->action->id!='update' and $this->$attribute!='')
		{
		$validate = User::model()->findByAttributes(array('email'=>$this->$attribute));
		if($validate!=NULL)
		{
        
            $this->addError($attribute,'Email allready in use');
		}
		}
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'assoc_truckerid' => 'Association Trucker ID',
			'membership_date' => 'Membership Date',
			'trucker' => 'Trucker',
			'owner' => 'Owner/ CEO/ GM',
			'batch_id' => 'Batch',
			'membership_expiration' => 'Membership Expiration',
			'trucker_status' => 'Trucker Status',
			'buss_type' => 'Business Type',
			'website' => 'Website',
			'buss_nature' => 'Nature of Business',
			'buss_reg' => 'Business Registration',
			'mayors_permit' => 'Mayors Permit',
			'sss' => 'SSS',
			'ph' => 'Philhealth',
			'pagibig' => 'Pagibig',
			'bir' => 'BIR TIN',
			'tel_no' => 'Telephone Number',
			'fax' => 'Fax Number',
			'email' => 'Email Address',
			'is_sms_enabled' => 'Is Sms Enabled',
			'photo_file_name' => 'Photo File Name',
			'photo_content_type' => 'Photo Content Type',
			'photo_data' => 'Photo Data',
			'status_description' => 'Status Description',
			'is_active' => 'Is Active',
			'is_deleted' => 'Is Deleted',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'has_paid_fees' => 'Has Paid Fees',
			'photo_file_size' => 'Photo File Size',
			'buss_add_prov' => 'Province/State',
			'buss_add_add' => 'St.no./St.Address/Brgy/Town',
			'buss_add_city' => 'City/Municipality',
			'buss_add_zip' => 'Zip Code',
			'alter_lname'=>'Last Name',
			'alter_fname'=>'First Name',
			'alter_mname'=>'Middle Name',
			'alter_desig'=>'Designation',
			'alter_telno'=>'Telephone No.',
			'alter_email'=>'Email Address',
			'alter_bdate'=>'Birthday',
			'alter_fax'=>'Fax No.',
			'alter_mobile'=>'Mobile No.',
			'rep_lname'=>'Last Name',
			'rep_fname'=>'First Name',
			'rep_mname'=>'Middle Name',
			'rep_desig'=>'Designation',
			'rep_telno'=>'Telephone No.',
			'rep_email'=>'Email Address',
			'rep_bdate'=>'Birthday',
			'rep_fax'=>'Fax No.',
			'rep_mobile'=>'Mobile No.',
			'garage' => 'Garage',
			'teus' => 'TEUs',
			'tons' => 'Tons',
			'Cases' => 'Cases',
			'garage_prov' => 'Province/State',
			'garage_add' => 'Street No./St. Address/Baranggay/Town',
			'garage_city' => 'City/Municipality',
			'garage_zip' => 'Zip Code',
			'garage_other_assoc' => 'Other Association',
			'o_specify' => 'Others (Specify)',
			'reg_owner'=>'Registered Owner',
			'reg_start_date'=>'Registration Start Date',
			'reg_end_date'=>'Registration End Date',
			'reg_end_date_present'=>'Present',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('membership_date',$this->membership_date,true);
		$criteria->compare('trucker',$this->trucker,true);
		$criteria->compare('owner',$this->owner,true);
		$criteria->compare('membership_expiration',$this->membership_expiration,true);
		$criteria->compare('trucker_status',$this->trucker_status,true);
		$criteria->compare('buss_type',$this->buss_type,true);
		$criteria->compare('nationality_id',$this->nationality_id);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('buss_reg',$this->buss_reg,true);
		$criteria->compare('mayors_permit',$this->mayors_permit,true);
		$criteria->compare('sss',$this->sss,true);
		$criteria->compare('ph',$this->ph,true);
		$criteria->compare('pagibig',$this->pagibig,true);
		$criteria->compare('bir',$this->bir);
		$criteria->compare('tel_no',$this->tel_no,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('is_sms_enabled',$this->is_sms_enabled);
		$criteria->compare('photo_file_name',$this->photo_file_name,true);
		$criteria->compare('photo_content_type',$this->photo_content_type,true);
		$criteria->compare('photo_data',$this->photo_data,true);
		$criteria->compare('status_description',$this->status_description,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('is_deleted',$this->is_deleted);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('has_paid_fees',$this->has_paid_fees);
		$criteria->compare('photo_file_size',$this->photo_file_size);
		$criteria->compare('reg_owner',$this->reg_owner);
		$criteria->compare('reg_start_date',$this->reg_start_date);
		$criteria->compare('reg_end_date',$this->reg_end_date);
		$criteria->compare('reg_end_date_present',$this->reg_end_date_present);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getval()
	{
		return '"123"';
	}
	
	public function getFullname()
	{
	
		return '</td><td  style="padding:0 0 0 20px;" >'.CHtml::link($this->trucker, array('/students/students/view', 'id'=>$this->id)).'
								   </td><td  style="padding:0 0 0 20px;">'.$this->membership_date.'</td>'.
								 '</tr>';
									 
	}
	public function getStudentname()
	{
		#return ucfirst($this->trucker).' '.ucfirst($this->middle_name).' '.ucfirst($this->owner);
		return ucfirst($this->trucker);
	}
	
	
	
}
