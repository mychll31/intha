<?php

/**
 * This is the model class for table "employees_remarks".
 *
 * The followings are the available columns in table 'employees_remarks':
 * @property integer $id
 * @property string $dateencoded
 * @property string $datereminder
 * @property string $description
 * @property integer $remarks
 * @property string $emp_no
 * @property string $createdby
 * @property string $datecreatedby
 * @property string $datemodified
 */
class EmployeesRemarks extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return EmployeesRemarks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'employees_remarks';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('remarks', 'numerical', 'integerOnly'=>true),
			array('description', 'length', 'max'=>10000),
			array('emp_no, createdby', 'length', 'max'=>255),
			array('dateencoded, datereminder, datecreatedby, datemodified', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, dateencoded, datereminder, description, remarks, emp_no, createdby, datecreatedby, datemodified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dateencoded' => 'Dateencoded',
			'datereminder' => 'Datereminder',
			'description' => 'Description',
			'remarks' => 'Remarks',
			'emp_no' => 'Emp No',
			'createdby' => 'Createdby',
			'datecreatedby' => 'Datecreatedby',
			'datemodified' => 'Datemodified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dateencoded',$this->dateencoded,true);
		$criteria->compare('datereminder',$this->datereminder,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('remarks',$this->remarks);
		$criteria->compare('emp_no',$this->emp_no,true);
		$criteria->compare('createdby',$this->createdby,true);
		$criteria->compare('datecreatedby',$this->datecreatedby,true);
		$criteria->compare('datemodified',$this->datemodified,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}