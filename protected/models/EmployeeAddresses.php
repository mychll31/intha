<?php

/**
 * This is the model class for table "employee_addresses".
 *
 * The followings are the available columns in table 'employee_addresses':
 * @property integer $id
 * @property integer $employee_id
 * @property string $house_no
 * @property string $town
 * @property string $city
 * @property string $province
 * @property integer $active
 */
class EmployeeAddresses extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return EmployeeAddresses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'employee_addresses';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('employee_id, active,zipcode', 'numerical', 'integerOnly'=>true),
			array('house_no, town, city, province', 'length', 'max'=>225),
			array('town,city,province','required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, employee_id, house_no, town, city, province, active, zipcode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'employee_id' => 'Employee',
			'house_no' => 'Street No./ Street Address',
			'town' => 'Town',
			'city' => 'City',
			'province' => 'Province',
			'active' => 'Active',
			'zipcode' => 'Zip Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('employee_id',$this->employee_id);
		$criteria->compare('house_no',$this->house_no,true);
		$criteria->compare('town',$this->town,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('province',$this->province,true);
		$criteria->compare('zipcode',$this->zipcode);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}