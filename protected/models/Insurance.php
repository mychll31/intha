<?php

/**
 * This is the model class for table "insurance".
 *
 * The followings are the available columns in table 'insurance':
 * @property integer $id
 * @property string $ctpl_broker
 * @property string $ctpl_insurer
 * @property string $ctpl_policy_no
 * @property string $ctpl_coverage
 * @property string $ctpl_dateissued
 * @property string $ctpl_dateexpire
 * @property string $compre_broker
 * @property string $compre_insurer
 * @property string $compre_policy_number
 * @property string $compre_coverage
 * @property string $compre_dateissued
 * @property string $compre_dateexpire
 * @property string $pa_broker
 * @property string $pa_insurer
 * @property string $pa_policy_no
 * @property string $pa_coverage
 * @property string $pa_dateissued
 * @property string $pa_dateexpire
 * @property string $truck_id
 * @property string $trucker_id
 * @property integer $active
 */
class Insurance extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Insurance the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'insurance';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('active', 'numerical', 'integerOnly'=>true),
			array('ctpl_broker', 'length', 'max'=>225),
			array('ctpl_insurer, ctpl_policy_no, ctpl_coverage, compre_broker, compre_insurer, compre_policy_number, compre_coverage, pa_broker, pa_insurer, pa_policy_no, pa_coverage, truck_id, trucker_id', 'length', 'max'=>255),
			array('ctpl_dateissued, ctpl_dateexpire, compre_dateissued, compre_dateexpire, pa_dateissued, pa_dateexpire', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, ctpl_broker, ctpl_insurer, ctpl_policy_no, ctpl_coverage, ctpl_dateissued, ctpl_dateexpire, compre_broker, compre_insurer, compre_policy_number, compre_coverage, compre_dateissued, compre_dateexpire, pa_broker, pa_insurer, pa_policy_no, pa_coverage, pa_dateissued, pa_dateexpire, truck_id, trucker_id, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ctpl_broker'=>'Insurance Broker/ Agent',
			'ctpl_insurer'=>'Insurer',
			'ctpl_policy_no'=>'Policy Number',
			'ctpl_coverage'=>'Coverage',
			'ctpl_dateissued'=>'Date Issued',
			'ctpl_dateexpire'=>'Date Expiry',
			'compre_broker'=>'Insurance Broker/ Agent',
			'compre_insurer'=>'Insurer',
			'compre_policy_number'=>'Policy Number',
			'compre_coverage'=>'Coverage',
			'compre_dateissued'=>'Date Issued',
			'compre_dateexpire'=>'Date Expiry',
			'pa_broker'=>'Insurance Broker/ Agent',
			'pa_insurer'=>'Insurer',
			'pa_policy_no'=>'Policy Number',
			'pa_coverage'=>'Coverage',
			'pa_dateissued'=>'Date Issued',
			'pa_dateexpire'=>'Date Expiry',
			'truck_id' => 'Truck',
			'trucker_id' => 'Trucker',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ctpl_broker',$this->ctpl_broker,true);
		$criteria->compare('ctpl_insurer',$this->ctpl_insurer,true);
		$criteria->compare('ctpl_policy_no',$this->ctpl_policy_no,true);
		$criteria->compare('ctpl_coverage',$this->ctpl_coverage,true);
		$criteria->compare('ctpl_dateissued',$this->ctpl_dateissued,true);
		$criteria->compare('ctpl_dateexpire',$this->ctpl_dateexpire,true);
		$criteria->compare('compre_broker',$this->compre_broker,true);
		$criteria->compare('compre_insurer',$this->compre_insurer,true);
		$criteria->compare('compre_policy_number',$this->compre_policy_number,true);
		$criteria->compare('compre_coverage',$this->compre_coverage,true);
		$criteria->compare('compre_dateissued',$this->compre_dateissued,true);
		$criteria->compare('compre_dateexpire',$this->compre_dateexpire,true);
		$criteria->compare('pa_broker',$this->pa_broker,true);
		$criteria->compare('pa_insurer',$this->pa_insurer,true);
		$criteria->compare('pa_policy_no',$this->pa_policy_no,true);
		$criteria->compare('pa_coverage',$this->pa_coverage,true);
		$criteria->compare('pa_dateissued',$this->pa_dateissued,true);
		$criteria->compare('pa_dateexpire',$this->pa_dateexpire,true);
		$criteria->compare('truck_id',$this->truck_id,true);
		$criteria->compare('trucker_id',$this->trucker_id,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}