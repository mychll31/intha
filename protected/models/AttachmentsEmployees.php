<?php

/**
 * This is the model class for table "attachments_employees".
 *
 * The followings are the available columns in table 'attachments_employees':
 * @property integer $id
 * @property integer $truckers_id
 * @property string $filename
 * @property string $fileext
 * @property string $fileloc
 * @property string $modified_date
 * @property integer $deleted
 */
class AttachmentsEmployees extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return AttachmentsEmployees the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'attachments_employees';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('truckers_id, deleted', 'numerical', 'integerOnly'=>true),
			array('filename', 'length', 'max'=>225),
			array('fileext', 'length', 'max'=>10),
			array('fileloc', 'length', 'max'=>1000),
			array('modified_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, truckers_id, filename, fileext, fileloc, modified_date, deleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'truckers_id' => 'Truckers',
			'filename' => 'Filename',
			'fileext' => 'Fileext',
			'fileloc' => 'Fileloc',
			'modified_date' => 'Modified Date',
			'deleted' => 'Deleted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('truckers_id',$this->truckers_id);
		$criteria->compare('filename',$this->filename,true);
		$criteria->compare('fileext',$this->fileext,true);
		$criteria->compare('fileloc',$this->fileloc,true);
		$criteria->compare('modified_date',$this->modified_date,true);
		$criteria->compare('deleted',$this->deleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}