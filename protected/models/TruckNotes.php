<?php

/**
 * This is the model class for table "truck_notes".
 *
 * The followings are the available columns in table 'truck_notes':
 * @property integer $id
 * @property string $truck_id
 * @property integer $type
 * @property string $details
 * @property string $date
 * @property string $reportedby
 * @property integer $active
 * @property string $datereported
 * @property integer $createdby
 * @property string $datecreatedby
 * @property integer $updatedby
 * @property string $dateupdatedby
 */
class TruckNotes extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return TruckNotes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'truck_notes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, active, createdby, updatedby', 'numerical', 'integerOnly'=>true),
			array('truck_id, reportedby', 'length', 'max'=>255),
			array('details', 'length', 'max'=>10000),
			array('date, datereported, datecreatedby, dateupdatedby', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, truck_id, type, details, date, reportedby, active, datereported, createdby, datecreatedby, updatedby, dateupdatedby', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'truck_id' => 'Truck',
			'type' => 'Type',
			'details' => 'Details',
			'date' => 'Date',
			'reportedby' => 'Reported by',
			'active' => 'Active',
			'datereported' => 'Date Reported',
			'createdby' => 'Created by',
			'datecreatedby' => 'Date Createdby',
			'updatedby' => 'Updated by',
			'dateupdatedby' => 'Date Updated by',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('truck_id',$this->truck_id,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('details',$this->details,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('reportedby',$this->reportedby,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('datereported',$this->datereported,true);
		$criteria->compare('createdby',$this->createdby);
		$criteria->compare('datecreatedby',$this->datecreatedby,true);
		$criteria->compare('updatedby',$this->updatedby);
		$criteria->compare('dateupdatedby',$this->dateupdatedby,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
