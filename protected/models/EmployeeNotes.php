<?php

/**
 * This is the model class for table "employee_notes".
 *
 * The followings are the available columns in table 'employee_notes':
 * @property integer $id
 * @property integer $emp_id
 * @property integer $type
 * @property string $details
 * @property string $date_ofnote
 * @property string $reportedby
 * @property string $datereportedby
 */
class EmployeeNotes extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return EmployeeNotes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'employee_notes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_id, type', 'numerical', 'integerOnly'=>true),
			array('details', 'length', 'max'=>10000),
			array('reportedby', 'length', 'max'=>500),
			array('date_ofnote, datereportedby', 'safe'),
			array('emp_id,type,date_ofnote,reportedby,details,datereportedby','required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, emp_id, type, details, date_ofnote, reportedby, datereportedby', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'emp_id' => 'Emp_id',
			'type' => 'Type of Note',
			'details' => 'Details of Note',
			'date_ofnote' => 'Date of Note',
			'reportedby' => 'Reported by',
			'datereportedby' => 'Date Reported by',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('emp_id',$this->emp_id);
		$criteria->compare('type',$this->type);
		$criteria->compare('details',$this->details,true);
		$criteria->compare('date_ofnote',$this->date_ofnote,true);
		$criteria->compare('reportedby',$this->reportedby,true);
		$criteria->compare('datereportedby',$this->datereportedby,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}