<?php

/**
 * This is the model class for table "employees".
 *
 * The followings are the available columns in table 'employees':
 * @property integer $id
 * @property integer $employee_category_id
 * @property string $employee_number
 * @property string $joining_date
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property integer $gender
 * @property string $job_title
 * @property integer $employee_position_id
 * @property integer $employee_department_id
 * @property integer $reporting_manager_id
 * @property integer $employee_grade_id
 * @property string $qualification
 * @property string $experience_detail
 * @property integer $experience_year
 * @property integer $experience_month
 * @property integer $status
 * @property string $status_description
 * @property string $date_of_birth
 * @property string $marital_status
 * @property integer $children_count
 * @property string $father_name
 * @property string $mother_name
 * @property string $husband_name
 * @property string $blood_group
 * @property integer $nationality_id
 * @property string $home_address_line1
 * @property string $home_address_line2
 * @property string $home_city
 * @property string $home_state
 * @property integer $home_country_id
 * @property string $home_pin_code
 * @property string $office_address_line1
 * @property string $office_address_line2
 * @property string $office_city
 * @property string $office_state
 * @property integer $office_country_id
 * @property string $office_pin_code
 * @property string $office_phone1
 * @property string $office_phone2
 * @property string $mobile_phone
 * @property string $home_phone
 * @property string $email
 * @property string $fax
 * @property string $photo_file_name
 * @property string $photo_content_type
 * @property string $photo_data
 * @property string $created_at
 * @property string $updated_at
 * @property integer $photo_file_size
 * @property integer $user_id
 */
class Employees extends CActiveRecord
{
	
	public $status;
	public $dobrange;
	public $joinrange;
	public $total_experience;
	/**
	 * Returns the static model of the specified AR class.
	 * @return Employees the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'employees';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		if(Yii::app()->controller->action->id=='create2' or Yii::app()->controller->action->id=='update2'){
			return array(
				#array('e_name,e_rel,e_add,e_contact,e_city,e_state','required'),
				array('e_contact,e_other_con,present_zipcode,per_zipcode,home_telephone,mobile,work_tel','numerical', 'integerOnly'=>true),
				#array('home_pin_code,office_pin_code','length', 'max'=>6),
				array('e_email,other_email,work_email','email'),
			);
		}
		if(Yii::app()->controller->action->id=='create3' or Yii::app()->controller->action->id=='update3'){
			return array(
				#array('elem_school,elem_indate,elem_exdate,hs_school,hs_indate,hs_exdate','required'),
				#array('e_contact,e_other_con,present_zipcode,per_zipcode,home_telephone,mobile,work_tel','numerical', 'integerOnly'=>true),
				#array('home_pin_code,office_pin_code','length', 'max'=>6),
				#array('e_email,other_email,work_email','email'),
			);
		}
		if(Yii::app()->controller->action->id=='create4' or Yii::app()->controller->action->id=='update4'){
			return array(
				#array('trucker_id,super_sub,method, dept, post, start_emp, end_emp, active, emp_stat, pay_period, salary, pay_type, basic_pay, e_cola, min_wage, region','required'),
				array('e_cola, salary, basic_pay, min_wage','numerical', 'integerOnly'=>true),
				#array('home_pin_code,office_pin_code','length', 'max'=>6),
				#array('e_email,other_email,work_email','email'),
			);
		}
		
		return array(
			array('father_contact, mother_contact, spouse_contact,sibling,height,weight','numerical', 'integerOnly'=>true),
			array('association_employee_id, lastname, firstname, birth_date, nationality, gender, marital_status', 'required'),
			array('photo_data', 'file', 'types'=>'jpg, gif, png','allowEmpty' => true, 'maxSize' => 5242880),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'trucker_id'=>'Trucker',
			'id'=>'Date',
			'joining_date'=>'Joining Date',
			'association_employee_id'=>'Association Employee ID',
			'lastname'=>'Last Name',
			'firstname'=>'First Name',
			'middlename'=>'Middle Name',
			'nickname'=>'Nick Name',
			'prefix'=>'Prefix',
			'name_ext'=>'Name Extension',
			'sss_id'=>'SSS No.',
			'philhealth_id'=>'PhilHealth No.',
			'tin_id'=>'TIN No.',
			'hmdf'=>'HMDF No.',
			'tax_status'=>'Tax Status',
			'atm'=>'ATM No.',
			'height'=>'Height',
			'weight'=>'Weight',
			'birth_date'=>'Birth Date',
			'birth_place'=>'Birth Place',
			'nationality'=>'Nationality',
			'religion'=>'Religion',
			'gender'=>'Gender',
			'blood_type'=>'Blood Type',
			'marital_status'=>'Marital Status',
			'photo_file_name' => 'Photo File Name',
			'photo_content_type' => 'Photo Content Type',
			'photo_data' => 'Photo Data',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'photo_file_size' => 'Photo File Size',
			'user_id' => 'User',
			'father_name'=>'Father\'s Name',
			'father_occupation'=>'Occupation',
			'father_contact'=>'Contact Number',
			'father_address'=>'Address',
			'father_city'=>'City',
			'father_state'=>'Province/State',
			'mother_name'=>'Mother\'s Name',
			'mother_occupation'=>'Occupation',
			'mother_contact'=>'Contact Number',
			'mother_address'=>'Address',
			'mother_city'=>'City',
			'mother_state'=>'Province/State',
			'sibling'=>'Siblings',
			'spouse_name'=>'Spouse\'s Name',
			'spouse_occupation'=>'Occupation',
			'spouse_contact'=>'Contact Number',
			'spouse_address'=>'Address',
			'spouse_city'=>'City',
			'spouse_state'=>'Province',
			'spouse_birth_date'=>'Spouse\'s Date of Birth',

			'present_address'=>'Address',
			'present_city'=>'City',
			'present_state'=>'State/Province',
			'present_zipcode'=>'Zip Code',
			'per_address'=>'Address',
			'per_city'=>'City',
			'per_state'=>'State/Province',
			'per_zipcode'=>'Zip Code',
			'home_telephone'=>'Telephone Number',
			'mobile'=>'Mobile Number',
			'other_email'=>'Email Address',
			'work_tel'=>'Work Telephone No.',
			'work_email'=>'Work Email Address',
			'e_name'=>'Name',
			'e_rel'=>'Relationship',
			'e_add'=>'Address',
			'e_contact'=>'Contact Number',
			'e_other_con'=>'Other Contact Number',
			'e_email'=>'Email Address',
			'e_city'=>'City',
			'e_state'=>'Province/State',

			'elem_school'=>'School Name',
			'elem_indate'=>'Inclusive Date',
			'elem_exdate'=>'Exclusive Date',
			'hs_school'=>'High School',
			'hs_indate'=>'Inclusive Date',
			'hs_exdate'=>'Exclusive Date',
			'col_school'=>'School Name',
			'col_course'=>'Course',
			'col_indate'=>'Inclusive Date',
			'col_exdate'=>'Exclusive Date',
			'o_school'=>'School Name',
			'o_course'=>'Course',
			'o_indate'=>'Inclusive Date',
			'o_exdate'=>'Exclusive Date',

			'dept'=>'Department',
			'post'=>'Position',
			'start_emp'=>'Date of Employment',
			'end_emp'=>'Last Day of Employment',
			'active'=>'Active',
			'emp_stat'=>'Employment Status',
			'pay_period'=>'Pay Period',
			'salary'=>'Salary',
			'pay_type'=>'Pay Type',
			'basic_pay'=>'Basic Pay',
			'e_cola'=>'E COLA',
			'min_wage'=>'Minimum Wage Earner',
			'region'=>'Region',
			'super_sub'=>'Supervisor / Subordinate',
			'method'=>'Method',
			
			'cur_err_truckerid' => 'Association Trucker ID',
			'cur_err_trucker' => 'Employee Number',
			'cur_err_indate' => 'Inclusive Date',
			'cur_err_exdate' => 'Exclusive Date',
			'cur_err_reason' => 'Reason for Leaving',
			'cur_err_notes' => 'Notes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('joining_date',$this->joining_date);
		$criteria->compare('association_employee_id',$this->association_employee_id);
		$criteria->compare('lastname',$this->lastname);
		$criteria->compare('firstname',$this->firstname);
		$criteria->compare('middlename',$this->middlename);
		$criteria->compare('nickname',$this->nickname);
		$criteria->compare('prefix',$this->prefix);
		$criteria->compare('name_ext',$this->name_ext);
		$criteria->compare('sss_id',$this->sss_id);
		$criteria->compare('philhealth_id',$this->philhealth_id);
		$criteria->compare('tin_id',$this->tin_id);
		$criteria->compare('hmdf',$this->hmdf);
		$criteria->compare('tax_status',$this->tax_status);
		$criteria->compare('union_member',$this->union_member);
		$criteria->compare('atm',$this->atm);
		$criteria->compare('height',$this->height);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('birth_date',$this->birth_date);
		$criteria->compare('birth_place',$this->birth_place);
		$criteria->compare('nationality',$this->nationality);
		$criteria->compare('religion',$this->religion);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('blood_type',$this->blood_type);
		$criteria->compare('marital_status',$this->marital_status);
		$criteria->compare('address_id',$this->address_id);
		$criteria->compare('present_address',$this->present_address);
		$criteria->compare('present_city',$this->present_city);
		$criteria->compare('present_state',$this->present_state);
		$criteria->compare('present_zipcode',$this->present_zipcode);
		$criteria->compare('per_address',$this->per_address);
		$criteria->compare('per_city',$this->per_city);
		$criteria->compare('per_state',$this->per_state);
		$criteria->compare('per_zipcode',$this->per_zipcode);
		$criteria->compare('zip_code',$this->zip_code);
		$criteria->compare('home_telephone',$this->home_telephone);
		$criteria->compare('mobile',$this->mobile);
		$criteria->compare('work_tel',$this->work_tel);
		$criteria->compare('work_email',$this->work_email);
		$criteria->compare('other_email',$this->other_email);
		$criteria->compare('father_name',$this->father_name);
		$criteria->compare('father_fnale',$this->father_fnale);
		$criteria->compare('father_mname',$this->father_mname);
		$criteria->compare('father_lname',$this->father_lname);
		$criteria->compare('father_occupation',$this->father_occupation);
		$criteria->compare('father_address',$this->father_address);
		$criteria->compare('father_city',$this->father_city);
		$criteria->compare('father_state',$this->father_state);
		$criteria->compare('father_contact',$this->father_contact);
		$criteria->compare('mother_name',$this->mother_name);
		$criteria->compare('mother_fname',$this->mother_fname);
		$criteria->compare('mother_lname',$this->mother_lname);
		$criteria->compare('mother_mname',$this->mother_mname);
		$criteria->compare('mother_occupation',$this->mother_occupation);
		$criteria->compare('mother_address',$this->mother_address);
		$criteria->compare('mother_city',$this->mother_city);
		$criteria->compare('mother_state',$this->mother_state);
		$criteria->compare('mother_contact',$this->mother_contact);
		$criteria->compare('sibling',$this->sibling);
		$criteria->compare('spouse_name',$this->spouse_name);
		$criteria->compare('spouse_occupation',$this->spouse_occupation);
		$criteria->compare('spouse_address',$this->spouse_address);
		$criteria->compare('spouse_city',$this->spouse_city);
		$criteria->compare('spouse_state',$this->spouse_state);
		$criteria->compare('spouse_contact',$this->spouse_contact);
		$criteria->compare('spouse_birth_date',$this->spouse_birth_date);
		$criteria->compare('report_id',$this->report_id);
		$criteria->compare('e_name',$this->e_name);
		$criteria->compare('e_rel',$this->e_rel);
		$criteria->compare('e_add',$this->e_add);
		$criteria->compare('e_city',$this->e_city);
		$criteria->compare('e_state',$this->e_state);
		$criteria->compare('e_contact',$this->e_contact);
		$criteria->compare('e_other_con',$this->e_other_con);
		$criteria->compare('e_email',$this->e_email);
		$criteria->compare('elem_school',$this->elem_school);
		$criteria->compare('elem_address',$this->elem_address);
		$criteria->compare('hs_address',$this->hs_address);
		$criteria->compare('col_address',$this->col_address);
		$criteria->compare('elem_indate',$this->elem_indate);
		$criteria->compare('elem_exdate',$this->elem_exdate);
		$criteria->compare('hs_school',$this->hs_school);
		$criteria->compare('hs_indate',$this->hs_indate);
		$criteria->compare('hs_exdate',$this->hs_exdate);
		$criteria->compare('col_school',$this->col_school);
		$criteria->compare('col_course',$this->col_course);
		$criteria->compare('col_indate',$this->col_indate);
		$criteria->compare('col_exdate',$this->col_exdate);
		$criteria->compare('o_school',$this->o_school);
		$criteria->compare('o_course',$this->o_course);
		$criteria->compare('o_indate',$this->o_indate);
		$criteria->compare('o_exdate',$this->o_exdate);
		$criteria->compare('employment_status_id',$this->employment_status_id);
		$criteria->compare('trucker_id',$this->trucker_id);
		$criteria->compare('dept',$this->dept);
		$criteria->compare('post',$this->post);
		$criteria->compare('emp_number',$this->emp_number);
		$criteria->compare('start_emp',$this->start_emp);
		$criteria->compare('end_emp',$this->end_emp);
		$criteria->compare('active',$this->active);
		$criteria->compare('emp_stat',$this->emp_stat);
		$criteria->compare('pay_period',$this->pay_period);
		$criteria->compare('salary',$this->salary);
		$criteria->compare('pay_type',$this->pay_type);
		$criteria->compare('basic_pay',$this->basic_pay);
		$criteria->compare('e_cola',$this->e_cola);
		$criteria->compare('min_wage',$this->min_wage);
		$criteria->compare('region',$this->region);
		$criteria->compare('photo_file_name',$this->photo_file_name);
		$criteria->compare('photo_content_type',$this->photo_content_type);
		$criteria->compare('photo_data',$this->photo_data);
		$criteria->compare('created_at',$this->created_at);
		$criteria->compare('updated_at',$this->updated_at);
		$criteria->compare('photo_file_size',$this->photo_file_size);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('is_deleted',$this->is_deleted);
		$criteria->compare('license_id',$this->license_id);
		$criteria->compare('cur_err_truckerid',$this->cur_err_truckerid);
		$criteria->compare('curemp',$this->curemp);
		$criteria->compare('cur_err_trucker',$this->cur_err_trucker);
		$criteria->compare('cur_err_indate',$this->cur_err_indate);
		$criteria->compare('cur_err_exdate',$this->cur_err_exdate);
		$criteria->compare('cur_err_reason',$this->cur_err_reason);
		$criteria->compare('cur_err_notes',$this->cur_err_notes);
		$criteria->compare('super_sub',$this->super_sub);
		$criteria->compare('method',$this->method);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/*
	public function exp_validation(){
    if($this->experience_year == ''&&$this->experience_month == ''){
         $this->addError('experience_month', 'Enter experience details');
   	 }
	}
	
	public function exp_details_validation(){
    if(!$this->experience_detail){
         $this->addError('experience_detail', 'Enter experience details');
    }
   }*/
   
   public function getConcatened()
	{
			return $this->firstname.' '.$this->middlename.' '.$this->lastname;
	}
}