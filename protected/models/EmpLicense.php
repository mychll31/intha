<?php

/**
 * This is the model class for table "emp_license".
 *
 * The followings are the available columns in table 'emp_license':
 * @property integer $id
 * @property integer $emp_no
 * @property integer $license_type
 * @property integer $license_no
 * @property string $issuance_date
 * @property string $expiry_date
 */
class EmpLicense extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return EmpLicense the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'emp_license';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_no, license_type, license_no', 'numerical', 'integerOnly'=>true),
			array('issuance_date, expiry_date', 'safe'),
			array('emp_no,license_type,license_no,issuance_date,expiry_date','required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, emp_no, license_type, license_no, issuance_date, expiry_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'emp_no' => 'Employee Number',
			'license_type' => 'License Type',
			'license_no' => 'License Number',
			'issuance_date' => 'Issuance Date',
			'expiry_date' => 'Expiry Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('emp_no',$this->emp_no);
		$criteria->compare('license_type',$this->license_type);
		$criteria->compare('license_no',$this->license_no);
		$criteria->compare('issuance_date',$this->issuance_date,true);
		$criteria->compare('expiry_date',$this->expiry_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}