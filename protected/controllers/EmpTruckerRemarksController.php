<?php

class EmpTruckerRemarksController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','remarks','remove'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id,$eid)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($eid),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id)
	{
		$model=new EmpTruckerRemarks;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EmpTruckerRemarks']))
		{
			$model->attributes=$_POST['EmpTruckerRemarks'];
			$list = $_POST['EmpTruckerRemarks'];
			$model->dateencoded= $list['dateencoded'];
			$model->datereminder= $list['datereminder'];
			$model->description= $list['description'];
			$model->remarks= $list['remarks'];
			$model->emp_no= $_REQUEST['eid'];
			$model->createdby= Yii::app()->user->id;
			$model->datecreatedby= date('Y-m-d H:i:s',time());
			$model->active= 1;
			if($model->save())
				$this->redirect(array('view','id'=>$_REQUEST['id'],'eid'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id,$eid)
	{
		$model=$this->loadModel($eid);
		$err = null;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EmpTruckerRemarks']))
		{
			$empnumber = Employees::model()->findByAttributes(array('id'=>$_REQUEST['id']));
			$model->attributes=$_POST['EmpTruckerRemarks'];
			$list = $_POST['EmpTruckerRemarks'];
			$model->dateencoded= $list['dateencoded'];
			$model->datereminder= $list['datereminder'];
			$model->description= $list['description'];
			$model->remarks= $list['remarks'];
			$model->modifiedby= Yii::app()->user->id;
			$model->datemodified= date('Y-m-d H:i:s',time());
			
			#USER UPLOAD
			$number_of_uploaded_files = 1;
			$uploaded_files = array();
			$upload_directory = 'attachments/requests/';
			for($i= 0; $i < count($_FILES['ufile']['name']); $i++){
			   if($_FILES['ufile']['size'][$i] < 8000000 ){
			     $uploaded_files[] = $_FILES['ufile']['name'][$i];
			     $temp = explode(".", $_FILES["ufile"]["name"][$i]);
			     $extension = end($temp);
			     $count = 0;
			     $f_name = trim($_FILES['ufile']['name'][$i],'.'.$extension);
			     $f_size = $_FILES['ufile']['size'][$i];
			     $f_type = $extension;

			     if($f_name!=null){
			     while(file_exists($upload_directory . $_FILES['ufile']['name'][$i])) {
				$count += 1;
				$_FILES['ufile']['name'][$i] = $f_name.'_copy('.$count.').'.$extension;
				$f_name= trim($_FILES['ufile']['name'][$i],'.'.$extension); 
				$f_type= $extension;
				$f_size= $_FILES['ufile']['size'][$i];
			     }
			     $model_1 = new RemarksEmployeeAttachment;
			     $model_1->rem_id=$model->id;
			     $model_1->filename=$f_name;
			     $model_1->fileext=$f_type;
			     $model_1->fileloc=$upload_directory.$f_name;
			     $model_1->createdby= Yii::app()->user->id;
			     $model_1->createddate= date('Y-m-d H:i:s',time());
			     $model_1->save();
			     if(move_uploaded_file($_FILES['ufile']['tmp_name'][$i], $upload_directory.$_FILES['ufile']['name'][$i])){
				$number_of_moved_files++;
			     } }
			   }else{
			    $err='file size is too Large';
			   }#end *8MB
			}
			if($err==null){
			    if($model->save())
				$this->redirect(array('view','id'=>$_REQUEST['id'],'eid'=>$model->id));
			}
		}
		$this->render('update',array(
			'model'=>$model,
			'err'=>$err,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($eid)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($eid)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('EmpTruckerRemarks');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionRemove($id,$eid,$aid)
	{
		$this->loadattModel($aid)->delete();
		$this->redirect(array('view','id'=>$id,'eid'=>$eid));
	}

	public function actionRemarks($id,$eid)
	{
		$this->render('remarks');
	}
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new EmpTruckerRemarks('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['EmpTruckerRemarks']))
			$model->attributes=$_GET['EmpTruckerRemarks'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=EmpTruckerRemarks::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadattModel($aid)
	{
		$model=RemarksEmployeeAttachment::model()->findByPk($aid);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='emp-trucker-remarks-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
