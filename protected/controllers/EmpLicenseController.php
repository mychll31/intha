<?php

class EmpLicenseController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
			    );
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
				array('allow',  // allow all users to perform 'index' and 'view' actions
					'actions'=>array('index','view','manage'),
					'users'=>array('*'),
				     ),
				array('allow', // allow authenticated user to perform 'create' and 'update' actions
					'actions'=>array('create','update','createall','managealerts'),
					'users'=>array('@'),
				     ),
				array('allow', // allow admin user to perform 'admin' and 'delete' actions
					'actions'=>array('admin','delete'),
					'users'=>array('admin'),
				     ),
				array('deny',  // deny all users
					'users'=>array('*'),
				     ),
			    );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */

	public function actionManage()
	{
		$model=new EmpLicense;
		$criteria = new CDbCriteria;

		$criteria->condition='(active = :match)';
		$criteria->params = array(':match' => 1);

		if(isset($_REQUEST['val']))
		{
			if ($_REQUEST['val'] != null or ''){
				$criteria_e = new CDbCriteria;
				$criteria_emp = new CDbCriteria;
				$criteria_e->condition='(lastname LIKE :emplastname)';
				$criteria_e->params = array(':emplastname' => $_REQUEST['val'].'%');
				$empno= Employees::model()->findAll($criteria_e);
				$ctrAnd = 0;
				foreach($empno as $e_no){
					if ($ctrAnd == 0){				
						$criteria->condition=$criteria->condition.' and '.'emp_no = :eid';
						$criteria->params[':eid'] = $e_no->association_employee_id;
					}
					$ctrAnd++;
					$criteria->condition=$criteria->condition.' or '.'emp_no = :e_id';
					$criteria->params[':e_id'] = $e_no->association_employee_id;
				}
			#	$criteria_emp->condition='(lastname LIKE :match)';
			#	$criteria_emp->params = array(':match' => $_REQUEST['val'].'%');

			}/*
			$empno1 = Employees::model()->findAll($criteria_emp);
			foreach($empno1 as $e){
			  echo $e->id.'<br>';
			}*/
		}

		if(isset($_REQUEST['name']) and $_REQUEST['name'] != '' and $_REQUEST['name'] != null)
		{
			if ($_REQUEST['name'] != null or ''){
				$criteria_e = new CDbCriteria;
				$criteria_e->condition='(firstname LIKE :name or lastname like :name)';
				$criteria_e->params = array(':name' => $_REQUEST['name'].'%');
				$empno= Employees::model()->findAll($criteria_e);
				$ctrAnd = 0;

				foreach($laste as $laste){
					$last++;
				}

				foreach($empno as $e_no){
					$getlast++;
					if ($ctrAnd == 0){				
						$criteria->condition=$criteria->condition.' and '.'emp_no = :eid';
						$criteria->params[':eid'] = $e_no->association_employee_id;
					}$ctrAnd++;
					$criteria->condition=$criteria->condition.' or '.'emp_no = :e_id';
					$criteria->params[':e_id'] = $e_no->association_employee_id;

					if($last == $getlast){
						$criteria->condition=$criteria->condition.' or '.'emp_no = :e_id';
						$criteria->params[':e_id'] = $e_no->association_employee_id;	
					}
				}
			}else{
				$criteria->condition=$criteria->condition.' and '.'emp_no = :e_id';
				$criteria->params[':e_id'] = '';
			}
		}

		if(isset($_REQUEST['lice_no']) and $_REQUEST['lice_no'] != '' and $_REQUEST['lice_no'] != null)
		{
			$criteria->condition=$criteria->condition.' and '.'license_no = :lice_no';
			$criteria->params[':lice_no'] = $_REQUEST['lice_no']; 
		}

		if(isset($_REQUEST['lice_type']) and $_REQUEST['lice_type'] != '' and $_REQUEST['lice_type'] != null)
		{
			$criteria->condition=$criteria->condition.' and '.'license_type = :lice_type';
			$criteria->params[':lice_type'] = $_REQUEST['lice_type']; 
		}

		if(isset($_REQUEST['issuance_date']) and $_REQUEST['EmpLicense']['issuance_date']!=NULL )
		{
			if(isset($_REQUEST['issuance_date']) and $_REQUEST['EmpLicense']['issuance_date']!=NULL)
			{
				if($_REQUEST['EmpLicense']['issuance_date']=='2')
				{  
					$criteria->condition=$criteria->condition.' and '.'issuance_date = :issuance_date';
					$criteria->params[':issuance_date'] = date('Y-m-d',strtotime($_REQUEST['issuance_date']));
				}
				if($_REQUEST['EmpLicense']['issuance_date']=='1')
				{  
					$criteria->condition=$criteria->condition.' and '.'issuance_date < :issuance_date';
					$criteria->params[':issuance_date'] = date('Y-m-d',strtotime($_REQUEST['issuance_date']));
				}
				if($_REQUEST['EmpLicense']['issuance_date']=='3')
				{  
					$criteria->condition=$criteria->condition.' and '.'issuance_date > :issuance_date';
					$criteria->params[':issuance_date'] = date('Y-m-d',strtotime($_REQUEST['issuance_date']));
				}

			}
		}else if(isset($_REQUEST['EmpLicense']['issuance_date']) and $_REQUEST['issuance_date']==NULL)
		{
			if(isset($_REQUEST['EmpLicense']['issuance_date']) and $_REQUEST['issuance_date']!=NULL)
			{
				$criteria->condition=$criteria->condition.' and '.'issuance_date = :issuance_date';
				$criteria->params[':issuance_date'] = date('Y-m-d',strtotime($_REQUEST['issuance_date']));
			}
		}

		$criteria->order = 'issuance_date DESC';

		$total = EmpLicense::model()->count($criteria);
		print_r($total);	
		$pages = new CPagination($total);
		$pages->setPageSize(Yii::app()->params['listPerPage']);
		$pages->applyLimit($criteria);  // the trick is here!
		$posts = EmpLicense::model()->findAll($criteria);
		$this->render('manageLicense',array('model'=>$model,
					'list'=>$posts,
					'pages' => $pages,
					'item_count'=>$total,
					'page_size'=>Yii::app()->params['listPerPage'],)) ;
	}

	public function actionView($id)
	{
		/*$this->render('view',array(
		  'model'=>$this->loadModel($id),
		  ));*/
		$this->render('view',array(
					'model'=>$model,
					));
	}

	public function actionManagealerts()
	{
		$model=new EmpLicense;
		$criteria = new CDbCriteria;
		$exp1 = date('Y-m-d',time());
		$exp2 = date('Y-m-d',(date(strtotime('+30 day'),time())));

		$criteria->condition='active =  1 and expiry_date >=  :expiry and expiry_date <= :expiry1';
		$criteria->params[':expiry']=$exp1;
		$criteria->params[':expiry1']=$exp2;
		$criteria->order = 'expiry_date DESC';

		$total = EmpLicense::model()->count($criteria);
		$pages = new CPagination($total);
		$pages->setPageSize(Yii::app()->params['listPerPage']);
		$pages->applyLimit($criteria);  // the trick is here!
		$posts = EmpLicense::model()->findAll($criteria);
		$this->render('manage_alerts',array('model'=>$model,
					'list'=>$posts,
					'pages' => $pages,
					'item_count'=>$total,
					'page_size'=>Yii::app()->params['listPerPage'],)) ;
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new EmpLicense;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EmpLicense']))
		{
			$model->attributes=$_POST['EmpLicense'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->emp_no));
		}

		$this->render('create',array(
					'model'=>$model,
					));
	}

	public function actionCreateall()
	{
		$model=new EmpLicense;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EmpLicense']))
		{
			$model->attributes=$_POST['EmpLicense'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->emp_no));
		}

		$this->render('createall',array(
					'model'=>$model,
					));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($licid)
	{
		$model=$this->loadModel($licid);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EmpLicense']))
		{
			$model->attributes=$_POST['EmpLicense'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->emp_no));
		}

		$this->render('update',array(
					'model'=>$model,
					));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('EmpLicense');
		$this->render('index',array(
					'dataProvider'=>$dataProvider,
					));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new EmpLicense('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['EmpLicense']))
			$model->attributes=$_GET['EmpLicense'];

		$this->render('admin',array(
					'model'=>$model,
					));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=EmpLicense::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='emp-license-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
