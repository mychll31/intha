<?php

class DefaultController extends RController
{
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}
	
	public function actionIndex()
	{
                 ##Trucker membership notifications
                $criteria_a = new CDbCriteria;
                $exp1 = date('Y-m-d',time());
                $exp2 = date('Y-m-d',(date(strtotime('+60 day'),time())));

                #Truck Registration Date
                $criteria_b = new CDbCriteria;
                $truckCount = 0;
                $criteria_b->condition=' reg_enddate >=  :expiry and reg_enddate <= :expiry1';
                $criteria_b->params[':expiry']=$exp1;
                $criteria_b->params[':expiry1']=$exp2;
                $truckCount = Courses::model()->count($criteria_b);

                #TruckerRegdate Date
                $criteria_lic = new CDbCriteria;
                $licCount = 0;
                $criteria_lic->condition=' active=1 and expiry_date>=  :expiry and expiry_date <= :expiry1';
                $criteria_lic->params[':expiry']=$exp1;
                $criteria_lic->params[':expiry1']=$exp2;
                $licCount = EmpLicense::model()->count($criteria_lic);

                #Comprehensive Insurance
                $criteria_99 = new CDbCriteria;
                $orcrCount = 0;
                $criteria_99->condition=' compre_dateexpire >=  :expiry and compre_dateexpire <= :expiry1';
                $criteria_99->params[':expiry']=$exp1;
                $criteria_99->params[':expiry1']=$exp2;
                $compreCount = Insurance::model()->count($criteria_99);

                $totalCount = $licCount + $truckCount + $compreCount;

		$this->render('index',array(
			'totalCount'=>$totalCount,
		));
	}
	public function actionInlandmarine()
	{
		##Trucker membership notifications
		$criteria_a = new CDbCriteria;
		$exp1 = date('Y-m-d',time());
		$exp2 = date('Y-m-d',(date(strtotime('+60 day'),time())));
		
		#Truck Registration Date
		$criteria_b = new CDbCriteria;
		$truckCount = 0;
		$criteria_b->condition=' reg_enddate >=  :expiry and reg_enddate <= :expiry1';
		$criteria_b->params[':expiry']=$exp1;
		$criteria_b->params[':expiry1']=$exp2;
		$truckCount = Courses::model()->count($criteria_b);
		
		#TruckerRegdate Date
		$criteria_lic = new CDbCriteria;
		$licCount = 0;
		$criteria_lic->condition=' active=1 and expiry_date>=  :expiry and expiry_date <= :expiry1';
		$criteria_lic->params[':expiry']=$exp1;
		$criteria_lic->params[':expiry1']=$exp2;
		$licCount = EmpLicense::model()->count($criteria_lic);
		
		#Comprehensive Insurance
		$criteria_99 = new CDbCriteria;
		$orcrCount = 0;
		$criteria_99->condition=' compre_dateexpire >=  :expiry and compre_dateexpire <= :expiry1';
		$criteria_99->params[':expiry']=$exp1;
		$criteria_99->params[':expiry1']=$exp2;
		$compreCount = Insurance::model()->count($criteria_99);
		
		#inland marine truckers
		$criteria_inland = new CDbCriteria;
		$criteria_inland->order = 'subs_date asc';
		$inlandtruckersCount = InlandMarineTrucker::model()->count();
		$inlandtruckers=InlandMarineTrucker::model()->findAll($criteria_inland);
		$inlandtruckCount = InlandMarineTrucks::model()->count();
		$inlandtruck=InlandMarineTrucks::model()->findAll($criteria_inland);
		$inlandbrokername=Inlandbrokers::model()->findAll();
		
		$totalCount = $licCount + $truckCount + $compreCount;
		
		$this->render('inlandmarine',array(
			'inlandtruckCount'=>$inlandtruckCount,
			'inlandtruck'=>$inlandtruck,
			'inlandtruckersCount'=>$inlandtruckersCount,
			'inlandtruckers'=>$inlandtruckers,
			'inlandbrokername'=>$inlandbrokername,
			'totalCount'=>$totalCount,
		));
	 /*
		$model=new InlandMarineTrucker;


		if(isset($_POST['InlandMarineTrucker']))
		{
			$model->attributes=$_POST['InlandMarineTrucker'];
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('manage',array(
			'model'=>$model,
		));
	*/
	}

	public function actionNoti()
	{
		##Trucker membership notifications
		$criteria_a = new CDbCriteria;
		$exp1 = date('Y-m-d',time());
		$exp2 = date('Y-m-d',(date(strtotime('+60 day'),time())));
		$truckermemberCount = 0;
		$criteria_a->condition='is_active =  1 and membership_expiration >=  :expiry and membership_expiration <= :expiry1';
		$criteria_a->params[':expiry']=$exp1;
		$criteria_a->params[':expiry1']=$exp2;
		$truckermemberCount = Students::model()->count($criteria_a);
		$truckermem= Students::model()->findAll($criteria_a);
		
		#Truck Details
		#Registration Date
		$criteria_b = new CDbCriteria;
		$truckCount = 0;
		$criteria_b->condition=' reg_enddate >=  :expiry and reg_enddate <= :expiry1';
		$criteria_b->params[':expiry']=$exp1;
		$criteria_b->params[':expiry1']=$exp2;
		$truckCount = Courses::model()->count($criteria_b);
		$trucks= Courses::model()->findAll($criteria_b);
		
		#franchise Date
		$criteria_c = new CDbCriteria;
		$truckregCount = 0;
		$criteria_c->condition=' franchise_date_of_expiration >=  :expiry and franchise_date_of_expiration <= :expiry1';
		$criteria_c->params[':expiry']=$exp1;
		$criteria_c->params[':expiry1']=$exp2;
		$truckregCount = Courses::model()->count($criteria_c);
		$truckreg= Courses::model()->findAll($criteria_c);

		#TruckerRegdate Date
		$criteria_lic = new CDbCriteria;
		$licCount = 0;
		$criteria_lic->condition=' active=1 and expiry_date>=  :expiry and expiry_date <= :expiry1';
		$criteria_lic->params[':expiry']=$exp1;
		$criteria_lic->params[':expiry1']=$exp2;
		$licCount = EmpLicense::model()->count($criteria_lic);
		$liclist= EmpLicense::model()->findAll($criteria_lic);
		
		#ORCR Date
		$criteria_d = new CDbCriteria;
		$orcrCount = 0;
		$criteria_d->condition=' or_cr_date >=  :expiry and or_cr_date <= :expiry1';
		$criteria_d->params[':expiry']=$exp1;
		$criteria_d->params[':expiry1']=$exp2;
		$orcrCount = Courses::model()->count($criteria_d);
		$orcrReg= Courses::model()->findAll($criteria_d);
		
		#Comprehensive Insurance
		$criteria_99 = new CDbCriteria;
		$orcrCount = 0;
		$criteria_99->condition=' compre_dateexpire >=  :expiry and compre_dateexpire <= :expiry1';
		$criteria_99->params[':expiry']=$exp1;
		$criteria_99->params[':expiry1']=$exp2;
		$compreCount = Insurance::model()->count($criteria_99);
		$compreReg= Insurance::model()->findAll($criteria_99);
		
		$totalCount = $licCount + $truckCount + $compreCount;
		
		$this->render('noti',array(
			'truckermemberCount'=>$truckermemberCount,
			'truckermem'=>$truckermem,
			'truckCount'=>$truckCount,
			'trucks'=>$trucks,
			'truckregCount'=>$truckregCount,
			'truckreg'=>$truckreg,
			'orcrCount'=>$orcrCount,
			'orcrReg'=>$orcrReg,
			'licCount'=>$licCount,
			'liclist'=>$liclist,
			'compreCount'=>$compreCount,
			'compreReg'=>$compreReg,
			'totalCount'=>$totalCount,
		));
	}
}
