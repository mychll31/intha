<?php

class InsuranceController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Insurance;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Insurance']))
		{
			$model->attributes=$_POST['Insurance'];
			$list = $_POST['Insurance'];
			$model->ctpl_broker= $list['ctpl_broker'];
			$model->ctpl_insurer= $list['ctpl_insurer'];
			$model->ctpl_policy_no= $list['ctpl_policy_no'];
			$model->ctpl_coverage= $list['ctpl_coverage'];
			$model->ctpl_dateissued= $list['ctpl_dateissued'];
			$model->ctpl_dateexpire= $list['ctpl_dateexpire'];
			$model->compre_broker= $list['compre_broker'];
			$model->compre_insurer= $list['compre_insurer'];
			$model->compre_policy_number= $list['compre_policy_number'];
			$model->compre_coverage= $list['compre_coverage'];
			$model->compre_dateissued= $list['compre_dateissued'];
			$model->compre_dateexpire= $list['compre_dateexpire'];
			$model->pa_broker= $list['pa_broker'];
			$model->pa_insurer= $list['pa_insurer'];
			$model->pa_policy_no= $list['pa_policy_no'];
			$model->pa_coverage= $list['pa_coverage'];
			$model->pa_dateissued= $list['pa_dateissued'];
			$model->pa_dateexpire= $list['pa_dateexpire'];
			
			$truckid = Courses::model()->findByAttributes(array('id'=>$id));
			$model->truck_id = $truckid->association_truck_id;
			$model->trucker_id= $list['trucker_id'];
			$model->createdby= $list['createdby'];
			$model->createdby= Yii::app()->user->id;
			$model->createddate= date('Y-m-d H:i:s',time());
			
			if($model->save())
				$this->redirect(array('courses/courses/compreinsurance','id'=>$truckid->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Insurance']))
		{
			$model->attributes=$_POST['Insurance'];
			$list= $_POST['Insurance'];
			$model->ctpl_broker= $list['ctpl_broker'];
			$model->ctpl_insurer= $list['ctpl_insurer'];
			$model->ctpl_policy_no= $list['ctpl_policy_no'];
			$model->ctpl_coverage= $list['ctpl_coverage'];
			$model->ctpl_dateissued= $list['ctpl_dateissued'];
			$model->ctpl_dateexpire= $list['ctpl_dateexpire'];
			$model->compre_broker= $list['compre_broker'];
			$model->compre_insurer= $list['compre_insurer'];
			$model->compre_policy_number= $list['compre_policy_number'];
			$model->compre_coverage= $list['compre_coverage'];
			$model->compre_dateissued= $list['compre_dateissued'];
			$model->compre_dateexpire= $list['compre_dateexpire'];
			$model->pa_broker= $list['pa_broker'];
			$model->pa_insurer= $list['pa_insurer'];
			$model->pa_policy_no= $list['pa_policy_no'];
			$model->pa_coverage= $list['pa_coverage'];
			$model->pa_dateissued= $list['pa_dateissued'];
			$model->pa_dateexpire= $list['pa_dateexpire'];
			
			$truckid= Courses::model()->findByAttributes(array('id'=>$id));
			$model->truck_id= $truckid->association_truck_id;
			$model->trucker_id= $list['trucker_id'];
			$model->lastupdatedby= Yii::app()->user->id;
			$model->lastupdatedate= date('Y-m-d H:i:s',time());
			
			if($model->save())
				$this->redirect(array('courses/courses/compreinsurance','id'=>$truckid->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Insurance');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Insurance('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Insurance']))
			$model->attributes=$_GET['Insurance'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Insurance::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='insurance-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
