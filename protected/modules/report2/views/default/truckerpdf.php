<table width=100%>
	<tr><td><img src='images/login-logo.png' style="width:130px; height:75px;" /></td>
	<td style="padding-left:20px;">
	<b style="font-size:15px;line-height:25px;">INTEGRATED NORTH HARBOR TRUCKERS ASSOCIATION</b><br>
	<font style="font-size:11px;line-height:15px;">Commercial Lot 4, Road 10, Tondo, Manila<br>
	Tel. No.: 254-5695 / 254-5754  Fax: 254-5759<br>
	Email: intha@skyinet.net</font>
	</td></tr>
</table>
<style>
.listbxtop_hdng
{
	text-align:left;
	
}
.table_listbx tr td, tr th {
border-left:1px solid #ccc;
border-right:1px solid #ccc;
}
td.listbx_subhdng
{
	color:#333333;
	font-weight:bold;
}

.odd
{
	background:#DFDFDF;
	border-top:1px solid #ccc;
}
td.subhdng_nrmal
{
	color:#333333;	
}
.table_listbx
{
	margin:0px;
	padding:0px;
	
}
.table_listbx td
{
	padding:5px;
	margin:0px;
	font-size:10px;
}
.table_listbxlast td
{
	border-bottom:none;

	
}


td.subhdng_nrmal
{
	color:#333333;
}
.last
{
	border-bottom:1px solid #ccc;
}
.first
{
	border:none;
}
</style>
<br>
<div align='right' style="font-size:10px;"><?php echo date('M. d, Y H:i A');?></div>
<h4>Trucker Master List</h4>
<table class="table_listbx" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
	<tr>
		<td class="listbx_subhdng odd" style='width:5px;text-align:center'>#</td>
		<td class="listbx_subhdng odd" style='width:120px;'>Company</td>
		<td class="listbx_subhdng odd" style='width:20px;'>Units</td>
		<td class="listbx_subhdng odd" style='width:160px;'>Address</td>
		<td class="listbx_subhdng odd" style='width:100px;'>Contact Person</td>
		<td class="listbx_subhdng odd" style='width:80px;'>Office No.</td>
		<td class="listbx_subhdng odd" style='width:110px;'>Email</td>
	</tr>
<?php 
	$ctr=1;
	foreach($model as $model){
	?>
	<tr>
		<td class="listbx_subhdng last" style='width:5px;text-align:center'><?=$ctr?></td>
		<td class="listbx_subhdng last" style='width:120px;'><?=strtoupper($model->trucker)?></td>
		<?php
		$criteria = new CDbCriteria;
		$criteria->compare('association_trucker_id',$model->assoc_truckerid);
		$totalunits = Courses::model()->count($criteria);?>
		<td class="listbx_subhdng last" style='width:20px;'><?=$totalunits?></td>
		<td class="listbx_subhdng last" style='width:160px;'><?=ucwords($model->buss_add_add.' ,'.$model->buss_add_prov.', '.$model->buss_add_city.' '.$model->buss_add_zip)?></td>
		<td class="listbx_subhdng last" style='width:100px;'><?=ucwords($model->rep_lname.' '.$model->rep_fname.' '.substr($model->rep_mname,0,1).'.')?></td>
		<td class="listbx_subhdng last" style='width:80px;'><?=$model->rep_telno?></td>
		<td class="listbx_subhdng last" style='width:110px;'><?=$model->rep_email?></td>
	</tr>
	<?php
	$ctr++; }
?>
</table>