<?php
$this->breadcrumbs=array(
	'Report'=>array('index'),
	'Employee Master List'=>array('/report/default/employees'),
);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
    <?php $this->renderPartial('left_side');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <h1></h1>
	<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employees',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	)); ?>
	<?php if($error !=null){echo '<div class="errorSummary">Input Error<br />
    <span>'.$error.'</span></div>';}?>
    <div class="formCon">
<div class="formConInner">
<div class="form">
	<h3>Joining Date</h3>
	From:
	<?php
	$date = 'yy-mm-dd';
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'name'=>'joining_from',
			'options'=>array(
				'showAnim'=>'fold',
				'dateFormat'=>$date,
				'changeMonth'=> true,
				'changeYear'=>true,
				'yearRange'=>'1950:2050'),
			'htmlOptions'=>array(
				'style' => 'width:100px;',),
		))
	?>
	&nbsp;&nbsp;&nbsp;
	To:
	<?php
	$date = 'yy-mm-dd';
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'name'=>'joining_to',
			'options'=>array(
				'showAnim'=>'fold',
				'dateFormat'=>$date,
				'changeMonth'=> true,
				'changeYear'=>true,
				'yearRange'=>'1950:2050'),
			'htmlOptions'=>array(
				'style' => 'width:100px;',),
		))
	?>
	&nbsp;&nbsp;&nbsp;
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Go' : 'Go',array('class'=>'formbut','name'=>'joiningdate')); ?>
	<br><br><br>
	<h3>Employee Details</h3>
	<table width="80%">
		<tr><td>Firstname</td><td>Middlename</td><td>Lastname</td><td></td></tr>
		<tr>
			<td><input type="text" name="fname" /></td>
			<td><input type="text" name="mname" /></td>
			<td><input type="text" name="lname" /></td>
			<td><?php echo CHtml::submitButton($model->isNewRecord ? 'Go' : 'Go',array('class'=>'formbut','name'=>'employeesearch')); ?></td>
		</tr>
	</table>
	<br><br><br>
	<h3>Trucker</h3>
	Choose Trucker&nbsp;
	<select name='tlist'>
	<option value=null></option>
	<?php $truckerList = Students::model()->findAll();
		foreach($truckerList as $trucker){
			echo '<option value="'.$trucker->assoc_truckerid.'">'.$trucker->trucker.'</option>';
		}
	?>
	</select>
	&nbsp;&nbsp;&nbsp;
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Go' : 'Go',array('class'=>'formbut','name'=>'truckersearch')); ?>
	<br><br><br><br>
	<h3></h3>
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Print All' : 'Print All',array('class'=>'formbut','name'=>'printall')); ?>
	<?php $this->endWidget(); ?>
</div><!-- form -->
</div>
</div>

</div>
    </td>
  </tr>
</table>