<?php

class DefaultController extends RController
{
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}
	
	public function actionIndex()
	{
		$this->render('/default/employees');
	}
	
	public function actionEmployees()
	{
		$model = new Employees;
		$criteria = new CDbCriteria;
		$criteria->compare('is_deleted',0);
		$error = null;
		if(isset($_POST['joiningdate']))
		{
			$from = $_POST['joining_from'];
			$to = $_POST['joining_to'];
			if($from !=null and $to !=null){
				if($from > $to){
					$error = 'Joining Date from <u>'.$from.'</u> to <u>'.$to.'</u> is Invalid.';
				}
			}
			if($error ==null){
				if($from !=null){
					$model->joining_date = $from;
					$criteria->condition=$criteria->condition.' and '.'joining_date >= :joining_from';
					$criteria->params[':joining_from'] = $from;
				  }
				  if($to !=null){  
					$model->joining_date = $to;
					$criteria->condition=$criteria->condition.' and '.'joining_date <= :joining_to';
					$criteria->params[':joining_to'] = $to;
				  }
				$posts = Employees::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('employeespdf', array('model'=>$posts), true));
				$html2pdf->Output('employee_masterlist.pdf');
			}
		}
		if(isset($_POST['employeesearch'])){
			$fname = $_POST['fname'];
			$lname = $_POST['lname'];
			$mname = $_POST['mname'];
			if($fname !=null){  
				$model->firstname = $fname;
				$criteria->condition=$criteria->condition.' and '.'firstname <= :firstn';
				$criteria->params[':firstn'] = $fname;
			}
			if($lname !=null){  
				$model->lastname = $lname;
				$criteria->condition=$criteria->condition.' and '.'lastname <= :lastn';
				$criteria->params[':lastn'] = $lname;
			}
			if($mname !=null){  
				$model->middlename = $mname;
				$criteria->condition=$criteria->condition.' and '.'middlename <= :midn';
				$criteria->params[':midn'] = $mname;
			}
			if($fname!=null or $lname!=null or $mname!=null){
				$posts = Employees::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('employeespdf', array('model'=>$posts), true));
				$html2pdf->Output('employee_masterlist.pdf');	
			}
		}
		if(isset($_POST['truckersearch'])){
			$trucker = $_POST['tlist'];
			if($trucker!=null){
				$model->trucker_id = $trucker;
				$criteria->condition=$criteria->condition.' and '.'trucker_id <= :truckerid';
				$criteria->params[':truckerid'] = $trucker;
				
				$posts = Employees::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('employeespdf', array('model'=>$posts), true));
				$html2pdf->Output('employee_masterlist.pdf');
			}
		}
		if(isset($_POST['printall'])){
			$posts = Employees::model()->findAll($criteria);
			$html2pdf = Yii::app()->ePdf->HTML2PDF();
			$html2pdf->WriteHTML($this->renderPartial('employeespdf', array('model'=>$posts), true));
			$html2pdf->Output('employee_masterlist.pdf');
		}
		
		$this->render('employees',array(
			'model'=>$posts,
			'error'=>$error,
		));
	}
	
	public function actionTrucks()
	{
		$model = new Courses;
		$criteria = new CDbCriteria;
		if(isset($_POST['choosetrucker'])){
			$truckerlist= $_POST['truckerl'];		
			if($truckerlist !=null){
				$model->association_trucker_id = $truckerlist;
				$criteria->condition='association_trucker_id <= :truckerid';
				$criteria->params[':truckerid'] = $truckerlist;
				
				$posts = Courses::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('truckpdf', array('model'=>$posts), true));
				$html2pdf->Output('truck_masterlist.pdf');
			}
		}
		if(isset($_POST['chooseowner'])){
			$ownernme= $_POST['owner'];		
			if($ownernme !=null){
				$model->reg_owner = $ownernme;
				$criteria->condition='reg_owner <= :ownername';
				$criteria->params[':ownername'] = $ownernme;
				
				$posts = Courses::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('truckpdf', array('model'=>$posts), true));
				$html2pdf->Output('truck_masterlist.pdf');
			}
		}
		if(isset($_POST['printall'])){
			$posts = Courses::model()->findAll();
			$html2pdf = Yii::app()->ePdf->HTML2PDF();
			$html2pdf->WriteHTML($this->renderPartial('truckpdf', array('model'=>$posts), true));
			$html2pdf->Output('truck_masterlist.pdf');
		}
		
		$this->render('trucks',array(
			'model'=>$posts,
		));
	}
	
	public function actionForm()
	{
		$model = new Courses;
		$criteria = new CDbCriteria;
		if(isset($_POST['choosetruckerexcel'])){
			$truckerlist= $_POST['truckerl'];		
			if($truckerlist !=null){
				$sql = "SELECT trucker as Trucker, (SELECT COUNT(*) FROM courses where association_trucker_id = assoc_truckerid) as Units, CONCAT(buss_add_add,', ',buss_add_prov,', ',buss_add_city,', ',buss_add_zip) as Address, CONCAT(rep_lname,', ',rep_fname,' ',rep_mname) as 'Contact Person', rep_telno as 'Office No', rep_email as Email FROM `students` where trucker = '".$truckerlist."'"; 
				$rawData =  Yii::app()->db->createCommand($sql)->queryAll(); 
				$count = Yii::app()->db->createCommand('SELECT COUNT(*) FROM (' . $sql . ') as count_alias')->queryScalar();
				$model = new CArrayDataProvider($posts,
							array(
								'totalItemCount' => $count,
								'sort' => array(
									'attributes' => array(
										'Trucker','Units', 'Address', 'Email'
									),
								),
								'pagination' => array(
									'pageSize' => 5000000, 
								),
							)
						);
						$worksheet->write(0, 0,  "Hi Excel!");

						$this->renderPartial('truckerexcel', array(
							'model' => $model, 'truckerid' => $truckerlist,
						));
			}
		}
		
		$this->render('form_report',array(
			'model'=>$posts,
		));
	}
	
	public function actionChangeplate()
	{
	$this->render('changeplate',array(
			'model'=>$posts,
		));
	}
	
	public function actionChangemotor()
	{
	$this->render('changeplate',array(
			'model'=>$posts,
		));
	}
	
	public function actionChangechasis()
	{
	$this->render('changechasis',array(
			'model'=>$posts,
		));
	}
	
	public function actionReplacementunit()
	{
	$this->render('replacementunit',array(
			'model'=>$posts,
		));
	}
	
	public function actionChangeownership()
	{
	$this->render('changeownership',array(
			'model'=>$posts,
		));
	}
	
	public function actionTruckers()
	{
		$model = new Students;
		$criteria = new CDbCriteria;
		if(isset($_POST['choosetrucker'])){
			$truckerlist= $_POST['truckerl'];		
			if($truckerlist !=null){
				$model->assoc_truckerid = $truckerlist;
				$criteria->condition='assoc_truckerid <= :truckerid';
				$criteria->params[':truckerid'] = $truckerlist;
				
				$posts = Students::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('truckerpdf', array('model'=>$posts), true));
				$html2pdf->Output('trucker_masterlist.pdf');
			}
		}
		if(isset($_POST['choosetruckerexcel'])){
			$truckerlist= $_POST['truckerl'];		
			if($truckerlist !=null){
				$sql = "SELECT trucker as Trucker, (SELECT COUNT(*) FROM courses where association_trucker_id = assoc_truckerid) as Units, CONCAT(buss_add_add,', ',buss_add_prov,', ',buss_add_city,', ',buss_add_zip) as Address, CONCAT(rep_lname,', ',rep_fname,' ',rep_mname) as 'Contact Person', rep_telno as 'Office No', rep_email as Email FROM `students` where trucker = '".$truckerlist."'"; // your sql
				$rawData =  Yii::app()->db->createCommand($sql)->queryAll(); // you get them in an array
				$count = Yii::app()->db->createCommand('SELECT COUNT(*) FROM (' . $sql . ') as count_alias')->queryScalar();
				$model = new CArrayDataProvider($posts,
							array(
								'totalItemCount' => $count,
								'sort' => array(
									'attributes' => array(
										'Trucker','Units', 'Address', 'Email'
									),
								),
								'pagination' => array(
									'pageSize' => 5000000, // large number so you export all to excel
								),
							)
						);
						$this->renderPartial('truckerexcel', array(
							'model' => $model, 'truckerid' => $truckerlist,
						));
			}
		}
		
		if(isset($_POST['chooseowner'])){
			$ownernme= $_POST['owner'];		
			if($ownernme !=null){
				$model->owner = $ownernme;
				$criteria->condition='owner <= :ownername';
				$criteria->params[':ownername'] = $ownernme;
				
				$posts = Students::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('truckerpdf', array('model'=>$posts), true));
				$html2pdf->Output('trucker_masterlist.pdf');
			}
		}
		if(isset($_POST['printall'])){
			$posts = Students::model()->findAll();
			$html2pdf = Yii::app()->ePdf->HTML2PDF();
			$html2pdf->WriteHTML($this->renderPartial('truckerpdf', array('model'=>$posts), true));
			$html2pdf->Output('trucker_masterlist.pdf');
		}
		if(isset($_POST['printallexcel'])){
			$posts = Students::model()->findAll();

		}
		
		$this->render('truckers',array(
			'model'=>$posts,
		));
	}
	
	public function actionTruckerscert()
	{
		$model = new Students;
		$criteria = new CDbCriteria;
		if(isset($_POST['choosetrucker'])){
			$truckerlist= $_POST['truckerl'];		
			if($truckerlist !=null){
				$model->assoc_truckerid = $truckerlist;
				$criteria->condition='assoc_truckerid <= :truckerid';
				$criteria->params[':truckerid'] = $truckerlist;
				
				$posts = Students::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('cert_truckers', array('model'=>$posts), true));
				$html2pdf->Output('trucker_cert.pdf');
			}
		}
		if(isset($_POST['chooseowner'])){
			$ownernme= $_POST['owner'];		
			if($ownernme !=null){
				$model->owner = $ownernme;
				$criteria->condition='owner <= :ownername';
				$criteria->params[':ownername'] = $ownernme;
				
				$posts = Students::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('cert_truckers', array('model'=>$posts), true));
				$html2pdf->Output('trucker_cert.pdf');
			}
		}
		if(isset($_POST['printall'])){
			$posts = Students::model()->findAll();
			$html2pdf = Yii::app()->ePdf->HTML2PDF();
			$html2pdf->WriteHTML($this->renderPartial('cert_truckers', array('model'=>$posts), true));
			$html2pdf->Output('trucker_cert.pdf');
		}
		
		$this->render('truckerscert',array(
			'model'=>$posts,
		));
	}
	
}