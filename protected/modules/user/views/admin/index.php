
<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('/user'),
	UserModule::t('Manage'),
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});	
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('user-grid', {
        data: $(this).serialize()
    });
    return false;
});
");

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
<div id="othleft-sidebar">
<?php $this->renderPartial('//configurations/left_side');?>
  </div>
 </td>
 <td valign="top">
<div class="cont_right formWrapper">
<h1><?php echo UserModule::t("Manage Users"); ?></h1>

    <div class="edit_bttns" style="top:15px; right:20px">
    <ul>
    <li>
    <?php echo CHtml::link('<span>Create User</span>',array('/user/admin/create'),array('class'=>'addbttn last'));?>    </li>
    </ul>
    <div class="clear"></div>
    </div>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form -->
<div class="tableinnerlist">
<table width="100%" cellpadding="0" cellspacing="0">
<tr class="pdtab-h">
	<td>No.</td>
	<td>User ID</td>
	<td>Username</td>
	<td>Email</td>
	<td>Role</td>
	<td>Active</td>
	<td>Action</td>
</tr>
<?php
	$no=1;
	$allusers = User::model()->findAll();
	foreach($allusers as $user){
	?>
	<tr>
		<td><?=$no++?></td>
		<td><?php if($user->uid!=null){echo $user->uid;}else{echo '-';} ?></td>
		<td><?=$user->username?></td>
		<td><?=$user->email?></td>
		<?php $roles=CustomRole::model()->findByAttributes(array('id'=>$user->role));?>
		<td><?=$roles->name?></td>
		<td><?php
			$status= User::itemAlias('UserStatus');
			echo $status[$user->status];
			?></td>
		<td><?php echo CHtml::link(Yii::t('User','Edit'), array('update','id'=>$user->id));?></td>
	</tr>
	<?php
	}
?>
</table>
</div>

</div>
 </td>
  </tr>
</table>