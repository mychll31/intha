<?php
$this->breadcrumbs=array(
	'Trucks'=>array('index'),
	'View',
);
$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
?>

<style>.emp_cntntbx{min-height:0px;}</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
 <?php 
	if($role->role==1){
		$this->renderPartial('left_side');
	}else{
		$this->renderPartial('profileleft');
	}	
	?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <?php if($role->role==1){?>
	<div class="edit_bttns">
    <ul>
    <li></li>
    </ul>
    </div>
	<?php }?>
    <div class="clear"></div>
    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
     <?php 
		if($role->role==1){
			$this->renderPartial('tab');
		}else{
			$this->renderPartial('tab2');
		}
	?>
    <div class="clear"></div>

    </div>
    <div class="clear"></div>
 <div class="emp_cntntbx">
  <!--div class="ea_pdf" style="top:4px; right:6px;"><?php #echo CHtml::link('<img src="images/pdf-but.png">', array('courses/pdf','id'=>$_REQUEST['id']),array('target'=>'_blank')); ?></div-->
   
 </div>
 
 
 
</div>
</div>
</div>
    
    </td>
  </tr>
</table>
