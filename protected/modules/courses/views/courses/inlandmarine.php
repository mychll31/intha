<?php
$this->breadcrumbs=array(
	'Trucks'=>array('/courses/courses/managecourse'),
	'Inland Marine',
);
?>
<?php
	$truckid = Courses::model()->findByAttributes(array('id'=>$_REQUEST['id']));
	$inlandmarine=InlandMarineTrucks::model()->findByAttributes(array('truck_id'=>$truckid->association_truck_id));
?>
<style>.emp_cntntbx{min-height:0px;}</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
 <?php $this->renderPartial('left_side');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
        
    <div class="clear"></div>
    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
     <?php $this->renderPartial('tab');?>
    <div class="clear"></div>

    </div>
    <div class="clear"></div>
 <div class="emp_cntntbx">
	<?php if($inlandmarine!=null){?>
    <div class="table_listbx"> 
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr class="listbxtop_hdng">
		  <td><?php echo Yii ::t('courses','Inland Marine Details');?></span></td>
		</tr>
	  </table>
	  <table width="100%">
		<tr>
		  <?php $inlandStat= InlandMarineStatus::model()->findByAttributes(array('id'=>$inlandmarine->status)); ?>
		  <td class="subhdng_nrmal"><span class="datahead">Status :</span><span class="data"><?=$inlandStat->name?></span></td>
		</tr>
		</table>
		<?php
		}else{ ?>
    <div class="table_listbx"> 
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr class="listbxtop_hdng">
		  <td><?php echo Yii ::t('courses','Inland Marine Details');?></span></td>
		</tr>
	  </table>
	  <table width="100%">
		<tr>
		  <td class="subhdng_nrmal"><span class="datahead">Status :</span><span class="data">Not Member</span></td>
		</tr>
		</table>
	<?php	}	?>
 </div>


    </div>
    <div class="clear"></div>
 <div class="emp_cntntbx">

	</div>
 </div>
 
 
 
</div>
</div>
</div>
    
    </td>
  </tr>
</table>
