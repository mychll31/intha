<?php
$this->breadcrumbs=array(
	'Trucks'=>array('/courses/courses/managecourse'),
	'Notes',
);

    $criteria = new CDbCriteria;
    $truckid = Courses::model()->findByAttributes(array('id'=>$_REQUEST['id']));
    $criteria->condition='truck_id = :match';
    $criteria->params[':match'] = $truckerid->association_truck_id;
    $criteria->order = 'id ASC';

    $model= TruckNotes::model()->findAll($criteria); 
  ?>

<style>.emp_cntntbx{min-height:0px;}</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
 <?php $this->renderPartial('left_side');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
        
	<div class="edit_bttns">
    <ul>
	<?php
		if($model!=null){
	?>
    <li><?php echo CHtml::link(Yii::t('courses','<span>Edit Note</span>'), array('/truckNotes/update', 'id'=>$model->id),array('class'=>'edit last')); ?></li>
	<?php
		}else{
	?>
    <li><?php echo CHtml::link(Yii::t('courses','<span>Add Note</span>'), array('/truckNotes/create', 'id'=>$_REQUEST['id']),array('class'=>'edit last')); ?></li>
    <?php
		}
	?>
	</ul>
    </div>
	
    <div class="clear"></div>
    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
     <?php $this->renderPartial('tab');?>
    <div class="clear"></div>
	
	<div class="emp_cntntbx">
    <div class="table_listbx"> 
  <table width="100%">
	<tr class="listbxtop_hdng">
	    <td colspan=2><span><?php echo Yii ::t('courses','Notes');;?></span></td>
	</tr>
	<?php foreach($model as $model){ ?>
	<tr>
	    <td class="subhdng_nrmal"><span class="datahead">Type :</span>
	    <span class="data"><?php echo $model->type; ?></span></td>
	    <td class="subhdng_nrmal"><span class="datahead">Date :</span>
	    <span class="data"><?php echo $model->date; ?></span></td>
	</tr>
	<tr>
	    <td class="subhdng_nrmal"><span class="datahead">Reported by :</span>
	    <span class="data"><?php echo $model->reportedby; ?></span></td>
	    <td class="subhdng_nrmal"><span class="datahead">Date Reported by :</span>
	    <span class="data"><?php echo $model->datereported; ?></span></td>
	</tr>
	<tr>
	    <td colspan=2 class="subhdng_nrmal"><span class="datahead">Details :</span>
	    <span class="data"><?php echo $model->details; ?></span></td>
	</tr>
	<?php } ?>
  </table>
   
 </div>
 </div>
    </div>
    <div class="clear"></div>
 <div class="emp_cntntbx">
  <!--div class="ea_pdf" style="top:4px; right:6px;"><?php #echo CHtml::link('<img src="images/pdf-but.png">', array('courses/pdf','id'=>$_REQUEST['id']),array('target'=>'_blank')); ?></div-->
 </div>
 
</div>
</div>
</div>
    
    </td>
  </tr>
</table>
