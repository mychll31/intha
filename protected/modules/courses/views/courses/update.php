<?php
$this->breadcrumbs=array(
	'Trucks'=>array('/courses/courses/managecourse'),
	'Create',
);

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
    <?php $this->renderPartial('left_side');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">

<h2><?php echo Yii::t('courses','Update Truck');?></h2><br />
<div class="edit_bttns">
<ul>
<li><?php echo CHtml::link(Yii::t('employees','<span>Back</span>'), array('view','id'=>$_REQUEST['id']),array('class'=>'edit       last')); ?></li>
</ul>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model,'model_1'=>$model_1)); ?>

 	</div>
    </td>
  </tr>
</table>
