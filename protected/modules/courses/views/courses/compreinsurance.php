<?php
$this->breadcrumbs=array(
	'Trucks'=>array('/courses/courses/managecourse'),
	'Insurance',
);
?>

<style>.emp_cntntbx{min-height:0px;}</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
 <?php $this->renderPartial('left_side');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
        
	<div class="edit_bttns">
    <ul>
	<?php
		if($model!=null){
	?>
    <li><?php echo CHtml::link(Yii::t('courses','<span>Edit Insurance</span>'), array('/insurance/update', 'id'=>$model->id),array('class'=>'edit last')); ?></li>
	<?php
		}else{
	?>
    <li><?php echo CHtml::link(Yii::t('courses','<span>Add Insurance</span>'), array('/insurance/create', 'id'=>$_REQUEST['id']),array('class'=>'edit last')); ?></li>
    <?php
		}
	?>
	</ul>
    </div>
	
    <div class="clear"></div>
    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
     <?php $this->renderPartial('tab');?>
    <div class="clear"></div>
	
	<div class="emp_cntntbx">
    <div class="table_listbx"> 

  <table width="100%">
	<tr class="listbxtop_hdng">
		<td colspan=2><span><?php echo Yii ::t('courses','CTPL Details');;?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Insurance Broker/ Agent :</span><span class="data"><?php   echo $model->ctpl_broker;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Insurer :</span><span class="data"><?php   echo $model->ctpl_insurer;?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Policy Number :</span><span class="data"><?php   echo $model->ctpl_policy_no;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Coverage :</span><span class="data"><?php   echo $model->ctpl_coverage;?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Date Issued :</span><span class="data"><?php   echo $model->ctpl_dateissued;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Date Expiry :</span><span class="data"><?php   echo $model->ctpl_dateexpire;?></span></td>
	</tr>
	<tr class="listbxtop_hdng">
		<td colspan=2><span><?php echo Yii ::t('courses','Compre Details');;?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Insurance Broker/ Agent :</span><span class="data"><?php   echo $model->compre_broker;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Insurer :</span><span class="data"><?php   echo $model->compre_insurer;?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Policy Number :</span><span class="data"><?php   echo $model->compre_policy_number;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Coverage :</span><span class="data"><?php   echo $model->compre_coverage;?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Date Issued :</span><span class="data"><?php   echo $model->compre_dateissued;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Date Expiry :</span><span class="data"><?php   echo $model->compre_dateexpire;?></span></td>
	</tr>
	<tr class="listbxtop_hdng">
		<td colspan=2><span><?php echo Yii ::t('courses','PA Details');;?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Insurance Broker/ Agent :</span><span class="data"><?php   echo $model->pa_broker;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Insurer :</span><span class="data"><?php   echo $model->pa_insurer;?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Policy Number :</span><span class="data"><?php   echo $model->pa_policy_no;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Coverage :</span><span class="data"><?php   echo $model->pa_coverage;?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Date Issued :</span><span class="data"><?php   echo $model->pa_dateissued;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Date Expiry :</span><span class="data"><?php   echo $model->pa_dateexpire;?></span></td>
	</tr>
  </table>

   
 </div>
 
 </div>
 
	
    </div>
    <div class="clear"></div>
 <div class="emp_cntntbx">
  <!--div class="ea_pdf" style="top:4px; right:6px;"><?php #echo CHtml::link('<img src="images/pdf-but.png">', array('courses/pdf','id'=>$_REQUEST['id']),array('target'=>'_blank')); ?></div-->
   
 </div>
 
 
 
</div>
</div>
</div>
    
    </td>
  </tr>
</table>
