	<?php
		$emp_id_1= '';
	 if(Yii::app()->controller->action->id=='create')
			{
			$adm_no	= Courses::model()->findAll(array('order' => 'id DESC','limit' => 1));
			$adm_no_1=$adm_no[0]['id']+1;
			}
			else
			{
			$adm_no	= Courses::model()->findByAttributes(array('id' => $_REQUEST['id']));
			$adm_no_1=$adm_no->association_truck_id;
			}
			$criteria = new CDbCriteria;
			$criteria->compare('active',1);
			$adm_no_1 = 'T-'.sprintf('%07u',$adm_no_1);
			?>
	<div class="captionWrapper">
        <ul>
            <li><h2 class="cur">Truck Details</h2></li>
        </ul>
	</div>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'courses-form',
	'enableAjaxValidation'=>false,
));  
	if($form->errorSummary($model)){
	?>
        <div class="errorSummary">Input Error<br />
        	<span>Please fix the following error(s).</span>
        </div>

	<?php 
	}
	?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <div class="formCon" style="background:url(images/yellow-pattern.png); width:100%; border:0px #fac94a solid; color:#000; ">
        <div class="formConInner"  style="padding:5px;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
		  <td align="right"><?php echo $form->labelEx($model,Yii::t('courses','association_truck_id')); ?></td>
                  <td style="padding-left:8px;" width="30%">
		    <?php 
		      if($model->isNewRecord){
			echo $form->textField($model,'association_truck_id',array('size'=>20,'maxlength'=>255,'value'=>$adm_no_1,'disabled'=>true,'style'=>'width:70% !important'));
		      }else{
			echo $form->textField($model,'association_truck_id',array('size'=>20,'maxlength'=>255,'disabled'=>true,'style'=>'width:70% !important'));
		      }
			echo $form->hiddenField($model,'association_truck_id',array('value'=>$adm_no_1));
                        echo $form->error($model,'association_truck_id'); 
		     ?>
		   </td>
		   <td align="right">
			<?php echo $form->labelEx($model,Yii::t('courses','association_trucker_id')); ?></td>
                    <td style="padding-left:8px;">
			<?php
				$asstrucker = CHtml::listData(Students::model()->findAll(),'assoc_truckerid','trucker');
				echo $form->dropDownList($model,'association_trucker_id',$asstrucker,array('empty'=>'Select Trucker'));
                        	echo $form->error($model,'association_trucker_id'); ?>
		    </td>
		</tr>
	       </table>
		</div>
	</div>
<div class="formCon">
<div class="formConInner">
<div>
<h3>Truck Details</h3>
	<table width="90%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','truck_status')); ?></td>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','tractor_type')); ?></td>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','Brand')); ?></td>
	  </tr>
	  <tr>
	    <td><?php
	    	$truckerstatus = CHtml::listData(TruckerStatus::model()->findAll($criteria),'id','name');
		echo $form->dropDownList($model,'truck_status',$truckerstatus,array('empty'=>'Select Status'));
		echo $form->error($model,'truck_status');
	    ?></td>
	    <td><?php
	    	$body = CHtml::listData(TruckersTractorType::model()->findAll($criteria),'id','name');
		echo $form->dropDownList($model,'tractor_type',$body,array('empty'=>'Select Body'));
		echo $form->error($model,'tractor_type');
	    ?></td>
	    <td><?php
	    	$brand = CHtml::listData(TruckerBrand::model()->findAll($criteria),'id','name');
		echo $form->dropDownList($model,'brand',$body,array('empty'=>'Select Brand'));
		echo $form->error($model,'brand');
	    ?></td>
	  </tr>
	  <tr><td colspan=3>&nbsp;</td></tr>
	  <tr>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','wheeler')); ?></td>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','year_model')); ?></td>
	    <td></td>
	  </tr>
	  <tr>
	    <td><?php
	    	$wheeler = CHtml::listData(TruckerWheeler::model()->findAll($criteria),'id','name');
		echo $form->dropDownList($model,'wheeler',$wheeler,array('empty'=>'Select Number of Tires'));
		echo $form->error($model,'wheeler');
	    ?></td>
	    <td><?php
	       echo $form->textField($model,'year_model',array('maxlength'=>4));
	       echo $form->error($model,'year_model');
	    ?></td>
	    <td></td>
	  </tr>
	  <tr><td colspan=3>&nbsp;</td></tr>
	  <tr>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','truck_cr_number')); ?></td>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','truck_cr_dateissued')); ?></td>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','reg_owner')); ?></td>
	  </tr>
	  <tr>
	    <td><?php
	       echo $form->textField($model,'truck_cr_number',array('maxlength'=>4));
	       echo $form->error($model,'truck_cr_number');
	    ?></td>
	    <td><?php
		  $date = 'y-m-d';
		  $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'attribute'=>'truck_cr_dateissued',
			'model'=>$model,
			'options'=>array(
			  'showAnim'=>'fold',
			  'dateFormat'=>$date,
			  'changeMonth'=> true,
			  'changeYear'=>true,
			  'yearRange'=>'1950:2050'
			),
			'htmlOptions'=>array(
			  'style' => '',
			),
		));
		echo $form->error($model,'truck_cr_dateissued');
	    ?></td>
	    <td><?php
	       echo $form->textField($model,'reg_owner',array('maxlength'=>255));
	       echo $form->error($model,'reg_owner');
	    ?></td>
	  </tr>
	  <tr><td colspan=3>&nbsp;</td></tr>
	  <tr>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','truck_or_number')); ?></td>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','truck_or_dateissued')); ?></td>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','truck_or_dateexpire')); ?></td>
	  </tr>
	  <tr>
	    <td><?php
	       echo $form->textField($model,'truck_or_number',array('maxlength'=>255));
	       echo $form->error($model,'truck_or_number');
	    ?></td>
	    <td><?php
		  $date = 'y-m-d';
		  $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'attribute'=>'truck_or_dateissued',
			'model'=>$model,
			'options'=>array(
			  'showAnim'=>'fold',
			  'dateFormat'=>$date,
			  'changeMonth'=> true,
			  'changeYear'=>true,
			  'yearRange'=>'1950:2050'
			),
			'htmlOptions'=>array(
			  'style' => '',
			),
		));
		echo $form->error($model,'truck_or_dateissued');
	    ?></td>
	    <td><?php
		  $date = 'y-m-d';
		  $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'attribute'=>'truck_or_dateexpire',
			'model'=>$model,
			'options'=>array(
			  'showAnim'=>'fold',
			  'dateFormat'=>$date,
			  'changeMonth'=> true,
			  'changeYear'=>true,
			  'yearRange'=>'1950:2050'
			),
			'htmlOptions'=>array(
			  'style' => '',
			),
		));
		echo $form->error($model,'truck_or_dateexpire');
	    ?></td>
	  </tr>
	  <tr><td colspan=3>&nbsp;</td></tr>
	  <tr>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','truck_lto_field')); ?></td>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','truck_mv_file_number')); ?></td>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','plate_number')); ?></td>
	  </tr>
	  <tr>
	    <td><?php
	       echo $form->textField($model,'truck_lto_field',array('maxlength'=>255));
	       echo $form->error($model,'truck_lto_field');
	    ?></td>
	    <td><?php
	       echo $form->textField($model,'truck_mv_file_number',array('maxlength'=>255));
	       echo $form->error($model,'truck_mv_file_number');
	    ?></td>
	    <td><?php
	       echo $form->textField($model,'plate_number',array('maxlength'=>255));
	       echo $form->error($model,'plate_number');
	    ?></td>
	  </tr>
	  <tr><td colspan=3>&nbsp;</td></tr>
	  <tr>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','truck_engine_number')); ?></td>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','chasis_number')); ?></td>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','truck_gross_w')); ?></td>
	  </tr>
	  <tr>
	    <td><?php
	       echo $form->textField($model,'truck_engine_number',array('maxlength'=>255));
	       echo $form->error($model,'truck_engine_number');
	    ?></td>
	    <td><?php
	       echo $form->textField($model,'chasis_number',array('maxlength'=>255));
	       echo $form->error($model,'chasis_number');
	    ?></td>
	    <td><?php
	       echo $form->textField($model,'truck_gross_w',array('maxlength'=>255));
	       echo $form->error($model,'truck_gross_w');
	    ?></td>
	  </tr>
	  <tr><td colspan=3>&nbsp;</td></tr>
	  <tr>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','truck_net_capacity')); ?></td>
	    <td></td>
	    <td></td>
	  </tr>
	  <tr>
	    <td><?php
	       echo $form->textField($model,'truck_net_capacity',array('maxlength'=>5));
	       echo $form->error($model,'truck_net_capacitiy');
	    ?></td>
	  <td></td>
	  <td></td>
	  </tr>
	  <tr><td colspan=3>&nbsp;</td></tr>
	  <tr><td colspan=3>&nbsp;</td></tr>
	</table>
	<h3>Franchise Details</h3>
	<table width="90%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','franchise_number')); ?></td>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','franchise_date_of_issuance')); ?></td>
	    <td><?php echo $form->labelEx($model,Yii::t('courses','franchise_date_of_expiration')); ?></td>
	  </tr>
	  <tr>
	    <td><?php
	       echo $form->textField($model,'franchise_number',array('maxlength'=>255));
	       echo $form->error($model,'franchise_number');
	    ?></td>
	    <td><?php
		  $date = 'y-m-d';
		  $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'attribute'=>'franchise_date_of_issuance',
			'model'=>$model,
			'options'=>array(
			  'showAnim'=>'fold',
			  'dateFormat'=>$date,
			  'changeMonth'=> true,
			  'changeYear'=>true,
			  'yearRange'=>'1950:2050'
			),
			'htmlOptions'=>array(
			  'style' => '',
			),
		));
		echo $form->error($model,'franchise_date_of_issuance');
	    ?></td>
	    <td><?php
		  $date = 'y-m-d';
		  $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'attribute'=>'franchise_date_of_expiration',
			'model'=>$model,
			'options'=>array(
			  'showAnim'=>'fold',
			  'dateFormat'=>$date,
			  'changeMonth'=> true,
			  'changeYear'=>true,
			  'yearRange'=>'1950:2050'
			),
			'htmlOptions'=>array(
			  'style' => '',
			),
		));
		echo $form->error($model,'franchise_date_of_expiration');
	    ?></td>
	  </tr>
	</table>
	<br><br>
	<div style="padding:0px 0 0 0px; text-align:left">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Save',array('class'=>'formbut')); ?>
	</div>

<?php $this->endWidget(); ?>
</div>
</div>
