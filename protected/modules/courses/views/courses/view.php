<?php
  $this->breadcrumbs=array(
	'Trucks'=>array('/courses/courses/managecourse',),
	'Truck Details',
  );
  $role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
?>
<style>.emp_cntntbx{min-height:0px;}</style>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="247" valign="top"><?php $this->renderPartial('left_side');?></td>
      <td valign="top">
      <div class="cont_right formWrapper">
	<?php if($role->role==1){ ?>
	<div class="edit_bttns">
	<ul>
	  <li><?php echo CHtml::link(Yii::t('employees','<span>Edit</span>'), array('update', 'id'=>$_REQUEST['id']),array('class'=>'edit last')); ?></li>
	  <li><?php echo CHtml::link(Yii::t('employees','<span>Trucks</span>'), array('/courses/courses/managecourse'),array('class'=>'edit last')); ?></li>
        </ul>
    </div>
    <?php }  ?>

    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
<?php
	$this->renderPartial('tab');
?>
    <div class="clear"></div>
 <div class="emp_cntntbx">
 <div class="table_listbx">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr class="listbxtop_hdng"><td colspan=2><?php echo Yii::t('employees','Truck Details');?></td><tr>
	<tr>
	  <td class="subhdng_nrmal">
	    <span class="datahead">Association Truck Id :</span><span class="data"><?php echo $model->association_truck_id;?></span>
	  </td>
	  <td class="subhdng_nrmal"><span class="datahead">Trucker :</span><span class="data">
	  <?php
		$truckername = Students::model()->findByAttributes(array('assoc_truckerid'=>$model->association_trucker_id));
		echo $truckername->trucker;
	  ?></span>
	  </td>
	</tr>
    </table> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	  <td class="subhdng_nrmal">
	    <span class="datahead">Status :</span>
	    <span class="data">
	    <?php $status = TruckerStatus::model()->findByAttributes(array('id'=>$model->truck_status)); echo $status->name; ?>
	    </span>
	  </td>
	  <td class="subhdng_nrmal">
	    <span class="datahead">Body Type :</span>
	    <span class="data">
	    <?php $body = TruckersTractorType::model()->findByAttributes(array('id'=>$model->tractor_type)); echo $body->name; ?>
	    </span>
	  </td>
	  <td class="subhdng_nrmal">
	    <span class="datahead">Brand :</span>
	    <span class="data">
	    <?php $brand = TruckerBrand::model()->findByAttributes(array('id'=>$model->brand)); echo $brand->name; ?>
	    </span>
	  </td>
	</tr>
	<tr>
	  <td class="subhdng_nrmal">
	    <span class="datahead">Number of Tires :</span>
	    <span class="data">
	    <?php $wheeler = TruckerWheeler::model()->findByAttributes(array('id'=>$model->wheeler)); echo $wheeler->name; ?>
	    </span>
	  </td>
	  <td class="subhdng_nrmal">
	    <span class="datahead">Year Model :</span>
	    <span class="data"><?php echo $model->year_model; ?></span>
	  </td>
	  <td class="subhdng_nrmal">
	    <span class="datahead"></span><span class="data">
	    <span class="data"></span>
	  </td>
	</tr>
	<tr>
	  <td class="subhdng_nrmal">
	    <span class="datahead">CR Number :</span>
	    <span class="data"><?php echo $model->truck_cr_number; ?></span>
	  </td>
	  <td class="subhdng_nrmal">
	    <span class="datahead">CR Date Issued :</span>
	    <span class="data"><?php echo date('M. d, Y',strtotime($model->truck_cr_dateissued)); ?></span>
	  </td>
	  <td class="subhdng_nrmal">
	    <span class="datahead">Registered Owner :</span>
	    <span class="data"><?php echo $model->reg_owner; ?></span>
	  </td>
	</tr>
	<tr>
	  <td class="subhdng_nrmal">
	    <span class="datahead">OR Number :</span>
	    <span class="data"><?php echo $model->truck_or_number; ?></span>
	  </td>
	  <td class="subhdng_nrmal">
	    <span class="datahead">OR Date Issued :</span>
	    <span class="data"><?php echo date('M. d, Y',strtotime($model->truck_or_dateissued)); ?></span>
	  </td>
	  <td class="subhdng_nrmal">
	    <span class="datahead">OR Date Expire :</span>
	    <span class="data"><?php echo date('M. d, Y',strtotime($model->truck_or_dateexpire)); ?></span>
	  </td>
	</tr>
	<tr>
	  <td class="subhdng_nrmal">
	    <span class="datahead">LTO Field Office :</span>
	    <span class="data"><?php echo $model->truck_lto_field; ?></span>
	  </td>
	  <td class="subhdng_nrmal">
	    <span class="datahead">MV File Number :</span>
	    <span class="data"><?php echo $model->truck_mv_file_number; ?></span>
	  </td>
	  <td class="subhdng_nrmal">
	    <span class="datahead">Plate Number :</span>
	    <span class="data"><?php echo $model->plate_number; ?></span>
	  </td>
	</tr>
	<tr>
	  <td class="subhdng_nrmal">
	    <span class="datahead">Engine Number :</span>
	    <span class="data"><?php echo $model->truck_engine_number; ?></span>
	  </td>
	  <td class="subhdng_nrmal">
	    <span class="datahead">Chasis Number :</span>
	    <span class="data"><?php echo $model->chasis_number; ?></span>
	  </td>
	  <td class="subhdng_nrmal">
	    <span class="datahead">Gross Weight :</span>
	    <span class="data"><?php echo $model->truck_gross_w; ?></span>
	  </td>
	</tr>
	<tr>
	  <td class="subhdng_nrmal">
	    <span class="datahead">Net Capacity :</span>
	    <span class="data"><?php echo $model->truck_net_capacity; ?></span>
	  </td>
	  <td class="subhdng_nrmal">
	    <span class="datahead"></span>
	    <span class="data"></span>
	  </td>
	  <td class="subhdng_nrmal">
	    <span class="datahead"></span><span class="data">
	    <span class="data"></span>
	  </td>
	</tr>
	<tr class="listbxtop_hdng"><td colspan=3><?php echo Yii::t('employees','Franchise Details');?></td><tr>
	<tr>
	  <td class="subhdng_nrmal">
	    <span class="datahead">Franchise Number :</span>
	    <span class="data"><?php echo $model->franchise_number; ?></span>
	  </td>
	  <td class="subhdng_nrmal">
	    <span class="datahead">Date of Issuance :</span>
	    <span class="data"><?php echo date('M. d, Y',strtotime($model->franchise_date_of_issuance)); ?></span>
	  </td>
	  <td class="subhdng_nrmal">
	    <span class="datahead">Date of Expiration :</span>
	    <span class="data"><?php echo date('M. d, Y',strtotime($model->franchise_date_of_expiration)); ?></span>
	  </td>
	</tr>
    </table> 
</div>
</div>
</div>

    </td>
  </tr>
</table>
