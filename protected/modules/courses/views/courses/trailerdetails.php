<?php
  $this->breadcrumbs=array(
	'Trucks'=>array('/courses/courses/managecourse'),
	'View',
  );
  $role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
?>
<style>.emp_cntntbx{min-height:0px;}</style>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="247" valign="top"><?php $this->renderPartial('left_side');?></td>
      <td valign="top">
      <div class="cont_right formWrapper">
	<?php if($role->role==1){ ?>
	<div class="edit_bttns">
	<ul>
	  <li><?php echo CHtml::link(Yii::t('employees','<span>Edit</span>'), array('update', 'id'=>$_REQUEST['id']),array('class'=>'edit last')); ?></li>
	  <li><?php echo CHtml::link(Yii::t('employees','<span>Trucks</span>'), array('/courses/courses/managecourse'),array('class'=>'edit last')); ?></li>
        </ul>
    </div>
    <?php }  ?>

    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
	<?php $this->renderPartial('tab');?>
    <div class="clear"></div>
 <div class="emp_cntntbx">
    <div class="table_listbx">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr class="listbxtop_hdng">
			<td colspan=2><?php echo Yii::t('employees','Trailer Details');?></td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Type :</span>
				<span class="data">
				<?php
				$trailertype = TruckerTrailerType::model()->findByAttributes(array('id'=>$model->trailer_type));
  				echo $trailertype->name;
				?></span>
			</td>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Plate Number :</span>
				<span class="data"><?php echo $model->trailer_plate_number;?></span>
			</td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Footer :</span>
				<span class="data">
				<?php
				$trailerfooter = TruckTrailerFooter::model()->findByAttributes(array('id'=>$model->trailer_footer));
  				echo $trailerfooter->name;
				?></span>
			</td>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Axle :</span>
				<span class="data">
				<?php
				$traileraxle = TruckTrailerAxle::model()->findByAttributes(array('id'=>$model->trailer_axle));
  				echo $traileraxle->name;
				?></span>
			</td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Chasis Number :</span>
				<span class="data"><?php echo $model->trailer_chasis_number;?></span>
			</td>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Chassis Serial Number :</span>
				<span class="data"><?php echo $model->trailer_chasis_serial; ?></span>
			</td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Chassis Serial Number :</span>
				<span class="data"><?php echo $model->trailer_chasis_serial;?></span>
			</td>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Registered Owner :</span>
				<span class="data"><?php echo $model->trailer_reg_owner; ?></span>
			</td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">MV File Number :</span>
				<span class="data"><?php echo $model->trailer_mv_file_number;?></span>
			</td>
			<td class="subhdng_nrmal">
				<span class="datahead">CR Number :</span>
				<span class="data"><?php echo $model->trailer_cr_number;?></span>
			</td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">CR Date Isssued :</span>
				<span class="data">
				<?php 
				if($model->trailer_cr_dateissued !=null and $model->trailer_cr_dateissued != '0000-00-00'){
					echo date('M. d, Y',strtotime($model->trailer_cr_dateissued)); } 
				?>
				</span>
			</td>
			<td class="subhdng_nrmal">
				<span class="datahead">CR Registered Owner :</span>
				<span class="data"><?php echo $model->trailer_cr_reg_owner;?></span>
			</td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">OR Number :</span>
				<span class="data"><?php echo $model->trailer_or_number;?></span></td>
			<td class="subhdng_nrmal">
				<span class="datahead">OR Date Issued :</span>
				<span class="data">
				<?php 
				if($model->trailer_or_dateissued !=null and $model->trailer_or_dateissued !='0000-00-00'){
					echo date('M. d, Y',strtotime($model->trailer_or_dateissued)); } 
				?>
				</span>
			</td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">OR Date Expire :</span>
				<span class="data">
				<?php 
				if($model->trailer_or_dateexpire !=null and $model->trailer_or_dateexpire !='0000-00-00'){
					echo date('M. d, Y',strtotime($model->trailer_or_dateexpire)); } 
				?>
				</span>
			</td>
			<td class="subhdng_nrmal">
				<span class="datahead">LTO Field Office	:</span>
				<span class="data"><?php echo $model->trailer_lto_field;?></span></td>
		</tr>
		</table>
  <div class="ea_pdf" style="top:4px; right:6px;"><?php echo CHtml::link('<img src="images/pdf-but.png">', array('Employees/pdf','id'=>$_REQUEST['id']),array('target'=>'_blank')); ?></div>

 </div>

 </div>



</div>
</div>
</div>

    </td>
  </tr>
</table>
