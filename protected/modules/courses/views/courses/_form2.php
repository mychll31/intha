	<?php
		$emp_id_1= '';
	 if(Yii::app()->controller->action->id=='create')
			{
			$adm_no	= Courses::model()->findAll(array('order' => 'id DESC','limit' => 1));
			$adm_no_1=$adm_no[0]['id']+1;
			}
			else
			{
			$adm_no	= Courses::model()->findByAttributes(array('id' => $_REQUEST['id']));
			$adm_no_1=$adm_no->association_truck_id;
			}
			$criteria = new CDbCriteria;
			$criteria->compare('active',1);
			$adm_no_1 = 'T-'.sprintf('%07u',$adm_no_1);
			?>
	<div class="captionWrapper">
        <ul>
            <li><h2 class="cur">Truck Details</h2></li>
        </ul>
	</div>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'courses-form',
	'enableAjaxValidation'=>false,
));  
	if($form->errorSummary($model)){
	?>
        <div class="errorSummary">Input Error<br />
        	<span>Please fix the following error(s).</span>
        </div>

	<?php 
	}
	?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <div class="formCon" style="background:url(images/yellow-pattern.png); width:100%; border:0px #fac94a solid; color:#000; ">
        <div class="formConInner"  style="padding:5px;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
					<td align="right"><?php echo $form->labelEx($model,Yii::t('courses','association_truck_id')); ?></td>
                    <td style="padding-left:8px;">
						<?php 
							if($model->isNewRecord){
							echo $form->textField($model,'association_truck_id',array('size'=>20,'maxlength'=>255,'value'=>$adm_no_1,'disabled'=>true,));}
							else{
							echo $form->textField($model,'association_truck_id',array('size'=>20,'maxlength'=>255,'disabled'=>true,));
							}
							  echo $form->hiddenField($model,'association_truck_id',array('value'=>$adm_no_1)); ?>
                        <?php echo $form->error($model,'association_truck_id'); ?>
					</td>
					<td align="right"><?php echo $form->labelEx($model,Yii::t('courses','association_trucker_id')); ?></td>
                    <td style="padding-left:8px;">
						<?php
							$asstrucker = CHtml::listData(Students::model()->findAll(),'assoc_truckerid','trucker');
							echo $form->dropDownList($model,'association_trucker_id',$asstrucker,array('empty'=>'Select Trucker')); ?>	
                        <?php echo $form->error($model,'association_trucker_id'); ?>
					</td>
				</tr>
			</table>
		</div>
	</div>
<div class="formCon">
<div class="formConInner">
<div>
<h3>Truck Details</h3>
	<table width="90%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><?php echo $form->labelEx($model,Yii::t('courses','truck_status')); ?></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td><?php
				$truckstat = CHtml::listData(TruckerStatus::model()->findAll($criteria),'id','name');
				echo $form->dropDownList($model,'truck_status',$truckstat,array('empty'=>'')); ?>
				<?php echo $form->error($model,'truck_status'); ?></td>
			<td></td>
			<td></td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
		<tr>
			<td><?php echo $form->labelEx($model,Yii::t('courses','tractor_type')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','brand')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','wheeler')); ?></td>
		</tr>
		<tr>
			<td><?php
				$tractorType = CHtml::listData(TruckersTractorType::model()->findAll($criteria),'id','name');
				echo $form->dropDownList($model,'tractor_type',$tractorType,array('empty'=>'')); ?>
				<?php echo $form->error($model,'tractor_type'); ?></td>
			<td><?php
				$brand = CHtml::listData(TruckerBrand::model()->findAll($criteria),'id','name');
				echo $form->dropDownList($model,'brand',$brand,array('empty'=>'')); ?>
				<?php echo $form->error($model,'brand'); ?></td>
			<td><?php
				$wheel = CHtml::listData(TruckerWheeler::model()->findAll($criteria),'id','name');
				echo $form->dropDownList($model,'wheeler',$wheel,array('empty'=>'')); ?>
				<?php echo $form->error($model,'wheeler'); ?></td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
		<tr>
			<td><?php echo $form->labelEx($model,Yii::t('courses','year_model')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','truck_cr_number')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','truck_cr_dateissued')); ?></td>
		</tr>
		<tr>
			<td><?php echo $form->textField($model,'year_model'); ?>
				<?php echo $form->error($model,'year_model'); ?></td>
			<td><?php echo $form->textField($model,'truck_cr_number'); ?>
				<?php echo $form->error($model,'truck_cr_number'); ?></td>
			<td><?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'truck_cr_dateissued',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
			?>
				<?php echo $form->error($model,'truck_cr_dateissued'); ?></td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
		<tr>
			<td><?php echo $form->labelEx($model,Yii::t('courses','truck_cr_reg_owner')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','truck_or_number')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','truck_or_dateissued')); ?></td>
		</tr>
		<tr>
			<td><?php echo $form->textField($model,'truck_cr_reg_owner'); ?>
				<?php echo $form->error($model,'truck_cr_reg_owner'); ?></td>
			<td><?php echo $form->textField($model,'truck_or_number'); ?>
				<?php echo $form->error($model,'truck_or_number'); ?></td>
			<td><?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'truck_or_dateissued',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
			?>
				<?php echo $form->error($model,'truck_or_dateissued'); ?></td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
		<tr>
			<td><?php echo $form->labelEx($model,Yii::t('courses','truck_or_dateexpire')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','truck_lto_field')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','truck_mv_file_number')); ?></td>
		</tr>
		<tr>
			<td><?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'truck_or_dateexpire',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
			?>
				<?php echo $form->error($model,'truck_or_dateexpire'); ?></td>
			<td><?php echo $form->textField($model,'truck_lto_field'); ?>
				<?php echo $form->error($model,'truck_lto_field'); ?></td>
			<td><?php echo $form->textField($model,'truck_mv_file_number'); ?>
				<?php echo $form->error($model,'truck_mv_file_number'); ?></td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
		<tr>
			<td><?php echo $form->labelEx($model,Yii::t('courses','plate_number')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','truck_engine_number')); ?></td>
			<td></td>
		</tr>
		<tr>
		<td><?php echo $form->textField($model,'plate_number'); ?>
			<?php echo $form->error($model,'plate_number'); ?></td>
		<td><?php echo $form->textField($model,'truck_engine_number'); ?>
			<?php echo $form->error($model,'truck_engine_number'); ?></td>
		<td></td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
		<tr>
			<td><?php echo $form->labelEx($model,Yii::t('courses','chasis_number')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','truck_gross_w')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','truck_net_capacity')); ?></td>
		</tr>
		<tr>
			<td><?php echo $form->textField($model,'chasis_number'); ?>
				<?php echo $form->error($model,'chasis_number'); ?></td>
			<td><?php echo $form->textField($model,'truck_gross_w'); ?>
				<?php echo $form->error($model,'truck_gross_w'); ?></td>
			<td><?php echo $form->textField($model,'truck_net_capacity'); ?>
				<?php echo $form->error($model,'truck_net_capacity'); ?></td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
	</table>
	<br>
	<h3>Trailer Details</h3>
	<table width="90%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><?php echo $form->labelEx($model,Yii::t('courses','trailer_type')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','trailer_plate_number')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','trailer_footer')); ?></td>
		</tr>
		<tr>
			<td><?php 
				$trailer = CHtml::listData(TruckerTrailerType::model()->findAll($criteria),'id','name');
				echo $form->dropDownList($model,'trailer_type',$trailer,array('empty'=>''));?>
				<?php echo $form->error($model,'trailer_type'); ?></td>
			<td><?php echo $form->textField($model,'trailer_plate_number'); ?>
				<?php echo $form->error($model,'trailer_plate_number'); ?></td>
			<td><?php
				$trailerfooter = CHtml::listData(TruckTrailerFooter::model()->findAll($criteria),'id','name');
				echo $form->dropDownList($model,'trailer_footer',$trailerfooter,array('empty'=>'')); ?>
				<?php echo $form->error($model,'trailer_footer'); ?></td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
		<tr>
			<td><?php echo $form->labelEx($model,Yii::t('courses','trailer_axle')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','trailer_chasis_number')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','trailer_chasis_serial')); ?></td>
		</tr>
		<tr>
			<td><?php 
				$traileraxle = CHtml::listData(TruckTrailerAxle::model()->findAll($criteria),'id','name');
				echo $form->dropDownList($model,'trailer_axle',$traileraxle,array('empty'=>''));?>
				<?php echo $form->error($model,'trailer_axle'); ?></td>
			<td><?php echo $form->textField($model,'trailer_chasis_number'); ?>
				<?php echo $form->error($model,'trailer_chasis_number'); ?></td>
			<td><?php echo $form->textField($model,'trailer_chasis_serial'); ?>
				<?php echo $form->error($model,'trailer_chasis_serial'); ?></td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
		<tr>
			<td><?php echo $form->labelEx($model,Yii::t('courses','trailer_reg_owner')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','trailer_mv_file_number')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','trailer_cr_number')); ?></td>
		</tr>
		<tr>
			<td><?php echo $form->textField($model,'trailer_reg_owner'); ?>
				<?php echo $form->error($model,'trailer_reg_owner'); ?></td>
			<td><?php echo $form->textField($model,'trailer_mv_file_number'); ?>
				<?php echo $form->error($model,'trailer_mv_file_number'); ?></td>
			<td><?php echo $form->textField($model,'trailer_cr_number'); ?>
				<?php echo $form->error($model,'trailer_cr_number'); ?></td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
		<tr>
			<td><?php echo $form->labelEx($model,Yii::t('courses','trailer_cr_dateissued')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','trailer_cr_reg_owner')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','trailer_or_number')); ?></td>
		</tr>
		<tr>
			<td><?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'trailer_cr_dateissued',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
			?>
				<?php echo $form->error($model,'trailer_cr_dateissued'); ?></td>
			<td><?php echo $form->textField($model,'trailer_cr_reg_owner'); ?>
				<?php echo $form->error($model,'trailer_cr_reg_owner'); ?></td>
			<td><?php echo $form->textField($model,'trailer_or_number'); ?>
				<?php echo $form->error($model,'trailer_or_number'); ?></td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
		<tr>
			<td><?php echo $form->labelEx($model,Yii::t('courses','trailer_or_dateissued')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','trailer_or_dateexpire')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','trailer_lto_field')); ?></td>
		</tr>
		<tr>
			<td><?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'trailer_or_dateissued',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
			?>
				<?php echo $form->error($model,'trailer_or_dateissued'); ?></td>
			<td><?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'trailer_or_dateexpire',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
			?>
				<?php echo $form->error($model,'trailer_or_dateexpire'); ?></td>
			<td><?php echo $form->textField($model,'trailer_lto_field',array('size'=>30,'maxlength'=>225)); ?>
				<?php echo $form->error($model,'trailer_lto_field'); ?></td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
	</table>
	<br>
	<h3>Franchise Details</h3>
	<table width="70%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><?php echo $form->labelEx($model,Yii::t('courses','franchise_number')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','franchise_date_of_issuance')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','franchise_date_of_expiration')); ?></td>
		</tr>
		<tr>
			<td><?php echo $form->textField($model,'franchise_number',array('size'=>30,'maxlength'=>225)); ?>
				<?php echo $form->error($model,'franchise_number'); ?></td>
			<td>
			<?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'franchise_date_of_issuance',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
			?>
			<?php echo $form->error($model,'franchise_date_of_issuance'); ?>
			</td>
			<td>
			<?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'franchise_date_of_expiration',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
			?>
			<?php echo $form->error($model,'franchise_date_of_expiration'); ?>
			</td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
	</table>
	<br>
	<h3>Notes</h3>
	<table width="90%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><?php echo $form->labelEx($model,Yii::t('courses','type_of_note')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','date_of_note')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','details_of_note')); ?></td>
		</tr>
		<tr>
			<td><?php 
				$type_note = CHtml::listData(TruckerTypeNote::model()->findAll($criteria),'id','name');
				echo $form->dropDownList($model,'type_of_note',$type_note,array('empty'=>''));?>
				<?php echo $form->error($model,'type_of_note'); ?></td>
			<td>
			<?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'date_of_note',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
			?>
				<?php echo $form->error($model,'date_of_note'); ?></td>
			<td rowspan=2><?php echo $form->textArea($model,'details_of_note', array('col'=>6,'row'=>30)); ?>
				<?php echo $form->error($model,'details_of_note'); ?></td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
		<tr>
			<td><?php echo $form->labelEx($model,Yii::t('courses','reported_by')); ?></td>
			<td><?php echo $form->labelEx($model,Yii::t('courses','date_reported_by')); ?></td>
			
		</tr>
		<tr>
			<td><?php echo $form->textField($model,'reported_by'); ?>
				<?php echo $form->error($model,'reported_by'); ?></td>
			<td>
			<?php
					$date = 'yy-mm-dd';
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'date_reported_by',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
			?>
			<?php echo $form->error($model,'date_reported_by'); ?></td>
			<td></td>
		</tr>
		<tr><td colspan=3>&nbsp;</td></tr>
	</table>
	<br><br>
    <!-- Batch Form Ends -->
	<div style="padding:0px 0 0 0px; text-align:left">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Save',array('class'=>'formbut')); ?>
	</div>

<?php $this->endWidget(); ?>
</div>
</div>