<?php
$this->breadcrumbs=array(
	'Trucker'=>array('index'),
	'View',
);


?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    <?php $this->renderPartial('profileleft');?>
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <h1 style="margin-top:.67em;"><?php echo Yii::t('students','Truck List');?></h1>
        
    <div class="edit_bttns last">
 
    </div>
        
    <div class="clear"></div>
    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
    <div class="clear"></div>
    <div class="tablebx">  
         <div class="pagecon">
		<br>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr class="tablebx_topbg">
				<td><?php echo Yii::t('empLicense','No.');?></td>	
				<td><?php echo Yii::t('empLicense','Truck ID');?></td>
				<td><?php echo Yii::t('empLicense','Plate Number');?></td>
				<td><?php echo Yii::t('empLicense','Engine Number');?></td>
				<td><?php echo Yii::t('empLicense','Description');?></td>
				<td><?php echo Yii::t('empLicense','Registered Owner');?></td>
			</tr>
			<?php
				$cls="even"; $ctr = 0;
				$criteria = new CDbCriteria;
				$userid=User::model()->findByAttributes(array('id'=>Yii::app()->user->id));
				$criteria->compare('association_trucker_id',$userid->uid);
				$criteria->order = 'id ASC';
				$posts=Courses::model()->findAll($criteria);
				
				foreach($posts as $post){ ?>
					<tr class=<?php echo $cls;?> id=<?php echo $i;?>>
					<td><?php $ctr++; echo $ctr; ?></td>
					<td><?php echo $post->association_truck_id;
					?></td>
					<td><?php 
					echo $post->plate_number; ?></td>
					<td><?php 
					echo $post->truck_engine_number; ?></td>
					<td><?php echo $post->description; ?></td>
					<td><?php echo $post->reg_owner; ?></td>
					</tr><?php	if($cls=="even"){$cls="odd";}else{$cls="even"; }$i++;} ?>
		</table>
    </div>
    </div>
    </div>
   </div>
    </td>
  </tr>
</table>
