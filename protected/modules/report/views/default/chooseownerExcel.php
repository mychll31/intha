<?php

set_time_limit(10);

require_once "/protected/vendors/excelwriter/class.writeexcel_workbook.inc.php";
require_once "/protected/vendors/excelwriter/class.writeexcel_worksheet.inc.php";

$fname = tempnam("/tmp", "simple.xls");
$workbook = &new writeexcel_workbook($fname);
$worksheet = &$workbook->addworksheet();

# The general syntax is write($row, $column, $token). Note that row and
# column are zero indexed
#

# Header
$worksheet->write(0, 0,  "INTEGRATED NORTH HARBOR TRUCKERS ASSOCIATION");
$worksheet->write(1, 0,  "Truck Master List");

$worksheet->write(3, 0,  $owner);
$worksheet->write(4, 0,  "");
$worksheet->write(4, 1,  "Company");
$worksheet->write(4, 2,  "Plate No.");
$worksheet->write(4, 3,  "Chasis/Serial No.");
$worksheet->write(4, 4,  "Type/Make");

$x =4;
$no=0;
foreach($posts as $emp){
	$x++;
	$no++;
	$worksheet->write($x, 0, $no);
	$truckername = Students::model()->findByAttributes(array('assoc_truckerid'=>$emp->association_trucker_id));
	$worksheet->write($x, 1, strtoupper($truckername->trucker));
	$worksheet->write($x, 2, $emp->plate_number);
	$worksheet->write($x, 3, $emp->chasis_number);
	
	$brand = TruckerBrand::model()->findByAttributes(array('id'=>$emp->brand));
	$tractorType = TruckersTractorType::model()->findByAttributes(array('id'=>$emp->tractor_type));
	$wheel = TruckerWheeler::model()->findByAttributes(array('id'=>$emp->wheeler));
	
	$worksheet->write($x, 4, strtoupper($brand->name.' '.$tractorType->name.' '.$wheel->name));
}

$workbook->close();

header("Content-Type: application/x-msexcel; name=\"TruckMasterlist_".$owner.'_'.time().".xls\"");
header("Content-Disposition: inline; filename=\"TruckMasterlist_".$owner.'_'.time().".xls\"");
$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);

?>
