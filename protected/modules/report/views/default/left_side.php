<?php Yii::app()->clientScript->registerCoreScript('jquery');

         //IMPORTANT about Fancybox.You can use the newest 2.0 version or the old one
        //If you use the new one,as below,you can use it for free only for your personal non-commercial site.For more info see
		//If you decide to switch back to fancybox 1 you must do a search and replace in index view file for "beforeClose" and replace with 
		//"onClosed"
        // http://fancyapps.com/fancybox/#license
          // FancyBox2
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js_plugins/fancybox2/jquery.fancybox.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/js_plugins/fancybox2/jquery.fancybox.css', 'screen');
         // FancyBox
         //Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js_plugins/fancybox/jquery.fancybox-1.3.4.js', CClientScript::POS_HEAD);
         // Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js_plugins/fancybox/jquery.fancybox-1.3.4.css','screen');
        //JQueryUI (for delete confirmation  dialog)
         Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js_plugins/jqui1812/js/jquery-ui-1.8.12.custom.min.js', CClientScript::POS_HEAD);
         Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js_plugins/jqui1812/css/dark-hive/jquery-ui-1.8.12.custom.css','screen');
          ///JSON2JS
         Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js_plugins/json2/json2.js');
       

           //jqueryform js
               Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js_plugins/ajaxform/jquery.form.js', CClientScript::POS_HEAD);
              Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js_plugins/ajaxform/form_ajax_binding.js', CClientScript::POS_HEAD);
              Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js_plugins/ajaxform/client_val_form.css','screen');  ?>
              
              



<!--upgrade_div_starts-->
<div class="upgrade_bx">
	<div class="up_banr_imgbx"><a href="http://open-school.org/contact.php" target="_blank"><img src="http://tryopenschool.com/images/promo_bnnr_innerpage.png" width="231" height="200" /></a></div>
	<div class="up_banr_firstbx">
   	  <h1>You are Using Community Edition</h1>
	  <a href="http://open-school.org/contact.php" target="_blank">upgrade to premium version!</a>
    </div>
	
</div>
<!--upgrade_div_ends-->

<div id="othleft-sidebar">
<!--<div class="lsearch_bar">
             	<input type="text" value="Search" class="lsearch_bar_left" name="">
                <input type="button" class="sbut" name="">
                <div class="clear"></div>
  </div> --> <h1><?php echo Yii::t('Courses','Master List');?></h1>
                    
                    <?php
			function t($message, $category = 'cms', $params = array(), $source = null, $language = null) 
{
    return $message;
}

			$this->widget('zii.widgets.CMenu',array(
			'encodeLabel'=>false,
			'activateItems'=>true,
			'activeCssClass'=>'list_active',
			'items'=>array(
				array('label'=>Yii::t('Courses','Employees').'<span>'.Yii::t('Courses','Employee Master list').'</span>', 'url'=>array('/report/default/employees'), 'active'=> ((Yii::app()->controller->action->id=='employees') or (Yii::app()->controller->action->id=='index') ? true : false),'linkOptions'=>array('class'=>'lbook_ico' )),
				
				array('label'=>Yii::t('Courses','Truckers').'<span>'.Yii::t('Courses','Truckers Master list').'</span>', 'url'=>array('/report/default/truckers'), 'active'=> ((Yii::app()->controller->action->id=='truckers') ? true : false),'linkOptions'=>array('class'=>'lbook_ico' )),
				
				array('label'=>Yii::t('Courses','Trucks').'<span>'.Yii::t('Courses','Truck Master list').'</span>', 'url'=>array('/report/default/trucks'), 'active'=> ((Yii::app()->controller->action->id=='trucks') ? true : false),'linkOptions'=>array('class'=>'lbook_ico' )),
				
				array('label'=>''.'<h1>'.Yii::t('Courses','Certifications').'</h1>'),
				
				array('label'=>Yii::t('Courses','Trucker Certification').'<span>'.Yii::t('Courses','Trucker List of Trucks').'</span>', 'url'=>array('/report/default/truckerscert'), 'active'=> ((Yii::app()->controller->action->id=='truckerscert') ? true : false),'linkOptions'=>array('class'=>'sl_ico' )),
				
				array('label'=>''.'<h1>'.Yii::t('Courses','Inland Marine Report').'</h1>'),
				
				array('label'=>Yii::t('Courses','Form').'<span>'.Yii::t('Courses','Excel and Pdf Report').'</span>', 'url'=>array('/report/default/form'), 'active'=> ((Yii::app()->controller->action->id=='form') ? true : false),'linkOptions'=>array('class'=>'lbook_ico' )),
				
				array('label'=>Yii::t('Courses','Change Plate').'<span>'.Yii::t('Courses','Excel and Pdf Report').'</span>', 'url'=>array('/report/default/changeplate'), 'active'=> ((Yii::app()->controller->action->id=='changeplate') ? true : false),'linkOptions'=>array('class'=>'lbook_ico' )),
				
				array('label'=>Yii::t('Courses','Change Motor').'<span>'.Yii::t('Courses','Excel and Pdf Report').'</span>', 'url'=>array('/report/default/changemotor'), 'active'=> ((Yii::app()->controller->action->id=='changemotor') ? true : false),'linkOptions'=>array('class'=>'lbook_ico' )),
				
				array('label'=>Yii::t('Courses','Change Chasis').'<span>'.Yii::t('Courses','Excel and Pdf Report').'</span>', 'url'=>array('/report/default/changechasis'), 'active'=> ((Yii::app()->controller->action->id=='changechasis') ? true : false),'linkOptions'=>array('class'=>'lbook_ico' )),
				
				array('label'=>Yii::t('Courses','Replacement Unit').'<span>'.Yii::t('Courses','Excel and Pdf Report').'</span>', 'url'=>array('/report/default/replacementunit'), 'active'=> ((Yii::app()->controller->action->id=='replacementunit') ? true : false),'linkOptions'=>array('class'=>'lbook_ico' )),
				
				array('label'=>Yii::t('Courses','Change Ownership').'<span>'.Yii::t('Courses','Excel and Pdf Report').'</span>', 'url'=>array('/report/default/changeownership'), 'active'=> ((Yii::app()->controller->action->id=='changeownership') ? true : false),'linkOptions'=>array('class'=>'lbook_ico' )),
				
			))); ?>
		<div id="subject-name-ajax-grid"></div>

 <script type="text/javascript">

	$(document).ready(function () {
            //Hide the second level menu
            $('#othleft-sidebar ul li ul').hide();            
            //Show the second level menu if an item inside it active
            $('li.list_active').parent("ul").show();
            
            $('#othleft-sidebar').children('ul').children('li').children('a').click(function () {                    
                
                 if($(this).parent().children('ul').length>0){                  
                    $(this).parent().children('ul').toggle();    
                 }
                 
            });
          
            
        });
		
		
		//CREATE 
    
    $('#add_subject-name-ajax').bind('click', function() {
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->request->baseUrl;?>/index.php?r=subjectNameAjax/returnForm",
            data:{"YII_CSRF_TOKEN":"<?php echo Yii::app()->request->csrfToken;?>"},
                beforeSend : function() {
                    $("#subject-name-ajax-grid").addClass("ajax-sending");
                },
                complete : function() {
                    $("#subject-name-ajax-grid").removeClass("ajax-sending");
                },
            success: function(data) {
                $.fancybox(data,
                        {    "transitionIn"      : "elastic",
                            "transitionOut"   : "elastic",
                            "speedIn"                : 600,
                            "speedOut"            : 200,
                            "overlayShow"     : false,
                            "hideOnContentClick": false,
							"afterClose":    function() {
								window.location.reload();
								} 
							
							
							
                             //onclosed function
                        });//fancybox
            } //success
        });//ajax
        return false;
    });//bind
	
    </script>
    
   