<?php

set_time_limit(10);

require_once "/protected/vendors/excelwriter/class.writeexcel_workbook.inc.php";
require_once "/protected/vendors/excelwriter/class.writeexcel_worksheet.inc.php";

$fname = tempnam("/tmp", "simple.xls");
$workbook = &new writeexcel_workbook($fname);
$worksheet = &$workbook->addworksheet();

# The general syntax is write($row, $column, $token). Note that row and
# column are zero indexed
#

# Header
$worksheet->write(0, 0,  "INTEGRATED NORTH HARBOR TRUCKERS ASSOCIATION");
$worksheet->write(1, 0,  "INLAND MARINE 2014");

$worksheet->write(3, 0,  $truckername);
$worksheet->write(4, 1,  "ORIGINAL UNIT");
$worksheet->write(5, 1,  "Plate No.");
$worksheet->write(5, 2,  "Motor No.");
$worksheet->write(5, 3,  "Chasis/Serial No.");
$worksheet->write(5, 4,  "Type/Make");
$worksheet->write(5, 5,  "Registered Owner");
$worksheet->write(4, 6,  "REPLACEMENT UNIT");
$worksheet->write(5, 6,  "Plate No.");
$worksheet->write(5, 7,  "Motor No.");
$worksheet->write(5, 8,  "Chasis/Serial No.");
$worksheet->write(5, 9,  "Type/Make");
$worksheet->write(5, 10,  "Registered Owner");

$x =5;
$no=0;
foreach($truckdetails as $trucks){
	$x++;
	$no++;
	$worksheet->write($x, 0, $no);
	$worksheet->write($x, 1, $trucks->plate_number);
	$worksheet->write($x, 2, $trucks->motor);
	$worksheet->write($x, 3, $trucks->trailer_chasis_serial);
	foreach($truckbrand as $tbrand){
		if($tbrand->id == $trucks->brand){
			$brand =$tbrand->name;
		}
	}
	$worksheet->write($x, 4, $brand);
	$worksheet->write($x, 5, $trucks->reg_owner);
	$worksheet->write($x, 6, "");
	$worksheet->write($x, 7, "");
	$worksheet->write($x, 8, "");
	$worksheet->write($x, 9, "");
	$worksheet->write($x, 10, "");
}

$workbook->close();

header("Content-Type: application/x-msexcel; name=\"inland_replacementunit_".time().".xls\"");
header("Content-Disposition: inline; filename=\"inland_replacementunit_".time().".xls\"");
$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);

?>
