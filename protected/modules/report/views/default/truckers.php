<?php
$this->breadcrumbs=array(
	'Report'=>array('index'),
	'Truckers Master List'
);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
    <?php $this->renderPartial('left_side');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <h1></h1>
	<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employees',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	)); ?>
	<?php if($error !=null){echo '<div class="errorSummary">Input Error<br />
    <span>'.$error.'</span></div>';}?>
    <div class="formCon">
<div class="formConInner">
<div class="form">
	<h3>Trucker</h3>
	Choose Trucker&nbsp;
	<select name='truckerl'>
	<option value=null></option>
	<?php $truckerList = Students::model()->findAll();
		foreach($truckerList as $trucker){
			echo '<option value="'.$trucker->assoc_truckerid.'">'.$trucker->trucker.'</option>';
		}
	?>
	</select>
	&nbsp;&nbsp;&nbsp;
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Export Pdf' : 'Export Pdf',array('class'=>'formbut','name'=>'choosetrucker')); ?>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Export Excel' : 'Export Excel',array('class'=>'formbut','name'=>'choosetruckerexcel')); ?>
	<br><br>
	<h3></h3>
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Export All to Pdf' : 'Export All to Pdf',array('class'=>'formbut','name'=>'printall')); ?>
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Export All to Excel' : 'Export All to Excel',array('class'=>'formbut','name'=>'printallexcel')); ?>
	<?php $this->endWidget(); ?>
</div><!-- form -->
</div>
</div>

</div>
    </td>
  </tr>
</table>
