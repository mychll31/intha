<?php
$this->widget('EExcelWriter', array(
    'dataProvider' => $model,
    'title' => 'EExcelWriter',
    'stream' => TRUE,
    'fileName' => 'my-file-name.xls',
    'columns' => array(
        'col1',
        'col2',
        'col3',
        'col_etc',
    ),
));
?>