<table width=100%>
	<tr><td><img src='images/login-logo.png' style="width:130px; height:75px;" /></td>
	<td style="padding-left:20px;">
	<b style="font-size:15px;line-height:25px;">INTEGRATED NORTH HARBOR TRUCKERS ASSOCIATION</b><br>
	<font style="font-size:11px;line-height:15px;">Commercial Lot 4, Road 10, Tondo, Manila<br>
	Tel. No.: 254-5695 / 254-5754  Fax: 254-5759<br>
	Email: intha@skyinet.net</font>
	</td></tr>
</table>
<style>
.listbxtop_hdng
{
	text-align:left;
	
}
.table_listbx tr td, tr th {
border-left:1px solid #ccc;
border-right:1px solid #ccc;
}
td.listbx_subhdng
{
	color:#333333;
	font-weight:bold;
}

.odd
{
	background:#DFDFDF;
	border-top:1px solid #ccc;
}
td.subhdng_nrmal
{
	color:#333333;	
}
.table_listbx
{
	margin:0px;
	padding:0px;
	
}
.table_listbx td
{
	padding:5px;
	margin:0px;
	font-size:10px;
}
.table_listbxlast td
{
	border-bottom:none;

	
}


td.subhdng_nrmal
{
	color:#333333;
}
.last
{
	border-bottom:1px solid #ccc;
}
.first
{
	border:none;
}
</style>
<br>
<div align='right' style="font-size:10px;"><?php echo date('M. d, Y H:i A');?></div>
<h4>Employee Master List</h4>
<table class="table_listbx" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
	<tr>
		<td class="listbx_subhdng odd" style='width:10px'>#</td>
		<td class="listbx_subhdng odd" style='width:130px'>Name</td>
		<td class="listbx_subhdng odd" style='width:100px'>St. No./ St. Address</td>
		<td class="listbx_subhdng odd" style='width:90px'>City/Municipality</td>
		<td class="listbx_subhdng odd" style='width:85px'>State/Province</td>
		<td class="listbx_subhdng odd" style='width:45px'>Zip Code</td>
		<td class="listbx_subhdng odd" style='width:115px'>Trucker</td>
	</tr>
<?php
	$ctr=1;
	foreach($model as $model){
	?>
	<tr>
		<td class="listbx_subhdng last" style='width:10px'><?=$ctr?></td>
		<td class="listbx_subhdng last" style='width:130px'><?=ucwords($model->lastname.', '.$model->firstname.' '.substr($model->middlename,0,1).'.')?></td>
		<td class="listbx_subhdng last" style='width:100px'><?=ucwords($model->present_address)?></td>
		<td class="listbx_subhdng last" style='width:90px'><?=ucwords($model->present_city)?></td>
		<td class="listbx_subhdng last" style='width:85px'><?=ucwords($model->present_state)?></td>
		<td class="listbx_subhdng last" style='width:45px'><?=ucwords($model->present_zipcode)?></td>
		<?php $trucker = Students::model()->findByAttributes(array('assoc_truckerid'=>$model->trucker_id))?>
		<td class="listbx_subhdng last" style='width:115px'><?=ucwords($trucker->trucker)?></td>
	</tr>
	<?php
	$ctr++;}
?>
</table>