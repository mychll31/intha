<?php

set_time_limit(10);

require_once "/protected/vendors/excelwriter/class.writeexcel_workbook.inc.php";
require_once "/protected/vendors/excelwriter/class.writeexcel_worksheet.inc.php";

$fname = tempnam("/tmp", "simple.xls");
$workbook = &new writeexcel_workbook($fname);
$worksheet = &$workbook->addworksheet();

# The general syntax is write($row, $column, $token). Note that row and
# column are zero indexed
#

# Header
$worksheet->write(0, 0,  "INTEGRATED NORTH HARBOR TRUCKERS ASSOCIATION");
$worksheet->write(1, 0,  "Master List");

$worksheet->write(4, 0,  "");
$worksheet->write(4, 1,  "Company");
$worksheet->write(4, 2,  "Units");
$worksheet->write(4, 3,  "Address");
$worksheet->write(4, 4,  "Contact Person");
$worksheet->write(4, 5,  "Office No.");
$worksheet->write(4, 6,  "Email Address");

$x =4;
$no=0;
foreach($posts as $emp){
	$x++;
	$no++;
	$worksheet->write($x, 0, $no);
	$worksheet->write($x, 1, $emp->trucker);
	
	$criteria = new CDbCriteria;
	$criteria->compare('association_trucker_id',$emp->assoc_truckerid);
	$totalunits = Courses::model()->count($criteria);
	
	$worksheet->write($x, 2, $totalunits);
	$worksheet->write($x, 3, ucwords($emp->buss_add_add.' ,'.$emp->buss_add_prov.', '.$emp->buss_add_city.' '.$emp->buss_add_zip));
	$worksheet->write($x, 4, ucwords($emp->rep_lname.' '.$emp->rep_fname.' '.substr($emp->rep_mname,0,1).'.'));
	$worksheet->write($x, 5, $emp->rep_telno);
	$worksheet->write($x, 6, $emp->rep_email);
}

$workbook->close();

header("Content-Type: application/x-msexcel; name=\"truckerMasterlist".time().".xls\"");
header("Content-Disposition: inline; filename=\"truckerMasterlist".time().".xls\"");
$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);

?>
