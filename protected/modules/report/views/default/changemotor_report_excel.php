<?php

set_time_limit(10);

require_once "/protected/vendors/excelwriter/class.writeexcel_workbook.inc.php";
require_once "/protected/vendors/excelwriter/class.writeexcel_worksheet.inc.php";

$fname = tempnam("/tmp", "simple.xls");
$workbook = &new writeexcel_workbook($fname);
$worksheet = &$workbook->addworksheet();

# The general syntax is write($row, $column, $token). Note that row and
# column are zero indexed
#

# Header
$worksheet->write(0, 0,  "INTEGRATED NORTH HARBOR TRUCKERS ASSOCIATION");
$worksheet->write(1, 0,  "INLAND MARINE 2014");

$worksheet->write(3, 0,  $truckername);
$worksheet->write(4, 0,  "");
$worksheet->write(4, 1,  "Old Motor No.");
$worksheet->write(4, 2,  "New Motor No.");
$worksheet->write(4, 3,  "Chasis/Serial No.");
$worksheet->write(4, 4,  "Type/Make");
$worksheet->write(4, 5,  "Registered Owner");

$x =4;
$no=0;
foreach($truckdetails as $trucks){
	$x++;
	$no++;
	$worksheet->write($x, 0, $no);
	$worksheet->write($x, 1, $trucks->motor);
	$worksheet->write($x, 2, "");
	$worksheet->write($x, 3, $trucks->trailer_chasis_serial);
	foreach($truckbrand as $tbrand){
		if($tbrand->id == $trucks->brand){
			$brand =$tbrand->name;
		}
	}
	$worksheet->write($x, 4, $brand);
	$worksheet->write($x, 5, $trucks->reg_owner);
}

$worksheet->write($x+2, 0, "Prepared by:");
$workbook->close();

header("Content-Type: application/x-msexcel; name=\"inland_change_motor_".time().".xls\"");
header("Content-Disposition: inline; filename=\"inland_change_motor_".time().".xls\"");
$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);

?>
