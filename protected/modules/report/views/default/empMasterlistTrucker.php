<?php

set_time_limit(10);

require_once "/protected/vendors/excelwriter/class.writeexcel_workbook.inc.php";
require_once "/protected/vendors/excelwriter/class.writeexcel_worksheet.inc.php";

$fname = tempnam("/tmp", "simple.xls");
$workbook = &new writeexcel_workbook($fname);
$worksheet = &$workbook->addworksheet();

# The general syntax is write($row, $column, $token). Note that row and
# column are zero indexed
#

# Header
$worksheet->write(0, 0,  "INTEGRATED NORTH HARBOR TRUCKERS ASSOCIATION");
$worksheet->write(1, 0,  "Master List");

$worksheet->write(3, 0,  $truckername);
$worksheet->write(4, 0,  "");
$worksheet->write(4, 1,  "Lastname");
$worksheet->write(4, 2,  "Firstname");
$worksheet->write(4, 3,  "Middlename");
$worksheet->write(4, 4,  "St. No./ St. Address");
$worksheet->write(4, 5,  "City/Municipality");
$worksheet->write(4, 6,  "State/Province");
$worksheet->write(4, 7,  "ZipCode");

$x =4;
$no=0;
foreach($posts as $emp){
	$x++;
	$no++;
	$worksheet->write($x, 0, $no);
	$worksheet->write($x, 1, $emp->lastname);
	$worksheet->write($x, 2, $emp->firstname);
	$worksheet->write($x, 3, $emp->middlename);
	$worksheet->write($x, 4, $emp->present_address);
	$worksheet->write($x, 5, $emp->present_city);
	$worksheet->write($x, 6, $emp->present_state);
	$worksheet->write($x, 7, $emp->present_zipcode);
}

$workbook->close();

header("Content-Type: application/x-msexcel; name=\"EmployeeMasterlist_".$truckername.'_'.time().".xls\"");
header("Content-Disposition: inline; filename=\"EmployeeMasterlist_".$truckername.'_'.time().".xls\"");
$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);

?>
