<?php

set_time_limit(10);

require_once "/protected/vendors/excelwriter/class.writeexcel_workbook.inc.php";
require_once "/protected/vendors/excelwriter/class.writeexcel_worksheet.inc.php";

$fname = tempnam("/tmp", "simple.xls");
$workbook = &new writeexcel_workbook($fname);
$worksheet = &$workbook->addworksheet();

# The general syntax is write($row, $column, $token). Note that row and
# column are zero indexed
#

# Header
$worksheet->write(0, 0,  "INTEGRATED NORTH HARBOR TRUCKERS ASSOCIATION");
$worksheet->write(1, 0,  "Truck Master List");

$worksheet->write(3, 0,  $truckername);
$worksheet->write(4, 0,  "");
$worksheet->write(4, 1,  "Plate Number");
$worksheet->write(4, 2,  "Chasis/Serial");
$worksheet->write(4, 3,  "Type");
$worksheet->write(4, 4,  "Registered Owner");

$x =4;
$no=0;
foreach($posts as $emp){
	$x++;
	$no++;
	$worksheet->write($x, 0, $no);
	$worksheet->write($x, 1, $emp->plate_number);
	$worksheet->write($x, 2, $emp->chasis_number);
	
	$brand = TruckerBrand::model()->findByAttributes(array('id'=>$emp->brand));
	$tractorType = TruckersTractorType::model()->findByAttributes(array('id'=>$emp->tractor_type));
	$wheel = TruckerWheeler::model()->findByAttributes(array('id'=>$emp->wheeler));
	
	$worksheet->write($x, 3, strtoupper($brand->name.' '.$tractorType->name.' '.$wheel->name));
	$worksheet->write($x, 4, ucwords($emp->reg_owner));
}

$workbook->close();

header("Content-Type: application/x-msexcel; name=\"TruckMasterlist_".$truckername.'_'.time().".xls\"");
header("Content-Disposition: inline; filename=\"TruckMasterlist_".$truckername.'_'.time().".xls\"");
$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);

?>
