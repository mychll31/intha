<?php

set_time_limit(10);

require_once "/protected/vendors/excelwriter/class.writeexcel_workbook.inc.php";
require_once "/protected/vendors/excelwriter/class.writeexcel_worksheet.inc.php";

$fname = tempnam("/tmp", "simple.xls");
$workbook = &new writeexcel_workbook($fname);
$worksheet = &$workbook->addworksheet();

# The general syntax is write($row, $column, $token). Note that row and
# column are zero indexed
#

# Header
$worksheet->write(0, 0,  "INTEGRATED NORTH HARBOR TRUCKERS ASSOCIATION");
$worksheet->write(1, 0,  "INLAND MARINE 2014");

$worksheet->write(4, 0,  " ");
$worksheet->write(4, 1,  "Trucker");
$worksheet->write(4, 2,  "Old Motor No.");
$worksheet->write(4, 3,  "New Motor No.");
$worksheet->write(4, 4,  "Chasis/Serial No.");
$worksheet->write(4, 5,  "Type/Make");
$worksheet->write(4, 6,  "Registered Owner");

$x =4;
$no=0;
$brand="";
$truckernames="";

foreach($truckdetails as $trucks){
	$x = $x+1;
	$no=$no+1;
	$worksheet->write($x, 0, $no);
	foreach($truckername as $tname){
		if($tname->assoc_truckerid == $trucks->association_trucker_id){
			$truckernames =$tname->trucker;
		}
	}
	$worksheet->write($x, 1, $truckernames);
	$worksheet->write($x, 2, $trucks->motor);
	$worksheet->write($x, 3, "");
	$worksheet->write($x, 4, $trucks->trailer_chasis_serial);
	foreach($truckbrand as $tbrand){
		if($tbrand->id == $trucks->brand){
			$brand =$tbrand->name;
		}
	}
	$worksheet->write($x, 5, $brand);
	$worksheet->write($x, 6, $trucks->reg_owner);

}

$worksheet->write($x+2, 0, "Prepared by:");
$workbook->close();

header("Content-Type: application/x-msexcel; name=\"inland_change_motor_all_".time().".xls\"");
header("Content-Disposition: inline; filename=\"inland_change_motor_all_".time().".xls\"");
$fh=fopen($fname, "rb");
fpassthru($fh);
unlink($fname);

?>
