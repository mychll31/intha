<?php

class DefaultController extends RController
{
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}
	
	public function actionIndex()
	{
		$this->render('/default/employees');
	}
	
	public function actionEmployees()
	{
		$model = new Employees;
		$criteria = new CDbCriteria;
		$criteria1 = new CDbCriteria;
		$criteria->compare('is_deleted',0);
		$criteria1->compare('is_deleted',0);
		$error = null;
		if(isset($_POST['joiningdate']))
		{
			$from = $_POST['joining_from'];
			$to = $_POST['joining_to'];
			if($from !=null and $to !=null){
				if($from > $to){
					$error = 'Joining Date from <u>'.$from.'</u> to <u>'.$to.'</u> is Invalid.';
				}
			}
			if($error ==null){
				if($from !=null){
					$model->joining_date = $from;
					$criteria->condition=$criteria->condition.' and '.'joining_date >= :joining_from';
					$criteria->params[':joining_from'] = $from;
				  }
				  if($to !=null){  
					$model->joining_date = $to;
					$criteria->condition=$criteria->condition.' and '.'joining_date <= :joining_to';
					$criteria->params[':joining_to'] = $to;
				  }
				$posts = Employees::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('employeespdf', array('model'=>$posts), true));
				$html2pdf->Output('employee_masterlist.pdf');
			}
		}
		if(isset($_POST['employeesearch'])){
			$fname = $_POST['fname'];
			$lname = $_POST['lname'];
			$mname = $_POST['mname'];
			if($fname !=null){  
				$model->firstname = $fname;
				$criteria1->condition='firstname = :firstn ';
				$criteria1->params[':firstn'] = $fname;
			}
			if($lname !=null){  
				$model->lastname = $lname;
				$criteria1->condition=$criteria1->condition.' and '.'lastname = :lastn ';
				$criteria1->params[':lastn'] = $lname;
			}
			if($mname !=null){  
				$model->middlename = $mname;
				$criteria1->condition=$criteria1->condition.' and '.'middlename = :midn ';
				$criteria1->params[':midn'] = $mname;
			}
			
			if($fname!=null or $lname!=null or $mname!=null){
				$posts = Employees::model()->findAll($criteria1);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('employeepdf', array('post'=>$posts), true));
				$html2pdf->Output('employee'.time().'.pdf');	
			}
		}
		if(isset($_POST['truckersearch'])){
			$trucker = $_POST['tlist'];
			if($trucker!=null){
				$model->trucker_id = $trucker;
				$criteria->condition=$criteria->condition.' and '.'trucker_id <= :truckerid';
				$criteria->params[':truckerid'] = $trucker;
				
				$posts = Employees::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('employeespdf', array('model'=>$posts), true));
				$html2pdf->Output('employee_masterlist.pdf');
			}
		}
		if(isset($_POST['truckerxls'])){
			$trucker = $_POST['tlist'];
			if($trucker!=null){
				$model->trucker_id = $trucker;
				$criteria->condition=$criteria->condition.' and '.'trucker_id <= :truckerid';
				$criteria->params[':truckerid'] = $trucker;
				$truckername = Students::model()->findByAttributes(array('assoc_truckerid'=>$_POST['tlist']));
				$posts = Employees::model()->findAll($criteria);
				$this->renderPartial('empMasterlistTrucker',array(
					'posts'=>$posts,
					'truckername'=>$truckername->trucker,
				));
			}
		}
		if(isset($_POST['printall'])){
			$posts = Employees::model()->findAll($criteria);
			$html2pdf = Yii::app()->ePdf->HTML2PDF();
			$html2pdf->WriteHTML($this->renderPartial('employeespdf', array('model'=>$posts), true));
			$html2pdf->Output('employee_masterlist.pdf');
		}
		
		$this->render('employees',array(
			'model'=>$posts,
			'error'=>$error,
		));
	}
	
	public function actionTrucks()
	{
		$model = new Courses;
		$criteria = new CDbCriteria;
		if(isset($_POST['choosetrucker'])){
			$truckerlist= $_POST['truckerl'];		
			if($truckerlist !=null){
				$model->association_trucker_id = $truckerlist;
				$criteria->condition='association_trucker_id <= :truckerid';
				$criteria->params[':truckerid'] = $truckerlist;
				
				$posts = Courses::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('truckpdf', array('model'=>$posts), true));
				$html2pdf->Output('truck_masterlist.pdf');
			}
		}
		if(isset($_POST['choosetruckerexcel'])){
			$truckerlist= $_POST['truckerl'];		
			if($truckerlist !=null){
				$model->association_trucker_id = $truckerlist;
				$criteria->condition='association_trucker_id <= :truckerid';
				$criteria->params[':truckerid'] = $truckerlist;
				
				$posts = Courses::model()->findAll($criteria);
				$this->renderPartial('truckexcel',array(
					'posts'=>$posts,
				));
			}
		}
		if(isset($_POST['chooseowner'])){
			$ownernme= $_POST['owner'];		
			if($ownernme !=null){
				$model->reg_owner = $ownernme;
				$criteria->condition='reg_owner = :ownername';
				$criteria->params[':ownername'] = $ownernme;
				
				$posts = Courses::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('truckpdf', array('model'=>$posts,'owner'=>$ownernme), true));
				$html2pdf->Output('truck_masterlist.pdf');
			}
		}
		if(isset($_POST['chooseownerExcel'])){
			$ownernme= $_POST['owner'];		
			if($ownernme !=null){
				$model->reg_owner = $ownernme;
				$criteria->condition='reg_owner = :ownername';
				$criteria->params[':ownername'] = $ownernme;
				
				$posts = Courses::model()->findAll($criteria);
				$this->renderPartial('chooseownerExcel',array(
					'posts'=>$posts,
					'owner'=>$ownernme,
				));
			}
		}
		if(isset($_POST['printall'])){
			$posts = Courses::model()->findAll();
			$html2pdf = Yii::app()->ePdf->HTML2PDF();
			$html2pdf->WriteHTML($this->renderPartial('truckpdf', array('model'=>$posts), true));
			$html2pdf->Output('truck_masterlist.pdf');
		}
		
		$this->render('trucks',array(
			'model'=>$posts,
		));
	}
	
	public function actionForm()
	{
		$model = new Courses;
		$criteria = new CDbCriteria;
		if(isset($_POST['choosetruckerexcel'])){
			$truckername = Students::model()->findByAttributes(array('assoc_truckerid'=>$_POST['truckerl']));
			$criteria->condition='association_trucker_id = :truckerid';
			$criteria->params[':truckerid'] = $_POST['truckerl'];
			$truck_details = Courses::model()->findAll($criteria);
			$truck_brand = TruckerBrand::model()->findAll();
			
			$this->renderPartial('form_report_excel',array(
				'truckdetails'=>$truck_details,
				'truckername'=>$truckername->trucker,
				'truckbrand'=>$truck_brand,
			));
		}
		
		if(isset($_POST['printallexcel'])){
			$truckername = Students::model()->findAll();
			$truck_details = Courses::model()->findAll();
			$truck_brand = TruckerBrand::model()->findAll();
			
			$this->renderPartial('form_report_excel_all',array(
				'truckdetails'=>$truck_details,
				'truckername'=>$truckername,
				'truckbrand'=>$truck_brand,
			));
		}
		
		$this->render('form_report',array(
			'model'=>$posts,
		));
	}
	
	public function actionChangeplate()
	{
	$model = new Courses;
		$criteria = new CDbCriteria;
		if(isset($_POST['choosetruckerexcel'])){
			$truckername = Students::model()->findByAttributes(array('assoc_truckerid'=>$_POST['truckerl']));
			$criteria->condition='association_trucker_id = :truckerid';
			$criteria->params[':truckerid'] = $_POST['truckerl'];
			$truck_details = Courses::model()->findAll($criteria);
			$truck_brand = TruckerBrand::model()->findAll();
			
			$this->renderPartial('changeplate_report_excel',array(
				'truckdetails'=>$truck_details,
				'truckername'=>$truckername->trucker,
				'truckbrand'=>$truck_brand,
			));
		}
		
		if(isset($_POST['printallexcel'])){
			$truckername = Students::model()->findAll();
			$truck_details = Courses::model()->findAll();
			$truck_brand = TruckerBrand::model()->findAll();
			
			$this->renderPartial('changeplate_report_excel_all',array(
				'truckdetails'=>$truck_details,
				'truckername'=>$truckername,
				'truckbrand'=>$truck_brand,
			));
		}
		
		$this->render('changeplate',array(
			'model'=>$posts,
		));
	}
	
	public function actionChangemotor()
	{
	$model = new Courses;
		$criteria = new CDbCriteria;
		if(isset($_POST['choosetruckerexcel'])){
			$truckername = Students::model()->findByAttributes(array('assoc_truckerid'=>$_POST['truckerl']));
			$criteria->condition='association_trucker_id = :truckerid';
			$criteria->params[':truckerid'] = $_POST['truckerl'];
			$truck_details = Courses::model()->findAll($criteria);
			$truck_brand = TruckerBrand::model()->findAll();
			
			$this->renderPartial('changemotor_report_excel',array(
				'truckdetails'=>$truck_details,
				'truckername'=>$truckername->trucker,
				'truckbrand'=>$truck_brand,
			));
		}
		
		if(isset($_POST['printallexcel'])){
			$truckername = Students::model()->findAll();
			$truck_details = Courses::model()->findAll();
			$truck_brand = TruckerBrand::model()->findAll();
			
			$this->renderPartial('changemotor_report_excel_all',array(
				'truckdetails'=>$truck_details,
				'truckername'=>$truckername,
				'truckbrand'=>$truck_brand,
			));
		}
		
		$this->render('changemotor',array(
			'model'=>$posts,
		));
	}
	
	public function actionChangechasis()
	{
	$model = new Courses;
		$criteria = new CDbCriteria;
		if(isset($_POST['choosetruckerexcel'])){
			$truckername = Students::model()->findByAttributes(array('assoc_truckerid'=>$_POST['truckerl']));
			$criteria->condition='association_trucker_id = :truckerid';
			$criteria->params[':truckerid'] = $_POST['truckerl'];
			$truck_details = Courses::model()->findAll($criteria);
			$truck_brand = TruckerBrand::model()->findAll();
			
			$this->renderPartial('changechasis_report_excel',array(
				'truckdetails'=>$truck_details,
				'truckername'=>$truckername->trucker,
				'truckbrand'=>$truck_brand,
			));
		}
		
		if(isset($_POST['printallexcel'])){
			$truckername = Students::model()->findAll();
			$truck_details = Courses::model()->findAll();
			$truck_brand = TruckerBrand::model()->findAll();
			
			$this->renderPartial('changechasis_report_excel_all',array(
				'truckdetails'=>$truck_details,
				'truckername'=>$truckername,
				'truckbrand'=>$truck_brand,
			));
		}
		
		$this->render('changechasis',array(
			'model'=>$posts,
		));
	}
	
	public function actionReplacementunit()
	{
		$model = new Courses;
		$criteria = new CDbCriteria;
		if(isset($_POST['choosetruckerexcel'])){
			$truckername = Students::model()->findByAttributes(array('assoc_truckerid'=>$_POST['truckerl']));
			$criteria->condition='association_trucker_id = :truckerid';
			$criteria->params[':truckerid'] = $_POST['truckerl'];
			$truck_details = Courses::model()->findAll($criteria);
			$truck_brand = TruckerBrand::model()->findAll();
			
			$this->renderPartial('replacementunit_report_excel',array(
				'truckdetails'=>$truck_details,
				'truckername'=>$truckername->trucker,
				'truckbrand'=>$truck_brand,
			));
		}
		
		if(isset($_POST['printallexcel'])){
			$truckername = Students::model()->findAll();
			$truck_details = Courses::model()->findAll();
			$truck_brand = TruckerBrand::model()->findAll();
			
			$this->renderPartial('replacementunit_report_excel_all',array(
				'truckdetails'=>$truck_details,
				'truckername'=>$truckername,
				'truckbrand'=>$truck_brand,
			));
		}
		
		$this->render('replacementunit',array(
			'model'=>$posts,
		));
	}
	
	public function actionChangeownership()
	{
		$model = new Courses;
		$criteria = new CDbCriteria;
		if(isset($_POST['choosetruckerexcel'])){
			$truckername = Students::model()->findByAttributes(array('assoc_truckerid'=>$_POST['truckerl']));
			$criteria->condition='association_trucker_id = :truckerid';
			$criteria->params[':truckerid'] = $_POST['truckerl'];
			$truck_details = Courses::model()->findAll($criteria);
			$truck_brand = TruckerBrand::model()->findAll();
			
			$this->renderPartial('changeownership_report_excel',array(
				'truckdetails'=>$truck_details,
				'truckername'=>$truckername->trucker,
				'truckbrand'=>$truck_brand,
			));
		}
		
		if(isset($_POST['printallexcel'])){
			$truckername = Students::model()->findAll();
			$truck_details = Courses::model()->findAll();
			$truck_brand = TruckerBrand::model()->findAll();
			
			$this->renderPartial('changeownership_report_excel_all',array(
				'truckdetails'=>$truck_details,
				'truckername'=>$truckername,
				'truckbrand'=>$truck_brand,
			));
		}
		
		$this->render('changeownership',array(
			'model'=>$posts,
		));
	}
	
	public function actionTruckers()
	{
		$model = new Students;
		$criteria = new CDbCriteria;
		if(isset($_POST['choosetrucker'])){
			$truckerlist= $_POST['truckerl'];		
			if($truckerlist !=null){
				$model->assoc_truckerid = $truckerlist;
				$criteria->condition='assoc_truckerid <= :truckerid';
				$criteria->params[':truckerid'] = $truckerlist;
				
				$posts = Students::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('truckerpdf', array('model'=>$posts), true));
				$html2pdf->Output('trucker_masterlist.pdf');
			}
		}
		if(isset($_POST['choosetruckerexcel'])){
			$truckerlist= $_POST['truckerl'];		
			if($truckerlist !=null){
				$truckername = Students::model()->findByAttributes(array('assoc_truckerid'=>$_POST['truckerl']));
				$model->assoc_truckerid = $truckerlist;
				$criteria->condition='assoc_truckerid = :truckerid';
				$criteria->params[':truckerid'] = $truckerlist;
				
				$criteria12 = new CDbCriteria;
				$criteria12->compare('association_trucker_id',$model->assoc_truckerid);
				$totalunits = Courses::model()->count($criteria12);
				
				$posts = Students::model()->findAll($criteria);
				$this->renderPartial('truckerexcel',array(
					'posts'=>$posts,
					'totalunits'=>$totalunits,
					'truckername'=>$truckername->trucker,
				));
			}
		}
		
		if(isset($_POST['chooseowner'])){
			$ownernme= $_POST['owner'];		
			if($ownernme !=null){
				$model->owner = $ownernme;
				$criteria->condition='owner <= :ownername';
				$criteria->params[':ownername'] = $ownernme;
				
				$posts = Students::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('truckerpdf', array('model'=>$posts), true));
				$html2pdf->Output('trucker_masterlist.pdf');
			}
		}
		if(isset($_POST['printall'])){
			$posts = Students::model()->findAll();
			$html2pdf = Yii::app()->ePdf->HTML2PDF();
			$html2pdf->WriteHTML($this->renderPartial('truckerpdf', array('model'=>$posts), true));
			$html2pdf->Output('trucker_masterlist.pdf');
		}
		if(isset($_POST['printallexcel'])){
			$posts = Students::model()->findAll();
			$this->renderPartial('truckerAllexcel',array(
				'posts'=>$posts,
			));
		}
		
		$this->render('truckers',array(
			'model'=>$posts,
		));
	}
	
	public function actionTruckerscert()
	{
		$model = new Students;
		$criteria = new CDbCriteria;
		if(isset($_POST['choosetrucker'])){
			$truckerlist= $_POST['truckerl'];		
			if($truckerlist !=null){
				$model->assoc_truckerid = $truckerlist;
				$criteria->condition='assoc_truckerid <= :truckerid';
				$criteria->params[':truckerid'] = $truckerlist;
				
				$posts = Students::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('cert_truckers', array('model'=>$posts), true));
				$html2pdf->Output('trucker_cert.pdf');
			}
		}
		if(isset($_POST['chooseowner'])){
			$ownernme= $_POST['owner'];		
			if($ownernme !=null){
				$model->owner = $ownernme;
				$criteria->condition='owner <= :ownername';
				$criteria->params[':ownername'] = $ownernme;
				
				$posts = Students::model()->findAll($criteria);
				$html2pdf = Yii::app()->ePdf->HTML2PDF();
				$html2pdf->WriteHTML($this->renderPartial('cert_truckers', array('model'=>$posts), true));
				$html2pdf->Output('trucker_cert.pdf');
			}
		}
		if(isset($_POST['printallactive'])){
			$posts = Students::model()->findAll();
			$html2pdf = Yii::app()->ePdf->HTML2PDF();
			$html2pdf->WriteHTML($this->renderPartial('cert_truckers_active', array('model'=>$posts), true));
			$html2pdf->Output('trucker_cert_active'.time().'.pdf');
		}
		if(isset($_POST['printallinactive'])){
			$posts = Students::model()->findAll();
			$html2pdf = Yii::app()->ePdf->HTML2PDF();
			$html2pdf->WriteHTML($this->renderPartial('cert_truckers_inactive', array('model'=>$posts), true));
			$html2pdf->Output('trucker_cert_inactive'.time().'.pdf');
		}
		if(isset($_POST['printallsold'])){
			$posts = Students::model()->findAll();
			$html2pdf = Yii::app()->ePdf->HTML2PDF();
			$html2pdf->WriteHTML($this->renderPartial('cert_truckers_sold', array('model'=>$posts), true));
			$html2pdf->Output('trucker_cert_sold'.time().'.pdf');
		}
		
		$this->render('truckerscert',array(
			'model'=>$posts,
		));
	}
	
}
