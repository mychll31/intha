<?php
$this->breadcrumbs=array(
	'Calendar',
);
$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
?>
<?php
  ##Trucker membership notifications
                $criteria_a = new CDbCriteria;
                $exp1 = date('Y-m-d',time());
                $exp2 = date('Y-m-d',(date(strtotime('+60 day'),time())));

                #Truck Registration Date
                $criteria_b = new CDbCriteria;
                $truckCount = 0;
                $criteria_b->condition=' reg_enddate >=  :expiry and reg_enddate <= :expiry1';
                $criteria_b->params[':expiry']=$exp1;
                $criteria_b->params[':expiry1']=$exp2;
                $truckCount = Courses::model()->count($criteria_b);

                #TruckerRegdate Date
                $criteria_lic = new CDbCriteria;
                $licCount = 0;
                $criteria_lic->condition=' active=1 and expiry_date>=  :expiry and expiry_date <= :expiry1';
                $criteria_lic->params[':expiry']=$exp1;
                $criteria_lic->params[':expiry1']=$exp2;
                $licCount = EmpLicense::model()->count($criteria_lic);

                #Comprehensive Insurance
                $criteria_99 = new CDbCriteria;
                $orcrCount = 0;
                $criteria_99->condition=' compre_dateexpire >=  :expiry and compre_dateexpire <= :expiry1';
                $criteria_99->params[':expiry']=$exp1;
                $criteria_99->params[':expiry1']=$exp2;
                $compreCount = Insurance::model()->count($criteria_99);

                $totalCount = $licCount + $truckCount + $compreCount;
                #Start Alert for Events
		#Today
                $alertEvents = 0;
                $criteria_events = new CDbCriteria;
                $criteria_events->order = 'start desc';
                $allEvents = Events::model()->findAll($criteria_events);
                foreach($allEvents as $eve){
                        if(date('Y m d',$eve->start)==date('Y m d',time())){$alertEvents++;}
                }
                #End Alert for Events


?>
<script>
	function getType()
	{
		var eventid = document.getElementById('eventid').value;
		if(eventid == '')
		{
			window.location= 'index.php?r=cal';
		}
		else
		{
			window.location= 'index.php?r=cal&type='+eventid;
		}
	}
</script>
<?php $this->beginContent('//layouts/main'); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="247" valign="top">
        	<!--upgrade_div_starts-->
            <div class="upgrade_bx">
                <div class="up_banr_imgbx"><a href="http://open-school.org/contact.php" target="_blank"><img src="http://tryopenschool.com/images/promo_bnnr_innerpage.png" width="231" height="200" /></a></div>
                <div class="up_banr_firstbx">
                  <h1>You are Using Community Edition</h1>
                  <a href="http://open-school.org/contact.php" target="_blank">upgrade to premium version!</a>
                </div>
                
            </div>
            <!--upgrade_div_ends-->
            <div id="othleft-sidebar">
                <!--<div class="lsearch_bar">
                <input name="" type="text" class="lsearch_bar_left" value="Search" />
                <input name="" type="button" class="sbut" />
                <div class="clear"></div>
                </div>-->
                <h1>My Account</h1>  
                <?php 
                function t($message, $category = 'cms', $params = array(), $source = null, $language = null) 
                {
                	return $message;
                }
	$role=UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id));
        if($role->role==1){
            $this->widget('zii.widgets.CMenu',array(
            'encodeLabel'=>false,
            'activateItems'=>true,
            'activeCssClass'=>'list_active',
            'items'=>array(
			array('label'=>Yii::t('dashboard','Mailbox('.Yii::app()->getModule("mailbox")->getNewMsgs(Yii::app()->user->id).')<span>'.Yii::t('dashboard','All Received Messages').'</span>'), 'url'=>array('/mailbox'),
                'active'=> ((Yii::app()->controller->module->id=='mailbox' and  Yii::app()->controller->id!='news') ? true : false),'linkOptions'=>array('class'=>'inbox_ico')),
	
				array('label'=>''.t('<h1>Events</h1>'),
				'active'=> ((Yii::app()->controller->module->id=='cal') ? true : false)),
				
				array('label'=>t('Events List ( <font color=\'red\'>'.$alertEvents.'</font> ) <span>All Events</span>'), 'url'=>array('/dashboard/default/events'),
				'active'=> ((Yii::app()->controller->module->id=='dashboard') ? true : false),'linkOptions'=>array('class'=>'evntlist_ico')),
				
				array('label'=>t('Calendar<span>Schedule Events</span>'), 'url'=>array('/cal'),
				'active'=> (((Yii::app()->controller->module->id=='cal') and (Yii::app()->controller->id != 'eventsType')) ? true : false),'linkOptions'=>array('class'=>'cal_ico')),
				
				array('label'=>t('Inland Marine Information<span>Manage Event Types</span>'), 'url'=>array('/inlandMarineTrucker/manage'),
				'active'=> ((Yii::app()->controller->action->id=='inlandmarine') ? true : false),'linkOptions'=>array('class'=>'vt_ico')),
				
				array('label'=>t('Notifications ( <font color=\'red\'>'.$totalCount.'</font> ) <span>Manage Notifications</span>'), 'url'=>array('/default/noti'),
				'active'=> ((Yii::app()->controller->action->id=='noti') ? true : false),'linkOptions'=>array('class'=>'notify_ico')),
				
            ),
            )); }else{
            $this->widget('zii.widgets.CMenu',array(
            'encodeLabel'=>false,
            'activateItems'=>true,
            'activeCssClass'=>'list_active',
            'items'=>array(
			array('label'=>Yii::t('dashboard','Mailbox('.Yii::app()->getModule("mailbox")->getNewMsgs(Yii::app()->user->id).')<span>'.Yii::t('dashboard','All Received Messages').'</span>'), 'url'=>array('/mailbox'),
                'active'=> ((Yii::app()->controller->module->id=='mailbox' and  Yii::app()->controller->id!='news') ? true : false),'linkOptions'=>array('class'=>'inbox_ico')),
	
				array('label'=>''.t('<h1>Events</h1>'),
				'active'=> ((Yii::app()->controller->module->id=='cal') ? true : false)),
				
				array('label'=>t('Events List ( <font color=\'red\'>'.$alertEvents.'</font> ) <span>All Events</span>'), 'url'=>array('/dashboard/default/events'),
				'active'=> ((Yii::app()->controller->module->id=='dashboard') ? true : false),'linkOptions'=>array('class'=>'evntlist_ico')),
				
				array('label'=>t('Calendar<span>Schedule Events</span>'), 'url'=>array('/cal'),
				'active'=> (((Yii::app()->controller->module->id=='cal') and (Yii::app()->controller->id != 'eventsType')) ? true : false),'linkOptions'=>array('class'=>'cal_ico')),
				
				array('label'=>t('Inland Marine Information<span>Manage Event Types</span>'), 'url'=>array('/inlandMarineTrucker/manage'),
				'active'=> ((Yii::app()->controller->action->id=='inlandmarine') ? true : false),'linkOptions'=>array('class'=>'vt_ico')),
				
            ),
            )); 
		}?>
            </div>
        </td>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" width="75%">
                        <div style="padding-left:20px; position:relative;">
						<br>
                        <h1>Calendar</h1>
                        <div style="position:absolute; width:auto; z-index:10; top:105px; right:25px; font-size:14px;">
                        	<?php
								echo Yii::t('CalModule.fullCal', 'Show');
								$data = EventsType::model()->findAll();
								$events_type = CHtml::listData($data,'id','name');
								
								foreach($data as $datum)
								{
									$options["options"][$datum->id] = array("style" => "background-color:".$datum->colour_code);
									
								}
								$options["prompt"] = 'All Events';
								$options["style"] = 'margin:10px';
								$options["onchange"] = 'getType();';
								$options["id"] = 'eventid';
								echo CHtml::dropDownList("Event_type",$_REQUEST['type'], $events_type,$options);
							?>
                        </div>
                        <div id="calendar" style="width:98%; padding-top:5px"><?php  echo $content; ?></div>
                            <div style="width:99%;">
                                <table width="99%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td valign="top" width="1%"><img src="images/paperbtm_left.png" width="14" height="14" /></td>
                                        <td width="98%" class="paperbtm_mid">&nbsp;</td>
                                        <td valign="top" align="right" width="1%"><img src="images/paperbtm_right.png" width="14" height="14" /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<script type="text/javascript">
	$(document).ready(function () {
		//Hide the second level menu
		$('#othleft-sidebar ul li ul').hide();            
		//Show the second level menu if an item inside it active
		$('li.list_active').parent("ul").show();
		
		$('#othleft-sidebar').children('ul').children('li').children('a').click(function () { 
			 if($(this).parent().children('ul').length>0){                  
				$(this).parent().children('ul').toggle();    
			 }
			 
		});
	});
</script>
<br />
<br />
<br />
<?php $this->endContent(); ?>
