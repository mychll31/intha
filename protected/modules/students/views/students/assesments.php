<?php
$this->breadcrumbs=array(
        'Trucker'=>array('index'),
	        'Directors',
		);

$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id));
?>
<style>
#jobDialog123
{
	height:auto;
}
</style>
<script>
function details(id)
{
	
	var rr= document.getElementById("dropwin"+id).style.display;
	
	 if(document.getElementById("dropwin"+id).style.display=="block")
	 {
		 document.getElementById("dropwin"+id).style.display="none"; 
		 $("#openbutton"+id).removeClass('open');
		  $("#openbutton"+id).addClass('view');
	 }
	 else if(  document.getElementById("dropwin"+id).style.display=="none")
	 {
		 document.getElementById("dropwin"+id).style.display="block"; 
		   $("#openbutton"+id).removeClass('view');
		  $("#openbutton"+id).addClass('open');
	 }
}
function rowdelete(id)
{
	 $("#batchrow"+id).fadeOut("slow");
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
     <?php $this->renderPartial('profileleft');?>
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <br><?php if($role->role==1){ ?>
    <div class="edit_bttns last">
    <ul>
    <li>
    <?php echo CHtml::link(Yii::t('students','<span>Add Director</span>'), array('/truckerDirectors/create', 'id'=>$model->assoc_truckerid),array('class'=>' edit ')); ?>
    </li>
    </ul>
    </div>
    <?php } ?>
    <div class="clear"></div>
    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
     <?php $this->renderPartial('tab');?>
    <div class="clear"></div>
    <div class="tablebx">  
         <div class="pagecon">
		<br>
 <div id="jobDialog">
 <div id="jobDialog1">
 <?php 
  $posts_1=Students::model()->findByAttributes(array('id'=>$_REQUEST['id']));
   ?>
 <?php $this->renderPartial('_flash');?>

 </div>
  </div>
    <div class="mcb_Con">

<div class="mcbrow" id="jobDialog1">
	<ul>
    	<li class="gtcol1" onclick="details('<?php echo $posts_1->id;?>');" style="cursor:pointer;">
		<?php echo $posts_1->trucker; ?>
		<?php
		   $batch=$posts;
		 ?>
        <span><?php echo count($batch); ?> - Director(s)</span>
        </li>
        <a href="#" id="openbutton<?php echo $posts_1->id;?>" onclick="details('<?php echo $posts_1->id;?>');" class="view"><li class="col5"><span class="dwnbg">&nbsp;</span></li></a>
    </ul>
    
 <div class="clear"></div>
</div>
<?php $role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id));?>
<div class="pdtab_Con" id="dropwin<?php echo $posts_1->id; ?>" style="display: none; padding:0px 0px 10px 0px; ">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tbody>
		  <tr class="pdtab-h">
			<td align="center"><?php echo Yii::t('Courses','No.');?></td>
			<td align="center"><?php echo Yii::t('Courses','Name');?></td>
			<td align="center"><?php echo Yii::t('Courses','Birthday');?></td>
			<td align="center"><?php echo Yii::t('Courses','Address');?></td>
			<?php if($role->role==1){ ?>
			<td align="center"><?php echo Yii::t('Courses','Action');?></td>
			<?php } ?>
		  </tr>
          <?php 
	  	$ctr=1;
		  foreach($batch as $batch_1)
				{
				echo "<td>".$ctr++."</td>";
					$prefixx=EmpPrefix::model()->findByAttributes(array('id'=>$batch_1->prefix));
					if($batch_1->prefix!=null){$pref=$prefixx->name.'. ';}else{$pref='';}
					if($batch_1->first_name!=null){$fname=$batch_1->first_name;}else{$fname='';}
					if($batch_1->middle_name!=null){$mname=substr($batch_1->middle_name,0,1).'. ';}else{$mname='';}
					if($batch_1->last_name!=null){$lname=$batch_1->last_name;}else{$lname='';}
					if($batch_1->ext!=null){$ext=$batch_1->ext;}else{$ext='';}
					echo '<td>'.ucwords($pref.' '.$fname.' '.$mname.' '.$lname.' '.$ext).'</td>';
					echo '<td>'.$batch_1->bday.'</td>';
					if($batch_1->address!=null){$presadd =$batch_1->address.', ';}else{$presadd='';}
					if($batch_1->city!=null){$prescity =$batch_1->city.', ';}else{$prescity='';}
					if($batch_1->province!=null){$presstate =$batch_1->province.' ';}else{$presstate='';}
					if($batch_1->zipcode!=null){$preszip =$batch_1->zipcode;}else{$preszip='';}
					echo '<td >'.ucwords($presadd.$prescity.$presstate.$preszip).'</td>';
					if($role->role==1){ 
					echo '<td align="center">'.CHtml::link('View',array('/truckerDirectors/view','id'=>$_REQUEST['id'],'did'=>$batch_1->id)).'</td>';
					}
					echo '</tr>';
					echo '<div id="jobDialog123"></div>';
				}
			   ?>
         </tbody>
        </table>
		</div>
        <div id='check'></div>

</div>

</div>
    </td>
  </tr>
</table>

