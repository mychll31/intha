<?php
  $this->breadcrumbs=array(
	'Trucks'=>array('/courses/courses/managecourse'),
	'View',
  );
$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
$trailss = TruckTrailer::model()->findByAttributes(array('id'=>$_REQUEST['tid']));
?>
<style>.emp_cntntbx{min-height:0px;}</style>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="247" valign="top"><?php $this->renderPartial('profileleft');?></td>
      <td valign="top">
      <div class="cont_right formWrapper">
	<?php if($role->role==1){ ?>
	<div class="edit_bttns">
	<ul>
	  <li><?php echo CHtml::link(Yii::t('employees','<span>Back</span>'), array('trailer', 'id'=>$_REQUEST['id']),array('class'=>'edit last')); ?></li>
        </ul>
    </div>
    <?php }  ?>
<h1>Trailer Details</h1>
    <div class="table_listbx">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr class="listbxtop_hdng">
			<td colspan=2><?php echo Yii::t('employees','Trailer Details');?></td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Type :</span>
				<span class="data">
				<?php
				$trailertype = TruckerTrailerType::model()->findByAttributes(array('id'=>$trailss->trailer_type));
  				echo $trailertype->name;
				?></span>
			</td>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Plate Number :</span>
				<span class="data"><?php echo $trailss->trailer_plate_number;?></span>
			</td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Footer :</span>
				<span class="data">
				<?php
				$trailerfooter = TruckTrailerFooter::model()->findByAttributes(array('id'=>$trailss->trailer_footer));
  				echo $trailerfooter->name;
				?></span>
			</td>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Axle :</span>
				<span class="data">
				<?php
				$traileraxle = TruckTrailerAxle::model()->findByAttributes(array('id'=>$trailss->trailer_axle));
  				echo $traileraxle->name;
				?></span>
			</td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Chasis Number :</span>
				<span class="data"><?php echo $trailss->trailer_chasis_number;?></span>
			</td>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Chassis Serial Number :</span>
				<span class="data"><?php echo $trailss->trailer_chasis_serial_number; ?></span>
			</td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Chassis Serial Number :</span>
			<span class="data"><?php echo $trailss->trailer_chasis_serial_number;?></span>
			</td>
			<td class="subhdng_nrmal">
				<span class="datahead">Trailer Registered Owner :</span>
				<span class="data"><?php echo $trailss->trailer_registered_owner; ?></span>
			</td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">MV File Number :</span>
				<span class="data"><?php echo $trailss->mv_file_number;?></span>
			</td>
			<td class="subhdng_nrmal">
				<span class="datahead">CR Number :</span>
				<span class="data"><?php echo $trails->cr_number;?></span>
			</td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">CR Date Isssued :</span>
				<span class="data">
				<?php 
				if($trailss->cr_date_issued !=null and $trailss->cr_date_issued != '0000-00-00'){
					echo date('M. d, Y',strtotime($trailss->cr_date_issued)); } 
				?>
				</span>
			</td>
			<td class="subhdng_nrmal">
				<span class="datahead">CR Registered Owner :</span>
				<span class="data"><?php echo $trailss->cr_registered_owner;?></span>
			</td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">OR Number :</span>
				<span class="data"><?php echo $trailss->or_number;?></span></td>
			<td class="subhdng_nrmal">
				<span class="datahead">OR Date Issued :</span>
				<span class="data">
				<?php 
				if($trailss->or_date_issued !=null and $trailss->or_date_issued !='0000-00-00'){
					echo date('M. d, Y',strtotime($trailss->or_date_issued)); } 
				?>
				</span>
			</td>
		</tr><tr>
			<td class="subhdng_nrmal">
				<span class="datahead">OR Date Expire :</span>
				<span class="data">
				<?php 
				if($trailss->or_date_expire !=null and $trailss->or_date_expire !='0000-00-00'){
					echo date('M. d, Y',strtotime($trailss->or_date_expire)); } 
				?>
				</span>
			</td>
			<td class="subhdng_nrmal">
				<span class="datahead">LTO Field Office	:</span>
				<span class="data"><?php echo $trailss->lto_field_office;?></span></td>
		</tr>
		</table>

 </div>

 </div>



</div>
</div>
</div>

    </td>
  </tr>
</table>
