<style>
.table_listbx td {
padding: 10px 0px 10px 15px;}
</style>
<?php
$this->breadcrumbs=array(
	'Trucks'=>array('/courses/courses/managecourse'),
	'Insurance',
);
?>

<style>.emp_cntntbx{min-height:0px;}</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
 <?php $this->renderPartial('profileleft');?>
    
    </td>
    <td valign="top">
<div class="cont_right formWrapper">
<br>
    <div class="clear"></div>
    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
     <?php $this->renderPartial('tab');?>
    <div class="clear"></div>
	
	<div class="emp_cntntbx">
    <div class="table_listbx"> 
<?php  
  $truckerid = Students::model()->findByAttributes(array('id'=>$_REQUEST['id']));
  $criteria = new CDbCriteria;
  $criteria->compare('association_trucker_id',$truckerid->assoc_truckerid);
  $truck = Courses::model()->findAll($criteria);
?>
  <table width="100%">
	<tr class="listbxtop_hdng">
		<td colspan=2 style="padding: 0px 0px 0px 15px !important;">
		<?php echo CHtml::link(Yii::t('students','<span>CTPL Details</span>'), array('ctpl', 'id'=>$_REQUEST['id']),array('style'=>'color:#1a7701')); ?></td>
		<td colspan=2 style="padding: 0px 0px 0px 15px !important;">
		<?php echo CHtml::link(Yii::t('students','<span>Compre Details</span>'), array('compre', 'id'=>$_REQUEST['id']),array('style'=>'color:#1a7701')); ?></td>
		<td colspan=4 style="padding: 0px 0px 0px 15px !important;">
		<?php echo CHtml::link(Yii::t('students','<span>PA Details</span>'), array('pa', 'id'=>$_REQUEST['id'])); ?></td>
	</tr>
	<tr style="font-weight:bold;">
		<td class="subhdng_nrmal"><span class="datahead">Truck ID</span><span class="data"></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Plate No.</span><span class="data"></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Broker/Agent</span><span class="data"></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Insurer</span><span class="data"></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Policy No.</span><span class="data"></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Coverage</span><span class="data"></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Date Issued</span><span class="data"></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Date Expiry</span><span class="data"></span></td>
	</tr>
	<?php 
		$no=0; 
		foreach($truck as $tr){ 
	 	$insur = Insurance::model()->findByAttributes(array('truck_id'=>$tr->association_truck_id));	
		$no++; ?>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead"><?php echo $tr->association_truck_id; ?></span><span class="data"></span></td>
		<td class="subhdng_nrmal"><span class="datahead"><?php echo $tr->plate_number; ?></span><span class="data"></span></td>
		<td class="subhdng_nrmal"><span class="datahead"><?php echo $insur->pa_broker; ?></span><span class="data"></span></td>
		<td class="subhdng_nrmal"><span class="datahead"><?php echo $insur->pa_insurer; ?></span><span class="data"></span></td>
		<td class="subhdng_nrmal"><span class="datahead"><?php echo $insur->pa_policy_no; ?></span><span class="data"></span></td>
		<td class="subhdng_nrmal"><span class="datahead"><?php echo $insur->pa_coverage; ?></span><span class="data"></span></td>
		<td class="subhdng_nrmal"><span class="datahead"><?php if($insur->pa_dateissued!='0000-00-00')echo $insur->pa_dateissued; ?></span><span class="data"></span></td>
		<td class="subhdng_nrmal"><span class="datahead"><?php if($insur->pa_dateexpire!='0000-00-00')echo $insur->pa_dateissued; ?></span><span class="data"></span></td>
	</tr>
	<?php } ?>
 </div>
 </div>
    </div>
    <div class="clear"></div>
   
 </div>
 
 
 
</div>
</div>
</div>
    
    </td>
  </tr>
</table>
