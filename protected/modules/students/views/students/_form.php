<?php
	 $emp_id_1= '';
	 if(Yii::app()->controller->action->id=='create')
			{
			$emp_id	= Students::model()->findAll(array('order' => 'id DESC','limit' => 1));
			$emp_id_1=($emp_id[0]['id']+1);
			}
			else
			{
				$emp_id	= Students::model()->findByAttributes(array('id' => $_REQUEST['id']));
				$emp_id_1=$emp_id->assoc_truckerid;
			}
			$emp_id_1 = 'E-'.sprintf('%05u',$emp_id_1);
	 ?>
	<div class="captionWrapper">
        <ul>
            <li><h2 class="cur">Trucker Details</h2></li>
        </ul>
	</div>
<?php $form=$this->beginWidget('CActiveForm', array(
'id'=>'students-form',
'enableAjaxValidation'=>false,
'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<?php 
	if($form->errorSummary($model)){
	?>
        <div class="errorSummary">Input Error<br />
        	<span>Please fix the following error(s).</span>
        </div>
    <?php 
	}
	?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <div class="formCon" style="background:url(images/yellow-pattern.png); width:100%; border:0px #fac94a solid; color:#000; ">
        <div class="formConInner"  style="padding:5px;" align="center">
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="right" width="25%">
		    <?php echo $form->labelEx($model,Yii::t('students','assoc_truckerid')); ?></td>
                    <td style="padding-left:5px;">
		    <?php 
		    if($model->isNewRecord){
			    echo $form->textField($model,'assoc_truckerid',array('maxlength'=>255,'value'=>$emp_id_1,'readonly'=>true,'style'=>'width: 75% !important;'));
		    }else{
			    echo $form->textField($model,'assoc_truckerid',array('maxlength'=>255,'readonly'=>true));}
		    ?>
		    <?php echo $form->error($model,'assoc_truckerid'); ?>
		    </td>
                    <td align="right" width="25%"><?php echo $form->labelEx($model,Yii::t('students','membership_date')); ?></td>
                    <td style="padding-left:5px;">
					<?php
					$date = 'yy-mm-dd';
						$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							
							'attribute'=>'membership_date',
							'model'=>$model,
							// additional javascript options for the date picker plugin
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width: 75% !important;',
							),
						))
					?>
                        <?php echo $form->error($model,'membership_date'); ?>
                    </td>
                </tr>
				<tr>
				<td align="right"><?php echo $form->labelEx($model,Yii::t('students','membership_expiration')); ?></td>
				<td style="padding-left:5px;">
					<?php
					$date = 'yy-mm-dd';
						$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							
							'attribute'=>'membership_expiration',
							'model'=>$model,
							// additional javascript options for the date picker plugin
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width: 75% !important;',
							),
						))
					?>
                        <?php echo $form->error($model,'membership_expiration'); ?></td>
				</tr>
            </table>
        </div>
    </div>
    <div class="formCon">
        <div class="formConInner">
            <h3>Trucker Information</h3>
			<table cellspacing="0" cellpadding="0" width="95%">
			  <tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','trucker')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','trucker_status')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','buss_type')); ?></td>
			  </tr>
			  <tr>
				<td><?php echo $form->textField($model,'trucker',array('maxlength'=>255));?>
				<?php echo $form->error($model,'trucker'); ?></td>
				<td>
					<?php
					$criteria_a = new CDbCriteria;
					$criteria_a->compare('active',1);
					$trucker_status = CHtml::listData(TruckerTruckerStatus::model()->findAll($criteria_a),'id','name');
					echo $form->dropDownList($model,'trucker_status',$trucker_status,array('empty'=>'')); ?>
					<?php echo $form->error($model,'trucker_status'); ?></td>
				<td>
					<?php
					$criteria_b = new CDbCriteria;
					$criteria_b->compare('active',1);
					$buss_type = CHtml::listData(TruckerBussType::model()->findAll($criteria_b),'id','name');
					echo $form->dropDownList($model,'buss_type',$buss_type,array('empty'=>'')); ?>
					<?php echo $form->error($model,'buss_type'); ?></td>
			  </tr>
			  <tr><td colspan=3>&nbsp;</td></tr>
			  <tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','buss_nature')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','buss_add_add')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','buss_add_city')); ?></td>
			  </tr>
			  <tr>
				<td>
					<?php
					$buss_nature = CHtml::listData(TruckerBussNature::model()->findAll($criteria_a),'id','name');
					echo $form->dropDownList($model,'buss_nature',$buss_nature,array('empty'=>'')); ?>
					<?php echo $form->error($model,'buss_nature'); ?></td>
				<td><?php echo $form->textField($model,'buss_add_add',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'buss_add_add'); ?></td>
				<td><?php echo $form->textField($model,'buss_add_city',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'buss_add_city'); ?></td>
			  </tr>
			  <tr><td colspan=3>&nbsp;</td></tr>
			  <tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','buss_add_prov')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','buss_add_zip')); ?></td>
				<td></td>
			  </tr>
			  <tr>
				<td><?php echo $form->textField($model,'buss_add_prov',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'buss_add_prov'); ?></td>
				<td><?php echo $form->textField($model,'buss_add_zip',array('maxlength'=>5));?>
                    <?php echo $form->error($model,'buss_add_zip'); ?></td>
				<td></td>
			  </tr>
			  <tr><td colspan=3>&nbsp;</td></tr>
			  <tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','owner')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','buss_reg')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','mayors_permit')); ?></td>
			  </tr>
			  <tr>
				<td><?php echo $form->textField($model,'owner',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'owner'); ?></td>
				<td><?php echo $form->textField($model,'buss_reg',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'buss_reg'); ?></td>
				<td><?php echo $form->textField($model,'mayors_permit',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'mayors_permit'); ?></td>
			  </tr>
			  <tr><td colspan=3>&nbsp;</td></tr>
			  <tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','sss')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','ph')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','pagibig')); ?></td>
			  </tr>
			  <tr>
				<td><?php echo $form->textField($model,'sss',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'sss'); ?></td>
				<td><?php echo $form->textField($model,'ph',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'ph'); ?></td>
				<td><?php echo $form->textField($model,'pagibig',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'pagibig'); ?></td>
			  </tr>
			  <tr><td colspan=3>&nbsp;</td></tr>
			  <tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','bir')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','fax')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','tel_no')); ?></td>
			  </tr>
			  <tr>
				<td><?php echo $form->textField($model,'bir',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'bir'); ?></td>
				<td><?php echo $form->textField($model,'fax',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'fax'); ?></td>
				<td><?php echo $form->textField($model,'tel_no',array('maxlength'=>15));?>
                    <?php echo $form->error($model,'tel_no'); ?></td>
			  </tr>
			  <?php /* ?>
			  <tr><td colspan=3>&nbsp;</td></tr>
			  <tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','reg_owner')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','reg_start_date')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','reg_end_date')); ?></td>
			  </tr>
			  <tr>
				<td><?php echo $form->textField($model,'reg_owner',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'reg_owner'); ?></td>
				<td><?php
					$date = 'yy-mm-dd';
						$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'reg_start_date',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
					?>
                    <?php echo $form->error($model,'reg_start_date'); ?></td>
				<td><?php
					$date = 'yy-mm-dd';
						$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							
							'attribute'=>'reg_end_date',
							'model'=>$model,
							// additional javascript options for the date picker plugin
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
					?>
                    <?php echo $form->error($model,'reg_end_date'); ?></td>
			  </tr>
			  <?php */ ?>
			  <tr><td colspan=3>&nbsp;</td></tr>
			  <tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','website')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','email')); ?></td>
				<td></td>
			  </tr>
			  <tr>
				<td><?php echo $form->textField($model,'website',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'website'); ?></td>
				<td><?php echo $form->textField($model,'email',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'email'); ?></td>
				<td></td>
			  </tr>
			</table>
		</div>
	</div>
	
    <div class="formCon">
        <div class="formConInner">
            <h3>Latest Annual Volume of items Moved (in Units)</h3>
			<table width="90%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','tons')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','teus')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','cases')); ?></td>
			  </tr>
			  <tr>
				<td><?php echo $form->textField($model,'tons',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'tons'); ?></td>
				<td><?php echo $form->textField($model,'teus',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'teus'); ?></td>
				<td><?php echo $form->textField($model,'cases',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'cases'); ?></td>
			  </tr>
			  <tr><td>&nbsp;</td></tr>
			  <tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','o_specify')); ?></td>
				<td colspan=2></td>
			  </tr>
			  <tr>
				<td><?php echo $form->textField($model,'o_specify',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'o_specify'); ?></td>
				<td colspan=2><?php echo $form->textField($model,'o_specify_content',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'o_specify_content'); ?></td>
			  </tr>
			</table>
		</div>
	</div>
	
    <div class="formCon">
        <div class="formConInner">
            <h3>Official Representative</h3>
			<table width="90%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','rep_lname')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','rep_fname')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','rep_mname')); ?></td>
			  </tr>
			  <tr>
				<td><?php echo $form->textField($model,'rep_lname',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'rep_lname'); ?>
				<td><?php echo $form->textField($model,'rep_fname',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'rep_fname'); ?>
				<td><?php echo $form->textField($model,'rep_mname',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'rep_mname'); ?>
			  </tr>
			  <tr><td>&nbsp;</td></tr>
			  <tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','rep_bdate')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','rep_desig')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','rep_email')); ?></td>
			  </tr>
			  <tr>
				<td>
				<?php
					$date = 'yy-mm-dd';
						$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							
							'attribute'=>'rep_bdate',
							'model'=>$model,
							// additional javascript options for the date picker plugin
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
					?>
                    <?php echo $form->error($model,'rep_bdate'); ?></td>
				<td><?php echo $form->textField($model,'rep_desig',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'rep_desig'); ?></td>
				<td><?php echo $form->textField($model,'rep_email',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'rep_email'); ?></td>
			  </tr>
			  <tr><td>&nbsp;</td></tr>
			  <tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','rep_mobile')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','rep_fax')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','rep_telno')); ?></td>
			  </tr>
			  <tr>
				<td><?php echo $form->textField($model,'rep_mobile',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'rep_mobile'); ?></td>
				<td><?php echo $form->textField($model,'rep_fax',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'rep_fax'); ?></td>
				<td><?php echo $form->textField($model,'rep_telno',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'rep_telno'); ?></td>
			  </tr>
			</table>
		</div>
	</div>
	
	<div class="formCon">
        <div class="formConInner">
            <h3>Alternative Representative</h3>
			<table width="90%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','alter_lname')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','alter_fname')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','alter_mname')); ?></td>
			  </tr>
			  <tr>
				<td><?php echo $form->textField($model,'alter_lname',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'alter_lname'); ?>
				<td><?php echo $form->textField($model,'alter_fname',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'alter_fname'); ?>
				<td><?php echo $form->textField($model,'alter_mname',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'alter_mname'); ?>
			  </tr>
			  <tr><td>&nbsp;</td></tr>
			  <tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','alter_bdate')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','alter_desig')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','alter_email')); ?></td>
			  </tr>
			  <tr>
				<td><?php
					$date = 'yy-mm-dd';
						$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							
							'attribute'=>'alter_bdate',
							'model'=>$model,
							// additional javascript options for the date picker plugin
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
					?>
                    <?php echo $form->error($model,'alter_bdate'); ?></td>
				<td><?php echo $form->textField($model,'alter_desig',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'alter_desig'); ?></td>
				<td><?php echo $form->textField($model,'alter_email',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'alter_email'); ?></td>
			  </tr>
			  <tr><td>&nbsp;</td></tr>
			  <tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','alter_mobile')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','alter_fax')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','alter_telno')); ?></td>
			  </tr>
			  <tr>
				<td><?php echo $form->textField($model,'alter_mobile',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'alter_mobile'); ?></td>
				<td><?php echo $form->textField($model,'alter_fax',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'alter_fax'); ?></td>
				<td><?php echo $form->textField($model,'alter_telno',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'alter_telno'); ?></td>
			  </tr>
			</table>
		</div>
	</div>
	
	
    <div class="formCon">
        <div class="formConInner">
            <h3>Other Informations</h3>
			<table width="90%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','garage')); ?></td>
				<td colspan=2><?php echo $form->labelEx($model,Yii::t('students','garage_add')); ?></td>
			  </tr>
			  <tr>
				<td><?php
					$gar = CHtml::listData(TruckerGarage::model()->findAll($criteria_a),'id','name');
					echo $form->dropDownList($model,'garage',$gar,array('empty'=>'')); ?>
                    <?php echo $form->error($model,'garage'); ?></td>
				<td colspan=2><?php echo $form->textField($model,'garage_add',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'garage_add'); ?></td>
			  </tr>
			  <tr><td colspan=3>&nbsp;</td></tr>
			  <tr>
				<td><?php echo $form->labelEx($model,Yii::t('students','garage_prov')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','garage_city')); ?></td>
				<td><?php echo $form->labelEx($model,Yii::t('students','garage_zip')); ?></td>
			  </tr>
			  <tr>
				<td><?php echo $form->textField($model,'garage_prov',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'garage_prov'); ?></td>
				<td><?php echo $form->textField($model,'garage_city',array('maxlength'=>255));?>
                    <?php echo $form->error($model,'garage_city'); ?></td>
				<td><?php echo $form->textField($model,'garage_zip',array('maxlength'=>5));?>
                    <?php echo $form->error($model,'garage_zip'); ?></td>
			  </tr>
			  <tr><td colspan=3>&nbsp;</td></tr>
			  <tr>
				<td colspan=3><?php echo $form->labelEx($model,Yii::t('students','garage_other_assoc')); ?></td>
			  </tr>
			  <tr>
				<td>
				<?php
					$assoc = CHtml::listData(TruckersOtherAssoc::model()->findAll($criteria_a),'id','name');
					echo $form->dropDownList($model,'garage_other_assoc',$assoc,array('empty'=>'')); ?>
					<?php echo $form->error($model,'garage_other_assoc'); ?></td>
				<td></td><td></td>
			  </tr>
			</table>
		</div>
	</div>
	

<div class="formCon" style=" background:#EDF1D1; border:0px #c4da9b solid; color:#393; background:#EDF1D1 url(images/green-bg.png); border:0px #c4da9b solid; color:#393;  ">
<div class="formConInner" style="padding:10px;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	
  <tr>
    <td></td>
    <td> </td>
    <td><?php 
	if($model->photo_data==NULL)
	{
	echo $form->labelEx($model,'upload_photo');
	}
	else
	{
	echo $form->labelEx($model,'Photo');	
	}
	
	 ?>
		</td>
    <td>
	<?php 
		
		
		if($model->isNewRecord)
		{
			echo $form->fileField($model,'photo_data'); 
		    echo $form->error($model,'photo_data'); 
		}
		else
		{
			if($model->photo_data==NULL)
			{
				echo $form->fileField($model,'photo_data'); 
		        echo $form->error($model,'photo_data'); 
			}
			
			else
			{
				if(Yii::app()->controller->action->id=='update') {
					echo CHtml::link(Yii::t('students','Remove'), array('Employees/remove', 'id'=>$model->id),array('confirm'=>'Are you sure?')); 
					echo '<img class="imgbrder" src="'.$this->createUrl('Employees/DisplaySavedImage&id='.$model->primaryKey).'" alt="'.$model->photo_file_name.'" width="100" height="100" />';	
				}
				else if(Yii::app()->controller->action->id=='create') {
					echo CHtml::hiddenField('photo_file_name',$model->photo_file_name);
					echo CHtml::hiddenField('photo_content_type',$model->photo_content_type);
					echo CHtml::hiddenField('photo_file_size',$model->photo_file_size);
					echo CHtml::hiddenField('photo_data',bin2hex($model->photo_data));
					echo '<img class="imgbrder" src="'.$this->createUrl('Employees/DisplaySavedImage&id='.$model->primaryKey).'" alt="'.$model->photo_file_name.'" width="100" height="100" />';
				}
			}
		}
		
		 ?>
        
        </td>
  </tr>

</table>
<div class="row">
		<?php //echo $form->labelEx($model,'photo_file_size'); ?>
		<?php echo $form->hiddenField($model,'photo_file_size'); ?>
		<?php echo $form->error($model,'photo_file_size'); ?>
	</div>

</div>
</div>
	
    <div class="clear"></div>
    <div style="padding:0px 0 0 0px; text-align:left">
    	<?php echo CHtml::submitButton($model->isNewRecord ? 'Submit »' : 'Save',array('class'=>'formbut')); ?>
    </div>
<?php $this->endWidget(); ?>
