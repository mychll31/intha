<?php
$this->breadcrumbs=array(
	'Trucker'=>array('/students/guardians/admin'),
	'List of Directors'=>array('/students/guardians/assesments&id='.Yii::app()->user->id),
);

$userid=User::model()->findByAttributes(array('id'=>Yii::app()->user->id));
$model=Students::model()->findByAttributes(array('assoc_truckerid'=>$userid->uid));
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    <?php $this->renderPartial('profileleft');?>
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <h1 style="margin-top:.67em;"><?php echo Yii::t('students','Trucker Profile :');?> <?php echo ucfirst($model->trucker); ?><br /></h1>
        
    <div class="clear"></div>
    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
     <?php $this->renderPartial('tab');?>
    <div class="clear"></div>
    <div class="tablebx">  
         <div class="pagecon">
		<br>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr class="tablebx_topbg">
				<td><?php echo Yii::t('empLicense','No.');?></td>	
				<td><?php echo Yii::t('empLicense','Name');?></td>
				<td><?php echo Yii::t('empLicense','Birthday');?></td>
				<td><?php echo Yii::t('empLicense','Birth Place');?></td>
				<td><?php echo Yii::t('empLicense','Address');?></td>
			</tr>
			<?php
				$cls="even"; $ctr = 0;
				foreach($posts as $post){ ?>
					<tr class=<?php echo $cls;?> id=<?php echo $i;?>>
					<?php $pref = EmpPrefix::model()->findByAttributes(array('id'=>$post->prefix)); ?>
					<td><?php $ctr++; echo $ctr; ?></td>
					<td><?php 
					if($pref->name !=null){$pname= $pref->name.'. ';}else{$pname='';}
					if($post->last_name){$lname= $post->last_name.', ';}else{$lname='';}
					if($post->first_name){$fname= $post->first_name.' ';}else{$fname='';}
					if($post->middle_name){$mname= $post->middle_name.' ';}else{$mname='';}
					if($post->ext){$ename= $post->ext;}else{$ename='';}
					echo CHtml::link($pname.$lname.$fname.$mname.$ename,array('/truckerDirectors/update&id='.$post->id));
					?></td>
					<td><?php echo date('M. d, Y',strtotime($post->bday));?></td>
					<td><?php $post->bplace; ?></td>
					<td><?php
						if($post->address !=null){echo $post->address.', ';}
						if($post->city !=null){echo $post->city.', ';}
						if($post->province !=null){echo $post->province.', ';}
						if($post->zipcode !=null){echo $post->zipcode;}
					?></td>
					</tr><?php	if($cls=="even"){$cls="odd";}else{$cls="even"; }$i++;} ?>
		</table>
    </div>
    </div>
    </div>
   </div>
    </td>
  </tr>
</table>