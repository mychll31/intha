<?php
$this->breadcrumbs=array(
	'Trucker'=>array('/students/guardians/admin'),
	'View',
);

$userid=User::model()->findByAttributes(array('id'=>Yii::app()->user->id));
$model=Students::model()->findByAttributes(array('assoc_truckerid'=>$userid->uid));
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    <?php $this->renderPartial('profileleft');?>
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <h1 style="margin-top:.67em;"><?php echo Yii::t('students','Trucker Profile :');?> <?php echo ucfirst($model->trucker); ?><br /></h1>
        
    <div class="clear"></div>
    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
     <?php $this->renderPartial('tab');?>
    <div class="clear"></div>
    <div class="emp_cntntbx" >
    
    <div class="table_listbx">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="listbxtop_hdng">
    <td><?php echo Yii::t('students','General');?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
	<td class="subhdng_nrmal"><span class="datahead">Association Trucker ID :</span><span class="data"><?php echo $model->assoc_truckerid;?></span></td>
    <td class="subhdng_nrmal"><span class="datahead">Membership Date :</span><span class="data"><?php echo $model->membership_date;?></span></td>
    <td class="subhdng_nrmal"><span class="datahead">Membership Expiration :</span><span class="data"><?php echo $model->membership_expiration;?></span></td>
  </tr>
  </table>
  
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="listbxtop_hdng">
    <td><?php echo Yii::t('students','Trucker Information');?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
	<td class="subhdng_nrmal"><span class="datahead">Trucker :</span><span class="data"><?php echo $model->trucker;?></span></td>
    <td class="subhdng_nrmal"><span class="datahead">Status :</span><span class="data"><?php echo $model->trucker_status;?></span></td>
    <td class="subhdng_nrmal"><span class="datahead">Type of Business :</span><span class="data"><?php echo $model->buss_type;?></span></td>
  </tr>
  <tr>
	<td class="subhdng_nrmal"><span class="datahead">Nature of Business :</span><span class="data"><?php echo $model->buss_nature;?></span></td>
	<td class="subhdng_nrmal"  colspan=2>
	<span class="datahead">Street No./St. Address/Baranggay/Town :</span>
	<br><span class="data"><?php if($model->buss_add_add !=null){echo $model->buss_add_add;}?></span>
	</td>
  </tr>
  <tr><td class="subhdng_nrmal">
	<span class="datahead">Province/State :</span>
	<span class="data"><?php if($model->buss_add_prov !=null){echo $model->buss_add_prov;}?></span>
	</td><td class="subhdng_nrmal">
	<span class="datahead">City/Municipality :</span>
	<span class="data"><?php if($model->buss_add_city){echo $model->buss_add_city;}?></span>
	</td><td class="subhdng_nrmal">
	<span class="datahead">Zip Code :</span>
	<span class="data"><?php if($model->buss_add_zip){echo $model->buss_add_zip;}?></span>
	</td>
  </tr>
  <tr>
	<td class="subhdng_nrmal"><span class="datahead">Owner/CEO/GM :</span><span class="data"><?php echo ucwords($model->owner);?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Fax Number :</span><span class="data"><?php echo $model->fax;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Telephone Number :</span><span class="data"><?php echo $model->tel_no;?></span></td>
  </tr>
  <tr>
	<td class="subhdng_nrmal"><span class="datahead">Email Address :</span><span class="data"><?php echo $model->email;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Website :</span><span class="data"><?php echo $model->website;?></span></td>
	<td></td>
  </tr>
  <tr>
	<td class="subhdng_nrmal"><span class="datahead">Business Registration :</span><span class="data"><?php echo $model->buss_reg;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Mayors Permit :</span><span class="data"><?php echo $model->mayors_permit;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">SSS :</span><span class="data"><?php echo $model->sss;?></span></td>
  </tr>
  <?/*?>
  <tr>
	<td class="subhdng_nrmal"><span class="datahead">Registered Owner :</span><span class="data"><?php echo $model->reg_owner;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Registration Start Date :</span><span class="data"><?php echo $model->reg_start_date;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Registration End Date :</span><span class="data"><?php echo $model->reg_start_date;?></span></td>
  </tr>
  <?*/?>
  <tr>
	<td class="subhdng_nrmal"><span class="datahead">Philhealth :</span><span class="data"><?php echo $model->ph;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Pagibig :</span><span class="data"><?php echo $model->pagibig;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">TIN No. :</span><span class="data"><?php echo $model->bir;?></span></td>
  </tr>
  </table>
  
  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="listbxtop_hdng">
    <td><?php echo Yii::t('students','Latest Annual Volume of items Moved (in Units)');?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td class="subhdng_nrmal"><span class="datahead">Tons :</span><span class="data"><?php echo $model->tons;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Teus :</span><span class="data"><?php echo $model->teus;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Cases :</span><span class="data"><?php echo $model->cases;?></span></td>
  </tr>
  <tr>
	<td colspan=3 class="subhdng_nrmal"><span class="datahead">Others(Specify) :</span><span class="data"><?php echo $model->o_specify.' '.$model->o_specify_content;?></span></td>
  </tr>
  </table>
  
  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="listbxtop_hdng">
    <td><?php echo Yii::t('students','Official Representative');?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td colspan=3 class="subhdng_nrmal"><span class="datahead">Name :</span><span class="data">
	<?php
	if($model->rep_lname !=null){$replname=$model->rep_lname.', ';}else{$replname='';}
	if($model->rep_fname !=null){$repfname=$model->rep_fname.' ';}else{$repfname='';}
	if($model->rep_mname !=null){$repmname=$model->rep_mname;}else{$repmname='';}
	echo ucwords($replname.$repfname.$repmname);
	?></span></td>
  </tr>
  <tr>
	<td class="subhdng_nrmal"><span class="datahead">Birthday :</span><span class="data">
	<?php echo $model->rep_bdate;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Designation :</span><span class="data">
	<?php echo ucwords($model->rep_desig);?></span></td>
	<td class="subhdng_nrmal"><span class="datahead"></span><span class="data"></span></td>
  </tr>
  <tr>
	<td class="subhdng_nrmal"><span class="datahead">Mobile Number :</span><span class="data">
	<?php echo $model->rep_mobile;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Fax Number :</span><span class="data">
	<?php echo $model->rep_fax;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Telephone Number:</span><span class="data">
	<?php echo $model->rep_telno;?></span></td>
  </tr>
  </table>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="listbxtop_hdng">
    <td><?php echo Yii::t('students','Alternative Representative');?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td colspan=3 class="subhdng_nrmal"><span class="datahead">Name :</span><span class="data">
	<?php
	if($model->alter_lname !=null){$alterlname=$model->alter_lname.', ';}else{$alterlname='';}
	if($model->alter_fname !=null){$alterfname=$model->alter_fname.' ';}else{$alterfname='';}
	if($model->alter_mname !=null){$altermname=$model->alter_mname;}else{$altermname='';}
	echo ucwords($alterlname.$alterfname.$altermname);
	?></span></td>
  </tr>
  <tr>
	<td class="subhdng_nrmal"><span class="datahead">Birthday :</span><span class="data">
	<?php echo $model->alter_bdate;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Designation :</span><span class="data">
	<?php echo ucwords($model->alter_desig);?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Email Address :</span><span class="data">
	<?php echo ucwords($model->alter_email);?></span></td>
  </tr>
  <tr>
	<td class="subhdng_nrmal"><span class="datahead">Mobile Number :</span><span class="data">
	<?php echo $model->alter_mobile;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Fax Number :</span><span class="data">
	<?php echo $model->alter_fax;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Telephone Number:</span><span class="data">
	<?php echo $model->alter_telno;?></span></td>
  </tr>
  </table>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="listbxtop_hdng">
    <td><?php echo Yii::t('students','Other Informations');?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  <tr>
	<td class="subhdng_nrmal"><span class="datahead">Garage :</span><span class="data">
	<?php 
	$gg = TruckerGarage::model()->findByAttributes(array('id'=>$model->garage));
	echo $gg->name;?></span></td>
	<td class="subhdng_nrmal"><span class="datahead">Other Association :</span><span class="data">
	<?php 
	$gotherassoc =TruckersOtherAssoc::model()->findByAttributes(array('id'=>$model->garage_other_assoc));
	echo ucwords($gotherassoc->desc);
	if($gotherassoc->name != null){ echo '('.strtoupper($gotherassoc->name).')';}?></span></td>
	<td class="subhdng_nrmal"><span class="datahead"></span><span class="data"></span></td>
  </tr>
  <tr>
	<td class="subhdng_nrmal" colspan=3>
		<span class="datahead">Street No./St. Address/Baranggay/Town :</span>
		<br><span class="data"><?php if($model->garage_add !=null){echo $model->garage_add.', ';}?></span>
	</td>
  <tr>
		<td class="subhdng_nrmal">
		<span class="datahead">Province/State :</span>
		<span class="data"><?php if($model->garage_prov !=null){echo $model->garage_prov.', ';}?></span>
		</td><td class="subhdng_nrmal">
		<span class="datahead">City/Municipality :</span>
		<span class="data"><?php if($model->garage_city !=null){echo $model->garage_city.' ';}?></span>
		</td><td class="subhdng_nrmal">
		<span class="datahead">Zip Code :</span>
		<span class="data"><?php if($model->garage_city !=null){echo $model->garage_zip.' ';}?></span>
	</td>
  </tr>
  </table>
 <!--div class="ea_pdf" style="top:4px; right:6px;">
 <?php #echo CHtml::link('<img src="images/pdf-but.png">', array('Students/pdf','id'=>$_REQUEST['id']),array('target'=>'_blank')); ?>
	</div-->
 
    </div>
    </div>
    </div>
    
    </div>
    </div>
   
    </td>
  </tr>
</table>