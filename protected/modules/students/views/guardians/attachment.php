<?php
$this->breadcrumbs=array(
	'Trucker'=>array('/students/guardians/admin'),
	'Attachments'=>array('/students/guardians/attachment&id='.Yii::app()->user->id),
);

$userid=User::model()->findByAttributes(array('id'=>Yii::app()->user->id));
$model=Students::model()->findByAttributes(array('assoc_truckerid'=>$userid->uid));
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    <?php $this->renderPartial('profileleft');?>
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <h1 style="margin-top:.67em;"><?php echo Yii::t('students','Trucker Profile :');?> <?php echo ucfirst($model->trucker); ?><br /></h1>
        
    <div class="clear"></div>
    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
	<?php $this->renderPartial('tab');?>

    <div class="clear"></div>
    <div class="tablebx">  
        <div class="pagecon">
		<br>
		<table width="60%" border="0" cellspacing="0" cellpadding="0">
			<tr class="tablebx_topbg">
				<td><?php echo Yii::t('students','No.');?></td>
				<td><?php echo Yii::t('students','Filename');?></td>
			</tr>
		<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'students-form',
		'enableAjaxValidation'=>false,
		'htmlOptions'=>array('enctype'=>'multipart/form-data'),
		'method'=>'post', 
		)); ?>
		<?php
			$upload_directory = 'attachments/students/*';
			$criteria = new CDbCriteria;
			$criteria->condition='truckers_id = :match';
			$criteria->params[':match'] = $_REQUEST['id'];
			$stud_attachment = AttachmentsStudent::model()->findAll($criteria);
			$no=1;
			$cls="even";
			foreach($stud_attachment as $attach){
			?>
			<tr class=<?php echo $cls;?> id=<?php echo $i;?>>
			<?php
				echo '<td>'.$no++.'</td><td style="text-align:left;padding-left:20px;"><a href="'.$attach->fileloc.'">'.$attach->filename.'</a></td>';
			}
			?>
			</tr>
			</table>
			<br><br>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<br>
		<style>
		.addbutton{
			background-color: rgb(226, 146, 27) !important;
			color: rgb(97, 80, 12);
			background-color: oldlace;
			padding: 0px 5px;
			border-radius: 30px;
			font-size: 15pt;
			border: 1px solid #857710;
			font-weight: bolder;
		}
		.addbutton:hover{
			background-color: rgb(243, 169, 58) !important;
		}
		</style>
		<div id="file_container">
			<input name="file[]" type="file"  /><br />
		  </div>
		<br><a class="addbutton" href="javascript:void(0);" onClick="add_file_field();">+</a><br />
		</div>
		<br>
		</td></tr>
		<tr class="listbxtop_hdng"><td>
		<div class="clear"></div>
		<div style="padding:0px 0 0 0px; text-align:left">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Submit »' : 'Save',array('class'=>'formbut','name'=>'submit')); ?>
		</div>
		</td></tr></table>
    <?php $this->endWidget(); ?>
	</div>
    </div>
   </div>
    </td>
  </tr>
</table>
<script>
	function add_file_field(){
		var container=document.getElementById('file_container');
		var file_field=document.createElement('input');
		file_field.name='file[]';
		file_field.type='file';
		container.appendChild(file_field);
		var br_field=document.createElement('br');
		container.appendChild(br_field);
	}
</script>