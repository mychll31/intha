<?php

class StudentsController extends RController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','manage','Website','savesearch','events','attentance','Assesments','DisplaySavedImage','Fees','Payfees','Pdf','Printpdf','Remove','Search','inactive','active','deletes','trailer','trailerdetails'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','batch','add'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		//$this->layout='';
		//header("Content-type: image/jpeg");
		//echo $model->photo_data;
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionViewtruck($id)
	{
		//$this->layout='';
		//header("Content-type: image/jpeg");
		//echo $model->photo_data;
		$this->render('viewtruck',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionPrintpdf()
	{
		//$this->layout='';
		//header("Content-type: image/jpeg");
		//echo $model->photo_data;
		$this->render('printpdf',array(
			'model'=>$this->loadModel($_REQUEST['id']),
		));
	}
	 public function actionPdf()
    {
		$student = Students::model()->findByAttributes(array('id'=>$_REQUEST['id']));
		$student = $student->first_name.' '.$student->last_name.' Profile.pdf';
        
        # HTML2PDF has very similar syntax
        $html2pdf = Yii::app()->ePdf->HTML2PDF();

        $html2pdf->WriteHTML($this->renderPartial('printpdf', array('model'=>$this->loadModel($_REQUEST['id'])), true));
        $html2pdf->Output($student);
 
        ////////////////////////////////////////////////////////////////////////////////////
	}
	public function actionDisplaySavedImage()
		{
			$model=$this->loadModel($_GET['id']);
			header('Pragma: public');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Content-Transfer-Encoding: binary');
			header('Content-length: '.$model->photo_file_size);
			header('Content-Type: '.$model->photo_content_type);
			header('Content-Disposition: attachment; filename='.$model->photo_file_name);
			echo $model->photo_data;
		}
	
	public function actionRemove()
	{
		
		$model = Students::model()->findByAttributes(array('id'=>$_REQUEST['id']));
		$model->saveAttributes(array('photo_file_name'=>'','photo_data'=>''));
		$this->redirect(array('update','id'=>$_REQUEST['id']));
	}
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	 
	public function actionCreate()
	{
		$model=new Students;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Students']))
		{
		
			$model->attributes=$_POST['Students'];
			$list = $_POST['Students'];
			$model->assoc_truckerid= $list['assoc_truckerid'];
			$model->membership_date= $list['membership_date'];
			$model->membership_expiration= $list['membership_expiration'];
			$model->trucker= $list['trucker'];
			$model->trucker_status= $list['trucker_status'];
			$model->buss_type= $list['buss_type'];
			$model->buss_nature= $list['buss_nature'];
			$model->buss_add_add= $list['buss_add_add'];
			$model->buss_add_prov= $list['buss_add_prov'];
			$model->buss_add_city= $list['buss_add_city'];
			$model->buss_add_zip= $list['buss_add_zip'];
			$model->owner= $list['owner'];
			$model->buss_reg= $list['buss_reg'];
			$model->mayors_permit= $list['mayors_permit'];
			$model->sss= $list['sss'];
			$model->ph= $list['ph'];
			$model->pagibig= $list['pagibig'];
			$model->bir= $list['bir'];
			$model->tel_no= $list['tel_no'];
			$model->fax= $list['fax'];
			$model->website= $list['website'];
			$model->email= $list['email'];
			$model->tons= $list['tons'];
			$model->teus= $list['teus'];
			$model->cases= $list['cases'];
			$model->o_specify_content= $list['o_specify_content'];
			$model->o_specify= $list['o_specify'];
			$model->rep_lname= $list['rep_lname'];
			$model->rep_desig= $list['rep_desig'];
			$model->rep_mname= $list['rep_mname'];
			$model->rep_fname= $list['rep_fname'];
			$model->rep_telno= $list['rep_telno'];
			$model->rep_email= $list['rep_email'];
			$model->rep_mobile= $list['rep_mobile'];
			$model->rep_fax= $list['rep_fax'];
			$model->rep_bdate= $list['rep_bdate'];
			$model->alter_lname= $list['alter_lname'];
			$model->alter_fname= $list['alter_fname'];
			$model->alter_mname= $list['alter_mname'];
			$model->alter_desig= $list['alter_desig'];
			$model->alter_telno= $list['alter_telno'];
			$model->alter_email= $list['alter_email'];
			$model->alter_mobile= $list['alter_mobile'];
			$model->alter_fax= $list['alter_fax'];
			$model->alter_bdate= $list['alter_bdate'];
			$model->garage= $list['garage'];
			$model->garage_add= $list['garage_add'];
			$model->garage_prov= $list['garage_prov'];
			$model->garage_city= $list['garage_city'];
			$model->garage_zip= $list['garage_zip'];
			$model->garage_other_assoc= $list['garage_other_assoc'];
			$model->createdby= Yii::app()->user->id;
			$model->created_at= date('Y-m-d H:i:s',time());
			
			if($file=CUploadedFile::getInstance($model,'photo_data'))
			 {
				$model->photo_file_name=$file->name;
				$model->photo_content_type=$file->type;
				$model->photo_file_size=$file->size;
				$upload_directory = 'attachments/students/photos/';
				$temp = explode(".", $file->name);
				$extension = end($temp);
				$model->photo_src=$upload_directory.time().'.'.$extension;
				move_uploaded_file($file->tempName, $upload_directory . time().'.'.$extension);
				#$model->photo_data=file_get_contents($file->tempName);
      		  }
			
			if($model->save()){
				$this->redirect(array('students/manage'));
			}
		
		}
		
		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		if(isset($_POST['Students']))
		{
			$model->attributes=$_POST['Students'];
			$list = $_POST['Students'];
#			$model->assoc_truckerid= $list['assoc_truckerid'];
			$model->membership_date= $list['membership_date'];
			$model->membership_expiration= $list['membership_expiration'];
			$model->trucker= $list['trucker'];
			$model->trucker_status= $list['trucker_status'];
			$model->buss_type= $list['buss_type'];
			$model->buss_nature= $list['buss_nature'];
			$model->buss_add_add= $list['buss_add_add'];
			$model->buss_add_prov= $list['buss_add_prov'];
			$model->buss_add_city= $list['buss_add_city'];
			$model->buss_add_zip= $list['buss_add_zip'];
			$model->owner= $list['owner'];
			$model->buss_reg= $list['buss_reg'];
			$model->mayors_permit= $list['mayors_permit'];
			$model->sss= $list['sss'];
			$model->ph= $list['ph'];
			$model->pagibig= $list['pagibig'];
			$model->bir= $list['bir'];
			$model->tel_no= $list['tel_no'];
			$model->fax= $list['fax'];
			$model->website= $list['website'];
			$model->email= $list['email'];
			$model->tons= $list['tons'];
			$model->teus= $list['teus'];
			$model->cases= $list['cases'];
			$model->o_specify_content= $list['o_specify_content'];
			$model->o_specify= $list['o_specify'];
			$model->rep_lname= $list['rep_lname'];
			$model->rep_desig= $list['rep_desig'];
			$model->rep_mname= $list['rep_mname'];
			$model->rep_fname= $list['rep_fname'];
			$model->rep_telno= $list['rep_telno'];
			$model->rep_email= $list['rep_email'];
			$model->rep_mobile= $list['rep_mobile'];
			$model->rep_fax= $list['rep_fax'];
			$model->rep_bdate= $list['rep_bdate'];
			$model->alter_lname= $list['alter_lname'];
			$model->alter_fname= $list['alter_fname'];
			$model->alter_mname= $list['alter_mname'];
			$model->alter_desig= $list['alter_desig'];
			$model->alter_telno= $list['alter_telno'];
			$model->alter_email= $list['alter_email'];
			$model->alter_mobile= $list['alter_mobile'];
			$model->alter_fax= $list['alter_fax'];
			$model->alter_bdate= $list['alter_bdate'];
			$model->garage= $list['garage'];
			$model->garage_add= $list['garage_add'];
			$model->garage_prov= $list['garage_prov'];
			$model->garage_city= $list['garage_city'];
			$model->garage_zip= $list['garage_zip'];
			$model->garage_other_assoc= $list['garage_other_assoc'];
			$model->updated_at= date('Y-m-d H:i:s',time());
			$model->lastupdateby= Yii::app()->user->id;
			
			if($file=CUploadedFile::getInstance($model,'photo_data'))
       		 {
            $model->photo_file_name=$file->name;
            $model->photo_content_type=$file->type;
            $model->photo_file_size=$file->size;
            $upload_directory = 'attachments/students/photos/';
			$temp = explode(".", $file->name);
			$extension = end($temp);
			$model->photo_src=$upload_directory.time().'.'.$extension;
			move_uploaded_file($file->tempName, $upload_directory . time().'.'.$extension);
			#$model->photo_data=file_get_contents($file->tempName);
      		  }
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	public function actionAssesments()
	{
		
		$model=new Students;
		$criteria = new CDbCriteria;
		
		$trucker_truckerid = Students::model()->findByAttributes(array('id'=>$_REQUEST['id']));
		$criteria->condition='trucker_id = :match';
		$criteria->params[':match'] = $trucker_truckerid->assoc_truckerid;
		
		$criteria->order = 'id ASC';
	
		$posts = TruckerDirectors::model()->findAll($criteria);
			$this->render('assesments',array(
			'model'=>$this->loadModel($_REQUEST['id']),
			'posts' => $posts,
		));

	}
	
	public function actionInlandmarine()
	{
		$model=new Students;
		$criteria = new CDbCriteria;
		
		$trucker_truckerid = Students::model()->findByAttributes(array('id'=>$_REQUEST['id']));
		$criteria->condition='truckerid = :match';
		$criteria->params[':match'] = $trucker_truckerid->assoc_truckerid;
		
		$criteria->order = 'id ASC';
	
		$posts = InlandMarineTrucks::model()->findAll($criteria);
			$this->render('inlandmarine',array(
			'model'=>$this->loadModel($_REQUEST['id']),
			'posts' => $posts,
		));

	}
	
	public function actionInsurance()
	{
	$this->render('ctpl');
	}
	public function actionCtpl()
	{
	$this->render('ctpl');
	}
	
	public function actionCompre()
	{
	$this->render('compre');
	}
	public function actionPa()
	{
	$this->render('pa');
	}
	
	
	public function actionAttentance()
	{
		$model=new Students;
		$criteria = new CDbCriteria;
		
		$trucker_truckerid = Students::model()->findByAttributes(array('id'=>$_REQUEST['id']));
		$criteria->condition='trucker_id = :match';
		$criteria->params[':match'] = $trucker_truckerid->assoc_truckerid;
		
		$criteria->order = 'id ASC';
	
		$posts = Employees::model()->findAll($criteria);
			$this->render('attentance',array(
			'model'=>$this->loadModel($_REQUEST['id']),
			'posts' => $posts,
		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	 public function actionManage()
	 {
		 
		$model=new Students;
		$criteria = new CDbCriteria;
		$criteria->compare('is_deleted',0);  // normal DB field
		$criteria->condition='is_deleted=:is_del';
		$criteria->params = array(':is_del'=>0);
		
		if(isset($_REQUEST['val']))
		{
		 $criteria->condition=$criteria->condition.' and '.'(trucker LIKE :match or owner LIKE :match)';
		 //$criteria->params = array(':match' => $_REQUEST['val'].'%');
		 $criteria->params[':match'] = $_REQUEST['val'].'%';
		}
		
		if(isset($_REQUEST['name']) and $_REQUEST['name']!=NULL)
		{
		if((substr_count( $_REQUEST['name'],' '))==0)
		 { 	
		 $criteria->condition=$criteria->condition.' and '.'(trucker LIKE :name or owner LIKE :name)';
		 $criteria->params[':name'] = $_REQUEST['name'].'%';
		}
		else if((substr_count( $_REQUEST['name'],' '))>=1)
		{
		 $name=explode(" ",$_REQUEST['name']);
		 $criteria->condition=$criteria->condition.' and '.'(first_name LIKE :name or last_name LIKE :name or middle_name LIKE :name)';
		 $criteria->params[':name'] = $name[0].'%';
		 $criteria->condition=$criteria->condition.' and '.'(first_name LIKE :name1 or last_name LIKE :name1 or middle_name LIKE :name1)';
		 $criteria->params[':name1'] = $name[1].'%';
		 	
		}
		}
		
		if(isset($_REQUEST['admissionnumber']) and $_REQUEST['admissionnumber']!=NULL)
		{
		 $criteria->condition=$criteria->condition.' and '.'assoc_truckerid LIKE :admissionnumber';
		 $criteria->params[':admissionnumber'] = $_REQUEST['admissionnumber'].'%';
		}
		
		if(isset($_REQUEST['Students']['batch_id']) and $_REQUEST['Students']['batch_id']!=NULL)
		{
			$model->batch_id = $_REQUEST['Students']['batch_id'];
			$criteria->condition=$criteria->condition.' and '.'batch_id = :batch_id';
		    $criteria->params[':batch_id'] = $_REQUEST['Students']['batch_id'];
		}
		
		if(isset($_REQUEST['Students']['gender']) and $_REQUEST['Students']['gender']!=NULL)
		{
			$model->gender = $_REQUEST['Students']['gender'];
			$criteria->condition=$criteria->condition.' and '.'gender = :gender';
		    $criteria->params[':gender'] = $_REQUEST['Students']['gender'];
		}
		
		if(isset($_REQUEST['Students']['blood_group']) and $_REQUEST['Students']['blood_group']!=NULL)
		{
			$model->blood_group = $_REQUEST['Students']['blood_group'];
			$criteria->condition=$criteria->condition.' and '.'blood_group = :blood_group';
		    $criteria->params[':blood_group'] = $_REQUEST['Students']['blood_group'];
		}
		
		if(isset($_REQUEST['Students']['nationality_id']) and $_REQUEST['Students']['nationality_id']!=NULL)
		{
			$model->nationality_id = $_REQUEST['Students']['nationality_id'];
			$criteria->condition=$criteria->condition.' and '.'nationality_id = :nationality_id';
		    $criteria->params[':nationality_id'] = $_REQUEST['Students']['nationality_id'];
		}
		
		
		if(isset($_REQUEST['Students']['dobrange']) and $_REQUEST['Students']['dobrange']!=NULL)
		{
			  
			  $model->dobrange = $_REQUEST['Students']['dobrange'] ;
			  if(isset($_REQUEST['Students']['date_of_birth']) and $_REQUEST['Students']['date_of_birth']!=NULL)
			  {
				  if($_REQUEST['Students']['dobrange']=='2')
				  {  
					  $model->date_of_birth = $_REQUEST['Students']['date_of_birth'];
					  $criteria->condition=$criteria->condition.' and '.'date_of_birth = :date_of_birth';
					  $criteria->params[':date_of_birth'] = date('Y-m-d',strtotime($_REQUEST['Students']['date_of_birth']));
				  }
				  if($_REQUEST['Students']['dobrange']=='1')
				  {  
				  
					  $model->date_of_birth = $_REQUEST['Students']['date_of_birth'];
					  $criteria->condition=$criteria->condition.' and '.'date_of_birth < :date_of_birth';
					  $criteria->params[':date_of_birth'] = date('Y-m-d',strtotime($_REQUEST['Students']['date_of_birth']));
				  }
				  if($_REQUEST['Students']['dobrange']=='3')
				  {  
					  $model->date_of_birth = $_REQUEST['Students']['date_of_birth'];
					  $criteria->condition=$criteria->condition.' and '.'date_of_birth > :date_of_birth';
					  $criteria->params[':date_of_birth'] = date('Y-m-d',strtotime($_REQUEST['Students']['date_of_birth']));
				  }
				  
			  }
		}
		elseif(isset($_REQUEST['Students']['dobrange']) and $_REQUEST['Students']['dobrange']==NULL)
		{
			  if(isset($_REQUEST['Students']['date_of_birth']) and $_REQUEST['Students']['date_of_birth']!=NULL)
			  {
				  $model->date_of_birth = $_REQUEST['Students']['date_of_birth'];
				  $criteria->condition=$criteria->condition.' and '.'date_of_birth = :date_of_birth';
				  $criteria->params[':date_of_birth'] = date('Y-m-d',strtotime($_REQUEST['Students']['date_of_birth']));
			  }
		}
		
		
		if(isset($_REQUEST['Students']['admissionrange']) and $_REQUEST['Students']['admissionrange']!=NULL)
		{
			  
			  $model->admissionrange = $_REQUEST['Students']['admissionrange'] ;
			  if(isset($_REQUEST['Students']['admission_date']) and $_REQUEST['Students']['admission_date']!=NULL)
			  {
				  if($_REQUEST['Students']['admissionrange']=='2')
				  {  
					  $model->admission_date = $_REQUEST['Students']['admission_date'];
					  $criteria->condition=$criteria->condition.' and '.'admission_date = :admission_date';
					  $criteria->params[':admission_date'] = date('Y-m-d',strtotime($_REQUEST['Students']['admission_date']));
				  }
				  if($_REQUEST['Students']['admissionrange']=='1')
				  {  
				  
					  $model->admission_date = $_REQUEST['Students']['admission_date'];
					  $criteria->condition=$criteria->condition.' and '.'admission_date < :admission_date';
					  $criteria->params[':admission_date'] = date('Y-m-d',strtotime($_REQUEST['Students']['admission_date']));
				  }
				  if($_REQUEST['Students']['admissionrange']=='3')
				  {  
					  $model->admission_date = $_REQUEST['Students']['admission_date'];
					  $criteria->condition=$criteria->condition.' and '.'admission_date > :admission_date';
					  $criteria->params[':admission_date'] = date('Y-m-d',strtotime($_REQUEST['Students']['admission_date']));
				  }
				  
			  }
		}
		elseif(isset($_REQUEST['Students']['admissionrange']) and $_REQUEST['Students']['admissionrange']==NULL)
		{
			  if(isset($_REQUEST['Students']['admission_date']) and $_REQUEST['Students']['admission_date']!=NULL)
			  {
				  $model->admission_date = $_REQUEST['Students']['admission_date'];
				  $criteria->condition=$criteria->condition.' and '.'admission_date = :admission_date';
				  $criteria->params[':admission_date'] = date('Y-m-d',strtotime($_REQUEST['Students']['admission_date']));
			  }
		}
		
		if(isset($_REQUEST['Students']['status']) and $_REQUEST['Students']['status']!=NULL)
		{
			$model->status = $_REQUEST['Students']['status'];
			$criteria->condition=$criteria->condition.' and '.'is_active = :status';
		    $criteria->params[':status'] = $_REQUEST['Students']['status'];
		}
		
		
		$criteria->order = 'id ASC';
		
		$total = Students::model()->count($criteria);
		$pages = new CPagination($total);
        $pages->setPageSize(Yii::app()->params['listPerPage']);
        $pages->applyLimit($criteria);  // the trick is here!
		$posts = Students::model()->findAll($criteria);
		$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id));
		 
		$this->render('manage',array('model'=>$model,
		'list'=>$posts,
		'pages' => $pages,
		'item_count'=>$total,
		'page_size'=>Yii::app()->params['listPerPage'],)) ;
	 }
	 
	public function actionIndex()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('is_deleted',0);
		$total = Students::model()->count($criteria);
		$criteria->order = 'id DESC';
		$criteria->limit = '10';
		$posts = Students::model()->findAll($criteria);
		
		
		$this->render('index',array(
			'total'=>$total,'list'=>$posts
		));
	}
	
	public function actionSavesearch()
	{
		$dataProvider=new CActiveDataProvider('Students');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	
	public function actionPayfees()
	{
		$list  = FinanceFees::model()->findByAttributes(array('id'=>$_REQUEST['id']));
		$list->is_paid = 1;
		$list->save();
			$this->redirect(array('fees','id'=>$list->student_id));
	}
	public function actionFees($id)
	{
		$model=new Students;
		$criteria = new CDbCriteria;
		
		$trucker_truckerid = Students::model()->findByAttributes(array('id'=>$_REQUEST['id']));
		$criteria->condition='association_trucker_id = :match';
		$criteria->params[':match'] = $trucker_truckerid->assoc_truckerid;
		
		$criteria->order = 'id ASC';
	
		$posts = Courses::model()->findAll($criteria);
			$this->render('fees',array(
			'model'=>$this->loadModel($_REQUEST['id']),
			'posts' => $posts,
		));
	}
	
	public function actionAttachment($id)
	{
		$model= new Students;
		$number_of_file_fields = 0;
		$number_of_uploaded_files = 0;
		$uploaded_files = array();
		$upload_directory = 'attachments/students/';
		if(isset($_POST["submit"])) {
			for($i = 0; $i < count($_FILES['file']['name']); $i++){
				$number_of_file_fields++;
				if($_FILES["file"]["size"][$i] < 8000000 ){
					if ($_FILES['file']['name'][$i] != '') {
						$number_of_uploaded_files++;
						$uploaded_files[] = $_FILES['file']['name'][$i];
						$temp = explode(".", $_FILES["file"]["name"][$i]);
						$extension = end($temp);
						$filename = trim($_FILES['file']['name'][$i],'.'.$extension);
						$count = 0;
						$f_name = $_FILES['file']['name'][$i];
						$f_type = $extension;
						$f_size = $_FILES['file']['size'][$i];

						while(file_exists($upload_directory . $_FILES['file']['name'][$i])) {
						$count += 1;
						$_FILES['file']['name'][$i] = $filename.'_copy('.$count.').'.$extension;
						$f_name = $_FILES['file']['name'][$i];
						$f_type = $extension;
						$f_size = $_FILES['file']['size'][$i];
						}

						try{
						$insertAttachments = Yii::app()->db->createCommand('insert into attachments_student values(null,"'.$_REQUEST['id'].'","'.$f_name.'","'.$f_type.'","'.$upload_directory.$f_name.'","'.date('Y-m-d').'","0")')->queryAll();
						}catch(Exception $e){}
						if (move_uploaded_file($_FILES['file']['tmp_name'][$i], $upload_directory . $_FILES['file']['name'][$i])) {
							echo $number_of_moved_files++;
						}
					}
				}
			}
		}############


		$this->render('attachment',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionEvents()
	{
		$this->render('events');
	}
	
	public function actionAdd() {
		
		if (isset($_POST['title'])) {
			$data[MENU_TITLE] = trim($_POST['title']);
			if (!empty($data[MENU_TITLE])) {
				$data[MENU_URL] = $_POST['url'];
				$data[MENU_CLASS] = $_POST['class'];
				$data[MENU_GROUP] = $_POST['group_id'];
				$data[MENU_POSITION] = Menu::model()->get_last_position($_POST['group_id']);
				$data[MENU_POSITION] = $data[MENU_POSITION]+1;
				if (Menu::model()->insert(MENU_TABLE, $data)) {
					$data[MENU_ID] = $this->db->Insert_ID();
					$response['status'] = 1;
					$li_id = 'menu-'.$data[MENU_ID];
					$response['li'] = '<li id="'.$li_id.'" class="sortable">'.Menu::model()->get_label($data).'</li>';
					$response['li_id'] = $li_id;
				} else {
					$response['status'] = 2;
					$response['msg'] = 'Add menu error.';
				}
			} else {
				$response['status'] = 3;
			}
			echo 'eee';
			header('Content-type: application/json');
			echo json_encode($response);
		}
	}
	public function actionWebsite()
	{
		
		$group_id = 1;
		if (isset($_GET['group_id'])) {
			$group_id = (int)$_GET['group_id'];
		}
		$menu = Menu::model()->get_menu($group_id);
		$data['menu_ul'] = '<ul id="easymm"></ul>';
		if ($menu) {

			include 'includes/tree.php';
			$tree = new Tree;

			foreach ($menu as $row) {
				$tree->add_row(
					$row[MENU_ID],
					$row[MENU_PARENT],
					' id="menu-'.$row[MENU_ID].'" class="sortable"',
					Menu::model()->get_label($row)
				);
			}

			$data['menu_ul'] = $tree->generate_list('id="easymm"');
		}
		$data['group_id'] = $group_id;
		$data['group_title'] = Menu::model()->get_menu_group_title($group_id);
		$data['menu_groups'] = Menu::model()->get_menu_groups();
		//$this->view('menu', $data);
		$this->render('website',$data);
	}
	
	public function actionBatch()
	{
		
		
		$data=Batches::model()->findAll('course_id=:id', 
                  array(':id'=>(int) $_POST['cid']));
				  
		echo CHtml::tag('option', array('value' => 0), CHtml::encode('-Select-'), true);
 
         $data=CHtml::listData($data,'id','name');
		  foreach($data as $value=>$name)
		  {
			  echo CHtml::tag('option',
						 array('value'=>$value),CHtml::encode($name),true);
		  }
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Students('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Students']))
			$model->attributes=$_GET['Students'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionTrailer($id)
	{
		$model=new Students;
		$criteria = new CDbCriteria;
		
		$trucker_truckerid = Students::model()->findByAttributes(array('id'=>$_REQUEST['id']));
		$criteria->condition='truckerid = :match';
		$criteria->params[':match'] = $trucker_truckerid->assoc_truckerid;
		
		$criteria->order = 'id ASC';
	
		$posts = TruckTrailer::model()->findAll($criteria);
			$this->render('trailer',array(
			'model'=>$this->loadModel($_REQUEST['id']),
			'posts' => $posts,
		));
	}

	public function actionTrailerdetails($tid,$id)
	{
		$this->render('trailerdetails');
	}

	public function loadModel($id)
	{
		$model=Students::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function actionSearch()
	{
	
		$model=new Students;
		$criteria = new CDbCriteria;
		 $criteria->condition='first_name LIKE :match or middle_name LIKE :match or last_name LIKE :match';
		 $criteria->params = array(':match' => $_POST['char'].'%');
		  $criteria->order = 'first_name ASC';
		 $total = Students::model()->count($criteria);
		$pages = new CPagination($total);
        $pages->setPageSize(Yii::app()->params['listPerPage']);
        $pages->applyLimit($criteria);  // the trick is here!
		$posts = Students::model()->findAll($criteria);
		
		$emp=new Employees;
		$criteria_1 = new CDbCriteria;
		 $criteria_1->condition='first_name LIKE :match or middle_name LIKE :match or last_name LIKE :match';
		 $criteria_1->params = array(':match' => $_POST['char'].'%');
		 $criteria_1->order = 'first_name ASC';
		 $tot = Employees::model()->count($criteria_1);
		$pages_1 = new CPagination($total);
        $pages_1->setPageSize(Yii::app()->params['listPerPage']);
        $pages_1->applyLimit($criteria_1);  // the trick is here!
		$posts_1 = Employees::model()->findAll($criteria_1);
		
		 
		$this->render('search',array('model'=>$model,
		'list'=>$posts,
		'posts'=>$posts_1,
		'pages' => $pages,
		'item_count'=>$total,
		'page_size'=>10,)) ;
		
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='students-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionInactive()
	{
		$model = Students::model()->findByAttributes(array('id'=>$_REQUEST['sid']));
		$model->saveAttributes(array('is_active'=>'0'));
		if($model->uid and $model->uid!=NULL and $model->uid!=0)
		{
			$user = User::model()->findByPk($model->uid); // Making student user inactive
			$user->saveAttributes(array('status'=>'0'));
		}
		
		$guardian = Guardians::model()->findByAttributes(array('ward_id'=>$_REQUEST['sid']));
		if($guardian->uid and $guardian->uid!=NULL and $guardian->uid!=0){
			$parent_user = User::model()->findByPk($guardian->uid); // Making parent user inactive
			$parent_user->saveAttributes(array('status'=>'0'));
		}
		
		
		
		$this->redirect(array('/courses/batches/batchstudents','id'=>$_REQUEST['id']));
	}
	
	public function actionActive()
	{
		$model = Students::model()->findByAttributes(array('id'=>$_REQUEST['sid']));
		$model->saveAttributes(array('is_active'=>'1'));
		if($model->uid and $model->uid!=NULL and $model->uid!=0)
		{
			$user = User::model()->findByPk($model->uid); // Making student user active
			$user->saveAttributes(array('status'=>'1'));
		}
		
		$guardian = Guardians::model()->findByAttributes(array('ward_id'=>$_REQUEST['sid']));
		if($guardian->uid and $guardian->uid!=NULL and $guardian->uid!=0)
		{
			$parent_user = User::model()->findByPk($guardian->uid); // Making parent user active
			$parent_user->saveAttributes(array('status'=>'1'));
		}
		$this->redirect(array('/courses/batches/batchstudents','id'=>$_REQUEST['id']));
	}
	
	public function actionDeletes()
	{
		$model = Students::model()->findByAttributes(array('id'=>$_REQUEST['sid']));
		$model->saveAttributes(array('is_deleted'=>'1'));
		if($model->uid and $model->uid!=NULL and $model->uid!=0) // Deleting student user
		{
			$user = User::model()->findByPk($model->uid);
			if($user)
			{
			$profile = Profile::model()->findByPk($user->id);
			if($profile)
			$profile->delete();
			$user->delete();
			}
		}
		
		$guardian = Guardians::model()->findByAttributes(array('ward_id'=>$_REQUEST['sid']));
		if($guardian->uid and $guardian->uid!=NULL and $guardian->uid!=0) //Deleting parent user
		{
			$parent_user = User::model()->findByPk($guardian->uid);
			if($parent_user)
			{
			$profile = Profile::model()->findByPk($parent_user->id);
			if($profile)
			$profile->delete();
			$parent_user->delete();
			}
		}
		$examscores = ExamScores::model()->DeleteAllByAttributes(array('student_id'=>$_REQUEST['sid']));
		$this->redirect(array('/courses/batches/batchstudents','id'=>$_REQUEST['id']));
	}
}
