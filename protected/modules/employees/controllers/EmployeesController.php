<?php

class EmployeesController extends RController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','Create2','update2','Manage','savesearch','DisplaySavedImage','pdf','Address','Contact','Addinfo','Remove'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','batch','add'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionView2($id)
	{
		$this->render('view2',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionView3($id)
	{
		$this->render('view3',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionView4($id)
	{
		$this->render('view4',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionView5($id)
	{
		$this->render('view5',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionView6($id)
	{
		$this->render('view6');
	}
	
	public function actionIncident($id)
	{
		$this->render('incident');
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Employees;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Employees']))
		{
			
			$model->attributes=$_POST['Employees'];
			$list = $_POST['Employees'];
			
			/*
			* Checking whether date of birth is null
			*/
			if(!$list['birth_date']){
				$dob="";
			}
			else{
				$dob=date('Y-m-d',strtotime($list['birth_date']));
			}
			
				 $joindate = date('Y-m-d',strtotime($list['joining_date']));
				 $model->joining_date= $joindate;
				 $model->association_employee_id= $list['association_employee_id'];
				 $model->trucker_id= $list['trucker_id'];
				 $model->lastname= $list['lastname'];
				 $model->firstname= $list['firstname'];
				 $model->middlename= $list['middlename'];
				 $model->nickname= $list['nickname'];
				 $model->prefix= $list['prefix'];
				 $model->name_ext= $list['name_ext'];
				 $model->sss_id= $list['sss_id'];
				 $model->philhealth_id= $list['philhealth_id'];
				 $model->tin_id= $list['tin_id'];
				 $model->hmdf= $list['hmdf'];
				 $model->tax_status= $list['tax_status'];

				 $model->atm= $list['atm'];
				 $model->height= $list['height'];
				 $model->weight= $list['weight'];
				 $bday = date('Y-m-d',strtotime($list['birth_date']));
				 $model->birth_date= $bday;
				 $model->birth_place= $list['birth_place'];
				 $model->nationality= $list['nationality'];
				 $model->religion= $list['religion'];
				 $model->gender= $list['gender'];
				 $model->blood_type= $list['blood_type'];
				 $model->marital_status= $list['marital_status'];

				 $model->father_name=$list['father_name'];
				 $model->father_occupation=$list['father_occupation'];
				 $model->father_address=$list['father_address'];
				 $model->father_city=$list['father_city'];
				 $model->father_state=$list['father_state'];
				 $model->father_contact=$list['father_contact'];
				 
				 $model->mother_name=$list['mother_name'];
				 $model->mother_occupation=$list['mother_occupation'];
				 $model->mother_address=$list['mother_address'];
				 $model->mother_city=$list['mother_city'];
				 $model->mother_state=$list['mother_state'];
				 $model->mother_contact=$list['mother_contact'];
				 
				 $model->sibling =$list['sibling'];
				 
				 $model->spouse_name=$list['spouse_name'];
				 $model->spouse_occupation=$list['spouse_occupation'];
				 $model->spouse_address=$list['spouse_address'];
				 $model->spouse_city=$list['spouse_city'];
				 $model->spouse_state=$list['spouse_state'];
				 $model->spouse_contact=$list['spouse_contact'];
				 $model->spouse_birth_date=$list['spouse_birth_date'];

				if($file=CUploadedFile::getInstance($model,'photo_data'))
				{
					$model->photo_file_name=$file->name;
					$model->photo_content_type=$file->type;
					$model->photo_file_size=$file->size;
					$upload_directory = 'attachments/employees/photos/';
					$temp = explode(".", $file->name);
					$extension = end($temp);
					$model->photo_src=$upload_directory.time().'.'.$extension;
					move_uploaded_file($file->tempName, $upload_directory . time().'.'.$extension);
				}

				if($model->save())
				{
				$this->redirect(array('create2','id'=>$model->id));
				}
			//}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	public function actionDisplaySavedImage()
		{
			$model=$this->loadModel($_GET['id']);
			header('Pragma: public');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Content-Transfer-Encoding: binary');
			header('Content-length: '.$model->photo_file_size);
			header('Content-Type: '.$model->photo_content_type);
			header('Content-Disposition: attachment; filename='.$model->photo_file_name);
			echo $model->photo_data;
		}
	public function actionRemove()
	{
		$model = Employees::model()->findByAttributes(array('id'=>$_REQUEST['id']));
		$model->saveAttributes(array('photo_file_name'=>'','photo_data'=>''));
		$this->redirect(array('update','id'=>$_REQUEST['id']));
	}
	public function actionCreate2($id)
	{
		#$model=new Employees;
		$model=$this->loadModel($id);
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Employees']))
		{
			$model->attributes=$_POST['Employees'];
			$list = $_POST['Employees'];
			$model->e_name= $list['e_name'];	
			$model->e_rel= $list['e_rel'];	
			$model->e_add= $list['e_add'];	
			$model->e_contact= $list['e_contact'];	
			$model->e_city= $list['e_city'];	
			$model->e_state= $list['e_state'];	
			$model->present_address= $list['present_address'];	
			$model->present_city= $list['present_city'];	
			$model->present_state= $list['present_state'];	
			$model->present_zipcode= $list['present_zipcode'];	
			$model->per_address= $list['per_address'];	
			$model->per_city= $list['per_city'];	
			$model->per_state= $list['per_state'];	
			$model->per_zipcode= $list['per_zipcode'];	
			$model->mobile= $list['mobile'];	
			$model->other_email= $list['other_email'];
			if($model->save())
				{
				$this->redirect(array('create3','id'=>$model->id));
				}
		}
		$this->render('create2',array(
			'model'=>$model,
		));
	}
	public function actionCreate3($id)
	{
		#$model=new Employees;
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Employees']))
		{
			$model->attributes=$_POST['Employees'];
			$list = $_POST['Employees'];
			$model->elem_school= $list['elem_school'];
			$model->elem_indate= $list['elem_indate'];
			$model->elem_exdate= $list['elem_exdate'];
			$model->hs_school= $list['hs_school'];
			$model->hs_indate= $list['hs_indate'];
			$model->hs_exdate= $list['hs_exdate'];

			if($model->save())
				{
				$this->redirect(array('create4','id'=>$model->id));
				}
		}
		$this->render('create3',array(
			'model'=>$model,
		));
	}
	public function actionCreate4($id)
	{
		#$model=new Employees;	
		$model=$this->loadModel($id);
		$assocxp = new EmpAssocxp;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Employees']))
		{
			$model->attributes=$_POST['Employees'];
			$list = $_POST['Employees'];
			$model->super_sub= $list['super_sub'];
			$model->method= $list['method'];	
			
			$model->curemp =$list['curemp'];
			$model->cur_err_truckerid =$list['cur_err_truckerid'];
			$model->cur_err_trucker =$list['cur_err_trucker'];
			$model->cur_err_indate =$list['cur_err_indate'];
			$model->cur_err_exdate =$list['cur_err_exdate'];
			$model->cur_err_reason =$list['cur_err_reason'];
			$model->cur_err_notes =$list['cur_err_notes'];
			
			$assocxp->assoc_truckerid=$list['cur_err_truckerid'];
			$assocxp->emp_id=$model->association_employee_id;
			$assocxp->inc_date=$list['cur_err_indate'];
			$assocxp->exc_date=$list['cur_err_exdate'];
			$assocxp->reason_forleaving=$list['cur_err_reason'];
			$assocxp->emp_number=$list['cur_err_trucker'];
			$assocxp->employeetable_id=$id;
			$assocxp->current=$list['curemp'];
		
			$assocxp->save();
			if($model->save())
				{
				#$this->redirect(array('create4','id'=>$model->id));
				$this->redirect(array('manage'));
				}
		}
		$this->render('create4',array(
			'model'=>$model,
		));
	}
	public function actionAddress()
	{
		$model = new Employees;
		/*$this->render('address',array(
			'model'=>$model,
		));*/
		$this->render('address',array(
			'model'=>$this->loadModel($_REQUEST['id']),
		));
	}
		public function actionContact()
	{
		$model = new Employees;
		/*$this->render('address',array(
			'model'=>$model,
		));*/
		$this->render('contact',array(
			'model'=>$this->loadModel($_REQUEST['id']),
		));
	}
		public function actionAddinfo()
	{
		$model = new Employees;
		/*$this->render('address',array(
			'model'=>$model,
		));*/
		$this->render('addinfo',array(
			'model'=>$this->loadModel($_REQUEST['id']),
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Employees']))
		{
			$model->attributes=$_POST['Employees'];
			$list = $_POST['Employees'];
			
			if(!$list['birth_date']){
				$dob="";
			}
			else{
				$dob=date('Y-m-d',strtotime($list['birth_date']));
			}	
				 $joindate = date('Y-m-d',strtotime($list['joining_date']));
				 $model->joining_date= $joindate;
				 $model->association_employee_id= $list['association_employee_id'];
				 $model->trucker_id= $list['trucker_id'];
				 $model->lastname= $list['lastname'];
				 $model->firstname= $list['firstname'];
				 $model->middlename= $list['middlename'];
				 $model->nickname= $list['nickname'];
				 $model->prefix= $list['prefix'];
				 $model->name_ext= $list['name_ext'];
				 $model->sss_id= $list['sss_id'];
				 $model->philhealth_id= $list['philhealth_id'];
				 $model->tin_id= $list['tin_id'];
				 $model->hmdf= $list['hmdf'];
				 $model->tax_status= $list['tax_status'];

				 $model->atm= $list['atm'];
				 $model->height= $list['height'];
				 $model->weight= $list['weight'];
				 $bday = date('Y-m-d',strtotime($list['birth_date']));
				 $model->birth_date= $bday;
				 $model->birth_place= $list['birth_place'];
				 $model->nationality= $list['nationality'];
				 $model->religion= $list['religion'];
				 $model->gender= $list['gender'];
				 $model->blood_type= $list['blood_type'];
				 $model->marital_status= $list['marital_status'];

				 $model->father_name=$list['father_name'];
				 $model->father_occupation=$list['father_occupation'];
				 $model->father_address=$list['father_address'];
				 $model->father_city=$list['father_city'];
				 $model->father_state=$list['father_state'];
				 $model->father_contact=$list['father_contact'];
				 
				 $model->mother_name=$list['mother_name'];
				 $model->mother_occupation=$list['mother_occupation'];
				 $model->mother_address=$list['mother_address'];
				 $model->mother_city=$list['mother_city'];
				 $model->mother_state=$list['mother_state'];
				 $model->mother_contact=$list['mother_contact'];
				 
				 $model->sibling =$list['sibling'];
				 
				 $model->spouse_name=$list['spouse_name'];
				 $model->spouse_occupation=$list['spouse_occupation'];
				 $model->spouse_address=$list['spouse_address'];
				 $model->spouse_city=$list['spouse_city'];
				 $model->spouse_state=$list['spouse_state'];
				 $model->spouse_contact=$list['spouse_contact'];
				 $model->spouse_birth_date=$list['spouse_birth_date'];
				 
				$model->photo_file_name=$file->name;
				$model->photo_content_type=$file->type;
				$model->photo_file_size=$file->size;
				 
				 /*
				if($file=CUploadedFile::getInstance($model,'photo_data'))
					 {
					$model->photo_file_name=$file->name;
					$model->photo_content_type=$file->type;
					$model->photo_file_size=$file->size;
					$model->photo_data=file_get_contents($file->tempName);
					}*/
				if($file=CUploadedFile::getInstance($model,'photo_data'))
				{
					$model->photo_file_name=$file->name;
					$model->photo_content_type=$file->type;
					$model->photo_file_size=$file->size;
					$upload_directory = 'attachments/employees/photos/';
					$temp = explode(".", $file->name);
					$extension = end($temp);
					$model->photo_src=$upload_directory.time().'.'.$extension;
					move_uploaded_file($file->tempName, $upload_directory . time().'.'.$extension);
				}

				if($model->save())
				{
				$this->redirect(array('update2','id'=>$model->id));
				}
		}
		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	public function actionUpdate2($id)
	{
		#$model=new Employees;
		$model=$this->loadModel($id);
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Employees']))
		{
			$model->attributes=$_POST['Employees'];
			$list = $_POST['Employees'];
			$model->e_name= $list['e_name'];	
			$model->e_rel= $list['e_rel'];	
			$model->e_add= $list['e_add'];	
			$model->e_contact= $list['e_contact'];	
			$model->e_city= $list['e_city'];	
			$model->e_state= $list['e_state'];	
			$model->present_address= $list['present_address'];	
			$model->present_city= $list['present_city'];	
			$model->present_state= $list['present_state'];	
			$model->present_zipcode= $list['present_zipcode'];	
			$model->per_address= $list['per_address'];	
			$model->per_city= $list['per_city'];	
			$model->per_state= $list['per_state'];	
			$model->per_zipcode= $list['per_zipcode'];	
			$model->mobile= $list['mobile'];	
			$model->other_email= $list['other_email'];
			if($model->save())
				{
				$this->redirect(array('update3','id'=>$model->id));
				}
		}
		$this->render('update2',array(
			'model'=>$model,
		));
	}
	public function actionUpdate3($id)
	{
		#$model=new Employees;
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Employees']))
		{
			$model->attributes=$_POST['Employees'];
			$list = $_POST['Employees'];
			
			$model->elem_school= $list['elem_school'];
			$elemindate = date('Y-m-d',strtotime($list['elem_indate']));
			$model->elem_indate= $elemindate;
			$elemexdate = date('Y-m-d',strtotime($list['elem_exdate']));
			$model->elem_exdate= $elemexdate;
			
			$model->hs_school= $list['hs_school'];
			$hsindate = date('Y-m-d',strtotime($list['hs_indate']));
			$model->hs_indate= $hsindate;
			$hsexdate = date('Y-m-d',strtotime($list['hs_exdate']));
			$model->hs_exdate= $hsexdate;
			
			$model->col_school= $list['col_school'];
			$model->col_course= $list['col_course'];
			$colindate =date('Y-m-d',strtotime($list['col_indate']));
			$model->col_indate= $colindate;
			$colexdate = date('Y-m-d',strtotime($list['col_exdate']));
			$model->col_exdate= $colexdate;
			
			$model->o_school= $list['o_school'];
			$model->o_course= $list['o_course'];
			$oindate = date('Y-m-d',strtotime($list['o_indate']));
			$model->o_indate= $oindate;
			$oexdate = date('Y-m-d',strtotime($list['o_exdate']));
			$model->o_exdate= $oexdate;
			
			if($model->save())
				{
				$this->redirect(array('update4','id'=>$model->id));
				}
		}
		$this->render('update3',array(
			'model'=>$model,
		));
	}
	public function actionUpdate4($id)
	{
		#$model=new Employees;	
		$model=$this->loadModel($id);
		$assocxp=new EmpAssocxp;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Employees']))
		{
			$model->attributes=$_POST['Employees'];
			$list = $_POST['Employees'];
			$model->super_sub= $list['super_sub'];
			$model->method= $list['method'];	
			$model->dept= $list['dept'];	
			$model->post= $list['post'];	
			$model->start_emp= $list['start_emp'];	
			$model->end_emp= $list['end_emp'];	
			$model->active= $list['active'];	
			$model->emp_stat= $list['emp_stat'];	
			$model->pay_period= $list['pay_period'];	
			$model->salary= $list['salary'];	
			$model-> pay_type= $list['pay_type'];	
			$model->basic_pay= $list['basic_pay'];
			$model->e_cola= $list['e_cola'];	
			$model->min_wage= $list['min_wage'];	
			$model-> region= $list['region'];
			
			if($model->save())
				{
				#$this->redirect(array('create4','id'=>$model->id));
				$this->redirect(array('manage'));
				}
		}
		$this->render('update4',array(
			'model'=>$model,
		));
	}
	
	public function actionPdf()
    {
		$employee = Employees::model()->findByAttributes(array('id'=>$_REQUEST['id']));
		$employee = $employee->firstname.' '.$employee->lastname.' Profile.pdf';
		$html2pdf = Yii::app()->ePdf->HTML2PDF();
		$html2pdf->WriteHTML($this->renderPartial('print', array('model'=>$this->loadModel($_REQUEST['id'])), true));
        $html2pdf->Output($employee);
 
        ////////////////////////////////////////////////////////////////////////////////////
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('is_deleted',0);
		$total = Employees::model()->count($criteria);
		$criteria->order = 'id DESC';
		$criteria->limit = '10';
		$posts = Employees::model()->findAll($criteria);
		
		
		$this->render('managemployees',array(
			'total'=>$total,'list'=>$posts
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Employees('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Employees']))
			$model->attributes=$_GET['Employees'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Employees::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='employees-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	
	/**
	 * Performs the Advance search.
	 * By Rajith
	 */
	public function actionAddlicense()
	 {
	 $this->render('admin');
	 }
	public function actionManage()
	 {
		 
		$model=new Employees;
		$criteria = new CDbCriteria;
		$criteria->compare('is_deleted',0);
		
		if(isset($_REQUEST['val']))
		{
		 $criteria->condition='(lastname LIKE :match)';
		 $criteria->params = array(':match' => $_REQUEST['val'].'%');
		}
		if(isset($_REQUEST['name']) and $_REQUEST['name']!=NULL)
		{
			
	   if((substr_count( $_REQUEST['name'],' '))==0)
		 { 	
		 $criteria->condition=$criteria->condition.' and '.'(firstname LIKE :name or lastname LIKE :name)';
		 $criteria->params[':name'] = $_REQUEST['name'].'%';
		}
		else if((substr_count( $_REQUEST['name'],' '))<=1)
		{
		 $name=explode(" ",$_REQUEST['name']);
		 $criteria->condition=$criteria->condition.' and '.'(first_name LIKE :name or last_name LIKE :name or middle_name LIKE :name)';
		 $criteria->params[':name'] = $name[0].'%';
		 $criteria->condition=$criteria->condition.' and '.'(first_name LIKE :name1 or last_name LIKE :name1 or middle_name LIKE :name1)';
		 $criteria->params[':name1'] = $name[1].'%';
		 	
		}
		}
		
		if(isset($_REQUEST['employeenumber']) and $_REQUEST['employeenumber']!=NULL)
		{
		 $criteria->condition=$criteria->condition.' and '.'employee_number LIKE :employeenumber';
		 $criteria->params[':employeenumber'] = $_REQUEST['employeenumber'].'%';
		}
		
		if(isset($_REQUEST['Employees']['dept']) and $_REQUEST['Employees']['dept']!=NULL)
		{
			$model->dept = $_REQUEST['Employees']['dept'];
			$criteria->condition=$criteria->condition.' and '.'dept = :dept';
		    $criteria->params[':dept'] = $_REQUEST['Employees']['dept'];
		}
		
		if(isset($_REQUEST['Employees']['employee_category_id']) and $_REQUEST['Employees']['employee_category_id']!=NULL)
		{
			$model->employee_category_id = $_REQUEST['Employees']['employee_category_id'];
			$criteria->condition=$criteria->condition.' and '.'employee_category_id = :employee_category_id';
		    $criteria->params[':employee_category_id'] = $_REQUEST['Employees']['employee_category_id'];
		}
		
		if(isset($_REQUEST['Employees']['post']) and $_REQUEST['Employees']['post']!=NULL)
		{
			$model->post = $_REQUEST['Employees']['post'];
			$criteria->condition=$criteria->condition.' and '.'post = :post';
		    $criteria->params[':post'] = $_REQUEST['Employees']['post'];
		}
		
		
		if(isset($_REQUEST['Employees']['employee_grade_id']) and $_REQUEST['Employees']['employee_grade_id']!=NULL)
		{
			$model->employee_grade_id = $_REQUEST['Employees']['employee_grade_id'];
			$criteria->condition=$criteria->condition.' and '.'employee_grade_id = :employee_grade_id';
		    $criteria->params[':employee_grade_id'] = $_REQUEST['Employees']['employee_grade_id'];
		}
		
		
		if(isset($_REQUEST['Employees']['gender']) and $_REQUEST['Employees']['gender']!=NULL)
		{
			$model->gender = $_REQUEST['Employees']['gender'];
			$criteria->condition=$criteria->condition.' and '.'gender = :gender';
		    $criteria->params[':gender'] = $_REQUEST['Employees']['gender'];
		}
		
		if(isset($_REQUEST['Employees']['emp_stat']) and $_REQUEST['Employees']['emp_stat']!=NULL)
		{
			$model->emp_stat = $_REQUEST['Employees']['emp_stat'];
			$criteria->condition=$criteria->condition.' and '.'emp_stat = :emp_stat';
		    $criteria->params[':emp_stat'] = $_REQUEST['Employees']['emp_stat'];
		}
		
		if(isset($_REQUEST['Employees']['blood_group']) and $_REQUEST['Employees']['blood_group']!=NULL)
		{
			$model->blood_group = $_REQUEST['Employees']['blood_group'];
			$criteria->condition=$criteria->condition.' and '.'blood_group = :blood_group';
		    $criteria->params[':blood_group'] = $_REQUEST['Employees']['blood_group'];
		}
		
		if(isset($_REQUEST['Employees']['nationality_id']) and $_REQUEST['Employees']['nationality_id']!=NULL)
		{
			$model->nationality_id = $_REQUEST['Employees']['nationality_id'];
			$criteria->condition=$criteria->condition.' and '.'nationality_id = :nationality_id';
		    $criteria->params[':nationality_id'] = $_REQUEST['Employees']['nationality_id'];
		}
		
		
		if(isset($_REQUEST['Employees']['dobrange']) and $_REQUEST['Employees']['dobrange']!=NULL)
		{
			  
			  $model->dobrange = $_REQUEST['Employees']['dobrange'] ;
			  if(isset($_REQUEST['Employees']['date_of_birth']) and $_REQUEST['Employees']['date_of_birth']!=NULL)
			  {
				  if($_REQUEST['Employees']['dobrange']=='2')
				  {  
					  $model->date_of_birth = $_REQUEST['Employees']['date_of_birth'];
					  $criteria->condition=$criteria->condition.' and '.'date_of_birth = :date_of_birth';
					  $criteria->params[':date_of_birth'] = date('Y-m-d',strtotime($_REQUEST['Employees']['date_of_birth']));
				  }
				  if($_REQUEST['Employees']['dobrange']=='1')
				  {  
				  
					  $model->date_of_birth = $_REQUEST['Employees']['date_of_birth'];
					  $criteria->condition=$criteria->condition.' and '.'date_of_birth < :date_of_birth';
					  $criteria->params[':date_of_birth'] = date('Y-m-d',strtotime($_REQUEST['Employees']['date_of_birth']));
				  }
				  if($_REQUEST['Employees']['dobrange']=='3')
				  {  
					  $model->date_of_birth = $_REQUEST['Employees']['date_of_birth'];
					  $criteria->condition=$criteria->condition.' and '.'date_of_birth > :date_of_birth';
					  $criteria->params[':date_of_birth'] = date('Y-m-d',strtotime($_REQUEST['Employees']['date_of_birth']));
				  }
				  
			  }
		}
		elseif(isset($_REQUEST['Employees']['dobrange']) and $_REQUEST['Employees']['dobrange']==NULL)
		{
			  if(isset($_REQUEST['Employees']['date_of_birth']) and $_REQUEST['Employees']['date_of_birth']!=NULL)
			  {
				  $model->date_of_birth = $_REQUEST['Employees']['date_of_birth'];
				  $criteria->condition=$criteria->condition.' and '.'date_of_birth = :date_of_birth';
				  $criteria->params[':date_of_birth'] = date('Y-m-d',strtotime($_REQUEST['Employees']['date_of_birth']));
			  }
		}
		
		
		if(isset($_REQUEST['Employees']['joinrange']) and $_REQUEST['Employees']['joinrange']!=NULL)
		{
			  
			  $model->joinrange = $_REQUEST['Employees']['joinrange'] ;
			  if(isset($_REQUEST['Employees']['joining_date']) and $_REQUEST['Employees']['joining_date']!=NULL)
			  {
				  if($_REQUEST['Employees']['joinrange']=='2')
				  {  
					  $model->joining_date = $_REQUEST['Employees']['joining_date'];
					  $criteria->condition=$criteria->condition.' and '.'joining_date = :joining_date';
					  $criteria->params[':joining_date'] = date('Y-m-d',strtotime($_REQUEST['Employees']['joining_date']));
				  }
				  if($_REQUEST['Employees']['joinrange']=='1')
				  {  
				  
					  $model->joining_date = $_REQUEST['Employees']['joining_date'];
					  $criteria->condition=$criteria->condition.' and '.'joining_date < :joining_date';
					  $criteria->params[':joining_date'] = date('Y-m-d',strtotime($_REQUEST['Employees']['joining_date']));
				  }
				  if($_REQUEST['Employees']['joinrange']=='3')
				  {  
					  $model->joining_date = $_REQUEST['Employees']['joining_date'];
					  $criteria->condition=$criteria->condition.' and '.'joining_date > :joining_date';
					  $criteria->params[':joining_date'] = date('Y-m-d',strtotime($_REQUEST['Employees']['joining_date']));
				  }
				  
			  }
		}
		elseif(isset($_REQUEST['Employees']['joinrange']) and $_REQUEST['Employees']['joinrange']==NULL)
		{
			  if(isset($_REQUEST['Employees']['joining_date']) and $_REQUEST['Employees']['joining_date']!=NULL)
			  {
				  $model->joining_date = $_REQUEST['Employees']['joining_date'];
				  $criteria->condition=$criteria->condition.' and '.'joining_date = :joining_date';
				  $criteria->params[':joining_date'] = date('Y-m-d',strtotime($_REQUEST['Employees']['joining_date']));
			  }
		}
		
		if(isset($_REQUEST['Employees']['status']) and $_REQUEST['Employees']['status']!=NULL)
		{
			$model->status = $_REQUEST['Employees']['status'];
			$criteria->condition=$criteria->condition.' and '.'is_active = :status';
		    $criteria->params[':status'] = $_REQUEST['Employees']['status'];
		}
		
		$criteria->order = 'id ASC';
		
		$total = Employees::model()->count($criteria);
		$pages = new CPagination($total);
        $pages->setPageSize(Yii::app()->params['listPerPage']);
        $pages->applyLimit($criteria);  // the trick is here!
		$posts = Employees::model()->findAll($criteria);
		 
		$this->render('manage',array('model'=>$model,
		'list'=>$posts,
		'pages' => $pages,
		'item_count'=>$total,
		'page_size'=>Yii::app()->params['listPerPage'],)) ;
	 }
	 
	 public function actionDeletes()
	{
		
		$model = Employees::model()->findByAttributes(array('id'=>$_REQUEST['id']));
		$model->saveAttributes(array('is_deleted'=>'1'));
		echo $val;
		
	}
		public function actionAddnew() {
        /*$this->performAjaxValidation($model);
        $flag=true;
	   	if(isset($_POST['Submit']))
        {       $flag=false;

            $model->attributes=$_POST['Batches'];
			$model->start_date=date('Y-m-d', strtotime($model->start_date)); 
			$model->end_date=date('Y-m-d', strtotime($model->end_date)); 
            $model->save();
              
		}
		if($flag) {
			Yii::app()->clientScript->scriptMap['jquery.js'] = false;
			$this->renderPartial('create',array('model'=>$model,'val1'=>$_GET['val1']),false,true);
		}*/
			Yii::app()->clientScript->scriptMap['jquery.js'] = false;
			$this->renderPartial('create',array('model'=>$model));
		
   }
   
   public function actionAttachment($id)
	{
                $model= new Students;
                $number_of_file_fields = 0;
                $number_of_uploaded_files = 0;
                $uploaded_files = array();
                $upload_directory = 'attachments/students/';
                if(isset($_POST["submit"])) {
                        for($i = 0; $i < count($_FILES['file']['name']); $i++){
                                $number_of_file_fields++;
                                if($_FILES["file"]["size"][$i] < 8000000 ){
                                        if ($_FILES['file']['name'][$i] != '') {
                                                $number_of_uploaded_files++;
                                                $uploaded_files[] = $_FILES['file']['name'][$i];
                                                $temp = explode(".", $_FILES["file"]["name"][$i]);
                                                $extension = end($temp);
                                                $filename = trim($_FILES['file']['name'][$i],'.'.$extension);
                                                $count = 0;
                                                $f_name = $_FILES['file']['name'][$i];
                                                $f_type = $extension;
                                                $f_size = $_FILES['file']['size'][$i];

                                                while(file_exists($upload_directory . $_FILES['file']['name'][$i])) {
                                                $count += 1;
                                                $_FILES['file']['name'][$i] = $filename.'_copy('.$count.').'.$extension;
                                                $f_name = $_FILES['file']['name'][$i];
                                                $f_type = $extension;
                                                $f_size = $_FILES['file']['size'][$i];
                                                }

                                                try{
                                                $insertAttachments = Yii::app()->db->createCommand('insert into attachments_employees values(null,"'.$_REQUEST['id'].'","'.$f_name.'","'.$f_type.'","'.$upload_directory.$f_name.'","'.date('Y-m-d').'","0")')->queryAll();
                                                }catch(Exception $e){}
                                                if (move_uploaded_file($_FILES['file']['tmp_name'][$i], $upload_directory . $_FILES['file']['name'][$i])) {
                                                        echo $number_of_moved_files++;
                                                }
                                        }
                                }
                        }
                }############

		$this->render('attachment',array(
			'model'=>$this->loadModel($id),
		));
	}
}
