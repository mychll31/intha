<?php
$this->breadcrumbs=array(
	'Employees'=>array('index'),
	'View',
);
$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
?>
<style>.emp_cntntbx{min-height:0px;}</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
 <?php $this->renderPartial('profileleft');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
	<?php  if($role->role==1){?>
<div class="edit_bttns">
    <ul>
    <li><?php echo CHtml::link(Yii::t('employees','<span>Edit</span>'), array('update', 'id'=>$_REQUEST['id']),array('class'=>'edit last')); ?><!--<a class=" edit last" href="">Edit</a>--></li>
     <li><?php echo CHtml::link(Yii::t('employees','<span>Employees</span>'), array('employees/manage'),array('class'=>'edit last')); ?><!--<a class=" edit last" href="">Edit</a>--></li>
    </ul>
    </div>
    <?php }?>
	
    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
    <br>
     <?php
			$this->renderPartial('tab');
	 ?>
    <div class="clear"></div>
 <div class="emp_cntntbx">
    <div class="table_listbx">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="listbxtop_hdng">
    <td><?php echo Yii::t('employees','Personal Details');?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table>
  <table width="100%">
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Employee ID :</span><span class="data"><?php echo $model->association_employee_id;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Join Date :</span><span class="data">
						<?php $settings=UserSettings::model()->findByAttributes(array('user_id'=>Yii ::app()->user->id));
										if($settings!=NULL)
										{	
											$date1=date($settings->displaydate,strtotime($model->joining_date));
											echo $date1;
		
										}
										else
										echo $model->joining_date;  ?></span></td>
		<td class="subhdng_nrmal"></td>
	</tr>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Nick name :</span><span class="data"><?php echo $model->nickname;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Nationality :</span><span class="data"><?php echo $model->nationality;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead"></span><span class="data"></span></td>
	</tr>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">First name :</span><span class="data"><?php echo $model->firstname;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Middle name :</span><span class="data"><?php echo $model->middlename;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Last name :</span><span class="data"><?php echo $model->lastname;?></span></td>
	</tr>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Birthday :</span><span class="data"><?php $settings=UserSettings ::model()->findByAttributes(array('user_id'=>Yii ::app()->user->id));
										if($settings!=NULL)
										{	
											$date2=date($settings->displaydate,strtotime($model->birth_date));
											echo $date2;
		
										}
										else
										echo $model->birth_date;  ?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Birthplace :</span><span class="data"><?php echo $model->birth_place;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead"></span><span class="data"></span></td>
	</tr>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Gender :</span><span class="data"><?php if($model->gender=='M') echo 'Male'; else echo 'Female'; ?></span></td></span></td>
		<?php $emp_stat = EmpPrefix ::model()->findByAttributes(array('id'=>$model->marital_status)); ?>
		<td class="subhdng_nrmal"><span class="datahead">Marital Status :</span><span class="data"><?php if($emp_stat!=NULL){echo $emp_stat->name; }?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Blood Type :</span><span class="data"><?php echo $model->blood_type;?></span></td>
	</tr>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Height :</span><span class="data"><?php if($model->height != null) {echo $model->height;}else{ echo "-" ;}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Weight :</span><span class="data"><?php if($model->weight != null) {echo $model->weight;}else{ echo "-" ;}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead"></span><span class="data"></span></td>
	</tr>
	<tr><td colspan=6>&nbsp;</span></td></tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="listbxtop_hdng">
    <td><?php echo Yii ::t('employees','Family Background');?></span></td>
    <td colspan=5>&nbsp;</span></td>
  </tr>
  </table>
  <table width="100%">
	<tr><td colspan=2><span style="font-size : 12px;font-weight : bold;">Father's Details</span></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Father's Name :</span><span class="data"><?php echo $model->father_name;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Occupation :</span><span class="data"><?php if($model->father_occupation !=null){echo $model->father_occupation;}else{echo "-";}?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">St. / Blk. / Lot :</span><span class="data"><?php if($model->father_address !=null){echo $model->father_address;}else{echo "-";}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">State/Province :</span><span class="data"><?php if($model->father_state !=null){echo $model->father_state;}else{echo "-";}?></span></td>
	</tr><tr>	
		<td class="subhdng_nrmal"><span class="datahead">City :</span><span class="data"><?php if($model->father_city !=null){echo $model->father_city;}else{echo "-";}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Contact Number :</span><span class="data"><?php if($model->father_contact !=null){echo $model->father_contact;}else{echo "-";}?></span></td>	
	</tr>
	<tr><td colspan=2><span style="font-size : 12px;font-weight : bold;">Mother's Details</span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Mother's Name :</span><span class="data"><?php echo $model->mother_name;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Occupation :</span><span class="data"><?php if($model->mother_occupation !=null){echo $model->mother_occupation;}else{echo "-";}?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">St. / Blk. / Lot :</span><span class="data"><?php if($model->mother_address !=null){echo $model->mother_address;}else{echo "-";}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">City :</span><span class="data"><?php if($model->mother_city !=null){echo $model->mother_city;}else{echo "-";}?></span></td>		
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">State/Province :</span><span class="data"><?php if($model->mother_state !=null){echo $model->mother_state;}else{echo "-";}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Contact Number :</span><span class="data"><?php if($model->mother_contact !=null){echo $model->mother_contact;}else{echo "-";}?></span></td>	
	</tr>
	<tr>
		<td colspan=2><span style="font-size : 12px;font-weight : bold;">Siblings</span></td>
	</tr><tr>
		<td colspan=2 class="subhdng_nrmal"><span class="datahead">No. of Siblings :</span><span class="data"><?php if($model->sibling !=null){echo $model->sibling;}else{echo "-";}?></span></td>
	</tr>
	<tr><td colspan=2><span style="font-size : 12px;font-weight : bold;">Spouse's Details</span></span></td></tr>
		<td class="subhdng_nrmal"><span class="datahead">Spouse's Name :</span><span class="data"><?php if($model->spouse_name !=null){echo $model->spouse_name;}else{echo "-";}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Occupation :</span><span class="data"><?php if($model->spouse_occupation !=null){echo $model->spouse_occupation;}else{echo "-";}?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">St. / Blk. / Lot :</span><span class="data"><?php if($model->spouse_address !=null){echo $model->spouse_address;}else{echo "-";}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">City :</span><span class="data"><?php if($model->spouse_city !=null){echo $model->spouse_city;}else{echo "-";}?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">State/Province :</span><span class="data"><?php if($model->spouse_state !=null){echo $model->spouse_state;}else{echo "-";}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Contact Number :</span><span class="data"><?php if($model->spouse_contact !=null){echo $model->spouse_contact;}else{echo "-";}?></span></td>
	</tr>
	<tr><td colspan=2>&nbsp;</span></td></tr>
  </table> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="listbxtop_hdng">
    <td><?php echo Yii ::t('employees','Government and Account Informations');?></span></td>
  </tr>
  </table>
  <table width="100%">
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">SSS No. :</span><span class="data"><?php echo $model->sss_id;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">PhilHealth No. :</span><span class="data"><?php echo $model->philhealth_id;?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">TIN No. :</span><span class="data"><?php echo $model->tin_id;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">HDMF No. :</span><span class="data"><?php echo $model->hmdf;?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Tax Status :</span><span class="data"><?php echo $model->tax_status;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">ATM No. :</span><span class="data"><?php echo $model->atm;?></span></td>
	</tr>
  </table> 
  <div class="ea_pdf" style="top:4px; right:6px;"><?php echo CHtml::link('<img src="images/pdf-but.png">', array('Employees/pdf','id'=>$_REQUEST['id']),array('target'=>'_blank')); ?></div>
   
 </div>
 
 </div>
 
 
 
</div>
</div>
</div>
    
    </td>
  </tr>
</table>
