<?php
$this->breadcrumbs=array(
	'Employees'=>array('index'),
	'View',
);
$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
?>
<style>.emp_cntntbx{min-height:0px;}</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
 <?php $this->renderPartial('profileleft');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
	<?php if($role->role==1){?>
<div class="edit_bttns">
    <ul>
    <li><?php echo CHtml::link(Yii::t('employees','<span>Edit</span>'), array('update2', 'id'=>$_REQUEST['id']),array('class'=>'edit last')); ?></li>
     <li><?php echo CHtml::link(Yii::t('employees','<span>Employees</span>'), array('employees/manage'),array('class'=>'edit last')); ?></li>
    </ul>
    </div>
    <?php }?>
	
    <div class="emp_right_contner">
    <div class="emp_tabwrapper"><br>
<?php
			$this->renderPartial('tab');
	 ?>
    <div class="clear"></div>
 <div class="emp_cntntbx">
    <div class="table_listbx">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="listbxtop_hdng">
    <td><?php echo Yii::t('employees','Address Details');?></td>
    <td>&nbsp;</td>
  </tr>
  </table>
  <table width="100%">
	<tr><td colspan=2><span style="font-size: 12px;font-weight: bold;">Present Address</span></span></td></tr>
  </table>
  <table width="100%">
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Address :</span><span class="data"><?php if($model->present_address !=null){echo $model->present_address;}else{echo "-";}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">City :</span><span class="data"><?php if($model->present_city !=null){echo $model->present_city;}else{echo "-";}?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">State/Province :</span><span class="data"><?php if($model->present_state !=null){echo $model->present_state;}else{echo "-";}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Zip Code :</span><span class="data"><?php if($model->present_zipcode !=null){echo $model->present_zipcode;}else{echo "-";}?></span></td>
	</tr>
	</table>
	<table width="100%">
	<tr><td colspan=2><span style="font-size: 12px;font-weight: bold;">Permanent Address</span></span></td></tr>
  </table>
  <table width="100%">
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Address :</span><span class="data"><?php if($model->per_address !=null){echo $model->per_address;}else{echo "-";}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">City :</span><span class="data"><?php if($model->per_city !=null){echo $model->per_city;}else{echo "-";}?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">State/Province :</span><span class="data"><?php if($model->per_state !=null){echo $model->per_state;}else{echo "-";}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Zip Code :</span><span class="data"><?php if($model->per_zipcode !=null){echo $model->per_zipcode;}else{echo "-";}?></span></td>
	</tr>
	<tr><td colspan=2>&nbsp;</span></td></tr>
	</table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="listbxtop_hdng">
    <td><?php echo Yii::t('employees','Contact Details');?></span></td>
  </tr>
  </table>
  <table width="100%">
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Tel. No. :</span><span class="data"><?php if($model->home_telephone != null){echo $model->home_telephone;}else{echo "-";} ?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Mobile No. :</span><span class="data"><?php echo $model->mobile;?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Email :</span><span class="data"><?php echo $model->other_email;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Work Tel. No. :</span><span class="data"><?php if($model->work_tel != null){echo $model->work_tel;}else{echo "-";}?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Work Email :</span><span class="data"><?php if($model->work_email != null){echo $model->work_email;}else{echo "-";}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead"></span><span class="data"></span></td>
	</tr>
	<tr><td colspan=2>&nbsp;</span></td></tr>
  </table> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="listbxtop_hdng">
    <td><?php echo Yii::t('employees','Person to contact in case of Emergency');?></span></td>
  </tr>
  </table>
  <table width="100%">
	<tr>
		<td class="subhdng_nrmal" ><span class="datahead">Name :</span><span class="data"><?php echo $model->e_name;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Relationship :</span><span class="data"><?php echo $model->e_rel;?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Address :</span><span class="data"><?php echo $model->e_add;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">City :</span><span class="data"><?php echo $model->e_city;?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">State/Province :</span><span class="data"><?php echo $model->e_state;?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Other Contact :</span><span class="data"><?php if($model->e_other_con != null){echo $model->e_other_con;}else{echo "-";}?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Email :</span><span class="data"><?php if($model->e_email != null){echo $model->e_email;}else{echo "-";}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Contact No. :</span><span class="data"><?php echo $model->e_contact;?></span></td>
	</tr>
  </table> 	
  <div class="ea_pdf" style="top:4px; right:6px;"><?php echo CHtml::link('<img src="images/pdf-but.png">', array('Employees/pdf','id'=>$_REQUEST['id']),array('target'=>'_blank')); ?></div>
   
 </div>
 
 </div>
 
 
 
</div>
</div>
</div>
    
    </td>
  </tr>
</table>
