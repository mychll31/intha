<?php
$this->breadcrumbs=array(
	'Employees'=>array('index'),
	'View',
);
$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
?>
<style>.emp_cntntbx{min-height:0px;}</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
 <?php $this->renderPartial('profileleft');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <div class="edit_bttns">
    <ul>
    <li><?php echo CHtml::link(Yii::t('employees','<span>Edit</span>'), array('update3','id'=>$_REQUEST['id']),array('class'=>'edit last')); ?></li>
    <li><?php echo CHtml::link(Yii::t('employees','<span>Employees</span>'), array('manage'),array('class'=>'edit last')); ?></li>
    </ul>
    </div>
	<?php  if($role->role==1){?>
    <?php }?>
	
    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
<br>
<?php
			$this->renderPartial('tab');
	 ?>
    <div class="clear"></div>
 <div class="emp_cntntbx">
    <div class="table_listbx">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="listbxtop_hdng">
    <td><?php echo Yii::t('employees','Educational Attainment');?></td>
    <td>&nbsp;</td>
  </tr>
  </table>
  <table width="100%">
	<tr><td colspan=6><span style="font-size: 12px;font-weight: bold;">Elementary</span></td></tr>
  </table>
  <table width="100%">
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">School Name :</span><span class="data"><?php echo $model->elem_school;?></span></td>
		<td class="subhdng_nrmal"></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Inclusive Date :</span><span class="data"><?php echo date('M. d, Y',strtotime($model->elem_indate));?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Exclusive Date :</span><span class="data"><?php echo date('M. d, Y',strtotime($model->elem_exdate));?></span></td>
	</tr>
	</table>
  <table width="100%">
	<tr><td colspan=6><span style="font-size: 12px;font-weight: bold;">High School</span></td></tr>
  </table>
  <table width="100%">
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">School Name :</span><span class="data"><?php echo $model->hs_school;?></span></td>
		<td class="subhdng_nrmal"></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Inclusive Date :</span><span class="data"><?php echo date('M. d, Y',strtotime($model->hs_indate));?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Exclusive Date :</span><span class="data"><?php echo date('M. d, Y',strtotime($model->hs_exdate));?></span></td>
	</tr>
	</table>
  <table width="100%">
	<tr><td colspan=6><span style="font-size: 12px;font-weight: bold;">College</span></td></tr>
  </table>
  <table width="100%">
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">School Name :</span><span class="data"><?php if($model->col_school == null){echo "-";}else{echo $model->col_school;}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Course :</span><span class="data"><?php if($model->col_course == null){echo "-";}else{echo $model->col_course;}?></span></td>
	</tr>
	<tr>	
		<td class="subhdng_nrmal"><span class="datahead">Inclusive Date :</span><span class="data"><?php if($model->col_indate == '0000-00-00' or $model->col_indate == null){echo "-";}else{echo date('M. d, Y',strtotime($model->col_indate));}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Exclusive Date :</span><span class="data"><?php if($model->col_exdate == '0000-00-00' or $model->col_exdate == null){echo "-";}else{echo date('M. d, Y',strtotime($model->col_exdate));}?></span></td>
	</tr>
	</table>
  <table width="100%">
	<tr><td colspan=6><span style="font-size: 12px;font-weight: bold;">Other School</span></td></tr>
  </table>
  <table width="100%">
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">School Name :</span><span class="data"><?php if($model->o_school == null){echo "-";}else{echo $model->o_school;}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Course :</span><span class="data"><?php if($model->o_course == null){echo "-";}else{echo $model->o_course;}?></span></td>
	</tr><tr>
		<td class="subhdng_nrmal"><span class="datahead">Inclusive Date :</span><span class="data"><?php if($model->o_indate == '0000-00-00' or $model->o_indate == null){echo "-";}else{echo date('M. d, Y',strtotime($model->o_indate));}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Exclusive Date :</span><span class="data"><?php if($model->o_exdate == '0000-00-00' or $model->o_exdate == null){echo "-";}else{echo date('M. d, Y',strtotime($model->o_exdate));}?></span></td>
	</tr>
	</table>
 </div>
 
 </div>
 
 
 
</div>
</div>
</div>
    
    </td>
  </tr>
</table>
