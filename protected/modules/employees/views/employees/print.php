NORTH HARBOR TRUCKERS ASSOCIATION
<br><br>
<style>
  .thhead{
    font-size: 12px;
    color: #1A7701;
    font-weight: bold;
    text-transform: uppercase;
    padding:10px;
    border-top:1px solid #EAEEF1;
    background: #EAEEF1;
  }
  td{
    border-bottom:1px solid #EAEEF1;
    padding: 10px;
    width: 200px;
  }
  .title{
    color: #6E8171;     
    font-size: 12px !important;
   }
   .data{
     margin-left: 15px;
     font-size: 12px !important;
   }
</style>
<h3>EMPLOYEE PROFILE</h3>
  <table style="border:1px solid #EAEEF1;">
    <tr>
        <td colspan=3 class="thhead">PERSONAL DETAILS</td>
    </tr>
    <tr>
	<td><span class="title">First name :</span><span class="data"><?php echo $model->firstname;?></span></td>
	<td><span class="title">Middle name :</span><span class="data"><?php echo $model->middlename;?></span></td>
	<td><span class="title">Last name :</span><span class="data"><?php echo $model->lastname;?></span></td>
    </tr>
    <tr>
	<td><span class="title">Employee ID :</span><span class="data"><?php echo $model->association_employee_id;?></span></td>
	<td><span class="title">Join Date :</span><span class="data"><?php if($model->joining_date!=null and $model->joining_date!='0000-00-00'){echo date('M d. Y',strtotime($model->joining_date));}?></span></td>
	<td><span class="title"></span><span class="data"></span></td>
    </tr>
    <tr>
	<td><span class="title">Nick name :</span><span class="data"><?php echo $model->nickname;?></span></td>
	<td><span class="title">Nationality :</span><span class="data"><?php echo $model->nationality;?></span></td>
	<td><span class="title"></span><span class="data"></span></td>
    </tr>
    <tr>
	<td><span class="title">Birthday :</span><span class="data"><?php if($model->birth_date!=null and $model->birth_date!='0000-00-00') echo date('M. d, Y',strtotime($model->birth_date)); ?></span></td>
	<td><span class="title">Birthplace :</span><span class="data"><?php echo $model->birth_place;?></span></td>
	<td><span class="title"></span><span class="data"></span></td>
    </tr>
    <tr>
	<td><span class="title">Gender :</span><span class="data"><?php if($model->gender=='M') echo 'Male'; else echo 'Female';?></span></td>
	<td><span class="title">Marital Status :</span><span class="data"><?php $estat = EmpTaxstatus::model()->findByAttributes(array('id'=>$model->marital_status)); echo $estat->name; ?></span></td>
	<td><span class="title">Blood Type :</span><span class="data"><?php echo $model->blood_type;?></span></td>
    </tr>
    <tr>
	<td><span class="title">Height :</span><span class="data"><?php echo $model->height;?></span></td>
	<td><span class="title">Weight :</span><span class="data"><?php echo $model->weight;?></span></td>
	<td><span class="title"></span><span class="data"></span></td>
    </tr><tr>
        <td colspan=3 class="thhead">FAMILY BACKGROUND</td>
    </tr>
    <tr><td colspan=3><b>Father's Details</b></td></tr>
    <tr>
	<td colspan=2><span class="title">Name :</span><span class="data"><?php echo ucwords($model->father_fnale.' '.$model->father_lname);?></span></td>
	<td><span class="title">Occupation :</span><span class="data"><?php echo $model->father_occupation;?></span></td>
    </tr>
    <tr>
	<td colspan=2><span class="title">St. /Blk. / Lot :</span><span class="data"><?php echo ucwords($model->father_address);?></span></td>
	<td><span class="title">State/Province :</span><span class="data"><?php echo ucwords($model->father_state);?></span></td>
    </tr>
    <tr>
	<td colspan=2><span class="title">City :</span><span class="data"><?php echo ucwords($model->father_city);?></span></td>
	<td><span class="title">Contact Number :</span><span class="data"><?php echo $model->father_contact;?></span></td>
    </tr>
    <tr><td colspan=3><b>Mother's Details</b></td></tr>
    <tr>
	<td colspan=2><span class="title">Name :</span><span class="data"><?php echo ucwords($model->mother_fname.' '.$model->mother_lname);?></span></td>
	<td><span class="title">Occupation :</span><span class="data"><?php echo $model->mother_occupation;?></span></td>
    </tr>
    <tr>
	<td colspan=2><span class="title">St. /Blk. / Lot :</span><span class="data"><?php echo ucwords($model->mother_address);?></span></td>
	<td><span class="title">State/Province :</span><span class="data"><?php echo ucwords($model->mother_state);?></span></td>
    </tr>
    <tr>
	<td colspan=2><span class="title">City :</span><span class="data"><?php echo ucwords($model->mother_city);?></span></td>
	<td><span class="title">Contact Number :</span><span class="data"><?php echo $model->mother_contact;?></span></td>
    </tr>
    <tr><td colspan=3><b>Siblings</b></td></tr>
    <tr>
	<td colspan=2><span class="title">No. of Siblings :</span><span class="data"><?php echo $model->sibling;?></span></td>
	<td><span class="title"></span><span class="data"></span></td>
    </tr>
    <tr><td colspan=3><b>Spouse's Details</b></td></tr>
    <tr>
	<td colspan=2><span class="title">Name :</span><span class="data"><?php echo ucwords($model->spouse_name);?></span></td>
	<td><span class="title">Occupation :</span><span class="data"><?php echo $model->spouse_occupation;?></span></td>
    </tr>
    <tr>
	<td colspan=2><span class="title">St. /Blk. / Lot :</span><span class="data"><?php echo ucwords($model->spouse_address);?></span></td>
	<td><span class="title">State/Province :</span><span class="data"><?php echo ucwords($model->spouse_state);?></span></td>
    </tr>
    <tr>
	<td colspan=2><span class="title">City :</span><span class="data"><?php echo ucwords($model->spouse_city);?></span></td>
	<td><span class="title">Contact Number :</span><span class="data"><?php echo $model->spouse_contact;?></span></td>
    </tr>
    <tr>
        <td colspan=3 class="thhead">GOVERNMENT AND ACCOUNT INFORMATIONS</td>
    </tr><tr>
	<td><span class="title">SSS No. :</span><span class="data"><?php echo $model->sss_id;?></span></td>
	<td><span class="title"></span><span class="data"></span></td>
	<td><span class="title">PhilHealth No. :</span><span class="data"><?php echo $model->philhealth_id;?></span></td>
    </tr><tr>
	<td><span class="title">TIN No. :</span><span class="data"><?php echo $model->tin_id;?></span></td>
	<td><span class="title"></span><span class="data"></span></td>
	<td><span class="title">HMDF No. :</span><span class="data"><?php echo $model->hmdf;?></span></td>
    </tr><tr>
	<td><span class="title">Tax Status :</span><span class="data"><?php echo $model->tax_status;?></span></td>
	<td><span class="title"></span><span class="data"></span></td>
	<td><span class="title">ATM No. :</span><span class="data"><?php echo $model->atm;?></span></td>
    </tr>
    <tr>
        <td colspan=3 class="thhead">ADDRESS DETAILS</td>
    </tr>
    <tr>
        <td colspan=3><b>Present Address</b></td></tr>
    <tr>
	<td colspan=2><span class="title">Address :</span><span class="data"><?php echo ucwords($model->present_address);?></span></td>
	<td><span class="title">City :</span><span class="data"><?php echo $model->present_city;?></span></td>
    </tr>
    <tr>
	<td colspan=2><span class="title">State/Province :</span><span class="data"><?php echo ucwords($model->present_state);?></span></td>
	<td><span class="title">Zip Code :</span><span class="data"><?php echo ucwords($model->present_zipcode);?></span></td>
    </tr>
    <tr>
        <td colspan=3><b>Permanent Address</b></td></tr>
    <tr>
	<td colspan=2><span class="title">Address :</span><span class="data"><?php echo ucwords($model->per_address);?></span></td>
	<td><span class="title">City :</span><span class="data"><?php echo $model->per_city;?></span></td>
    </tr>
    <tr>
	<td colspan=2><span class="title">State/Province :</span><span class="data"><?php echo ucwords($model->per_state);?></span></td>
	<td><span class="title">Zip Code :</span><span class="data"><?php echo ucwords($model->per_zipcode);?></span></td>
    </tr><tr>
        <td colspan=3 class="thhead">CONTACT DETAILS</td>
    </tr>
    <tr>
	<td colspan=2><span class="title">Tel. No. :</span><span class="data"><?php echo ucwords($model->home_telephone);?></span></td>
	<td><span class="title">Mobile No. :</span><span class="data"><?php echo $model->mobile;?></span></td>
    </tr>
    <tr>
	<td colspan=2><span class="title">Email. :</span><span class="data"><?php echo ucwords($model->other_email);?></span></td>
	<td><span class="title">Work Tel No. :</span><span class="data"><?php echo $model->work_tel;?></span></td>
    </tr>
    <tr>
	<td colspan=2><span class="title">Work Email :</span><span class="data"><?php echo ucwords($model->work_email);?></span></td>
	<td><span class="title"></span><span class="data"></span></td>
    </tr><tr>
        <td colspan=3 class="thhead">PERSON TO CONTACT IN CASE OF EMERGENCY</td>
    </tr>
    <tr>
	<td colspan=2><span class="title">Name :</span><span class="data"><?php echo ucwords($model->e_name);?></span></td>
	<td><span class="title">Relationship :</span><span class="data"><?php echo $model->e_rel;?></span></td>
    </tr>
    <tr>
	<td colspan=2><span class="title">Address :</span><span class="data"><?php echo ucwords($model->e_add);?></span></td>
	<td><span class="title">City :</span><span class="data"><?php echo $model->e_city;?></span></td>
    </tr>
    <tr>
	<td colspan=2><span class="title">State/Province :</span><span class="data"><?php echo ucwords($model->e_state);?></span></td>
	<td><span class="title">Other Contact :</span><span class="data"><?php echo $model->e_other_con;?></span></td>
    </tr>
    <tr>
	<td colspan=2><span class="title">email :</span><span class="data"><?php echo ucwords($model->e_email);?></span></td>
	<td><span class="title">Contact No. :</span><span class="data"><?php echo $model->e_contact;?></span></td>
    </tr><tr>
        <td colspan=3 class="thhead">EDUCATIONAL ATTAINMENT</td>
    </tr><tr>
        <td colspan=3><b>Elementary</b></td></tr>
    <tr>
	<td colspan=3><span class="title">School Name :</span><span class="data"><?php echo ucwords($model->elem_school);?></span></td>
    </tr>
    <tr>
	<td colspan=2><span class="title">Inclusive Date :</span><span class="data"><?php if($model->elem_indate!=null and $model->elem_indate!='0000-00-00') echo date('M. d, Y',strtotime($model->elem_indate));?></span></td>
	<td><span class="title">Excusive Date :</span><span class="data"><?php if($model->hs_exdate!=null and $model->hs_exdate!='0000-00-00') echo date('M. d, Y',strtotime($model->hs_exdate));?></span></td>
    </tr><tr>
        <td colspan=3><b>High School</b></td></tr>
    <tr>
	<td colspan=3><span class="title">School Name :</span><span class="data"><?php echo ucwords($model->hs_school);?></span></td>
    </tr>
    <tr>
	<td colspan=2><span class="title">Inclusive Date :</span><span class="data"><?php if($model->hs_indate!=null and $model->hs_indate!='0000-00-00') echo date('M. d, Y',strtotime($model->hs_indate));?></span></td>
	<td><span class="title">Excusive Date :</span><span class="data"><?php if($model->hs_exdate!=null and $model->hs_exdate!='0000-00-00') echo date('M. d, Y',strtotime($model->hs_exdate));?></span></td>
    </tr><tr>
        <td colspan=3><b>College</b></td></tr>
    <tr>
	<td colspan=2><span class="title">School Name :</span><span class="data"><?php echo ucwords($model->col_school);?></span></td>
	<td><span class="title">Course :</span><span class="data"><?php echo ucwords($model->col_course);?></span></td>
    </tr>
    <tr>
	<td colspan=2><span class="title">Inclusive Date :</span><span class="data"><?php if($model->elem_indate!=null and $model->elem_indate!='0000-00-00') echo date('M. d, Y',strtotime($model->elem_indate));?></span></td>
	<td><span class="title">Excusive Date :</span><span class="data"><?php if($model->elem_exdate!=null and $model->elem_exdate!='0000-00-00') echo date('M. d, Y',strtotime($model->elem_exdate));?></span></td>
    </tr>
    </table>
