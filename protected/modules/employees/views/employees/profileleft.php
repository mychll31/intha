 <div class="emp_cont_left">
    <div class="empleftbx">
<div class="empimgbx" style="height:128px;">
    <ul>
    <li>
   <?php
   $employee=Employees::model()->findByAttributes(array('id'=>$_REQUEST['id']));
	 if($employee->photo_file_name){ 
	 echo '<img src="'.$employee->photo_src.'" style="width: 200px !important;height: 200px !important" />';
	 }else{
		echo '<img class="imgbrder" src="images/super_avatar.png" alt='.$employee->firstname.' width="170" height="140" />'; 
	 }
	 ?>
     </li>
    <li class="img_text">
    	<div style="line-height:9px; margin:20px 0px 5px 0px; font-size:14px;line-height: 22px;">
		<?php
		$pref = EmpPrefix::model()->findByAttributes(array('id'=>$employee->prefix));
		if($pref!=NULL){$pname= $pref->name.'. ';}else{$pname='';}
		if($employee->lastname !=null){$lname =$employee->lastname.' ';}else{$lname='';}
		if($employee->firstname !=null){$fname =$employee->firstname.' ';}else{$fname='';}
		if($employee->middlename !=null){$mname =substr($employee->middlename,1,1).'. ';}else{$mname='';}
		echo CHtml::link(ucwords($pname.$fname.$mname.$lname), array('/employees/employees/view','id'=>$_REQUEST['id']));
		?></div>
		
        <a style="font-size:12px; color:#C30; padding-top:6px; display:block"><?php echo $employee->other_email; ?></a>
		<?php $truckername =Students::model()->findByAttributes(array('assoc_truckerid'=>$employee->trucker_id)); ?>
		<h5><?php echo $truckername->trucker?></h5>
    </li>
    </ul>
    </div>
    <div class="clear"></div>
	<br><br>
    <div id="othleft-sidebar">
        <h1><?php echo Yii::t('employees','Manage Details');?></h1>   
            <?php

			$this->widget('zii.widgets.CMenu',array(
			'encodeLabel'=>false,
			'activateItems'=>true,
			'activeCssClass'=>'list_active',
			'items'=>array(
					#array('label'=>''.Yii::t('employees','Manage Address').'<span>'.Yii::t('employees','Employee Address Details').'</span>', 'url'=>array('/employeeAddresses/view','id'=>ucfirst($employee->id)) ,'linkOptions'=>array('class'=>'lbook_ico'),'active'=> (Yii::app()->controller->action->id=='manage')),                  
					array('label'=>''.Yii::t('employees','Manage License').'<span>'.Yii::t('employees','Employee License Details').'</span>',  'url'=>array('/empLicense/view','id'=>ucfirst($employee->id)),'linkOptions'=>array('class'=>'sl_ico' ),'active'=> (Yii::app()->controller->id=='employees' and (Yii::app()->controller->action->id=='create' or Yii::app()->controller->action->id=='create2')), 'itemOptions'=>array('id'=>'menu_1') 
					       ),
						   
					array('label'=>''.'<h1>'.Yii::t('employees','Work Experience').'</h1>'), 
						array('label'=>Yii::t('employees','Work Experience').'<span>'.Yii::t('employees','').'</span>', 'url'=>array('/empWorkexp/index&id='.$employee->id),'linkOptions'=>array('class'=>'abook_ico'),'active'=> (Yii::app()->controller->id=='employeeLeaveTypes')),
						array('label'=>Yii::t('employees','Work Experience with Association').'<span>'.Yii::t('employees','').'</span>', 'url'=>array('/empAssocxp/index&id='.$employee->id),'linkOptions'=>array('class'=>'abook_ico'),'active'=> (Yii::app()->controller->id=='employeeLeaveTypes')),
						array('label'=>Yii::t('employees','Notes').'<span>'.Yii::t('employees','').'</span>', 'url'=>array('/employeeNotes/index&id='.$employee->id),'linkOptions'=>array('class'=>'abook_ico'),'active'=> (Yii::app()->controller->id=='employeeLeaveTypes')),
						array('label'=>Yii::t('employees','Disciplinary Actions').'<span>'.Yii::t('employees','').'</span>', 'url'=>array('/empDicipAction/index&id='.$employee->id),'linkOptions'=>array('class'=>'abook_ico'),'active'=> (Yii::app()->controller->id=='employeeLeaveTypes')),
						array('label'=>Yii::t('employees','Dependants').'<span>'.Yii::t('employees','').'</span>', 'url'=>array('/empDependants/index&id='.$employee->id),'linkOptions'=>array('class'=>'abook_ico'),'active'=> (Yii::app()->controller->id=='empDependants')),
				),
			)); ?>
		
		</div>
 
    <div class="clear"></div>
    </div>
    
    </div>
