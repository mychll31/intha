<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'address-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

<?php echo CHtml::submitButton($model->isNewRecord ? 'Add Address' : 'Save',array('class'=>'formbut')); ?>
<?php $this->endWidget(); ?>