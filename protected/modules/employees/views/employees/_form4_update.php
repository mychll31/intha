<div class="captionWrapper">
	<ul>
    	<li><h2>Personal Details</h2></li>
        <li><h2>Contact Details</h2></li>
        <li><h2>Educational Attainment</h2></li>
        <li><h2 class="cur">Job Details</h2></li>
    </ul>
</div>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employees-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); 
	$date = 'yy-mm-dd';
	?>

	<?php if($form->errorSummary($model)){; ?>
    
    <div class="errorSummary">Input Error<br />
    <span>Please fix the following error(s).</span>
    </div>
    
    <?php } ?>
    
<p class="note">Fields with <span class="required">*</span> are required.</p>
<?php /*?>
<div class="formCon" >

<div class="formConInner">

<h3>Job Detail</h3>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr><td><?php echo $form->labelEx($model,Yii::t('employees','trucker_id')); ?></td>
	  <td><?php echo $form->labelEx($model,Yii::t('employees','active')); ?></td>
	  <td></td></tr>
  <tr>
	<td><?php 
			$meth = CHtml::listData(Students::model()->findAll(),'assoc_truckerid','trucker');
			echo $form->dropDownList($model,'trucker_id',$meth,array('empty'=>'Select Trucker')); ?>
	</td>
	<td><?php echo $form->dropDownList($model,'active',array('1'=>'Active','2'=>'InActive'),array('empty'=>'')); ?>
		<?php echo $form->error($model,'active'); ?></td><td></td></tr>
  <tr><td colspan=3>&nbsp;</td></tr>
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','dept')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','post')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','start_emp')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','end_emp')); ?></td>
  </tr>
  <tr>
	<td><?php 
		$dep = CHtml::listData(EmpDept::model()->findAll(),'id','name');
			echo $form->dropDownList($model,'dept',$dep,array('empty'=>'')); ?>
		<?php echo $form->error($model,'dept'); ?></td>
	<td><?php 
		$poss = CHtml::listData(EmpPos::model()->findAll(),'id','name');
			echo $form->dropDownList($model,'post',$poss,array('empty'=>'')); ?>
		<?php echo $form->error($model,'post'); ?></td>
	<td><?php
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'start_emp',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
		?>
		<?php echo $form->error($model,'start_emp'); ?></td>
	<td><?php
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'end_emp',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
		?>
		<?php echo $form->error($model,'end_emp'); ?></td>
  </tr>
  <tr><td colspan=3>&nbsp;</td></tr>
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','region')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','emp_stat')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','e_cola')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','salary')); ?></td>
  </tr>
  <tr>
	<td><?php
			$reg = CHtml::listData(EmpRegion::model()->findAll(),'id','name');
			echo $form->dropDownList($model,'region',$reg,array('empty'=>''));	?>
		<?php echo $form->error($model,'region'); ?></td>
	<td><?php
			$emp_s = CHtml::listData(EmpStatus::model()->findAll(),'id','name');
			echo $form->dropDownList($model,'emp_stat',$emp_s,array('empty'=>'')); ?>
		<?php echo $form->error($model,'emp_stat'); ?></td>
	<td><?php echo $form->textField($model,'e_cola',array('size'=>20)); ?>
		<?php echo $form->error($model,'e_cola'); ?></td>
	<td><?php echo $form->textField($model,'salary',array('size'=>20));?>
		<?php echo $form->error($model,'salary'); ?></td>
  </tr>
  <tr><td colspan=3>&nbsp;</td></tr>
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','pay_type')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','pay_period')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','basic_pay')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','min_wage')); ?></td>
  </tr>
  <tr>
	<td><?php
			$payType = CHtml::listData(EmpPaytype::model()->findAll(),'id','name');
			echo $form->dropDownList($model,'pay_type',$payType,array('empty'=>'')); ?>
		<?php echo $form->error($model,'pay_type'); ?></td>
	<td><?php 
			$period = CHtml::listData(EmpPayperiod::model()->findAll(),'id','name');
			echo $form->dropDownList($model,'pay_period',$period,array('empty'=>'')); ?>
		<?php echo $form->error($model,'pay_period'); ?></td>
	<td><?php echo $form->textField($model,'basic_pay',array('size'=>20)); ?>
		<?php echo $form->error($model,'basic_pay'); ?></td>
	<td><?php echo $form->textField($model,'min_wage',array('size'=>20));?>
		<?php echo $form->error($model,'min_wage'); ?></td>
  </tr>
  <tr><td colspan=3>&nbsp;</td></tr>
</table>
</div>
</div>
<?php */?>
<script>
  $().ready(function(){
  var x = 1;
  var idnow = "";
  
  
	if(document.getElementById('Employees_curemp').checked){
		$('#currently_emp').show();
	}else{
		$('#currently_emp').hide();
	}
		$('#Employees_curemp').change(function(){
			$('#currently_emp').toggle();
		});
		
  });
  
</script>

<div class="formCon" >
<div class="formConInner">
<h3>REPORT TO</h3>
<table width="75%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','super_sub')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','method')); ?></td>
	<td></td>
  <tr>
  <tr>
	<td><?php echo $form->textField($model,'super_sub',array('size'=>60)); ?></td>
	<td><?php 
			$meth = CHtml::listData(EmpReportingMethod::model()->findAll(),'id','name');
			echo $form->dropDownList($model,'method',$meth,array('empty'=>'')); ?>
		<?php echo $form->error($model,'method'); ?></td>
	<td></td>
  </tr>
</table>
</div>
</div>

<!-- form -->
	<!-- Hidden Values Ends -->
	<div style="padding:0px 0 0 0px; text-align:left">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Next Step »' : 'Save',array('class'=>'formbut')); ?>
	</div>

<?php $this->endWidget(); ?>
