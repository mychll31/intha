<style>
.table_listbx td {
    border-bottom: 1px solid rgba(204, 204, 204, 0.65);
}
</style>
<?php
$this->breadcrumbs=array(
	'Employees'=>array('index'),
	'View',
);

$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id));
?>

<style>.emp_cntntbx{min-height:0px;}</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
 <?php $this->renderPartial('profileleft');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <div class="edit_bttns">
    <ul>
    <li><?php echo CHtml::link(Yii::t('employees','<span>Employees</span>'), array('manage'),array('class'=>'edit last')); ?></li>
    </ul>
    </div>
	
    <div class="emp_right_contner">
    <div class="emp_tabwrapper"><br>
     <?php
			$this->renderPartial('tab');
	 ?>
    <div class="clear"></div>
 <div class="emp_cntntbx">
    <div class="table_listbx">
<?php
$criteria = new CDbCriteria;
$criteria->condition='emp_id =:x';
$criteria->params[':x'] = $_REQUEST['id'];
$criteria->condition=$criteria->condition.' and reportedbytrucker =:y';
$criteria->params[':y'] = $_REQUEST['trucker'];
$criteria->order = 'date DESC';
$posts = EmpDicipAction::model()->findAll($criteria);
?>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr class="listbxtop_hdng">
		<td colspan=2><?php echo Yii::t('employees','Incident Reports');?></td>
    </tr>
  <?php	foreach ($posts as $post) { ?>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Date :</span><span class="data">
		<?php if($post->date !=null){
			echo date('M. d, Y',strtotime($post->date));
			}
		?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Incident Reported by :</span><span class="data"><?php echo $post->incireportedby; ?></span></td>
	</tr>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Action Taken :</span><span class="data"><?php echo $post->actiontaken; ?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Status :</span><span class="data">
		<?php 
		$stat = IncidentStatus::model()->findByAttributes(array('id'=>$post->status));
		echo $stat->name; 
		?></span></td>
	</tr>
	<tr>
		<td class="subhdng_nrmal" colspan=2><span class="datahead">Incident :</span><span class="data"><?php echo $post->incident; ?></span></td>
	</tr>
	<tr>
		<td class="subhdng_nrmal" colspan=2><span class="datahead"><font color="white">asdf</font></span><span class="data"></span></td>
	</tr>
  <?php } ?>
  </table>
  
 </div>
 
 </div>
    
    </td>
  </tr>
</table>
