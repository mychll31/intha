<?php
$this->breadcrumbs=array(
	'Employees'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);


?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
      <?php $this->renderPartial('/employees/profileleft');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
<?php /*
		if($model->prefix!=null){$pref=$model->prefix.'. ';}else{$pref='';}
		if($model->firstname!=null){$fname=$model->firstname.' ';}else{$fname='';}
		if($model->middlename!=null){$mname=substr($model->middlename,0,1).'. ';}else{$mname='';}
		if($model->lastname!=null){$lname=$model->lastname;}else{$lname='';}
	*/ ?>
<h1><?php #echo Yii::t('employees','Update ');?><?php #echo ucwords($pref.$fname.$mname.$lname); ?></h1>
  <div class="edit_bttns">
    <ul>
    <li><?php echo CHtml::link(Yii::t('employees','<span>Employees</span>'), array('employees/manage'),array('class'=>'edit last')); ?><!--<a class=" edit last" href="">Edit</a>--></li>
    </ul>
  </div>
<?php echo $this->renderPartial('_form4_update', array('model'=>$model)); ?>
</div>
    </td>
  </tr>
</table>