<style>
.table_listbx td {
    border-bottom: 1px solid rgba(204, 204, 204, 0.65);
}
</style>
<?php
$this->breadcrumbs=array(
	'Employees'=>array('index'),
	'View',
);

$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id));
$empid = Employees::model()->findByAttributes(array('id'=>$_REQUEST['id']));

$criteria = new CDbCriteria;
$criteria->condition='(emp_id = :match)';
$criteria->params = array(':match' => $_REQUEST['id']);

$criteria1 = new CDbCriteria;
$criteria1->condition='(emp_id = :matchid)';
$criteria1->params = array(':matchid' => $empid->association_employee_id);

$workexp = EmpWorkexp::model()->findAll($criteria);
$workexp_assoc = EmpAssocxp::model()->findAll($criteria1);

$wexp = array();
foreach($workexp as $ww){
 $wexp[] = array(
 	'id'=>'exp',
	'co_name'=>$ww['co_name'],
	'co_add'=>$ww['co_add'],
	'co_province'=>$ww['co_province'],
	'co_city'=>$ww['co_city'],
	'co_zip'=>$ww['co_zip'],
	'position'=>$ww['position'],
	'co_contact'=>$ww['co_contact'],
	'head'=>$ww['head'],
	'reason'=>$ww['reason'],
	'notes'=>$ww['notes'],
	'trucker'=>null,
	'emp_number'=>null,
	'in_date'=>$ww['in_date'],
	'ex_date'=>$ww['ex_date'],
	'emp_status'=>null,
	'current'=>$ww['current'],
 	);
}
foreach($workexp_assoc as $wwa){
 $wexp[] = array(
 	'id'=>'expa',
	'co_name'=>null,
	'co_add'=>null,
	'co_province'=>null,
	'co_city'=>null,
	'co_zip'=>null,
	'position'=>$wwa['position'],
	'co_contact'=>null,
	'head'=>null,
	'reason'=>null,
	'notes'=>$wwa['reason_forleaving'],
	'trucker'=>$wwa['assoc_truckerid'],
	'emp_number'=>$wwa['emp_number'],
	'in_date'=>$wwa['inc_date'],
	'ex_date'=>$wwa['exc_date'],
	'emp_status'=>$wwa['emp_status'],
	'current'=>$wwa['current'],
 	);
}

foreach($wexp as $key=>$row){
	$mid[$key]=$row['in_date'];
}
array_multisort($mid,SORT_DESC,$wexp);
?>

<style>.emp_cntntbx{min-height:0px;}</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
 <?php $this->renderPartial('profileleft');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <div class="edit_bttns">
    <ul>
    <li><?php echo CHtml::link(Yii::t('employees','<span>Employees</span>'), array('manage'),array('class'=>'edit last')); ?></li>
    </ul>
    </div>
	<?php  if($role->role==1){?>
    <?php }?>
	
    <div class="emp_right_contner">
    <div class="emp_tabwrapper"><br>
     <?php
			$this->renderPartial('tab');
	 ?>
    <div class="clear"></div>
 <div class="emp_cntntbx">
    <div class="table_listbx">

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr class="listbxtop_hdng">
		<td colspan=2><?php echo Yii::t('employees','Work Experience');?></td>
    </tr>
	 <?php
		##################Start Work Experience############
		foreach($wexp as $w_exp){	
		if($w_exp['id']=='exp'){
    if($w_exp['current']==1){
    ?>
    <tbody style="background-color:rgba(255, 192, 203, 0.33);">
    <?php } else { ?>
    <tbody style="background-color:rgba(182, 182, 243, 0.26);">
    <?php } ?>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Company Name :</span><span class="data"></span><?php echo $w_exp['co_name'];?></td>
		<td class="subhdng_nrmal"><span class="datahead">Address :</span><span class="data"><?php echo $w_exp['co_add']; ?></span></td>
	</tr>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Province :</span><span class="data"><?php echo $w_exp['co_province']; ?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">City :</span><span class="data"><?php echo $w_exp['co_city']; ?></span></td>
	</tr>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Zip :</span><span class="data"><?php echo $w_exp['co_zip']; ?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Contact Number: </span><span class="data"><?php echo $w_exp['co_contact']; ?></span></td>
	</tr>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Position :</span><span class="data"><?php echo $w_exp['position']; ?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Immediate Head :</span><span class="data"><?php echo $w_exp['head']; ?></span></td>
	</tr>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Inclusive Date :</span><span class="data"><?php
			if($w_exp['in_date'] == '0000-00-00'){
				echo '-';
			}else{
				echo date('M. d, Y', strtotime($w_exp['in_date']));
			}
		?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Exclusive Date :</span><span class="data"><?php
			if($w_exp['ex_date'] == '0000-00-00'){
				echo '-';
			}else{
				echo date('M. d, Y', strtotime($w_exp['ex_date']));
			}
		?></span></td>
	</tr>
	<tr>
		<td colspan=2 class="subhdng_nrmal"><span class="datahead">Reason for Leaving :</span><span class="data"><?php echo ucwords($w_exp['reason']); ?></span></td>
	</tr>
	<tr>
		<td colspan=2 class="subhdng_nrmal" ><span class="datahead">Notes :</span><span class="data"><?php echo ucwords($w_exp['notes']); ?></span></td>
	</tr>
	</tbody>
	<?php 
	}else{
	##################End Work Experience############
    if($w_exp['current']==1){
    ?>
    <tbody style="background-color:rgba(255, 192, 203, 0.33);">
    <?php } else { ?>
    <tbody style="background-color:rgba(255, 255, 0, 0.2);">
	 <?php
	 } 
		$truckername = Students::model()->findByAttributes(array('assoc_truckerid'=>$w_exp['trucker']));
		$position = EmpPos::model()->findByAttributes(array('id'=>$w_exp['position']));
		$empstat = EmpStatus::model()->findByAttributes(array('id'=>$w_exp['emp_status']));
		?>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Trucker Name :</span><span class="data"></span>
		<?php 
		echo CHtml::link(Yii::t('employees',$truckername->trucker), array('incident', 'id'=>$_REQUEST['id'],'trucker'=>$truckername->id));?></td>
		<td class="subhdng_nrmal"><span class="datahead">Employee Number :</span><span class="data"><?php echo $w_exp['emp_number']; ?></span></td>
	</tr>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Inclusive Date :</span><span class="data"><?php
			if($w_exp['in_date'] == '0000-00-00'){
				echo '-';
			}else{
				echo date('M. d, Y', strtotime($w_exp['in_date']));
			}
		?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Exclusive Date :</span><span class="data"><?php
			if($w_exp['exc_date'] == '0000-00-00'){
				echo '-';
			}else{
				echo date('M. d, Y', strtotime($w_exp['ex_date']));
			}
		?></span></td>
	</tr>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Position :</span><span class="data"><?php echo $position->name; ?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Employment Status :</span><span class="data"><?php echo $empstat->name; ?></span></td>
	</tr>
	<tr>  
	<?php if($w_exp['current'] == 1) { ?>
		<td class="subhdng_nrmal" ><span class="datahead">Reason for Leaving :</span><span class="data"><?php echo ucwords($w_exp['reason']); ?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Reason for Leaving :</span><span class="data"><?php echo ucwords($w_exp['reason']); ?></span></td>
	<?php }else{ ?>
		<td colspan=2 class="subhdng_nrmal"><span class="datahead">Reason for Leaving :</span><span class="data"><?php echo ucwords($w_exp['reason']); ?></span></td>
	<?php } ?>
	</tr>
	</tbody>
	<?php	} ?>
	<tr><td colspan=2></td></tr>	
	<?php }	?>
  </table>
  
 </div>
 
 </div>
 
 
 
</div>
</div>
</div>
    
    </td>
  </tr>
</table>
