<?php $this->renderPartial('tabedit');?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employees-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('encrypt'=>'multipart/form-data'),
)); ?>

	<?php if($form->errorSummary($model)){; ?>
    
    <div class="errorSummary">Input Error<br />
    <span>Please fix the following error(s).</span>
    </div>
    
    <?php } ?>
    
<p class="note">Fields with <span class="required">*</span> are required.</p>
<div class="formCon" >
<div class="formConInner">
<h3>Elementary</h3>
<table width="85%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','elem_school')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','elem_indate')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','elem_exdate')); ?></td>
  </tr>
  <tr>
	<td><?php echo $form->textField($model,'elem_school',array('size'=>40,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'elem_school'); ?></td>
	<td><?php
		$date = 'yy-mm-dd';
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'elem_indate',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
		?>
		<?php echo $form->error($model,'elem_indate'); ?></td>
	<td><?php
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'elem_exdate',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
		?>
		<?php echo $form->error($model,'elem_exdate'); ?></td>
  </tr>
  <tr><td colspan=3>&nbsp;</td></tr>
  </table>
  <h3>High School</h3>
  <table width="85%" border="0" cellspacing="0" cellpadding="0">
    <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','hs_school')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','hs_indate')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','hs_exdate')); ?></td>
  </tr>
  <tr>
	<td><?php echo $form->textField($model,'hs_school',array('size'=>40,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'hs_school'); ?></td>
	<td><?php
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'hs_indate',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
		?>
		<?php echo $form->error($model,'hs_indate'); ?></td>
	<td><?php
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'hs_exdate',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
		?>
		<?php echo $form->error($model,'hs_exdate'); ?></td>
  </tr>
  <tr><td colspan=3>&nbsp;</td></tr>
</table>
  <h3>College</h3>
  <table width="95%" border="0" cellspacing="0" cellpadding="0">
    <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','col_school')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','col_course')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','col_indate')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','col_exdate')); ?></td>
  </tr>
  <tr>
	<td><?php echo $form->textField($model,'col_school',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'col_school'); ?></td>
	<td><?php echo $form->textField($model,'col_course',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'col_course'); ?></td>
	<td><?php
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'col_indate',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
		?>
		<?php echo $form->error($model,'col_indate'); ?></td>
	<td><?php
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'col_exdate',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
		?>
		<?php echo $form->error($model,'col_exdate'); ?></td>
  </tr>
  <tr><td colspan=3>&nbsp;</td></tr>
</table>
  <h3>Others</h3>
  <table width="95%" border="0" cellspacing="0" cellpadding="0">
    <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','o_school')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','o_course')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','o_indate')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','o_exdate')); ?></td>
  </tr>
  <tr>
	<td><?php echo $form->textField($model,'o_school',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'o_school'); ?></td>
	<td><?php echo $form->textField($model,'o_course',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'o_course'); ?></td>
	<td><?php
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'o_indate',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
		?>
		<?php echo $form->error($model,'o_indate'); ?></td>
	<td><?php
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute'=>'o_exdate',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
		?>
		<?php echo $form->error($model,'o_exdate'); ?></td>
  </tr>
</table>
</div>
</div>
<!-- form -->
	<!-- Hidden Values Ends -->
	<div style="padding:0px 0 0 0px; text-align:left">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Next Step »' : 'Next Step',array('class'=>'formbut')); ?>
	</div>

<?php $this->endWidget(); ?>
