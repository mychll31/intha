<?php
$this->breadcrumbs=array(
	'Employees'=>array('index'),
	'View',
);
$role = UserCustomerRole::model()->findByAttributes(array('user'=>Yii::app()->user->id)); 
?>
<style>.emp_cntntbx{min-height:0px;}</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
 <?php $this->renderPartial('profileleft');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
	<?php $pref = EmpPrefix::model()->findByAttributes(array('id'=>$model->prefix)); ?>
	<h1 ><?php echo Yii::t('employees','');?>
	<?php 
		if($pref!=NULL){$pname= $pref->name.'. ';}else{$pname='';}
		if($model->lastname !=null){$lname =$model->lastname.', ';}else{$lname='';}
		if($model->firstname !=null){$fname =$model->firstname.' ';}else{$fname='';}
		if($model->middlename !=null){$mname =$model->middlename.' ';}else{$mname='';}
		echo ucwords($pname.$lname.$fname.$mname);
	?>
	<br /></h1>
	<?php if($role->role==1){?>
<div class="edit_bttns">
    <ul>
    <li><?php echo CHtml::link(Yii::t('employees','<span>Edit</span>'), array('update', 'id'=>$_REQUEST['id']),array('class'=>'edit last')); ?><!--<a class=" edit last" href="">Edit</a>--></li>
     <li><?php echo CHtml::link(Yii::t('employees','<span>Employees</span>'), array('employees/manage'),array('class'=>'edit last')); ?><!--<a class=" edit last" href="">Edit</a>--></li>
    </ul>
    </div>
    <?php }?>
	
    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
    <div class="emp_tab_nav">
    <ul style="width:698px;">
    <li><?php echo CHtml::link(Yii::t('employees','Personal'), array('view', 'id'=>$_REQUEST['id'])); ?></li>
    <li><?php echo CHtml::link(Yii::t('employees','Contact Details'), array('view2', 'id'=>$_REQUEST['id'])); ?></li>
    <li><?php echo CHtml::link(Yii::t('employees','Education'), array('view3', 'id'=>$_REQUEST['id'])); ?></li>
    </ul>
    </div>
    <div class="clear"></div>
 <div class="emp_cntntbx">
    <div class="table_listbx">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="listbxtop_hdng">
    <td><?php echo Yii::t('employees','Personal Details');?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table>
  <table width="100%">
	<?php 
		$xps = EmpWorkexp::model()->findAll("emp_id=:x", array(':x'=>$_REQUEST['id']));
		foreach($xps as $xp){
	?>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Company Name :</span><span class="data"><?php if($xp->co_name != null) {echo $xp->co_name;}else{ echo "-" ;}?></span></td>
		<td colspan=2 class="subhdng_nrmal"><span class="datahead">Address :</span><span class="data"><?php
		if($xp->co_add != null) {$add= $xp->co_add.', ';}else{ $add='' ;}
		if($xp->co_city != null) {$city= $xp->co_city.', ';}else{ $city='' ;}
		if($xp->co_province != null) {$province= $xp->co_province.' ';}else{ $province='' ;}
		if($xp->co_zip != null) {$zip= $xp->co_zip;}else{ $zip='' ;}
		echo ucwords($add.$city.$province.$zip);
		?></span></td>
	</tr>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Contact Number :</span><span class="data"><?php if($xp->co_contact != null) {echo $xp->co_contact;}else{ echo "-" ;}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Position :</span><span class="data"><?php if($xp->position != null) {echo $xp->position;}else{ echo "-" ;}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Immediate Head :</span><span class="data"><?php if($xp->head != null) {echo $xp->head;}else{ echo "-" ;}?></span></td>
	</tr>
	<tr>
		<td class="subhdng_nrmal"><span class="datahead">Inclusive Date :</span><span class="data"><?php if($xp->in_date != null) {echo $xp->in_date;}else{ echo "-" ;}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Exclusive Date :</span><span class="data"><?php if($xp->ex_date != null) {echo $xp->ex_date;}else{ echo "-" ;}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead"></span><span class="data"></span></td>
	</tr>
	<tr>
		<td valign="top" class="subhdng_nrmal"><span class="datahead">Reason for Leaving :</span><br><span class="data"><?php if($xp->reason != null) {echo $xp->reason;}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead">Notes :</span><br><span class="data"><?php if($xp->notes != null) {echo $xp->notes;}?></span></td>
		<td class="subhdng_nrmal"><span class="datahead"></span><span class="data"></span></td>
	</tr>
	<tr class="listbxtop_hdng"><td colspan=3></td></tr>
	<?php }?>
  </table>
  <div class="ea_pdf" style="top:4px; right:6px;"><?php echo CHtml::link('<img src="images/pdf-but.png">', array('Employees/pdf','id'=>$_REQUEST['id']),array('target'=>'_blank')); ?></div>
   
 </div>
 
 </div>
 
 
 
</div>
</div>
</div>
    
    </td>
  </tr>
</table>