<?php $this->renderPartial('tabedit');?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employees-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<?php if($form->errorSummary($model)){; ?>
    
    <div class="errorSummary">Input Error<br />
    <span>Please fix the following error(s).</span>
    </div>
    
    <?php } ?>
    
<p class="note">Fields with <span class="required">*</span> are required.</p>
<div class="formCon" >
<div class="formConInner">
<h3>Address Details</h3>
<table width="80%" border="0" cellspacing="0" cellpadding="0">
  <tr><td colspan=3><h4>Present Address</h4></td></tr>
  <tr>
	<td colspan =2><?php echo $form->labelEx($model,Yii::t('employees','present_address')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','present_city')); ?></td>
  </tr>
  <tr>
	<td colspan=2><?php echo $form->textField($model,'present_address',array('size'=>58,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'present_address'); ?></td>
	<td><?php echo $form->textField($model,'present_city',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'present_city'); ?></td>
  </tr>
  <tr><td colspan=3>&nbsp;</td></tr>
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','present_state')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','present_zipcode')); ?></td>
	<td></td>
  </tr>
	<td><?php echo $form->textField($model,'present_state',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'present_state'); ?></td>
	<td><?php echo $form->textField($model,'present_zipcode',array('size'=>20,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'present_zipcode'); ?></td>
	<td></td>
  </tr>
  <tr><td colspan=3><h4>Permanent Address</h4></td></tr>
  <tr>
	<td colspan =2><?php echo $form->labelEx($model,Yii::t('employees','per_address')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','per_city')); ?></td>
  </tr>
  <tr>
	<td colspan=2><?php echo $form->textField($model,'per_address',array('size'=>58,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'per_address'); ?></td>
	<td><?php echo $form->textField($model,'per_city',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'per_city'); ?></td>
  </tr>
  <tr><td colspan=3>&nbsp;</td></tr>
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','per_state')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','per_zipcode')); ?></td>
	<td></td>
  </tr>
	<td><?php echo $form->textField($model,'per_state',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'per_state'); ?></td>
	<td><?php echo $form->textField($model,'per_zipcode',array('size'=>20,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'per_zipcode'); ?></td>
	<td></td>
  </tr>
</table>
</div>
</div>
<div class="formCon" >
<div class="formConInner">
<h3>Contact Details</h3>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','home_telephone')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','mobile')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','other_email')); ?></td>
  </tr>
  <tr>
	<td><?php echo $form->textField($model,'home_telephone',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'home_telephone'); ?></td>
	<td><?php echo $form->textField($model,'mobile',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'mobile'); ?></td>
	<td><?php echo $form->textField($model,'other_email',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'other_email'); ?></td>
  </tr>
  <tr><td colspan=3>&nbsp;</td></tr>
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','work_tel')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','work_email')); ?></td>
	<td></td>
  </tr>
	<td><?php echo $form->textField($model,'work_tel',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'work_tel'); ?></td>
	<td><?php echo $form->textField($model,'work_email',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'work_email'); ?></td>
	<td></td>
  </tr>
</table>
</div>
</div>
<div class="formCon" >
<div class="formConInner">
<h3>Person to contact in case of Emergency</h3>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','e_name')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','e_rel')); ?></td>
	<td></td>
  </tr>
  <tr>
	<td><?php echo $form->textField($model,'e_name',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'e_name'); ?></td>
	<td><?php echo $form->textField($model,'e_rel',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'e_rel'); ?></td>
	<td></td>
  </tr>
  <tr><td colspan=3>&nbsp;</td></tr>
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','e_add')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','e_city')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','e_state')); ?></td>
  </tr>
	<td><?php echo $form->textField($model,'e_add',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'e_add'); ?></td>
	<td><?php echo $form->textField($model,'e_city',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'e_city'); ?></td>
	<td><?php echo $form->textField($model,'e_state',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'e_state'); ?></td>
  </tr>
  <tr><td colspan=3>&nbsp;</td></tr>
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','e_contact')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','e_other_con')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','e_email')); ?></td>
  </tr>
	<td><?php echo $form->textField($model,'e_contact',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'e_contact'); ?></td>
	<td><?php echo $form->textField($model,'e_other_con',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'e_other_con'); ?></td>
	<td><?php echo $form->textField($model,'e_email',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'e_email'); ?></td>
  </tr>
</table>
</div>
</div>
<!-- form -->
	<!-- Hidden Values Ends -->
	<div style="padding:0px 0 0 0px; text-align:left">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Next Step »' : 'Next Step',array('class'=>'formbut')); ?>
	</div>

<?php $this->endWidget(); ?>
