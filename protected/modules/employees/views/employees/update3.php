<?php
$this->breadcrumbs=array(
	'Employees'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);


?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    
      <?php $this->renderPartial('/employees/profileleft');?>
    
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
<h1><?php #echo Yii::t('employees','Update ');?><?php #echo ucwords($pref.$fname.$mname.$lname); ?></h1>
  <div class="edit_bttns">
    <ul>
    <li><?php echo CHtml::link(Yii::t('employees','<span>Back</span>'), array('view3','id'=>$_REQUEST['id']),array('class'=>'edit last')); ?></li>
    </ul>
  </div>
  <br>
<?php echo $this->renderPartial('_form3', array('model'=>$model)); ?>
</div>
    </td>
  </tr>
</table>
