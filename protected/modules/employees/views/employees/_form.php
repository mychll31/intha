<?php $this->renderPartial('tabedit');?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employees-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<?php if($form->errorSummary($model)){; ?>
    
    <div class="errorSummary">Input Error<br />
    <span>Please fix the following error(s).</span>
    </div>
    
    <?php } ?>
    
<p class="note">Fields with <span class="required">*</span> are required.</p>
<div class="formCon" style="background:#fcf1d4; width:100%; border:0px #fac94a solid; color:#000;background:url(images/yellow-pattern.png); width:100%; border:0p ">

<div class="formConInner" style="padding:5px;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="25%" style="text-align:center;">
	<?php
		$emp_id_1= '';
	 if(Yii::app()->controller->action->id=='create')
			{
			$emp_id	= Employees::model()->findAll(array('order' => 'id DESC','limit' => 1));
			$emp_id_1=($emp_id[0]['id']+10001);
			}
			else
			{
				$emp_id	= Employees::model()->findByAttributes(array('id' => $_REQUEST['id']));
				$emp_id_1=$emp_id->association_employee_id;
			}
			?>
	<?php echo $form->labelEx($model,Yii::t('employees','association_employee_id')); ?></td>
    <td  width="15%"><?php echo $form->textField($model,'association_employee_id',array('size'=>10,'maxlength'=>255,'value'=>$emp_id_1)); ?>
		<?php echo $form->error($model,'association_employee_id'); ?></td>
    <style>
		select#Employees_trucker_id{width: 90% !important;}
	</style>
    <td  width="13%" style="text-align:center">
	<?php echo $form->labelEx($model,Yii::t('employees','trucker_id')); ?>
	<?php #echo $form->labelEx($model,Yii::t('employees','joining_date')); ?></td>
    <td>
	<?php 
		$meth = CHtml::listData(Students::model()->findAll(),'assoc_truckerid','trucker');
		echo $form->dropDownList($model,'trucker_id',$meth,array('empty'=>'Select Trucker')); ?>
		<?php echo $form->error($model,'trucker_id'); ?><?php
	$date = 'yy-mm-dd';
		?></td>
  </tr>
  </table>
</div>
</div>
<div class="formCon" >
<div class="formConInner">
<h3>Personal Details</h3>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','prefix')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','nickname')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','name_ext')); ?></td>
  </tr>
  <tr>
	<td><?php
			$pref = CHtml::listData(EmpPrefix::model()->findAll(),'id','name');
			echo $form->dropDownList($model,'prefix',$pref,array('empty'=>'')); ?>
		<?php echo $form->error($model,'prefix'); ?></td>
	<td><?php echo $form->textField($model,'nickname',array('size'=>15,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nickname'); ?></td>
	<td><?php echo $form->textField($model,'name_ext',array('size'=>10)); ?>
		<?php echo $form->error($model,'name_ext'); ?></td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','firstname')); ?></td>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','gender')); ?></td>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','religion')); ?></td>
  </tr>
  <tr>
	<td valign="top"><?php echo $form->textField($model,'firstname',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'firstname'); ?></td>
    <td valign="top"><?php echo $form->dropDownList($model,'gender',array('M' => 'Male', 'F' => 'Female'),array('empty' =>'Select Gender')); ?>
		<?php echo $form->error($model,'gender'); ?></td>
    <td valign="top"><?php echo $form->textField($model,'religion',array('size'=>25));?>
		<?php echo $form->error($model,'religion'); ?></td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','middlename')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','birth_date')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','height')); ?></td>
  </tr>
  <tr>
	<td>
		<?php echo $form->textField($model,'middlename',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'middlename'); ?>
		</td>
	<td>
	<?php
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							
							'attribute'=>'birth_date',
							'model'=>$model,
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
	 ?>
		<?php echo $form->error($model,'birth_date'); ?>
	</td>
	<td><?php echo $form->textField($model,'height',array('size'=>10,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'height'); ?>FT</td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><?php echo $form->labelEx($model,Yii::t('employees','lastname')); ?></td>
    <td><?php echo $form->labelEx($model,Yii::t('employees','birth_place')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','weight')); ?></td>
  </tr>
  <tr>
    <td><?php echo $form->textField($model,'lastname',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'lastname'); ?></td>
    <td><?php echo $form->textField($model,'birth_place',array('size'=>25)); ?>
		<?php echo $form->error($model,'birth_place'); ?></td>
    <td><?php echo $form->textField($model,'weight',array('size'=>10,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'weight'); ?>KG</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td><?php echo $form->labelEx($model,Yii::t('employees','marital_status')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','nationality')); ?></td>
    <td><?php echo $form->labelEx($model,Yii::t('employees','blood_type')); ?></td>
	</tr>
   <tr>
    <td><?php $marital = CHtml::listData(EmpMaritalStatus::model()->findAll(),'id','name');
			echo $form->dropDownList($model,'marital_status',$marital,array('empty'=>''));?>
		<?php echo $form->error($model,'marital_status'); ?></td>
	<td><?php echo $form->textField($model,'nationality',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nationality'); ?></td>
    <td><?php echo $form->dropDownList($model,'blood_type',array('A+' => 'A+', 'A-' => 'A-', 'B+' => 'B+', 'B-' => 'B-', 'O+' => 'O+', 'O-' => 'O-', 'AB+-' => 'AB+', 'AB-' => 'AB-'),array('empty' => 'Unknown')); ?>
		<?php echo $form->error($model,'blood_type'); ?></td>
  </tr>
</table>
</div>
</div>

<div class="formCon" >
<div class="formConInner">
<h3>Family Background</h3>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
  <tr><td colspan=3><h4>Father's Details</h4></td></tr>
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','father_name')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','father_occupation')); ?></td>
	<td><?php echo $form->labelEx($model,Yii::t('employees','father_contact')); ?></td>
  </tr>
  <tr>
	<td><?php echo $form->textField($model,'father_name',array('size'=>25,'maxlength'=>255));?>
		<?php echo $form->error($model,'father_name'); ?></td>
	<td><?php echo $form->textField($model,'father_occupation',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'father_occupation'); ?></td>
	<td><?php echo $form->textField($model,'father_contact',array('size'=>25)); ?>
		<?php echo $form->error($model,'father_contact'); ?></td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','father_address')); ?></td>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','father_city')); ?></td>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','father_state')); ?></td>
  </tr>
  <tr>
	<td valign="top"><?php echo $form->textField($model,'father_address',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'father_address'); ?></td>
    <td valign="top"><?php echo $form->textField($model,'father_city',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'father_city'); ?></td>
    <td valign="top"><?php echo $form->textField($model,'father_state',array('size'=>25));?>
		<?php echo $form->error($model,'father_state'); ?></td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr><td colspan=3><h4>Mother's Details</h4></td></tr>
  <tr>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','mother_name')); ?></td>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','mother_occupation')); ?></td>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','mother_contact')); ?></td>
  </tr>
  <tr>
	<td><?php echo $form->textField($model,'mother_name',array('size'=>25,'maxlength'=>255));?>
		<?php echo $form->error($model,'mother_name'); ?></td>
	<td><?php echo $form->textField($model,'mother_occupation',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'mother_occupation'); ?></td>
	<td><?php echo $form->textField($model,'mother_contact',array('size'=>25)); ?>
		<?php echo $form->error($model,'mother_contact'); ?></td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','mother_address')); ?></td>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','mother_city')); ?></td>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','mother_state')); ?></td>
  </tr>
  <tr>
	<td valign="top"><?php echo $form->textField($model,'mother_address',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'mother_address'); ?></td>
    <td valign="top"><?php echo $form->textField($model,'mother_city',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'mother_city'); ?></td>
    <td valign="top"><?php echo $form->textField($model,'mother_state',array('size'=>25));?>
		<?php echo $form->error($model,'mother_state'); ?></td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','sibling')); ?></td><td></td><td></td>
  </tr>
  <tr>
	<td valign="top"><?php echo $form->textField($model,'sibling',array('size'=>10));?>
		<?php echo $form->error($model,'sibling'); ?></td><td></td><td></td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr><td colspan=3><h4>Spouse's Details</h4></td></tr>
  <tr>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','spouse_name')); ?></td>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','spouse_occupation')); ?></td>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','spouse_contact')); ?></td>
  </tr>
  <tr>
	<td><?php echo $form->textField($model,'spouse_name',array('size'=>25,'maxlength'=>255));?>
		<?php echo $form->error($model,'spouse_name'); ?></td>
	<td><?php echo $form->textField($model,'spouse_occupation',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'spouse_occupation'); ?></td>
	<td><?php echo $form->textField($model,'spouse_contact',array('size'=>25)); ?>
		<?php echo $form->error($model,'spouse_contact'); ?></td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','spouse_address')); ?></td>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','spouse_city')); ?></td>
    <td valign="bottom" style="padding-bottom:3px;"><?php echo $form->labelEx($model,Yii::t('employees','spouse_state')); ?></td>
  </tr>
  <tr>
	<td valign="top"><?php echo $form->textField($model,'spouse_address',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'spouse_address'); ?></td>
    <td valign="top"><?php echo $form->textField($model,'spouse_city',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'spouse_city'); ?></td>
    <td valign="top"><?php echo $form->textField($model,'spouse_state',array('size'=>25));?>
		<?php echo $form->error($model,'spouse_state'); ?></td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
	<td><?php echo $form->labelEx($model,Yii::t('employees','spouse_birth_date')); ?></td>
	<td></td>
	<td></td>
  </tr>
  <tr>
	<td><?php
						$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							
							'attribute'=>'spouse_birth_date',
							'model'=>$model,
							// additional javascript options for the date picker plugin
							'options'=>array(
								'showAnim'=>'fold',
								'dateFormat'=>$date,
								'changeMonth'=> true,
									'changeYear'=>true,
									'yearRange'=>'1950:2050'
							),
							'htmlOptions'=>array(
								'style' => 'width:100px;',
							),
						))
	?>
	<?php echo $form->error($model,'spouse_birth_date'); ?>
	</td><td></td><td></td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</div>
</div>

<div class="formCon">
<div class="formConInner">
<h3><?php echo Yii::t('employees','Government and Account Informations');?></h3>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><?php echo $form->labelEx($model,Yii::t('employees','sss_id')); ?></td>
    <td><?php echo $form->labelEx($model,Yii::t('employees','philhealth_id')); ?></td>
    <td><?php echo $form->labelEx($model,Yii::t('employees','tin_id')); ?></td>
  </tr>
<tr>
    <td><?php echo $form->textField($model,'sss_id',array('size'=>25,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'sss_id'); ?></td>
	<td><?php echo $form->textField($model,'philhealth_id',array('size'=>25,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'philhealth_id'); ?></td>
	<td><?php echo $form->textField($model,'tin_id',array('size'=>25,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'tin_id'); ?></td>
  </tr><tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><?php echo $form->labelEx($model,Yii::t('employees','hmdf')); ?></td>
    <td><?php echo $form->labelEx($model,Yii::t('employees','tax_status')); ?></td>
    <td><?php #echo $form->labelEx($model,Yii::t('employees','atm')); ?></td>
  </tr>
  <tr>
    <td><?php echo $form->textField($model,'hmdf',array('size'=>25,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'hmdf'); ?></td>
	<td><?php $taxstat = CHtml::listData(EmpTaxStatus::model()->findAll(),'id','name');
			echo $form->dropDownList($model,'tax_status',$taxstat,array('empty'=>''));?>
		<?php echo $form->error($model,'tax_status'); ?></td>
	<td><?php #echo $form->textField($model,'atm',array('size'=>30,'maxlength'=>255)); ?>
		<?php #echo $form->error($model,'atm'); ?></td>
  </tr>
</table>
</div>
</div>

<div class="formCon" style=" background:#EDF1D1; border:0px #c4da9b solid; color:#393; background:#EDF1D1 url(images/green-bg.png); border:0px #c4da9b solid; color:#393;  ">
<div class="formConInner" style="padding:10px;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	
  <tr>
    <td></td>
    <td> </td>
    <td><?php 
	if($model->photo_data==NULL)
	{
	echo $form->labelEx($model,'upload_photo');
	}
	else
	{
	echo $form->labelEx($model,'Photo');	
	}
	
	 ?>
		</td>
    <td>
	<?php 
		
		
		if($model->isNewRecord)
		{
			echo $form->fileField($model,'photo_data'); 
		    echo $form->error($model,'photo_data'); 
		}
		else
		{
			if($model->photo_data==NULL)
			{
				echo $form->fileField($model,'photo_data'); 
		        echo $form->error($model,'photo_data'); 
			}
			
			else
			{
				if(Yii::app()->controller->action->id=='update') {
					echo CHtml::link(Yii::t('students','Remove'), array('Employees/remove', 'id'=>$model->id),array('confirm'=>'Are you sure?')); 
					echo '<img class="imgbrder" src="'.$this->createUrl('Employees/DisplaySavedImage&id='.$model->primaryKey).'" alt="'.$model->photo_file_name.'" width="100" height="100" />';	
				}
				else if(Yii::app()->controller->action->id=='create') {
					echo CHtml::hiddenField('photo_file_name',$model->photo_file_name);
					echo CHtml::hiddenField('photo_content_type',$model->photo_content_type);
					echo CHtml::hiddenField('photo_file_size',$model->photo_file_size);
					echo CHtml::hiddenField('photo_data',bin2hex($model->photo_data));
					echo '<img class="imgbrder" src="'.$this->createUrl('Employees/DisplaySavedImage&id='.$model->primaryKey).'" alt="'.$model->photo_file_name.'" width="100" height="100" />';
				}
			}
		}
		
		 ?>
        
        </td>
  </tr>

</table>
<div class="row">
		<?php //echo $form->labelEx($model,'photo_file_size'); ?>
		<?php echo $form->hiddenField($model,'photo_file_size'); ?>
		<?php echo $form->error($model,'photo_file_size'); ?>
	</div>

</div>
</div>
<div class="clear"></div>
	<div style="padding:0px 0 0 0px; text-align:left">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Next Step »' : 'Next Step',array('class'=>'formbut')); ?>
	</div>
<?php print_r($upd);?>
</div>
</div><!-- form -->
<?php $this->endWidget(); ?>
<script type="text/javascript">
	function star(){
		var year = '';
		var year_val = '';
		year = document.getElementById('experience_year');
		year_val = year.options[year.selectedIndex].value;
		if(year_val!='' && year_val!=0){
			//alert(year_val);
			document.getElementById('required').style.visibility='visible';
		}
	}
	function star2(){
		var mnth = '';
		var mnth_val = '';
		mnth = document.getElementById('experience_month');
		mnth_val = mnth.options[mnth.selectedIndex].value;
		if(mnth_val!='' && mnth_val!=0){
			//alert(year_val);
			document.getElementById('required').style.visibility='visible';
		}
	}
</script>
