<?php
$this->breadcrumbs=array(
	'Trucker'=>array('index'),
	'View',
);


?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="247" valign="top">
    <?php $this->renderPartial('profileleft');?>
    </td>
    <td valign="top">
    <div class="cont_right formWrapper">
    <h1 style="margin-top:.67em;"><?php echo Yii::t('students','Employee List');?></h1>
        
    <div class="edit_bttns last">

    </div>
        
    <div class="clear"></div>
    <div class="emp_right_contner">
    <div class="emp_tabwrapper">
    <div class="clear"></div>
    <div class="tablebx">  
         <div class="pagecon">
		<br>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr class="tablebx_topbg">
				<td><?php echo Yii::t('empLicense','#');?></td>	
				<td><?php echo Yii::t('empLicense','Employee ID');?></td>
				<td><?php echo Yii::t('empLicense','Name');?></td>
				<td><?php echo Yii::t('empLicense','Address');?></td>
				<td><?php echo Yii::t('empLicense','Date of Birth');?></td>
				<td><?php echo Yii::t('empLicense','Mobile No.');?></td>
			</tr>
			<?php
				$cls="even"; $ctr = 0;
				$criteria = new CDbCriteria;
				$userid=User::model()->findByAttributes(array('id'=>Yii::app()->user->id));
				$criteria->compare('trucker_id',$userid->uid);
				$criteria->order = 'id ASC';
				$posts=Employees::model()->findAll($criteria);
				
				foreach($posts as $post){ ?>
					<tr class=<?php echo $cls;?> id=<?php echo $i;?>>
					<?php $pref = EmpPrefix::model()->findByAttributes(array('id'=>$post->prefix)); ?>
					<td style="padding:3px;"><?php $ctr++; echo $ctr; ?></td>
					<td><?php 
					echo CHtml::link($post->association_employee_id,array('/employees/employees/useraccess&id='.$post->association_employee_id)); ?></td>
					<td><?php 
					if($pref->name !=null){$pname= $pref->name.'. ';}else{$pname='';}
					if($post->lastname){$lname= $post->lastname.', ';}else{$lname='';}
					if($post->firstname){$fname= $post->firstname.' ';}else{$fname='';}
					if($post->middlename){$mname= $post->middlename.' ';}else{$mname='';}
					if($post->name_ext){$ename= $post->name_ext;}else{$ename='';}
					echo $pname.$lname.$fname.$mname.$ename;
					?></td>
					<td><?php
						if($post->present_city !=null){$pcity = $post->present_city.', ';}else{$pcity='';}
						if($post->present_state !=null){$pstate = $post->present_state.' ';}else{$pstate='';}
						if($post->present_zipcode !=null){$pzipcode = $post->present_zipcode;}else{$pzipcode='';}
						echo ucwords($pcity.$pstate.$pzipcode);
					?>
					<td><?php 
						if($post->birth_date!=null and $post->birth_date!='0000-00-00'){echo date('M. d, Y',strtotime($post->birth_date));} ?></td>
					<td><?php echo $post->mobile; ?></td>
					</tr><?php	if($cls=="even"){$cls="odd";}else{$cls="even"; }$i++;} ?>
		</table>
    </div>
    </div>
    </div>
   </div>
    </td>
  </tr>
</table>
