<?php

/**
 * This is the model class for table "courses".
 *
 * The followings are the available columns in table 'courses':
 * @property integer $id
 * @property string $course_name
 * @property string $code
 * @property string $section_name
 * @property integer $is_deleted
 * @property string $created_at
 * @property string $updated_at
 */
class Courses extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Courses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'courses';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('year_model, wheeler, franchise_number', 'numerical', 'integerOnly'=>true),
			#array('association_truck_id,association_trucker_id,plate_number,chasis_number,motor', 'required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, course_name, code, section_name, is_deleted, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		
         //'courses'=>array(self::HAS_ONE, 'Courses', 'id'),
		 
		 'batches'=>array(self::HAS_MANY, 'Batches', 'course_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tractor_type'=>'Body Type',
			'course_name' => 'Course Name',
			'code' => 'Code',
			'section_name' => 'Section Name',
			'is_deleted' => 'Is Deleted',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'association_truck_id' => 'Association Truck Id',
			'association_trucker_id' => 'Trucker',
			'year_model' => 'Year Model',
			'plate_number' => 'Plate Number',
			'wheeler' => 'Truck Number of Tires',
			'brand' => 'Brand',
			'model' => 'Model',
			'trailer_type' => 'Trailer Type',
			'chasis_number' => 'Truck Chasis Number',
			'truck_status' => 'Truck Status',
			'franchise_number' => 'Franchise Number',
			'franchise_date_of_issuance' => 'Date of Issuance',
			'franchise_date_of_expiration' => 'Date of Expiration',
			'type_of_note' => 'Type of Note',
			'details_of_note' => 'Details of Note',
			'date_of_note' => 'Date of Note',
			'reported_by' => 'Reported by',
			'date_reported_by' => 'Date Reported by',
			'or_cr' => 'OR CR',
			'or_cr_date' => 'OR CR Date',
			'or_cr_from' => 'OR CR From',
			'trailer_footer'=>'Trailer Footer',
			'trailer_axle'=>'Trailer Axle',
			'motor'=>'Motor',
			'reg_owner'=>'Registered Owner',
			'reg_startdate'=>'Start Date',
			'reg_enddate'=>'End Date',
			'trailer_chasis_number'=>'Trailer Chasis Number',
			'trailer_chasis_serial'=>'Trailer Chasis Serial Number',
			'trailer_reg_owner'=>'Trailer Registered Owner',
			'trailer_mv_file_number'=>'MV File Number',
			'trailer_cr_number'=>'CR Number',
			'trailer_cr_dateissued'=>'CR Date Issued',
			'trailer_cr_reg_owner'=>'CR Registered Owner',
			'trailer_or_number'=>'OR Number',
			'trailer_or_dateissued'=>'OR Date Issued',
			'trailer_or_dateexpire'=>'OR Date Expire',
			'trailer_lto_field'=>'LTO Field Office',
			'trailer_plate_number'=>'Trailer Plate Number',
			'truck_gross_w'=>'Truck Gross Weight',
			'truck_net_capacity'=>'Truck Net Capacity',
			'truck_engine_number'=>'Engine Number',
			'truck_cr_number'=>'CR Number',
			'truck_cr_dateissued'=>'CR Date Issued',
			'truck_cr_reg_owner'=>'CR Registered Owner',
			'truck_or_number'=>'OR Number',
			'truck_dateissued'=>'OR Date Issued',
			'truck_or_dateexpire'=>'OR Date Expire',
			'truck_lto_field'=>'LTO Field Office',
			'truck_mv_file_number'=>'MV File Number',
			'truck_number_tire'=>'Truck Number of Tires',
			'body'=>'Body Type'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('course_name',$this->course_name,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('section_name',$this->section_name,true);
		$criteria->compare('is_deleted',0);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('year_model',$this->year_model,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
}