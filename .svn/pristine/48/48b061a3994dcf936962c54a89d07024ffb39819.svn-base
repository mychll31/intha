<?php

/**
 * This is the model class for table "trucker_directors".
 *
 * The followings are the available columns in table 'trucker_directors':
 * @property integer $id
 * @property integer $trucker_id
 * @property integer $prefix
 * @property string $last_name
 * @property string $first_name
 * @property string $middle_name
 * @property string $ext
 * @property string $bday
 * @property string $bplace
 * @property string $address
 * @property string $city
 * @property string $province
 * @property integer $zipcode
 * @property integer $gender
 */
class TruckerDirectors extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return TruckerDirectors the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trucker_directors';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('zipcode', 'numerical', 'integerOnly'=>true),
			array('last_name, gender, first_name, middle_name, ext, bplace, address, city, province', 'length', 'max'=>255),
			array('bday', 'safe'),
			array('trucker_id, last_name, first_name,address,city','required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, trucker_id, prefix, last_name, first_name, middle_name, ext, bday, bplace, address, city, province, zipcode, gender', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'trucker_id' => 'Trucker',
			'prefix' => 'Prefix',
			'last_name' => 'Last Name',
			'first_name' => 'First Name',
			'middle_name' => 'Middle Name',
			'ext' => 'Ext',
			'bday' => 'Birthday',
			'bplace' => 'Birth Place',
			'address' => 'St.no./St.Address/Brgy/Town',
			'city' => 'City/Municipality',
			'province' => 'Province/State',
			'zipcode' => 'Zip Code',
			'gender' => 'Gender',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('trucker_id',$this->trucker_id);
		$criteria->compare('prefix',$this->prefix);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('ext',$this->ext,true);
		$criteria->compare('bday',$this->bday,true);
		$criteria->compare('bplace',$this->bplace,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('province',$this->province,true);
		$criteria->compare('zipcode',$this->zipcode);
		$criteria->compare('gender',$this->gender);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
