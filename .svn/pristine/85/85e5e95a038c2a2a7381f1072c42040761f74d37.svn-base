<?php

/**
 * This is the model class for table "emp_dicip_action".
 *
 * The followings are the available columns in table 'emp_dicip_action':
 * @property integer $id
 * @property integer $emp_id
 * @property string $date
 * @property string $incident
 */
class EmpDicipAction extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return EmpDicipAction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'emp_dicip_action';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_id', 'numerical', 'integerOnly'=>true),
			array('incident', 'length', 'max'=>10000),
			array('date', 'safe'),
			array('date,incident','required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, emp_id, date, incident', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'emp_id' => 'Emp',
			'date' => 'Date',
			'incident' => 'Incident',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('emp_id',$this->emp_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('incident',$this->incident,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}