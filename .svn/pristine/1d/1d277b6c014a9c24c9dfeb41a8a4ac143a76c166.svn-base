<?php

/**
 * This is the model class for table "emp_workexp".
 *
 * The followings are the available columns in table 'emp_workexp':
 * @property integer $id
 * @property integer $emp_id
 * @property string $co_name
 * @property string $co_add
 * @property string $co_province
 * @property string $co_city
 * @property integer $co_zip
 * @property string $position
 * @property integer $co_contact
 * @property string $head
 * @property string $in_date
 * @property string $ex_date
 * @property string $reason
 * @property string $notes
 */
class EmpWorkexp extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return EmpWorkexp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'emp_workexp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_id, co_zip, co_contact', 'numerical', 'integerOnly'=>true),
			array('co_name, co_add, co_province, co_city, position, head', 'length', 'max'=>255),
			array('reason, notes', 'length', 'max'=>10000),
			array('in_date, ex_date', 'safe'),
			array('emp_id,co_name,co_add,co_province,co_city,co_zip,co_contact,position,head,in_date,ex_date,reason','required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, emp_id, co_name, co_add, co_province, co_city, co_zip, position, co_contact, head, in_date, ex_date, reason, notes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'emp_id' => 'Emp_id',
			'co_name' => 'Company Name',
			'co_add' => 'Address',
			'co_province' => 'Province',
			'co_city' => 'City',
			'co_zip' => 'Zip Code',
			'position' => 'Position',
			'co_contact' => 'Contact Number',
			'head' => 'Immediate Head',
			'in_date' => 'Inclusive Date',
			'ex_date' => 'Exclusive Date',
			'reason' => 'Reason for Leaving',
			'notes' => 'Notes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('emp_id',$this->emp_id);
		$criteria->compare('co_name',$this->co_name,true);
		$criteria->compare('co_add',$this->co_add,true);
		$criteria->compare('co_province',$this->co_province,true);
		$criteria->compare('co_city',$this->co_city,true);
		$criteria->compare('co_zip',$this->co_zip);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('co_contact',$this->co_contact);
		$criteria->compare('head',$this->head,true);
		$criteria->compare('in_date',$this->in_date,true);
		$criteria->compare('ex_date',$this->ex_date,true);
		$criteria->compare('reason',$this->reason,true);
		$criteria->compare('notes',$this->notes,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}